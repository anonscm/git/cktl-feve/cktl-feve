### ==================================================== ###
###   Fichier de configuration de l'application Feve     ###	
### ==================================================== ###



# -------------------------------------------------- #
# ---             PARAMETRES GENERAUX            --- #
# -------------------------------------------------- #


#* L'identifiant de l'application. Il peut etre utilise par differents
#* servces : authentification (SAUT), sauvegarde des documents
#* (GEDFS),... Le ID ne doit pas depasse 10 symboles et il doit etre
#* enregistre aupres de serveur concerne.
#* Il n'est pas conseille de modifier cette valeur.
APP_ID=FEVE

#* Description courte de l'application. Le meme ID pouvant etre partage
#* entre plusieurs versions de l'application, cette valeur permet
#* de fournir les informations complementaires
APP_ALIAS=Feve


#* Le URL de l'acces a l'application. Cette valeur est utilisee si
#* l'application ne peut pas detecter ce URL automatiquement
APP_URL=http://apps.domaine/cgi-bin/WebObjects/Feve.woa/


#* Le URL de service d'authentification. Il permet de recuperer
#* le dictionnaire de connexion a la base de donnees. Si le service
#* SAUT/ServAut n'est pas disponible dans votre systeme, mettez ce
#* parametre en commentaire et renseigner le dictionnaire de connexion
#* directement dans le fichier CongesWeb.eomodeld/index.eomodeld
#SAUT_URL=http://saut.domaine/cgi-bin/WebObjects/ServAut.woa/wa/


#* Le dictionnaire de translation des identifiants des dictionnaires
#* de connexion a la base de donnees. Voir la configuration de 
#* serveur SAUT/ServAut, s'il est disponible.
#* Si SAUT_URL n'est pas renseigne, alors cette valeur est ignoree.
#SAUT_ID_TRANSLATION=FEVE:GRHUM,MANGUE:GRHUM


#* Indique s'il faut sous traiter l'authentification au service CAS
#* installe dans l'etablissement. L'URL de contact du service est disponible
#* dans GRHUM.GRHUM_PARAMETRES, PARAM_KEY=CAS_SERVICE_URL
APP_USE_CAS=NO

#* Indique si les mots de passes vides peuvent etre acceptes comme
#* les mots de passes valides. N'est utilise que dans le cas ou le
#* parametre APP_USE_CAS=NO.
#* Les valeurs autorisees sont YES/NO
ACCEPT_EMPTY_PASSWORD=NO


# Indique si l'envoi des messages email est disponible dans l'application.
#* Sans ce support, aucun avertissement ne sera envoye suite a la creation
#* d'une demande ou toute autre action.
#* Les valeurs autorisees sont YES/NO
#* La valeur par defaut est YES
APP_USE_MAIL=YES

FEV_MODIFICATION_FORMATIONS_SUIVIES_DANS_FEVE=N
#* Le URL de site Web principale de l'etablissement
MAIN_WEB_SITE_URL=http://www.domaine


#* L'URL de l'image "logo" de l'etablissement. Elle est
#* affichee sur la page d'acceuil de l'application et 
#* dans la bare de menu
MAIN_LOGO_URL=http://www.domaine/logo_etablissement.gif


#* L'adresse mail de l'administrateur de l'application. Cette
#* adresse est utilisee pour le lien sur la page d'acceuil "Si vous avez un probleme..."
APP_ADMIN_MAIL=fonctionnel_feve@domaine


# L'adresse mail de reception des exceptions issues de l'application.
# Ce paramètre est facultatif.
# A defaut, c'est APP_ADMIN_MAIL qui recevra la mail d'exception
APP_ERROR_MAIL=informaticien_feve@domaine


#* Niveau de verbosite des fichiers de log
#* - 0 : aucun log 
#* - 1 : logs texte
#* - 2 : logs texte etendus
DEBUG_LEVEL=1


#* Le timezone utilise pour tout ce qui est manipulation des dates
#* (jouer sur cette valeur si vous avez des decalage de 1 ou 2 heures)
APP_TIME_ZONE=CEST



# -------------------------------------------------- #
# ---       PARAMETRES SPECIFIQUES A FEVE        --- #
# -------------------------------------------------- #

#* Le message personnalisé d'information pour les modifications
#* des meta-donnees d'un poste (code et dates) - fonction disponibles
#* uniquement pour l'administrateur.
MSG_INFO_MODIF_POSTE=Le code ainsi que les dates de validit&eacute; du poste ne sont<br/>modifiables que par le personnel de la DRH

#* Correction d'un bug qui a mis une position incorrecte pour
#* certains activites et competences associees aux fiches de poste
APP_FIX_POSITION=NO


#* répertoire racine pour la personnalisation des jaspers
#* exemple : org.cocktail.feve.reports.local.location=/tmp/jasper/
#* les jaspers sont alors :
#* /tmp/jasper/evaluation/evaluation.jasper
#* /tmp/japser/FicheDePoste/FicheDePoste.jasper
org.cocktail.feve.reports.local.location=

# affichage xml pour les éditions dans le log serveur
org.cocktail.feve.export.xml.dans.log=FALSE

# latence des fetchs en ms pour la gestion multi-instances
org.cocktail.feve.fetch.timestamp.lag=

