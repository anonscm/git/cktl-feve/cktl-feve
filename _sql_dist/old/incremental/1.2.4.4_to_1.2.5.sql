-- saisie des formations suivies dans Feve
INSERT INTO MANGUE.MANGUE_PARAMETRES ( PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES ) VALUES ( 
MANGUE.MANGUE_PARAMETRES_SEQ.NEXTVAL, 'FEV_MODIFICATION_FORMATIONS_SUIVIES_DANS_FEVE', 'O', 
'La saisie des formations suivies est-elle autorisée dans Feve (entretien professionnel) (O/N)');