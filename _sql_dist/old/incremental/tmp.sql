DROP TABLE MANGUE.REPART_APP_GENERAL;
CREATE TABLE MANGUE.REPART_APP_GENERAL (
													id NUMBER PRIMARY KEY NOT NULL,
													EVALUATION_KEY NUMBER,
													ITEM_VALEUR_TIT_KEY NUMBER,
													ITEM_TIT_KEY NUMBER
												)

CREATE SEQUENCE MANGUE.REPART_APP_GENERAL_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

----------------------

DECLARE
	compteur integer;

 BEGIN

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM 
 	 WHERE MANGUE.TPL_ITEM.TIT_KEY = 37;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (37, 'Compétences professionnelles et technicité', 1, 1, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY'), 'REPCOMPPRO' );
	END IF;
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM 
 	 WHERE MANGUE.TPL_ITEM.TIT_KEY = 38;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (38, 'Contribution à l''activité du service', 1, 1, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY'), 'CONTRIACTI');
	END IF;	
	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM 
 	 WHERE MANGUE.TPL_ITEM.TIT_KEY = 39;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (39, 'Capacités professionnelles et relationnelles', 1, 1, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY'), 'CAPACITEPR' );
	END IF;	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM 
 	 WHERE MANGUE.TPL_ITEM.TIT_KEY = 40;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (40, 'Aptitude à l''encadrement et/ou à la conduite de projets', 1, 1, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY') ,'APTITUDEEN');
	END IF;	
	
	-------------------------
	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR.TIT_KEY = 37;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (37, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'),TO_DATE( '06/03/2014', 'DD/MM/YYYY') );
	END IF;	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR.TIT_KEY = 38;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (38, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'),TO_DATE( '06/03/2014', 'DD/MM/YYYY') );
	END IF;
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR.TIT_KEY = 39;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (39, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'),TO_DATE( '06/03/2014', 'DD/MM/YYYY') );
	END IF;
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR.TIT_KEY = 40;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (40, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'),TO_DATE( '06/03/2014', 'DD/MM/YYYY') );
	END IF;

END;
/

----------------------------------------------------------------------


DECLARE
	compteur integer;

 BEGIN

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ONGLET 
 	 WHERE MANGUE.TPL_ONGLET.TON_KEY = 12;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ONGLET (TON_KEY, TON_LIBELLE, D_CREATION, D_MODIFICATION,TON_POSITION, TFI_KEY, TON_CODE, TEM_CONTRACTUEL, TEM_TITULAIRE) VALUES (12, 'Appreciation générale', TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY'),11, 3, 'APPGENERAL', 'O', 'O');
	END IF;
END;
/

----------------------PAGE EVOLUTION PROFESSIONNELLES---------------------------------
----evolution activite et carriere
--ALTER TABLE MANGUE.EVALUATION ADD EVOLUTION_ACTIVITES VARCHAR2(2000);
--ALTER TABLE MANGUE.EVALUATION ADD EVOLUTION_CARRIERE VARCHAR2(2000);


--competence a acquerir ou developper pour tenir le poste
DROP TABLE MANGUE.FEVE_REPART_COMP_POSTE;
CREATE TABLE MANGUE.FEVE_REPART_COMP_POSTE (
													ID NUMBER PRIMARY KEY NOT NULL,
													EVALUATION_KEY NUMBER,
													LIBELLE VARCHAR2(100),
													PERIODE VARCHAR2(100)
                          );
                          
CREATE SEQUENCE MANGUE.FEVE_REPART_COMP_POSTE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

ALTER TABLE MANGUE.EVALUATION ADD COMP_POST_COMMENTAIRE VARCHAR2(2000);
ALTER TABLE MANGUE.EVALUATION ADD FORMATION_PREVU CHAR(1);


--competence a acquerir ou developper en vur d'une evolution professionnelle
DROP TABLE MANGUE.FEVE_REPART_COMP_EVOLUTION;
CREATE TABLE MANGUE.FEVE_REPART_COMP_EVOLUTION (
													ID NUMBER PRIMARY KEY NOT NULL,
													EVALUATION_KEY NUMBER,
													LIBELLE VARCHAR2(100),
													PERIODE VARCHAR2(100)
                          );
                          
CREATE SEQUENCE MANGUE.FEVE_REPART_COMP_EVOLUTION_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

ALTER TABLE MANGUE.EVALUATION ADD COMP_EVOL_COMMENTAIRE VARCHAR2(2000);


--autre perspective de formation
DROP TABLE MANGUE.FEVE_REPART_PERS_FORMATION;
CREATE TABLE MANGUE.FEVE_REPART_PERS_FORMATION (
													ID NUMBER PRIMARY KEY NOT NULL,
													EVALUATION_KEY NUMBER,
													LIBELLE VARCHAR2(100),
													ECHEANCE VARCHAR2(100)
                          );
                          
CREATE SEQUENCE MANGUE.FEVE_REPART_PERS_FORMATION_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

ALTER TABLE MANGUE.EVALUATION ADD COMP_FORMATION_COMMENTAIRE VARCHAR2(2000);
ALTER TABLE MANGUE.EVALUATION ADD MOBILISER_DIF CHAR(1);

------------------------SUPPRESSION formatio Evolution pro-------------------------------------------




alter table MANGUE.tpl_item disable constraint FK_TPL_ITEM_BLOC;
DELETE FROM MANGUE.tpl_bloc WHERE TBL_CODE = 'FORSOUHAIT';
DELETE FROM MANGUE.tpl_bloc WHERE TBL_CODE = 'FORSOUHAI2';
alter table MANGUE.tpl_item enable constraint FK_TPL_ITEM_BLOC;




------------SUPRESSION ONGLET APTITUDE -----------------------
DELETE FROM MANGUE.TpL_bloc WHERE tbl_code = 'CONACTSERV';
DELETE FROM MANGUE.TpL_bloc WHERE tbl_code = 'QUAPERSREL';
DELETE FROM MANGUE.TpL_onglet WHERE ton_code = 'EVAAPTI';



--------------REORGANISATIOJNH MENU----------------------

UPDATE mangue.tpl_onglet set ton_position = 1   where ton_code = 'EVAAGENT';
UPDATE mangue.tpl_onglet set ton_position = 2   where ton_code = 'EVAOBJPREC';
UPDATE mangue.tpl_onglet set ton_position = 3   where ton_code = 'BILANECOUL';
UPDATE mangue.tpl_onglet set ton_position = 4   where ton_code = 'EVASITU';
UPDATE mangue.tpl_onglet set ton_position = 5   where ton_code = 'EVACOMP';
UPDATE mangue.tpl_onglet set ton_position = 6   where ton_code = 'SYNTVALPRO';
UPDATE mangue.tpl_onglet set ton_position = 7   where ton_code = 'EVAOBJSUIV';
UPDATE mangue.tpl_onglet set ton_position = 8   where ton_code = 'EVAEVOL';
UPDATE mangue.tpl_onglet set ton_position = 9   where ton_code = 'NOTICEPROM';
UPDATE mangue.tpl_onglet set ton_position = 10  where ton_code = 'EVAFIN';





------------------------------------------------
UPDATE MANGUE.TPL_BLOC SET D_FIN_VAL = '9/03/14' WHERE TBL_KEY=4;
UPDATE MANGUE.TPL_BLOC SET D_FIN_VAL = '9/03/14' WHERE TBL_KEY=7;





------------------------------------------------


UPDATE MANGUE.TPL_BLOC SET TBL_LIBELLE = 'Appréciation littérale' WHERE tbl_key = '10';
UPDATE MANGUE.TPL_ITEM SET Tit_LIBELLE = 'Merci d''apporter un soin particulier à cette appréciation qui constitue un critère pour l''avancement de grade des agents et pourra être repris dans les rapports liés à la promotion de grade.' WHERE tit_key = '35';

alter table WORKFLOW.HISTORIQUE_DEMANDE disable constraint FK_HD_ID_DEMANDE;
alter table WORKFLOW.HISTORIQUE_DEMANDE enable constraint FK_HD_ID_ETAPE_DEPART;

select * from MANGUE.TPL_ONGLET;
select * from MANGUE.tpl_bloc;
select * from mangue.TPL_ITEM_BLOC;

select * from MANGUE.TPL_BLOC;
SELECT * from MANGUE.TPL_ITEM;
select * from mangue.repart_app_general;

UPDATE MANGUE.TPL_BLOC SET TBL_LIBELLE = 'Appréciation littérale' WHERE tbl_key = '10';
UPDATE MANGUE.TPL_ITEM SET Tit_LIBELLE = 'Merci d''apporter un soin particulier à cette appréciation qui constitue un critère pour l''avancement de grade des agents et pourra être repris dans les rapports liés à la promotion de grade.' WHERE tit_key = '35';





DELETE FROM MANGUE.tpl_bloc WHERE TBL_CODE = FORSOUHAIT;
DELETE FROM MANGUE.tpl_bloc WHERE TBL_CODE = FORSOUHAI2

DELETE FROM MANGUE.tpl_bloc WHERE  TBL_KEY = 5;
DELETE FROM MANGUE.tpl_bloc WHERE  TBL_KEY = 6;

ALTER TABLE MANGUE.TPL_ONGLET NOCHECK CONSTRAINT MANGUE.FK_TPL_BLOC_ONGLET;
DELETE FROM MANGUE.TPL_ONGLET WHERE  TON_KEY = 5;
ALTER TABLE MANGUE.TPL_ONGLET CHECK CONSTRAINT MANGUE.FK_TPL_BLOC_ONGLET;


DELETE FROM MANGUE.TPL_ONGLET WHERE  TON_KEY = 5;
