ALTER TABLE MANGUE.FICHE_DE_POSTE ADD (FONCTIONS_CONDUITE_PROJET VARCHAR2(1) DEFAULT 'N' NOT NULL);
ALTER TABLE MANGUE.FICHE_DE_POSTE ADD (FONCTIONS_ENCADREMENT VARCHAR2(1) DEFAULT 'N' NOT NULL);

ALTER TABLE MANGUE.FICHE_DE_POSTE ADD NB_AGENTS NUMBER;
ALTER TABLE MANGUE.FICHE_DE_POSTE ADD NB_AGENTS_CATEGORIE_A NUMBER;
ALTER TABLE MANGUE.FICHE_DE_POSTE ADD NB_AGENTS_CATEGORIE_B NUMBER;
ALTER TABLE MANGUE.FICHE_DE_POSTE ADD NB_AGENTS_CATEGORIE_C NUMBER;



COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.FONCTIONS_CONDUITE_PROJET IS 'Si l''agent a des fonctions de conduite de projet';
COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.FONCTIONS_ENCADREMENT IS 'Si l''agent a des fonctions d''encadrement';
COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.NB_AGENTS IS 'nombre d''agents encadré par l''agent';
COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.NB_AGENTS_CATEGORIE_A IS 'nombre d''agents de catégorie A encadré par l''agent';
COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.NB_AGENTS_CATEGORIE_B IS 'nombre d''agents de catégorie B encadré par l''agent';
COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.NB_AGENTS_CATEGORIE_C IS 'nombre d''agents de catégorie C encadré par l''agent';


ALTER TABLE MANGUE.FICHE_DE_POSTE DROP COLUMN FONCTIONS_CONDUITE_PROJET;
ALTER TABLE MANGUE.FICHE_DE_POSTE DROP COLUMN FONCTIONS_ENCADREMENT;
ALTER TABLE MANGUE.FICHE_DE_POSTE DROP COLUMN NB_AGENTS;
ALTER TABLE MANGUE.FICHE_DE_POSTE DROP COLUMN NB_AGENTS_CATEGORIE_A;
ALTER TABLE MANGUE.FICHE_DE_POSTE DROP COLUMN NB_AGENTS_CATEGORIE_B;
ALTER TABLE MANGUE.FICHE_DE_POSTE DROP COLUMN NB_AGENTS_CATEGORIE_C;



-------------------------------------------------------
ALTER TABLE MANGUE.SITU_ACTIVITE ADD (EVA_TYPE NUMBER);

COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.EVA_KEY IS 'Clé étrangere vers l''evaluation';
COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.SAC_KEY IS 'Clé primaire de la table';
COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.SAC_SITUATION IS 'Texte de la situation';
COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.D_CREATION IS 'Date de création de la situation';
COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.D_MODIFICATION IS 'Date de modification de la situation';
COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.EVA_TYPE IS 'type d''evaluation';

ALTER TABLE MANGUE.SITU_ACTIVITE DROP EVA_TYPE;

--------------------------------------------------------

ALTER TABLE MANGUE.EVALUATION ADD (NIVEAU_COMPETENCE_PRO NUMBER DEFAULT 2);
ALTER TABLE MANGUE.EVALUATION ADD (NIVEAU_CONTRIBUTION NUMBER DEFAULT 2);
ALTER TABLE MANGUE.EVALUATION ADD (NIVEAU_CAPACITE_PRO NUMBER DEFAULT 2);
ALTER TABLE MANGUE.EVALUATION ADD (NIVEAU_APTITUDE_ENCADREMENT NUMBER DEFAULT 2);

COMMENT ON COLUMN MANGUE.EVALUATION.NIVEAU_COMPETENCE_PRO IS 'competence professionelle et technicité';
COMMENT ON COLUMN MANGUE.EVALUATION.NIVEAU_CONTRIBUTION IS 'contribution à l''activité du service';
COMMENT ON COLUMN MANGUE.EVALUATION.NIVEAU_CAPACITE_PRO IS 'capacités proféssionnelles et relationnelles';
COMMENT ON COLUMN MANGUE.EVALUATION.NIVEAU_APTITUDE_ENCADREMENT IS 'Aptitude encadrement et/ou conduite de projets';



----------------------------------------------------------------

INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (37, 'Competence professionnelles et technicite', 1, 1, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY'), 'REPCOMPPRO' );
INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (38, 'Contribution à l''activité du service', 1, 1, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY'), 'CONTRIACTI');
INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (39, 'Capacites professionnelles et relationnelles', 1, 1, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY'), 'CAPACITEPR' );
INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (40, 'Aptitude a l''encadrement et/ou a la conduite de projets', 1, 1, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'), TO_DATE( '06/03/2014', 'DD/MM/YYYY') ,'APTITUDEEN');

INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (37, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'),TO_DATE( '06/03/2014', 'DD/MM/YYYY') )
INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (38, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'),TO_DATE( '06/03/2014', 'DD/MM/YYYY') )
INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (39, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'),TO_DATE( '06/03/2014', 'DD/MM/YYYY') )
INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (40, 1, TO_DATE( '06/03/2014', 'DD/MM/YYYY'),TO_DATE( '06/03/2014', 'DD/MM/YYYY') )


CREATE TABLE MANGUE.REPART_APP_GENERAL (
													id NUMBER PRIMARY KEY NOT NULL,
													EVALUATION_KEY NUMBER,
													ITEM_VALEUR_TIV_KEY NUMBER,
													ITEM_TIT_KEY NUMBER
												)

CREATE SEQUENCE MANGUE.REPART_APP_GENERAL_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

INSERT INTO MANGUE.TPL_BLOC (TBL_KEY, TBL_LIBELLE,TBL_POSITION, D_CREATION, D_MODIFICATION, TON_KEY, TBN_KEY, TBL_CODE) VALUES (13, 'Contribution à l''activité du service', 2, TO_DATE( '01/09/2013', 'DD/MM/YYYY'), TO_DATE( '01/09/2013', 'DD/MM/YYYY'),  12, 1, 'APPGENCOAS')


--MANGUE.FK_TPL_BLOC_ONGLET--

---------------------------------------------------------
CREATE TABLE MANGUE.REPART_COMPETENCE_POSTE 
(					
										ID INT PRIMARY KEY NOT NULL ,  
										EVALUATION_KEY NUMBER,
										CODEEMPLOI VARCHAR(2),
										ORDRE VARCHAR(2),
										PERIODE_DEBUT DATE,
										PERIODE_FIN DATE
)

CREATE SEQUENCE MANGUE.REPART_COMPETENCE_POSTE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

ALTER TABLE MANGUE.EVALUATION ADD (NO_REPARTITION_POSTE NUMBER);
-----------------------------------------------------------------------------------------------------


