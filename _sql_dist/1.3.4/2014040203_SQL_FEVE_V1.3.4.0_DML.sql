--
-- Patch DML de FEVE du 02/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.3.4.0
-- Date de publication : 02/04/2014
-- Auteur(s) : Equipe FEVE
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


-------------------------V20140225.115917__DML_Chgt_niveau_competences.sql--------------------------



DECLARE
	compteur integer;
	date_max date;
 BEGIN
	 
	select max(MANGUE.evaluation_periode.EPE_D_FIN) into date_max from MANGUE.evaluation_periode;
  
  	if (date_max is null) then
    	date_max := TO_DATE( '01/01/2013', 'DD/MM/YYYY');
  	end if;
  
	 SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.MANGUE_PARAMETRES 
 	 WHERE MANGUE.MANGUE_PARAMETRES.PARAM_KEY = 'FEV_MODIFICATION_FORMATIONS_SUIVIES_DANS_FEVE';
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.MANGUE_PARAMETRES ( PARAM_ORDRE, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES ) VALUES ( 
		MANGUE.MANGUE_PARAMETRES_SEQ.NEXTVAL, 'FEV_MODIFICATION_FORMATIONS_SUIVIES_DANS_FEVE', 'O', 
		'La saisie des formations suivies est-elle autorisée dans Feve (entretien professionnel) (O/N)');
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_ITEM_VALEUR.TIV_LIBELLE = 'à acquérir';
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM_VALEUR (TIV_KEY,TIV_LIBELLE) VALUES (8,'à acquérir');
	END IF;
  

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_ITEM_VALEUR.TIV_LIBELLE = 'à développer';
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM_VALEUR (TIV_KEY,TIV_LIBELLE) VALUES (9,'à développer');
	END IF;
  

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_ITEM_VALEUR.TIV_LIBELLE = 'maîtrise';
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM_VALEUR (TIV_KEY,TIV_LIBELLE) VALUES (10,'maîtrise');
	END IF;


	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_ITEM_VALEUR.TIV_LIBELLE = 'expert';
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM_VALEUR (TIV_KEY,TIV_LIBELLE) VALUES (11,'expert');
	END IF;

 
-- association des nouveaux niveaux / positions
INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR( TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION, D_DEB_VAL, D_FIN_VAL, TIV_POSITION) 
SELECT TIT_KEY, 8, SYSDATE, SYSDATE, date_max+1, NULL, 1
FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR
WHERE TIV_KEY = 5 and D_FIN_VAL is null;

INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR( TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION, D_DEB_VAL, D_FIN_VAL, TIV_POSITION) 
SELECT TIT_KEY, 9, SYSDATE, SYSDATE, date_max+1, NULL, 2
FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR
WHERE TIV_KEY = 5 and D_FIN_VAL is null;

INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR( TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION, D_DEB_VAL, D_FIN_VAL, TIV_POSITION) 
SELECT TIT_KEY, 10, SYSDATE, SYSDATE, date_max+1, NULL, 3
FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR
WHERE TIV_KEY = 5 and D_FIN_VAL is null;

INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR( TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION, D_DEB_VAL, D_FIN_VAL, TIV_POSITION) 
SELECT TIT_KEY, 11, SYSDATE, SYSDATE, date_max+1, NULL, 4
FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR
WHERE TIV_KEY = 5 and D_FIN_VAL is null;

-- mise à jour des dates de validité et des positions
UPDATE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
SET D_FIN_VAL = date_max, D_MODIFICATION = SYSDATE
WHERE TIV_KEY IN (1,2,3,4,5,6,7) and D_FIN_VAL is null;


SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.NIVEAU_COMPETENCE_PRO 
 	 WHERE GRHUM.NIVEAU_COMPETENCE_PRO.NCP_LIBELLE = 'à acquérir';
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.NIVEAU_COMPETENCE_PRO(NCP_KEY,NCP_LIBELLE,D_DEB_VAL,D_FIN_VAL,NCP_POSITION) VALUES (8,'à acquérir',date_max+1,null, 1);
	END IF;
  

SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.NIVEAU_COMPETENCE_PRO 
 	 WHERE GRHUM.NIVEAU_COMPETENCE_PRO.NCP_LIBELLE = 'à développer';
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.NIVEAU_COMPETENCE_PRO(NCP_KEY,NCP_LIBELLE,D_DEB_VAL,D_FIN_VAL,NCP_POSITION) VALUES (9,'à développer',date_max+1, null, 2);
	END IF;

SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.NIVEAU_COMPETENCE_PRO 
 	 WHERE GRHUM.NIVEAU_COMPETENCE_PRO.NCP_LIBELLE = 'maîtrise';
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.NIVEAU_COMPETENCE_PRO(NCP_KEY,NCP_LIBELLE,D_DEB_VAL,D_FIN_VAL,NCP_POSITION) VALUES (10,'maîtrise',date_max+1, null, 3);
	END IF;

SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.NIVEAU_COMPETENCE_PRO 
 	 WHERE GRHUM.NIVEAU_COMPETENCE_PRO.NCP_LIBELLE = 'expert';
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.NIVEAU_COMPETENCE_PRO(NCP_KEY,NCP_LIBELLE,D_DEB_VAL,D_FIN_VAL,NCP_POSITION) VALUES (11,'expert',date_max+1, null, 4);
	END IF;
  

UPDATE GRHUM.NIVEAU_COMPETENCE_PRO  
SET D_FIN_VAL = date_max
WHERE NCP_KEY IN (5,6,7);

COMMIT;

END;
/



--------------------------V20140306.182125__DML_Chgt_modalites_recours.sql--------------------------

update mangue.tpl_item set 
    tit_libelle = ' ',
    tit_commentaire = '- recours spécifique (Article 6 du décret n° 2010-888 du 28 juillet 2010) :<br>
L''agent peut saisir l''autorité hiérarchique d''une demande de révision de son compte rendu d''entretien professionnel. Ce recours hiérarchique<br>
doit être exercé dans le délai de 15 jours francs suivant la notification du compte rendu d''entretien professionnel.<br>
La réponse de l''autorité hiérarchique doit être notifiée dans un délai de 15 jours francs à compter de la date de réception de la demande de<br>
révision du compte rendu de l''entretien professionnel.<br>
A compter de la date de la notification de cette réponse l''agent peut saisir la commission administrative paritaire dans un délai d''un mois. Le<br>
recours hiérarchique est le préalable obligatoire à la saisine de la CAP.<br>
- recours de droit commun :<br>
L''agent qui souhaite contester son compte rendu d''entretien professionnel peut exercer un recours de droit commun devant le juge administratif<br>
dans les 2 mois suivant la notification du compte rendu de l''entretien professionnel, sans exercer de recours gracieux ou hiérarchique (et sans<br>
saisir la CAP) ou après avoir exercé un recours administratif de droit commun (gracieux ou hiérarchique).<br>
Il peut enfin saisir le juge administratif à l''issue de la procédure spécifique définie par l''article 6 précité. Le délai de recours contentieux,<br>
suspendu durant cette procédure, repart à compter de la notification de la décision finale de l''administration faisant suite à l''avis rendu par la CAP.<br>'
 where tit_code = 'EVARECOURI';
----------------------------V20140314.142510__DML_SITUATION_ACTIVITE.sql----------------------------
-------------------------------------------------------------------------
--------------------SITUATION D'ACTIVITE --------------------------------
-- ----------------------------------------------------------------------




DECLARE
  compteur int;
  position_tableau integer;

BEGIN 

	
---------------Ajout nouvelles colonnes

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_BLOC 
 	 WHERE MANGUE.TPL_BLOC.TBL_CODE = 'TABLEAU';
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_BLOC ( TBL_KEY, TBL_LIBELLE, TBL_POSITION, D_CREATION, D_MODIFICATION, TON_KEY, TBL_FACULTATIF,TBN_KEY, TBL_CODE, D_FIN_VAL) VALUES (MANGUE.TPL_BLOC_SEQ.NEXTVAL, 'Tableau appréciation générale', 2, SYSDATE,SYSDATE, 8, 'O', 1,'TABLEAU',TO_DATE( '01/01/2000', 'DD/MM/YYYY'));
	END IF;

	select MANGUE.TPL_BLOC.TBL_KEY into position_tableau from MANGUE.TPL_BLOC where TBL_CODE='TABLEAU';

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM 
 	 WHERE MANGUE.TPL_ITEM.TIT_KEY = 37;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (37, 'Compétences professionnelles et technicité', 1, 1, position_tableau, SYSDATE, SYSDATE, 'REPCOMPPRO' );
	END IF;
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM 
 	 WHERE MANGUE.TPL_ITEM.TIT_KEY = 38;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (38, 'Contribution à l''activité du service', 1, 1, position_tableau, SYSDATE, SYSDATE, 'CONTRIACTI');
	END IF;	
	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM 
 	 WHERE MANGUE.TPL_ITEM.TIT_KEY = 39;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (39, 'Capacités professionnelles et relationnelles', 1, 1, position_tableau, SYSDATE, SYSDATE, 'CAPACITEPR' );
	END IF;	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ITEM 
 	 WHERE MANGUE.TPL_ITEM.TIT_KEY = 40;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_ITEM (TIT_KEY,TIT_LIBELLE, TIT_POSITION, TIN_KEY, TBL_KEY, D_CREATION, D_MODIFICATION, TIT_CODE) VALUES (40, 'Aptitude à l''encadrement et/ou à la conduite de projets', 1, 1, position_tableau, SYSDATE, SYSDATE ,'APTITUDEEN');
	END IF;	
	

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR.TIT_KEY = 37;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (37, 1, SYSDATE, SYSDATE);
	END IF;	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR.TIT_KEY = 38;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (38, 1, SYSDATE, SYSDATE);
	END IF;
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR.TIT_KEY = 39;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (39, 1,  SYSDATE, SYSDATE);
	END IF;
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_REPART_ITEM_ITEM_VALEUR 
 	 WHERE MANGUE.TPL_REPART_ITEM_ITEM_VALEUR.TIT_KEY = 40;
        IF (compteur = 0)
  	  THEN
		INSERT INTO MANGUE.TPL_REPART_ITEM_ITEM_VALEUR (TIT_KEY, TIV_KEY, D_CREATION, D_MODIFICATION) VALUES (40, 1,  SYSDATE, SYSDATE);
	END IF;

END;
/
-------------------------V20140314.142737__DML_Reorganisation_des_menus.sql-------------------------

UPDATE mangue.tpl_onglet set ton_position = 1   where ton_code = 'EVAAGENT';
UPDATE mangue.tpl_onglet set ton_position = 2   where ton_code = 'EVAOBJPREC';
UPDATE mangue.tpl_onglet set ton_position = 3   where ton_code = 'BILANECOUL';
UPDATE mangue.tpl_onglet set ton_position = 4   where ton_code = 'EVASITU';
UPDATE mangue.tpl_onglet set ton_position = 5   where ton_code = 'EVACOMP';
UPDATE mangue.tpl_onglet set ton_position = 6   where ton_code = 'SYNTVALPRO';
UPDATE mangue.tpl_onglet set ton_position = 7   where ton_code = 'EVAOBJSUIV';
UPDATE mangue.tpl_onglet set ton_position = 8   where ton_code = 'EVAEVOL';
UPDATE mangue.tpl_onglet set ton_position = 9   where ton_code = 'NOTICEPROM';
UPDATE mangue.tpl_onglet set ton_position = 10  where ton_code = 'EVAFIN';


DECLARE
  compteur integer;
  date_max date;
  position_max integer;

 BEGIN
  
  select max(MANGUE.evaluation_periode.EPE_D_FIN) into date_max from MANGUE.evaluation_periode;
  select max(MANGUE.TPL_ONGLET.ton_key)+1 into position_max from MANGUE.TPL_ONGLET;
  
  if (date_max is null) then
    date_max := TO_DATE( '01/01/2013', 'DD/MM/YYYY');
  end if;
  
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TpL_bloc 
 	 WHERE MANGUE.TpL_bloc.tbl_code = 'CONACTSERV';
        IF (compteur = 1)
  	  THEN
		UPDATE MANGUE.TPL_BLOC SET D_FIN_VAL = date_max WHERE MANGUE.TpL_bloc.tbl_code = 'CONACTSERV';
	END IF;
	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TpL_bloc 
 	 WHERE MANGUE.TpL_bloc.tbl_code = 'QUAPERSREL';
        IF (compteur = 1)
  	  THEN
		UPDATE MANGUE.TPL_BLOC SET D_FIN_VAL = date_max WHERE MANGUE.TpL_bloc.tbl_code = 'QUAPERSREL';
	END IF;

	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TpL_bloc 
 	 WHERE MANGUE.TpL_bloc.tbl_code = 'EVAAPTI';
        IF (compteur = 1)
  	  THEN
		UPDATE MANGUE.TPL_BLOC SET D_FIN_VAL = date_max WHERE MANGUE.TpL_bloc.tbl_code = 'EVAAPTI';
	END IF;
	
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.tpl_bloc 
 	 WHERE MANGUE.tpl_bloc .TBL_CODE = 'FORSOUHAIT';
        IF (compteur = 1)
  	  THEN
		UPDATE MANGUE.TPL_BLOC SET D_FIN_VAL = date_max WHERE MANGUE.TpL_bloc.tbl_code = 'FORSOUHAIT';
	END IF;

	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.tpl_bloc 
 	 WHERE MANGUE.tpl_bloc .TBL_CODE = 'FORSOUHAI2';
        IF (compteur = 1)
  	  THEN
		UPDATE MANGUE.TPL_BLOC SET D_FIN_VAL = date_max WHERE MANGUE.TpL_bloc.tbl_code = 'FORSOUHAI2';
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ONGLET 
 	 WHERE MANGUE.TPL_ONGLET.TON_CODE = 'EVAAPTI';
        IF (compteur = 1)
 	  THEN
		UPDATE MANGUE.TPL_ONGLET SET D_FIN_VAL = date_max WHERE  MANGUE.TPL_ONGLET.TON_CODE = 'EVAAPTI';
	END IF;
	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM MANGUE.TPL_ONGLET 
 	 WHERE MANGUE.TPL_ONGLET.TON_CODE = 'INVISIBLE';
        IF (compteur = 0)
  	  THEN
  	  	INSERT INTO MANGUE.TPL_ONGLET (TON_KEY,TON_LIBELLE,D_CREATION, D_MODIFICATION, TON_POSITION, TON_CODE , D_FIN_VAL , TEM_CONTRACTUEL , TEM_TITULAIRE)
		VALUES (MANGUE.TPL_ONGLET_SEQ.NEXTVAL, 'Onglet invisible', SYSDATE, SYSDATE,position_max, 'INVISIBLE', TO_DATE( '01/01/2000', 'DD/MM/YYYY'),'O','O');
	END IF;

END;
/



DECLARE
  
  CURSOR c_evaluations IS SELECT * from MANGUE.EVALUATION;
  l_evaluation MANGUE.EVALUATION%ROWTYPE;
  id_cle_situ number;  
  
begin    

FOR l_evaluation IN c_evaluations
LOOP
 
  SELECT min(MANGUE.SITU_ACTIVITE.sac_key) into id_cle_situ from MANGUE.SITU_ACTIVITE where MANGUE.SITU_ACTIVITE.eva_key=l_evaluation.eva_key
 and MANGUE.SITU_ACTIVITE.eva_type is null;  
  if (id_cle_situ is not null) then
    UPDATE MANGUE.SITU_ACTIVITE set eva_type=1  where sac_key=id_cle_situ;
  end if;
  
   SELECT min(MANGUE.SITU_ACTIVITE.sac_key) into id_cle_situ from MANGUE.SITU_ACTIVITE where MANGUE.SITU_ACTIVITE.eva_key=l_evaluation.eva_key
 and MANGUE.SITU_ACTIVITE.eva_type is null; 
  if (id_cle_situ is not null) then
    UPDATE MANGUE.SITU_ACTIVITE set eva_type=2  where sac_key=id_cle_situ;
  end if;
  
 SELECT min(MANGUE.SITU_ACTIVITE.sac_key) into id_cle_situ from MANGUE.SITU_ACTIVITE where MANGUE.SITU_ACTIVITE.eva_key=l_evaluation.eva_key
 and MANGUE.SITU_ACTIVITE.eva_type is null;  
  if (id_cle_situ is not null) then
    UPDATE MANGUE.SITU_ACTIVITE set eva_type=3  where sac_key=id_cle_situ;
  end if;
  
 SELECT min(MANGUE.SITU_ACTIVITE.sac_key) into id_cle_situ from MANGUE.SITU_ACTIVITE where MANGUE.SITU_ACTIVITE.eva_key=l_evaluation.eva_key
 and MANGUE.SITU_ACTIVITE.eva_type is null;   
  if (id_cle_situ is not null) then
    UPDATE MANGUE.SITU_ACTIVITE set eva_type=4  where sac_key=id_cle_situ;
  end if;
  
  SELECT min(MANGUE.SITU_ACTIVITE.sac_key) into id_cle_situ from MANGUE.SITU_ACTIVITE where MANGUE.SITU_ACTIVITE.eva_key=l_evaluation.eva_key
 and MANGUE.SITU_ACTIVITE.eva_type is null; 
  if (id_cle_situ is not null) then
    UPDATE MANGUE.SITU_ACTIVITE set eva_type=5  where sac_key=id_cle_situ;
  end if;
  
END LOOP;

end;
/
-- DB_VERSION
INSERT INTO MANGUE.DB_VERSION_FEVE (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (MANGUE.DB_VERSION_FEVE_SEQ.NEXTVAL, '1.3.4.0', TO_DATE('02/04/2014', 'DD/MM/YYYY'), SYSDATE, 'Evolutions professionnelles BO 30/05/2013');
COMMIT;
