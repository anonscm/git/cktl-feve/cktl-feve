--
-- Patch DDL de FEVE du 02/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.3.4.0
-- Date de publication : 02/04/2014
-- Auteur(s) : Equipe FEVE
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--------------------------V20140225.110116__DDL_Ajout_fct_encadrement.sql---------------------------

-- ---------------------------------------------------
-- Ajout colonnes pour encadrement et conduite projet dans la table FICHE_DE_POSTE
-- ---------------------------------------------------


DECLARE
  is_column_exists int;
BEGIN 
	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'FONCTIONS_CONDUITE_PROJET' 
	   AND table_name = 'FICHE_DE_POSTE';
    IF (is_column_exists  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.FICHE_DE_POSTE ADD (FONCTIONS_CONDUITE_PROJET VARCHAR2(1) DEFAULT ''N'' NOT NULL)';
		execute immediate 'COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.FONCTIONS_CONDUITE_PROJET IS ''Si l agent a des fonctions de conduite de projet''';
	  END IF;

	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'FONCTIONS_ENCADREMENT' 
	   AND table_name = 'FICHE_DE_POSTE';
    IF (is_column_exists  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.FICHE_DE_POSTE ADD (FONCTIONS_ENCADREMENT VARCHAR2(1) DEFAULT ''N'' NOT NULL)';
		execute immediate 'COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.FONCTIONS_ENCADREMENT IS ''Si l agent a des fonctions d encadrement''';
	  END IF;

	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'NB_AGENTS' 
	   AND table_name = 'FICHE_DE_POSTE';
    IF (is_column_exists  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.FICHE_DE_POSTE ADD NB_AGENTS NUMBER';
		execute immediate 'COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.NB_AGENTS IS ''nombre d agents encadrés par l agent''';
	  END IF;


	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'NB_AGENTS_CATEGORIE_A' 
	   AND table_name = 'FICHE_DE_POSTE';
    IF (is_column_exists  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.FICHE_DE_POSTE ADD NB_AGENTS_CATEGORIE_A NUMBER';
		execute immediate 'COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.NB_AGENTS_CATEGORIE_A IS ''nombre d agents de catégorie A encadrés par l agent''';
	  END IF;
    
	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'NB_AGENTS_CATEGORIE_B' 
	   AND table_name = 'FICHE_DE_POSTE';
    IF (is_column_exists  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.FICHE_DE_POSTE ADD NB_AGENTS_CATEGORIE_B NUMBER';
		execute immediate 'COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.NB_AGENTS_CATEGORIE_B IS ''nombre d agents de catégorie B encadrés par l agent''';
	  END IF;


	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'NB_AGENTS_CATEGORIE_C' 
	   AND table_name = 'FICHE_DE_POSTE';
    IF (is_column_exists  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.FICHE_DE_POSTE ADD NB_AGENTS_CATEGORIE_C NUMBER';
		execute immediate 'COMMENT ON COLUMN MANGUE.FICHE_DE_POSTE.NB_AGENTS_CATEGORIE_C IS ''nombre d agents de catégorie C encadrés par l agent''';
	  END IF;

END;
/


------------------------V20140314.141242__DDL_MODIFICATION_EVOLUTION_PRO.sql------------------------
----------------------PAGE EVOLUTION PROFESSIONNELLES---------------------------------


DECLARE
  compteur int;
  
BEGIN 

	SELECT count(*) 
	  INTO compteur 
	  FROM all_tables where table_name = 'FEVE_REPART_COMP_POSTE' and OWNER = 'MANGUE';
	IF (compteur  = 0) THEN
	
		--compétences à acquerir ou developper pour tenir le poste
		execute immediate 'CREATE TABLE MANGUE.FEVE_REPART_COMP_POSTE ('
													|| ' 		ID NUMBER PRIMARY KEY NOT NULL,'
													|| ' 		EVALUATION_KEY NUMBER,'
													|| ' 		LIBELLE VARCHAR2(2000),'
													|| ' 		PERIODE VARCHAR2(2000)'
													|| ' ) TABLESPACE GRH_INDX';
		                          
		                          
		execute immediate 'CREATE SEQUENCE MANGUE.FEVE_REPART_COMP_POSTE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER';
		
	END IF;	
	
	SELECT count(*) 
	  INTO compteur 
	  FROM all_tables where table_name = 'FEVE_REPART_COMP_EVOLUTION' and OWNER = 'MANGUE';
	IF (compteur  = 0) THEN
	
		--compétences à acquerir ou developper en vue d'une evolution professionnelle
		execute immediate 'CREATE TABLE MANGUE.FEVE_REPART_COMP_EVOLUTION ('
												|| '			ID NUMBER PRIMARY KEY NOT NULL,'
												|| '			EVALUATION_KEY NUMBER,'
												|| '			LIBELLE VARCHAR2(2000),'
												|| '			PERIODE VARCHAR2(2000)'
												|| ' ) TABLESPACE GRH_INDX';
		                          
		execute immediate 'CREATE SEQUENCE MANGUE.FEVE_REPART_COMP_EVOLUTION_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER';
		
	END IF;	
	
	SELECT count(*) 
	  INTO compteur 
	  FROM all_tables where table_name = 'FEVE_REPART_PERS_FORMATION' and OWNER = 'MANGUE';
	IF (compteur  = 0) THEN
	
	
		--autre perspectives de formation
		execute immediate 'CREATE TABLE MANGUE.FEVE_REPART_PERS_FORMATION ('
													|| '		ID NUMBER PRIMARY KEY NOT NULL,'
													|| '		EVALUATION_KEY NUMBER,'
													|| '		LIBELLE VARCHAR2(2000),'
													|| '	ECHEANCE VARCHAR2(2000)'
													|| ' ) TABLESPACE GRH_INDX';
		                          
		execute immediate 'CREATE SEQUENCE MANGUE.FEVE_REPART_PERS_FORMATION_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER';

	END IF;	

	SELECT count(*) 
	  INTO compteur 
	  FROM all_tab_cols 
	 WHERE column_name = 'COMP_POST_COMMENTAIRE' 
	   AND table_name = 'EVALUATION'
	   AND OWNER = 'MANGUE';
    IF (compteur  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.EVALUATION ADD COMP_POST_COMMENTAIRE VARCHAR2(2000)';
		execute immediate 'COMMENT ON COLUMN MANGUE.EVALUATION.COMP_POST_COMMENTAIRE IS  ''commentaire sur les compétences à acquerir ou developper pour tenir le poste''';
	  END IF;
	  
	  
  	SELECT count(*) 
	  INTO compteur 
	  FROM all_tab_cols 
	 WHERE column_name = 'FORMATION_PREVU' 
	   AND table_name = 'EVALUATION'
 	   AND OWNER = 'MANGUE';
    IF (compteur  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.EVALUATION ADD FORMATION_PREVU CHAR(1)';
		execute immediate 'COMMENT ON COLUMN MANGUE.EVALUATION.FORMATION_PREVU IS  ''témoin sur les formations prévus''';
	  END IF;
	  
	  ----
  	SELECT count(*) 
	  INTO compteur 
	  FROM all_tab_cols 
	 WHERE column_name = 'COMP_EVOL_COMMENTAIRE' 
	   AND table_name = 'EVALUATION'
	   AND OWNER = 'MANGUE';
    IF (compteur  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.EVALUATION ADD COMP_EVOL_COMMENTAIRE VARCHAR2(2000)';
		execute immediate 'COMMENT ON COLUMN MANGUE.EVALUATION.COMP_EVOL_COMMENTAIRE IS  ''commentaire sur les compétences à acquerir en vue d une évolution professionnelle''';
	  END IF;
	  
	  
	  
	  
 	SELECT count(*) 
	  INTO compteur 
	  FROM all_tab_cols 
	 WHERE column_name = 'COMP_FORMATION_COMMENTAIRE' 
	   AND table_name = 'EVALUATION'
	   AND OWNER = 'MANGUE';
    IF (compteur  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.EVALUATION ADD COMP_FORMATION_COMMENTAIRE VARCHAR2(2000)';
		execute immediate 'COMMENT ON COLUMN MANGUE.EVALUATION.COMP_FORMATION_COMMENTAIRE IS  ''utilisation du droit individuel à la formation''';
	  END IF;
	  
	  
  	SELECT count(*) 
	  INTO compteur 
	  FROM all_tab_cols 
	 WHERE column_name = 'MOBILISER_DIF' 
	   AND table_name = 'EVALUATION'
	   AND OWNER = 'MANGUE';
    IF (compteur  = 0) THEN
    execute immediate 'ALTER TABLE MANGUE.EVALUATION ADD MOBILISER_DIF CHAR(1)';
		execute immediate 'COMMENT ON COLUMN MANGUE.EVALUATION.MOBILISER_DIF IS  ''utilisation du droit individuel à la formation''';
	  END IF; 
	  
END;
/

----------------------------V20140314.142149__DDL_SITUATION_ACTIVITE.sql----------------------------

DECLARE
  compteur int;
BEGIN 
	SELECT count(*) 
	  INTO compteur 
	  FROM all_tables where table_name = 'REPART_APP_GENERAL' and OWNER = 'MANGUE';
	IF (compteur  = 0) THEN
			

		execute immediate 'CREATE TABLE MANGUE.REPART_APP_GENERAL ('
								 || ' 	id NUMBER PRIMARY KEY NOT NULL,'
													 || ' 		EVALUATION_KEY NUMBER,'
													 || ' 		ITEM_VALEUR_TIV_KEY NUMBER,'
													 || ' 		ITEM_TIT_KEY NUMBER'
											 || ' ) TABLESPACE GRH_INDX';

		execute immediate 'CREATE SEQUENCE MANGUE.REPART_APP_GENERAL_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER';
	END IF;




	SELECT count(*) 
	  INTO compteur 
	  FROM all_tab_cols 
	 WHERE column_name = 'EVA_TYPE' 
	   AND table_name = 'SITU_ACTIVITE'
	   AND OWNER = 'MANGUE';
    IF (compteur  = 0) THEN
    		execute immediate 'ALTER TABLE MANGUE.SITU_ACTIVITE ADD (EVA_TYPE NUMBER)';
		execute immediate 'COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.SAC_KEY IS ''Clé primaire de la table''';
		execute immediate 'COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.EVA_KEY IS ''Clé étrangere vers l evaluation''';
		execute immediate 'COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.SAC_SITUATION IS ''Texte de la situation''';
		execute immediate 'COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.D_CREATION IS ''Date de création de la situation''';
		execute immediate 'COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.D_MODIFICATION IS ''Date de modification de la situation''';
		execute immediate 'COMMENT ON COLUMN MANGUE.SITU_ACTIVITE.EVA_TYPE IS ''type d evaluation''';
	  END IF;
	  
END;
/

---------------------------V20140314.151534__DDL_Creation_DB_Version.sql----------------------------
--
-- DB_VERSION
--

DECLARE
  compteur int;
  
BEGIN 

	SELECT count(*) 
	  INTO compteur 
	  FROM all_tables where table_name = 'DB_VERSION_FEVE' and OWNER = 'MANGUE';
	IF (compteur  = 0) THEN


			execute immediate 'CREATE TABLE MANGUE.DB_VERSION_FEVE ('
					  || ' 		DBV_ID	NUMBER(4) PRIMARY KEY NOT NULL,' 
					  || ' 		DBV_LIBELLE	VARCHAR2(15) NOT NULL,' 
					  || ' 		DBV_DATE	DATE NOT NULL,' 
					  || ' 		DBV_INSTALL	DATE, '
					  || ' 		DBV_COMMENT	VARCHAR2(2000)'
					  || ' ) TABLESPACE GRH_INDX';

			execute immediate 'CREATE SEQUENCE MANGUE.DB_VERSION_FEVE_SEQ START WITH 1 NOCACHE';

			execute immediate 'COMMENT ON TABLE MANGUE.DB_VERSION_FEVE IS ''Historique des versions de FEVE'''; 
			execute immediate 'COMMENT ON COLUMN MANGUE.DB_VERSION_FEVE.DBV_ID IS ''Identifiant de la version'''; 
			execute immediate 'COMMENT ON COLUMN MANGUE.DB_VERSION_FEVE.DBV_LIBELLE IS ''Libellé de la version'''; 
			execute immediate 'COMMENT ON COLUMN MANGUE.DB_VERSION_FEVE.DBV_DATE IS ''Date de release de la version'''; 
			execute immediate 'COMMENT ON COLUMN MANGUE.DB_VERSION_FEVE.DBV_INSTALL IS ''Date d installation de la version. Si non renseigné, la version n est pas complétement installée.''';
			execute immediate 'COMMENT ON COLUMN MANGUE.DB_VERSION_FEVE.DBV_COMMENT IS ''Le commentaire : une courte description de cette version de la base de données.''';

	END IF;
END;
/



