package org.cocktail.feve.app.droits;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

/**
 * Un type de droit, une cible
 * @author juliencallewaert
 *
 */
public class DroitCibleIndividu extends DroitCible {
	
	
	private EOIndividu personneCible;
	
	
	public EOIndividu getPersonneCible() {
		return personneCible;
	}
	public void setPersonneCible(EOIndividu personneCible) {
		this.personneCible = personneCible;
	}
	
	public DroitCibleIndividu(String typeDroit, EOIndividu personneCible) {
		super();
		this.typeDroit = typeDroit;
		this.personneCible = personneCible;
	}
	
	/**
	 * 
	 * @param typeDroit 
	 */
	public DroitCibleIndividu(String typeDroit) {
		super();
		this.typeDroit = typeDroit;
	}
	
	/**
	 * 
	 * @return true/false
	 */
	public boolean estCibleEtablissement() {
		return this.personneCible == null;
	}
	
	
}
