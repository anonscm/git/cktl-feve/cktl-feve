package org.cocktail.feve.app.droits;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

public class DroitCibleStructure extends DroitCible {

	
	private EOStructure structureCible;
	private boolean isResponsableInclus;
	
	public EOStructure getStructureCible() {
		return structureCible;
	}
	
	public void setStructureCible(EOStructure structureCible) {
		this.structureCible = structureCible;
	}
	
	public DroitCibleStructure(String typeDroit, EOStructure structureCible, boolean isResponsableInclus) {
		super();
		this.typeDroit = typeDroit;
		this.structureCible = structureCible;
		this.isResponsableInclus = isResponsableInclus;
	}

	public DroitCibleStructure(String typeDroit) {
		super();
		this.typeDroit = typeDroit;
	}
	
	/**
	 * 
	 * @return true/false
	 */
	public boolean estCibleEtablissement() {
		return this.structureCible == null;
	}

	public boolean isResponsableInclus() {
		return isResponsableInclus;
	}

	public void setResponsableInclus(boolean isResponsableInclus) {
		this.isResponsableInclus = isResponsableInclus;
	}

	
	
}
