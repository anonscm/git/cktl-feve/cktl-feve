package org.cocktail.feve.app.droits;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

public class DroitPersonneResponsableHolder {

	
	private IPersonne personne;
	private boolean responsableInclus;
	
	public DroitPersonneResponsableHolder(IPersonne personne,
			boolean responsableInclus) {
		super();
		this.personne = personne;
		this.responsableInclus = responsableInclus;
	}
	
	public IPersonne getPersonne() {
		return personne;
	}
	public boolean isResponsableInclus() {
		return responsableInclus;
	}
	public void setPersonne(IPersonne personne) {
		this.personne = personne;
	}
	public void setResponsableInclus(boolean responsableInclus) {
		this.responsableInclus = responsableInclus;
	}
	
}
