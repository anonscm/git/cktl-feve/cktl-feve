package org.cocktail.feve.app.droits;


/**
 * Enum listant toutes les fonctions utilisés dans l'application Feve
 * @author juliencallewaert
 *
 */
public enum Fonction {

	// Catégorie "Poste"
	GERER_POSTE ("PGERER", true),
	EDITER_PROFIL_POSTE("PEDITER", true),

	// Catégorie "Fiche de Poste"
	GERER_FICHE_POSTE("FPGERER", true),
	VALIDER_FICHE_POSTE("FPVALIDER", true),
	EDITER_FICHE_POSTE("FPEDITER", true),

	// Catégorie "Entretien Professionnel"
	GERER_ENTRETIEN_PROFESSIONNEL("EPGERER", true),
	EDITER_COMPTE_RENDU_EP_VIERGE("EPEDITERV", true),
	EDITER_COMPTE_RENDU_EP_FINALISE("EPEDITERF", true),
	VALIDER_COMPTE_RENDU_EP_FINALISE("EPVALIDER", true),

	// Catégorie "Droits - Périodes"
	GERER_DROIT("DPACCRED", true),
	GERER_PERIODE_CAMPAGNE("DPCAMP", false),

	// Catégorie "Bibliothèque"	
	BIBLIO("BIBLIO", false),

	// Catégorie "Identité"
	CHANGER_IDENTITE("IDENTITE", false),

	// Catégorie "Administration"
	ADMIN_OUTILS_SUIVI("ADSUIVI", false),
	ADMIN_SILLAND_LOLF("ADLOLF", false),
	ADMIN_SYNC_LOLF("ADSYNCHR", false),
	ADMIN_PERIODE_SAISIE("ADPERIO", false),
	ADMIN_AUTRES("ADAUTRES", false),
	ADMIN_CREER_POSTE("ADPCREER", false),
	ADMIN_CONDITIONS_EVALUATION("ADEVAL", false),
	ADMIN_FORMATIONS("ADFORM", false),
	ADMIN_EDITION_FICHE_POSTE("ADEFP", false);

	/** Identifiant interne de la fonction (cf table GD_FONCTION). */
	private String idFonction;

	/** Est-ce que la fonction peut agir selon un périmétre donné
	 * Exemple : Sur Feve, changer d'identité est une fonction qui n'a pas de périmétre cible,
	 * alors que la fonction "GERER_POSTE" peut en avoir un (une structure cible, ou un individu en particulier)
	 */	
	private boolean isPerimetreCible;
	
	public String getIdFonction() {
		return idFonction;
	}

	/**
	 * Contructeur.
	 *
	 * @param idFonction L'identifiant interne de la fonction
	 */
	Fonction(String idFonction, boolean isPerimetreCible) {
		this.idFonction = idFonction;
		this.isPerimetreCible = isPerimetreCible;
	}

	public boolean isPerimetreCible() {
		return isPerimetreCible;
	}

	/**
	 * Récupérer la fonction depuis l'id
	 * @param fonIdInterne id interne
	 * @return {@link Fonction}
	 */
	public static Fonction getFonction(String fonIdInterne) {
		Fonction resultat = null;
		
		for (Fonction fonction : Fonction.values()) {
			
			if (fonction.getIdFonction().equals(fonIdInterne)) {
				resultat = fonction;
				break;
			}
		}
		return resultat;
	}
	

}
