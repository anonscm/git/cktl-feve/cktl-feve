package org.cocktail.feve.app.droits;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.droits.DroitsHelper;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.tri.StructureTri;
import org.cocktail.fwkcktlwebapp.common.CktlLog;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * 
 * @author juliencallewaert
 *
 */
public abstract class GRHAutorisationsCache {


	private String appStrId;

	/** map des droits de l'utilisateur connecté par individu cible */
	private Map<String, List<DroitCibleIndividu>> autorisationsOnFonctionCacheParticulierIndividuCible;
	/** map des droits de l'utilisateur connecté par structure cible */
	private Map<String, List<DroitCibleStructure>> autorisationsOnFonctionCacheStructureCible;
	/** map des droits de l'utilisateur connecté par défaut */
	private Map<String, DroitCible> autorisationsOnFonctionCacheParDefaut;
	
	private EOEditingContext ec;
	private EOIndividu individuConnecte;
	
	
	protected Map<EOIndividu, List<IStructure>> cacheAffectationStructures = new HashMap<EOIndividu, List<IStructure>>();
	protected List<IStructure> cacheStructures = new ArrayList<IStructure>(); // affectations des personnels actuels dans ces structures déjà recherchés
	protected List<IIndividu> cacheIndividus = new ArrayList<IIndividu>(); // affectations des individus déjà recherchés
	private IStructure structureCourante;
	
	/**
	 * 
	 * @param appStrId id de l'appli
	 * @param individuConnecte l'individu connecté
	 * @param ec editing context
	 */
	public GRHAutorisationsCache(String appStrId, EOIndividu individuConnecte, EOEditingContext ec) {
		this.appStrId = appStrId;
		this.individuConnecte = individuConnecte;
		this.ec = ec;
		refreshCache(appStrId, individuConnecte.persId(), ec);
	}

	private void refreshCache(String appStrId, Integer persId, EOEditingContext ec) {
		autorisationsOnFonctionCacheParticulierIndividuCible = new HashMap<String, List<DroitCibleIndividu>>();
		autorisationsOnFonctionCacheStructureCible = new HashMap<String, List<DroitCibleStructure>>();
		buildAutorizationOnFonctionCache(ec, appStrId, persId);
		determinerProfilDepuisAnnuaire(ec, persId);
	}


	private void  buildAutorizationOnFonctionCache(EOEditingContext ec, String appStrId, Integer persId) {
		long tDebut = System.currentTimeMillis();
		autorisationsOnFonctionCacheParDefaut = buildAutorizationParDefaut(ec, appStrId, persId);
		autorisationsOnFonctionCacheStructureCible = buildAutorizationFromAnnuaireOnFonctionCache(ec, appStrId, persId);
		buildAutorizationFromSpecificOnFonctionCache(ec);
		CktlLog.log("Chargement des droits (annuaire + spécifique Feve) réalisée en " + (System.currentTimeMillis() - tDebut) + "ms");
	}
	
	
	protected void determinerProfilDepuisAnnuaire(EOEditingContext ec, Integer persId) {
		List<EOGdProfil> profils = DroitsHelper.profilsForPersonne(ec, persId);
		determinerIsProfilSpecifiqueDepuisAnnuaire(ec, profils);
	}
	
	/**
	 * Sur Feve, nous avons besoin d'un isAdmin, et d'un isDRH
	 * @param profils
	 */
	protected abstract void determinerIsProfilSpecifiqueDepuisAnnuaire(EOEditingContext ec, List<EOGdProfil> profils);
	
	
	/**
	 * Construit les autorisations par défaut sur l'application s'il y en a
	 * (ex. sur Feve, la personne connectée a automatiquement le droit Agent sur elle-même)
	 * @param ec
	 * @param appStrId
	 * @param persId
	 * @return
	 */
	protected abstract Map<String, DroitCible> buildAutorizationParDefaut(EOEditingContext ec, String appStrId, Integer persId);
	
	/**
	 * Construit les autorisations définies dans l'annuaire AGrhum
	 * @param ec
	 * @param appStrId
	 * @param profilId
	 * @return
	 */
	private Map<String, List<DroitCibleStructure>> buildAutorizationFromAnnuaireOnFonctionCache(
			EOEditingContext ec, String appStrId, Integer persId) {
		
		List<EOGdProfilDroitFonction> droitsFonction = DroitsHelper.droitsFonctionForPersonneProfilPasHerite(ec, appStrId, persId);
		Map<String, List<DroitCibleStructure>> droitsCache = new HashMap<String, List<DroitCibleStructure>>();
		
		if (CollectionUtils.isNotEmpty(droitsFonction)) {
		
			List<EOStructure> listeStructures = getAllStructuresSpecific(ec);
			
			for (EOGdProfilDroitFonction droitFonction : droitsFonction) {
				for (EOStructure eoStructure : listeStructures) {
					createOrAddDroitCible(droitsCache, droitFonction.toGdFonction().fonIdInterne(), new DroitCibleStructure(droitFonction.toGdTypeDroitFonction().tdfStrId(), eoStructure, true));
				}
			}
		}
		return droitsCache;
	}

	/**
	 * Construit les autorisations définies dans l'application GRH
	 * @param ec
	 * @return
	 */
	protected void buildAutorizationFromSpecificOnFonctionCache(EOEditingContext ec) {
		Map<Integer, List<DroitPersonneResponsableHolder>> accreditationSpecificSurCible = getAccreditationsSpecificIndividu(ec);
		
		for (Integer profilId : accreditationSpecificSurCible.keySet()) {
			List<EOGdProfilDroitFonction> droitsFonction = DroitsHelper.droitsFonctionForProfilPasHerite(ec, appStrId, profilId);
			for (EOGdProfilDroitFonction droitFonction : droitsFonction) {
				gererCreationOrAddDroitCible(accreditationSpecificSurCible, profilId, droitFonction);
			}
		}
	}

	private void gererCreationOrAddDroitCible(Map<Integer, List<DroitPersonneResponsableHolder>> accreditationSpecificSurCible, Integer profilId, EOGdProfilDroitFonction droitFonction) {
		
		for (DroitPersonneResponsableHolder droitPersonneResponsableHolder : accreditationSpecificSurCible.get(profilId)) {
			if (isFonctionAvecCibleParticuliere(droitFonction.toGdFonction().fonIdInterne())) {
				if (droitPersonneResponsableHolder.getPersonne().isStructure()) {
					createOrAddDroitCible(autorisationsOnFonctionCacheStructureCible, droitFonction.toGdFonction().fonIdInterne(), 
							new DroitCibleStructure(droitFonction.toGdTypeDroitFonction().tdfStrId(), (EOStructure) droitPersonneResponsableHolder.getPersonne(), droitPersonneResponsableHolder.isResponsableInclus()));
				} else {
					createOrAddDroitCible(autorisationsOnFonctionCacheParticulierIndividuCible, droitFonction.toGdFonction().fonIdInterne(), 
							new DroitCibleIndividu(droitFonction.toGdTypeDroitFonction().tdfStrId(), (EOIndividu) droitPersonneResponsableHolder.getPersonne()));
				}
			} else {
				if (droitPersonneResponsableHolder.getPersonne().isStructure()) {
					createOrAddDroitCible(autorisationsOnFonctionCacheStructureCible, droitFonction.toGdFonction().fonIdInterne(), new DroitCibleStructure(droitFonction.toGdTypeDroitFonction().tdfStrId()));
				} else {
					createOrAddDroitCible(autorisationsOnFonctionCacheParticulierIndividuCible, droitFonction.toGdFonction().fonIdInterne(), new DroitCibleIndividu(droitFonction.toGdTypeDroitFonction().tdfStrId()));
				}
			}
		} 
	}
	
	/**
	 * Est-ce que la fonction a besoin d'être définie sur des cibles particulières ?
	 * (Ex. Sur Feve, la fonction Bibliotheque ne change pas selon la cible. Soit on l'a en utilisation, 
	 * soit en connaissance, peu importe si la personne a des droits particuliers sur quelqu'un
	 * @param fonIdInterne
	 * @return
	 */
	protected abstract boolean isFonctionAvecCibleParticuliere(String fonIdInterne);

	/**
	 * Liste toutes les structures utilisées par l'application
	 * @param ec
	 * @return
	 */
	protected abstract List<EOStructure> getAllStructuresSpecific(EOEditingContext ec);
	
	/**
	 * Liste les accréditations définies spécifiquement dans l'application
	 * @param ec editing contexte
	 * @return {@link Map} Integer correspondant à un profil ID, List<DroitPersonneResponsableHolder> aux personnes cibles
	 */
	protected abstract Map<Integer, List<DroitPersonneResponsableHolder>> getAccreditationsSpecificIndividu(EOEditingContext ec);
	
	protected List<IStructure> getAllShowStructuresOnFonction(String strId) {
    	
    	List<IStructure> listeStructures = new ArrayList<IStructure>();
    	
    	if (!autorisationsOnFonctionCacheStructureCible.isEmpty()) {
    	
	    	List<DroitCibleStructure> droitsSurCible = autorisationsOnFonctionCacheStructureCible.get(strId);
	    	
	    	for (DroitCibleStructure droitCibleStructure : droitsSurCible) {
	    		if (!listeStructures.contains(droitCibleStructure.getStructureCible())) {
	    			listeStructures.add(droitCibleStructure.getStructureCible());
	    		}
			}
    	
    	}

    	trierListeStructure(listeStructures);
    	
    	return listeStructures;
    }
    
    protected List<IStructure> getAllUtilisationStructuresOnFonction(String strId) {
    	
    	List<IStructure> listeStructures = new ArrayList<IStructure>();
    	
    	if (!autorisationsOnFonctionCacheStructureCible.isEmpty()) {
    	
	    	List<DroitCibleStructure> droitsSurCible = autorisationsOnFonctionCacheStructureCible.get(strId);
	    	
	    	for (DroitCibleStructure droitCibleStructure : droitsSurCible) {
	    		if (EOGdTypeDroitFonction.STR_ID_U.equals(droitCibleStructure.getTypeDroit())) {
		    		if (!listeStructures.contains(droitCibleStructure.getStructureCible())) {
		    			listeStructures.add(droitCibleStructure.getStructureCible());
		    		}
	    		}
			}
    	
    	}

    	trierListeStructure(listeStructures);
    	
    	return listeStructures;
    }

	private void trierListeStructure(List<IStructure> listeStructures) {
    	StructureTri structureTri = new StructureTri();
    	structureTri.trierParLibelleLong(listeStructures);
	}
    
    
    
    protected boolean hasDroitConnaissanceOnFonction(String strId) {
    	return hasDroitOnFonction(strId, Arrays.asList(EOGdTypeDroitFonction.STR_ID_K, EOGdTypeDroitFonction.STR_ID_U));
    }
    
    protected boolean hasDroitUtilisationOnFonction(String strId) {    	
    	return hasDroitOnFonction(strId, Arrays.asList(EOGdTypeDroitFonction.STR_ID_U));
    }
    
    private boolean hasDroitOnFonction(String strId, List<String> typeDroitFonctions) {
    	List<DroitCibleStructure> droitsSurCible = autorisationsOnFonctionCacheStructureCible.get(strId);
    	boolean hasDroit = false;
    	if (droitsSurCible != null) {
	    	for (DroitCibleStructure droitCible : droitsSurCible) {
				if (typeDroitFonctions.contains(droitCible.getTypeDroit())) {
					hasDroit = true;
				}
			}
    	} else {
    		DroitCible droitParDefaut = autorisationsOnFonctionCacheParDefaut.get(strId);
    		hasDroit = typeDroitFonctions.contains(droitParDefaut.getTypeDroit());
    	}
    	return hasDroit;
    }
    
    
    protected void createOrAddDroitCible(Map<String, List<DroitCibleIndividu>> droitsCache, String fonIdInterne, DroitCibleIndividu droitCible) {
    	if (droitsCache.containsKey(fonIdInterne)) {
    		droitsCache.get(fonIdInterne).add(droitCible);
    	} else {
    		List<DroitCibleIndividu> array = new ArrayList<DroitCibleIndividu>();
    		array.add(droitCible);
    		droitsCache.put(fonIdInterne, array);
    	}
    }
    
    protected void createOrAddDroitCible(Map<String, List<DroitCibleStructure>> droitsCache, String fonIdInterne, DroitCibleStructure droitCible) {
    	if (droitsCache.containsKey(fonIdInterne)) {
    		droitsCache.get(fonIdInterne).add(droitCible);
    	} else {
    		List<DroitCibleStructure> array = new ArrayList<DroitCibleStructure>();
    		array.add(droitCible);
    		droitsCache.put(fonIdInterne, array);
    	}
    	
    }
    
	/**
	 * Est-ce que l'utilisateur connecté est un utilisateur avec des droits de base ? (non ajouté dans l'application)
	 * @return true / false
	 */
	public boolean isBasicUser() {
		return autorisationsOnFonctionCacheParticulierIndividuCible.isEmpty() && autorisationsOnFonctionCacheStructureCible.isEmpty();
	}
	
	
	/**
	 * S'il a le droit Utilisation, alors il a le droit connaissance
	 * 
	 * @param strId id de la fonction
	 * @param personne  personne ou structure cible
	 * @return true / false
	 */
    protected boolean hasDroitConnaissanceOnFonctionEtCible(String strId, IPersonne personne) {
    	
    	boolean hasDroit = false;
    	
    	List<String> listeTypeDroit = Arrays.asList(EOGdTypeDroitFonction.STR_ID_K, EOGdTypeDroitFonction.STR_ID_U);
    	
    	if (personne.isStructure()) {
    		
    		hasDroit = hasDroitOnStructureCible(strId, personne, hasDroit, listeTypeDroit);
    		
    	}  else {
    		
    		hasDroit = hasDroitOnIndividuDansStructureCible(strId, personne, hasDroit, listeTypeDroit);
    		
			if (!hasDroit) {
				hasDroit = hasDroitOnIndividuParDefaut(strId, personne, hasDroit, listeTypeDroit);
			}
			
			if (!hasDroit) {
				hasDroit = hasDroitParticulierOnIndividu(strId, personne, hasDroit, listeTypeDroit);
			}
	        	
    	}
        return hasDroit;
    }

	private boolean hasDroitParticulierOnIndividu(String strId,
			IPersonne personne, boolean hasDroit, List<String> listeTypeDroit) {
		List<DroitCibleIndividu> droitsSurCible = autorisationsOnFonctionCacheParticulierIndividuCible.get(strId);
		
		if (droitsSurCible != null) {
			for (DroitCibleIndividu droitCibleIndividu : droitsSurCible) {
					
				if (personne.equals(droitCibleIndividu.getPersonneCible())) {
					
					if (listeTypeDroit.contains(droitCibleIndividu.getTypeDroit())) {
						hasDroit = true;
		    		}
					
				}
				
			}
			
		}
		return hasDroit;
	}

	private boolean hasDroitOnIndividuParDefaut(String strId,
			IPersonne personne, boolean hasDroit, List<String> listeTypeDroit) {
		
		DroitCibleIndividu droitParDefaut = (DroitCibleIndividu) autorisationsOnFonctionCacheParDefaut.get(strId);
		
		if (personne.equals(droitParDefaut.getPersonneCible()) && (listeTypeDroit.contains(droitParDefaut.getTypeDroit()))) {
			hasDroit = true;
		}
		
		return hasDroit;
	}

	private boolean hasDroitOnIndividuDansStructureCible(String strId,
			IPersonne personne, boolean hasDroit, List<String> listeTypeDroit) {
		
		List<IStructure> arrayAffectation = rechercherPerimetreAffectation(personne);
		
		if (CollectionUtils.isNotEmpty(arrayAffectation)) {
		
			for (IStructure structure : arrayAffectation) {
				
				List<DroitCibleStructure> droitsSurCible = autorisationsOnFonctionCacheStructureCible.get(strId);
			    
			    if (droitsSurCible != null) {
			    	for (DroitCibleStructure droitCibleStructure : droitsSurCible) {
			    		
			    		if (structure.equals(droitCibleStructure.getStructureCible())) {
	
			    			if (listeTypeDroit.contains(droitCibleStructure.getTypeDroit())) {
			    				hasDroit = true;
			        		}
							if ((!droitCibleStructure.isResponsableInclus() && structure.toResponsable().equals(personne)) 
									|| (personne.persId().equals(individuConnecte.persId()) && listeTypeDroit.contains(EOGdTypeDroitFonction.STR_ID_U))) {
								hasDroit = false;
							}
						}
			    	}	
			    }
				
			}
			
		}
		
		return hasDroit;
	}

	/**
	 * Sur Feve, on prend les affectations courantes pour savoir si telle personne du service A
	 * est habilité sur telle personne. (Pas de notion de période)
	 * @param personne
	 * @return List<IAffectation>
	 */
	protected List<IStructure> rechercherPerimetreAffectation(IPersonne personne) {
		return rechercherDansCacheAffectation((EOIndividu) personne);
	}
	
	
	/**
	 * 
	 * @param structure
	 * @return
	 */
	protected List<EOIndividu> alimenterCacheAffectation(IStructure structure) {
		
		List<EOIndividu> listeIndividu = null;
		
		if (structure != null) {
		
			if (!cacheStructures.contains(structure)) {
			
				listeIndividu = EOAffectation.getIndividuAffecteVPersonnelNonEns(ec, (EOStructure) structure);
				
				for (EOIndividu eoIndividu : listeIndividu) {
					if (cacheAffectationStructures.containsKey(eoIndividu)) {
						List<IStructure> listeAff = cacheAffectationStructures.get(eoIndividu);
						if (!listeAff.contains(structure)) {
							listeAff.add(structure);
							cacheAffectationStructures.put(eoIndividu, listeAff);
						}
					} else {
						List<IStructure> array = new ArrayList<IStructure>();
						array.add(structure);
						cacheAffectationStructures.put(eoIndividu, array);
					}
				}
				cacheStructures.add(structure);
			} 
			setStructureCourante(structure);
		}
		return listeIndividu;
	}

	private List<IStructure> rechercherDansCacheAffectation(EOIndividu personne) {
		List<IStructure> structures = new ArrayList<IStructure>();
		if (getStructureCourante() != null && cacheStructures.contains(getStructureCourante())) {
			structures = cacheAffectationStructures.get(personne);
			setStructureCourante(null);
		} else {
			if (cacheIndividus.contains(personne)) {
				structures = cacheAffectationStructures.get(personne);
			} else {
				NSArray<EOAffectation> affs = EOAffectation.affectationCourantes(ec, personne);
				for (EOAffectation eoAffectation : affs) {
					structures.add(eoAffectation.toStructure());
				}
				cacheAffectationStructures.put(personne, structures);
				cacheIndividus.add(personne);
			}
		}
		return structures;
	}

	private boolean hasDroitOnStructureCible(String strId, IPersonne personne, boolean hasDroit, List<String> listeTypeDroit) {
		List<DroitCibleStructure> droitsSurCible = autorisationsOnFonctionCacheStructureCible.get(strId);
		
		if (droitsSurCible != null) {
			for (DroitCibleStructure droitCibleStructure : droitsSurCible) {
				
				if (personne.equals(droitCibleStructure.getStructureCible())) {
					
					if (listeTypeDroit.contains(droitCibleStructure.getTypeDroit())) {
						hasDroit = true;
		    		}
					
				}
			}	
		}
		return hasDroit;
	}
	
    /**
	 * 
	 * 
	 * @param strId id de la fonction
	 * @param personne  personne ou structure cible
	 * @return true / false
	 */
	protected boolean hasDroitUtilisationOnFonctionEtCible(String strId, IPersonne personne) {
		
		List<String> listeTypeDroit = Arrays.asList(EOGdTypeDroitFonction.STR_ID_U);
		
		boolean hasDroit = false;
		
		if (personne.isStructure()) {
		
			hasDroit = hasDroitOnStructureCible(strId, personne, hasDroit, listeTypeDroit);
    		
		} else {
			
			hasDroit = hasDroitOnIndividuDansStructureCible(strId, personne, hasDroit, listeTypeDroit);
			
			if (!hasDroit) {
				hasDroit = hasDroitOnIndividuParDefaut(strId, personne, hasDroit, listeTypeDroit);
			}
			
			if (!hasDroit) {
				hasDroit = hasDroitParticulierOnIndividu(strId, personne, hasDroit, listeTypeDroit);
			}
	        
		}
        return hasDroit;
    }

	public IStructure getStructureCourante() {
		return structureCourante;
	}

	public void setStructureCourante(IStructure structureCourante) {
		this.structureCourante = structureCourante;
	}

	public EOIndividu getIndividuConnecte() {
		return individuConnecte;
	}

	public void setIndividuConnecte(EOIndividu individuConnecte) {
		this.individuConnecte = individuConnecte;
	}
	
	
	
	

}
