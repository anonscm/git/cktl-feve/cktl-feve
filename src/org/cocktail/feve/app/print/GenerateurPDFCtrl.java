package org.cocktail.feve.app.print;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.feve.app.Application;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.appserver.WOActionResults;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;

/**
 * Controleur permettant de gerer la génération des pdf.
 * 
 * @author etienne
 * 
 */
public class GenerateurPDFCtrl {

	private CktlAbstractReporter reporter;
	private IJrxmlReportListener listener = new ReporterAjaxProgress(100);
	
	private String reportFilename = "FicheDePoste.pdf";

	private String xml;

	private String defaultJasperFilePath;
	private String userJasperFilePath;
	private String evaluationJasperFilePath;


	/**
	 * Constructeur.
	 */
	public GenerateurPDFCtrl(String xml) {
		this.xml = xml;
		System.err.println(xml);
		defaultJasperFilePath = "report/Export/evaluation.jasper";
		userJasperFilePath = "report/Export/userEvaluation.jasper";
	}


	public WOActionResults genererCompteRenduEvaluation() throws IOException {
		
		//récupération du fichier jasper
		String jasperFilename = "Reports/evaluation/evaluation.jasper";
		
		String pathJasperEvaluation;
		pathJasperEvaluation = pathForJasper(jasperFilename);
		
		// tester si variable org.cocktail.appli.reports.local.location est renseigné et si le fichier existe
		String pathJasperEtablissement = Application.application().config().stringForKey("org.cocktail.feve.reports.local.location");
		CktlLog.log("valeur org.cocktail.feve.reports.local.location:"+pathJasperEtablissement);
		if (pathJasperEtablissement != null) {
			File f = new File(pathJasperEtablissement + "/evaluation/evaluation.jasper");
			if ( f.exists() ) {				
				pathJasperEvaluation = f.getAbsolutePath();				
			}
		}
		
		
		CktlLog.log("Path Jasper:"+pathJasperEvaluation);
		String recordPath = "/FevFicheEvaluation";
		JrxmlReporterWithXmlDataSource jr = null;
		//System.err.println(pathJasperEvaluation);
		//genération du PDF
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression evaluation", xmlFileStream, recordPath, pathJasperEvaluation,
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);	
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		reporter = jr;
		return null;
		
		
	}
	
	
	public WOActionResults genererFicheDePoste() throws IOException {
		
		//récupération du fichier jasper
		String jasperFilename = "Reports/FicheDePoste/FicheDePoste.jasper";
		
		String pathJasperFicheDePoste;
		pathJasperFicheDePoste = pathForJasper(jasperFilename);
		
		// tester si variable org.cocktail.appli.reports.local.location est renseigné et si le fichier existe
		String pathJasperEtablissement = Application.application().config().stringForKey("org.cocktail.feve.reports.local.location");
		CktlLog.log("valeur org.cocktail.feve.reports.local.location:"+pathJasperEtablissement);
		if (pathJasperEtablissement != null) {
			File f = new File(pathJasperEtablissement + "/FicheDePoste/FicheDePoste.jasper");
			if ( f.exists() ) {
				pathJasperFicheDePoste = f.getAbsolutePath();
			}
		}
		
		CktlLog.log("Path Jasper:"+pathJasperFicheDePoste);
		String recordPath = "/FevFicheDePoste";
		JrxmlReporterWithXmlDataSource jr = null;
		//System.err.println(pathJasperFicheDePoste);
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression de la fiche", xmlFileStream, recordPath, pathJasperFicheDePoste,
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);	
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		reporter = jr;
		return null;
	}
	

	protected String pathForJasper(String reportPath) {
		ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
		URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
		return url.getFile();
	}
	
	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public String getReportFilename() {
		return reportFilename;
	}

	public void setReporter(JrxmlReporterWithXmlDataSource reporter) {
		this.reporter = reporter;
	}
	
	public IJrxmlReportListener getListener() {
		return listener;
	}

	public static class ReporterAjaxProgress extends	CktlAbstractReporterAjaxProgress implements IJrxmlReportListener {

		public ReporterAjaxProgress(int maximum) {
			super(maximum);
		}
	}
	
	
	
	
}
