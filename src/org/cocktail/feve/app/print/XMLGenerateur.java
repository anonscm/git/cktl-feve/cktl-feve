package org.cocktail.feve.app.print;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.NoSuchElementException;

import org.cocktail.feve.app.Application;
import org.cocktail.feve.app.FeveUserInfo;
import org.cocktail.feve.app.Session;
import org.cocktail.feve.app.finder.FinderFeve;
import org.cocktail.feve.components.evaluation.AppreciationGenerale;
import org.cocktail.feve.utils.FeveStringCtrl;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveParametres;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOIndividuFormations;
import org.cocktail.fwkcktlgrh.common.metier.EOObjectif;
import org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral;
import org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution;
import org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem;
import org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp;
import org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation;
import org.cocktail.fwkcktlgrh.common.metier.EOSituActivite;
import org.cocktail.fwkcktlgrh.common.metier.EOTplBloc;
import org.cocktail.fwkcktlgrh.common.metier.EOTplItem;
import org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur;
import org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet;
import org.cocktail.fwkcktlgrh.common.metier.EOTplRepartItemItemValeur;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_RepartCompetence;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividuGrhService;
import org.cocktail.fwkcktlpersonne.common.metier.EONiveauCompetence;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensActivites;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter.SpecChars;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Class permettant de generer le XML de données pour la génération des PDF
 * @author etienne
 *
 */
public class XMLGenerateur {


	private CktlXMLWriter xmlWriter;
	private NSMutableDictionary dico;
	private boolean isBrouillon;
	private EOEditingContext ec;
	private Session session;
	//	private EOEvaluation inEvaluation;

	public XMLGenerateur(EOEditingContext edc, Session session) {
		ec = edc;
		this.session = session;
	}


	/**
	 * Genere les données XML pour la fiche de poste.
	 * @return un string contenant les données sous format XML.
	 * @throws IOException 
	 */
	public String generateFicheDePosteXML(EOFicheDePoste inFicheDePoste, boolean showInfosPersonnelles) throws IOException {
		StringWriter sw = new StringWriter();
		xmlWriter = new CktlXMLWriter(sw);
		
		
		showInfosPersonnelles = showInfosPersonnelles && ilYaDesInformationsPersonellesDansLaFiche(inFicheDePoste);

		
		//boolean isBlocActiCompVertical = ((Boolean) dico().objectForKey(PrintConsts.XML_KEY_BLOC_ACTI_COMP_VERTICAL)).booleanValue();
		boolean isBlocActiCompVertical = false;

		
		NSMutableDictionary dicoFicheDePoste = initialiseDicoFicheDePoste(inFicheDePoste,
				showInfosPersonnelles);

		startFicheDePoste();

		exportFicheDePoste(inFicheDePoste, showInfosPersonnelles,
				isBlocActiCompVertical, dicoFicheDePoste);
		
		endFicheDePoste();
		
		if (Application.affichageXMLdansLOG()) {
			CktlLog.log(sw.toString());
		}
		return sw.toString();
	}





	private void endFicheDePoste() throws IOException {
		xmlWriter.endDocument();
		xmlWriter.close();
	}


	private void startFicheDePoste() throws IOException {
		xmlWriter.startDocument();
		xmlWriter.writeComment("Edition de la fiche de poste");
	}


	private NSMutableDictionary initialiseDicoFicheDePoste(
			EOFicheDePoste inFicheDePoste, boolean showInfosPersonnelles) {
		
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoAgent = initialiseDicoAgent(inFicheDePoste, showInfosPersonnelles);
		
		if (dicoAgent != null) {
			dico.addEntriesFromDictionary(dicoAgent);
		}

		// recup du dico de l'environnement de la fiche
		dico.addEntriesFromDictionary(FinderFeve.findDicoFicheDePosteInContext(ec(), inFicheDePoste));

		dico = new NSMutableDictionary(cleanDico(dico.immutableClone()));
		return dico;
	}


	private boolean ilYaDesInformationsPersonellesDansLaFiche(EOFicheDePoste inFicheDePoste) {
		if (inFicheDePoste.toAffectationDetailActuelle() == null) {
			return false;
		}
		return true;
	}
	
	private NSDictionary initialiseDicoAgent(EOFicheDePoste inFicheDePoste, boolean showInfosPersonnelles) {
		NSDictionary dicoAgent = null;
		
		if (showInfosPersonnelles) {
			EOAffectationDetail eoAffectationDetail = inFicheDePoste.toAffectationDetailActuelle();

			IndividuGrhService individuService = new IndividuGrhService();
			dicoAgent = individuService.findDicoAgentGepetoInContext(ec(), eoAffectationDetail);
			
		}
		return dicoAgent;
	}


	private void exportFicheDePoste(EOFicheDePoste inFicheDePoste,
			boolean showInfosPersonnelles, boolean isBlocActiCompVertical,
			NSMutableDictionary dicoFicheDePoste)
			throws IOException {
		xmlWriter.startElement("FevFicheDePoste");
		{
			
			xmlWriter.writeElement("logo_etablissement", checkString((String) dicoFicheDePoste.objectForKey("logo_etablissement")));
			xmlWriter.writeElement("code_emploi", checkString((String) inFicheDePoste.codeEmploi()));
			
			
			//TODO 30/10/2014 A quoi ca sert (etienne)
			// bloc activités et compétences REFERENS vertical ou non
			xmlWriter.writeElement(PrintConsts.XML_KEY_BLOC_ACTI_COMP_VERTICAL,
					isBlocActiCompVertical ? PrintConsts.XML_VALUE_TRUE : PrintConsts.XML_VALUE_FALSE);

			exportFicheDePosteTagInfoPerso(showInfosPersonnelles);

			exportFicheDePosteStructure();

			exportFicheDePosteLibelle(dicoFicheDePoste);

			exportFicheDePosteInfoPerso(showInfosPersonnelles, dicoFicheDePoste);

			exportFicheDePosteSignature();
			
			exportFicheDePosteDetails(inFicheDePoste, dicoFicheDePoste);

			exportFicheDePosteActivites(inFicheDePoste);

			exportFicheDePosteActiviteAutre(inFicheDePoste);

			exportFicheDePosteCompetence(inFicheDePoste);

			exportFicheDePosteCompetenceAutre(inFicheDePoste);

		}
		xmlWriter.endElement();
	}


	private void exportFicheDePosteTagInfoPerso(boolean showInfosPersonnelles)
			throws IOException {
		// indiquer dans le XML s'il faut l'encart contenant les infos personelles
		xmlWriter.writeElement(PrintConsts.XML_KEY_SHOW_INFOS_PERSONNELLES,
				showInfosPersonnelles ? PrintConsts.XML_VALUE_TRUE : PrintConsts.XML_VALUE_FALSE);
	}


	private void exportFicheDePosteStructure() throws IOException {
		EOStructure structureRacine = EOStructure.findRacineInContext(ec());
		if (structureRacine != null && !StringCtrl.isEmpty(structureRacine.llStructure())) {
			xmlWriter.writeElement(PrintConsts.XML_KEY_ETABLISSEMENT, structureRacine.llStructure());
		}
	}


	private void exportFicheDePosteLibelle(NSMutableDictionary dicoFicheDePoste)
			throws IOException {
		xmlWriter.writeElement("libelle", checkString((String) dicoFicheDePoste.objectForKey("libelle")));
	}


	private void exportFicheDePosteDetails(EOFicheDePoste inFicheDePoste,
			NSMutableDictionary dicoFicheDePoste) throws IOException {
		xmlWriter.writeElement("dcp", checkString((String) dicoFicheDePoste.objectForKey("dcp")));
		xmlWriter.writeElement("famillePro", checkString((String) dicoFicheDePoste.objectForKey("famillePro")));
		xmlWriter.writeElement("emploiType", checkString((String) dicoFicheDePoste.objectForKey("emploiType")));

		xmlWriter.writeElement("composante", checkString((String) dicoFicheDePoste.objectForKey("composante")));
		xmlWriter.writeElement("structure", checkString((String) dicoFicheDePoste.objectForKey("structure")));
		xmlWriter.writeElement("poste", checkString(inFicheDePoste.toPoste().posLibelle()));

		xmlWriter.writeElement("missionComposante", checkString((String) dicoFicheDePoste.objectForKey("missionComposante")));
		xmlWriter.writeElement("missionService", checkString((String) dicoFicheDePoste.objectForKey("missionService")));
		xmlWriter.writeElement("projetService", checkString((String) dicoFicheDePoste.objectForKey("projetService")));
		xmlWriter.writeElement("missionPoste", checkString((String) dicoFicheDePoste.objectForKey("missionPoste")));
		xmlWriter.writeElement("contexte", checkString((String) dicoFicheDePoste.objectForKey("contexte")));
	}


	private void exportFicheDePosteSignature() throws IOException {
		NSArray<EOFeveParametres> param1 = EOFeveParametres.getParametres(ec(), EOFeveParametres.KEY_FEV_IS_LIBELLE_SIGNATURE_AGENT_FICHE_DE_POSTE, null);
		NSArray<EOFeveParametres> param2 = EOFeveParametres.getParametres(ec(), EOFeveParametres.KEY_FEV_IS_LIBELLE_SIGNATURE_SUPP_AGENT_FICHE_DE_POSTE, null);
		NSArray<EOFeveParametres> param3 = EOFeveParametres.getParametres(ec(), EOFeveParametres.KEY_FEV_IS_VISA_DIRECTEUR_FICHE_DE_POSTE, null);
		
		
		xmlWriter.writeElement("textSignatureAgent", param1.get(0).paramValue());
		xmlWriter.writeElement("textSignatureNp1", param2.get(0).paramValue());
		xmlWriter.writeElement("afficherSignatureNp1", param3.get(0).paramValue());
	}


	private void exportFicheDePosteInfoPerso(boolean showInfosPersonnelles,
			NSMutableDictionary dicoFicheDePoste) throws IOException {
		if (showInfosPersonnelles) {
			xmlWriter.writeElement("identifiant", checkString((String) dicoFicheDePoste.objectForKey("identifiant")));
			xmlWriter.writeElement("nom", checkString((String) dicoFicheDePoste.objectForKey("nomUsuel")));
			xmlWriter.writeElement("prenom", checkString((String) dicoFicheDePoste.objectForKey("prenom")));
			xmlWriter.writeElement("dNaissance", checkString((String) dicoFicheDePoste.objectForKey("dNaissance")));
			xmlWriter.writeElement("statut", checkString((String) dicoFicheDePoste.objectForKey("statut")));
			xmlWriter.writeElement("corps", checkString((String) dicoFicheDePoste.objectForKey("corps")));
			xmlWriter.writeElement("grade", checkString((String) dicoFicheDePoste.objectForKey("grade")));
		}
	}


	private void exportFicheDePosteActiviteAutre(EOFicheDePoste inFicheDePoste)
			throws IOException {
		// on affiche les activites autre que s'il y en a
		if (feveUserInfo().isFicheDePosteSaisieActiviteAutre()) {
			if (inFicheDePoste.tosRepartFdpActivitesAutres().count() > 0) {
				xmlWriter.startElement("activitesAutres");
				for (int i = 0; i < inFicheDePoste.tosRepartFdpActivitesAutres().count(); i++) {
					xmlWriter.writeElement("activiteAutre", checkString(((EORepartFdpAutre) inFicheDePoste.tosRepartFdpActivitesAutres().objectAtIndex(i)).fauChampLibre()));
				}
				xmlWriter.endElement();
			}
		}
	}


	private void exportFicheDePosteCompetenceAutre(EOFicheDePoste inFicheDePoste)
			throws IOException {
		// on affiche les competences autre que s'il y en a
		if (feveUserInfo().isFicheDePosteSaisieCompetenceAutre()) {
			if (inFicheDePoste.tosRepartFdpCompetencesAutres().count() > 0) {
				xmlWriter.startElement("competencesAutres");
				for (int i = 0; i < inFicheDePoste.tosRepartFdpCompetencesAutres().count(); i++) {
					xmlWriter.writeElement("competenceAutre", checkString(((EORepartFdpAutre) inFicheDePoste.tosRepartFdpCompetencesAutres().objectAtIndex(i)).fauChampLibre()));
				}
				xmlWriter.endElement();
			}
		}
	}


	private void exportFicheDePosteCompetence(EOFicheDePoste inFicheDePoste)
			throws IOException {
		xmlWriter.startElement("competences");
		for (int i = 0; i < inFicheDePoste.tosReferensCompetences().count(); i++) {
			xmlWriter.writeElement("competence", checkString(((EOReferensCompetences) inFicheDePoste.tosReferensCompetences().objectAtIndex(i)).displayLong()));
		}
		xmlWriter.endElement();
	}


	private void exportFicheDePosteActivites(EOFicheDePoste inFicheDePoste)
			throws IOException {
		xmlWriter.startElement("activites");

		for (int i = 0; i < inFicheDePoste.tosReferensActivites().count(); i++) {
			if (checkString(((EOReferensActivites) inFicheDePoste.tosReferensActivites().objectAtIndex(i)).displayLong()) != null
					&& !checkString(((EOReferensActivites) inFicheDePoste.tosReferensActivites().objectAtIndex(i)).displayLong()).equals("")) {
				xmlWriter.writeElement("activite", checkString(((EOReferensActivites) inFicheDePoste.tosReferensActivites().objectAtIndex(i)).displayLong()));
			}
			
		}
		xmlWriter.endElement();
	}



	public String generateEvaluationXML(boolean isEmptyEvaluation,EOEvaluation evaluation) throws IOException {

		StringWriter sw = new StringWriter();
		xmlWriter = new CktlXMLWriter(sw);


		//if (isEmptyEvaluation) {
		//	dico.setObjectForKey(PrintConsts.ENDING_MESSAGE_FICHE_EVALUATION_VIERGE, PrintConsts.DICO_KEY_ENDING_MESSAGE);
		//} else {
		//	dico.setObjectForKey(PrintConsts.ENDING_MESSAGE_FICHE_EVALUATION_NON_VIERGE, PrintConsts.DICO_KEY_ENDING_MESSAGE);
		//}

		// vierge ou pas ?
		isBrouillon = isEmptyEvaluation;

		// recup du dico gepeto de l'occupant de la fiche
		dico = new NSMutableDictionary();
		IndividuGrhService individuService = new IndividuGrhService();
		NSDictionary dicoAgent = individuService.findDicoAgentGepetoInContext(ec(), evaluation);
		dico.addEntriesFromDictionary(dicoAgent);

		// recup du dico de l'environnement de la fiche
		dico.addEntriesFromDictionary(FinderFeve.findDicoEvaluationInContext(ec(), evaluation));
		dico = new NSMutableDictionary(cleanDico(dico.immutableClone()));

		xmlWriter.setEscapeSpecChars(true);
		xmlWriter.startDocument();
		xmlWriter.writeComment("Edition de la fiche d evaluation");
		
		// liste des onglets attendus pour cette évaluation
		EOEvaluationPeriode periode = evaluation.toEvaluationPeriode();
		NSArray<EOTplOnglet> eoTplOngletArray = feveSession().getEoTplFicheEvaluation().tosOnglet(periode, evaluation);

		boolean hasManagement = false;
		try {
			hasManagement = EORepartFicheBlocActivation.isActif(ec(), EOTplBloc.TPL_BLOC_MANAGEMENT_CODE, evaluation);
		}catch (NoSuchElementException e) {
			// cas particulier qui est catché
		}

		xmlWriter.startElement("FevFicheEvaluation");
		{

			xmlWriter.writeElement("logo_etablissement", checkString((String) dico.objectForKey("logo_etablissement")));
			exportFicheDePosteStructure();

			
			
			xmlWriter.writeComment("Edition de la fiche d evaluation");
			
			if (evaluation.dTenueEntretien() != null) {
				xmlWriter.writeElement("periode", "" + DateCtrl.getYear(evaluation.dTenueEntretien()));
			} else {
				xmlWriter.writeElement("periode", "" + DateCtrl.getCurrentYear());
			}
			xmlWriter.writeElement("identifiant", checkString((String) dico.objectForKey("identifiant")));
			xmlWriter.writeElement("nom", checkString((String) dico.objectForKey("nomUsuel")));
			xmlWriter.writeElement("nomFamille", checkString((String) dico.objectForKey("nomFamille")));
			xmlWriter.writeElement("prenom", checkString((String) dico.objectForKey("prenom")));
			xmlWriter.writeElement("dNaissance", checkString((String) dico.objectForKey("dNaissance")));
			xmlWriter.writeElement("statut", checkString((String) dico.objectForKey("statut")));

			xmlWriter.writeElement("corps", checkString((String) dico.objectForKey("corps")));
			xmlWriter.writeElement("grade", checkString((String) dico.objectForKey("grade")));
			xmlWriter.writeElement("echelon", checkString((String) dico.objectForKey("echelon")));
			xmlWriter.writeElement("dEchelon", checkString((String) dico.objectForKey("dEchelon")));

			xmlWriter.writeElement("population", checkString(evaluation.getLibellePopulation()));

			xmlWriter.writeElement("emploiType", checkString((String) dico.objectForKey("emplois")));
			
			xmlWriter.writeElement("structure", checkString((String) dico.objectForKey("services")));

			xmlWriter.writeElement("responsableNom", checkString((String) dico.objectForKey("responsableNom")));
			xmlWriter.writeElement("responsablePrenom", checkString((String) dico.objectForKey("responsablePrenom")));

			xmlWriter.writeElement("responsableCorps", checkString((String) dico.objectForKey("responsableCorps")));
			xmlWriter.writeElement("responsableGrade", checkString((String) dico.objectForKey("responsableGrade")));
			
			xmlWriter.writeElement("responsableStructure", checkString((String) dico.objectForKey("responsableStructure")));
			
			xmlWriter.writeElement("responsableFonction", checkString((String) dico.objectForKey("responsableFonction")));
			
			
			
			
			
			xmlWriter.writeElement("cotationPartF", checkString((String) dico.objectForKey("cotationPartF")));
			


			NSArray fdPs = evaluation.tosLastFicheDePoste();

			if (fdPs.count() > 0) {
				// on choisit la dernière fiche
				EOFicheDePoste fiche = (EOFicheDePoste) fdPs.objectAtIndex(0);				
				NSMutableDictionary dicoFiche = new NSMutableDictionary();
				// recup du dico de l'environnement de la fiche
				dicoFiche.addEntriesFromDictionary(FinderFeve.findDicoFicheDePosteInContext(ec(), fiche));

				xmlWriter.writeElement("fonctionConduiteProjet", checkString((String) dicoFiche.objectForKey("fonctionConduiteProjet")));
				xmlWriter.writeElement("fonctionEncadrement", checkString((String) dicoFiche.objectForKey("fonctionEncadrement")));
				xmlWriter.writeElement("nbAgentsEncadres", checkString(dicoFiche.objectForKey("nbAgentsEncadres").toString()));
				xmlWriter.writeElement("nbAgentsEncadresCategorieA", checkString(dicoFiche.objectForKey("nbAgentsEncadresCategorieA").toString()));
				xmlWriter.writeElement("nbAgentsEncadresCategorieB", checkString(dicoFiche.objectForKey("nbAgentsEncadresCategorieB").toString()));
				xmlWriter.writeElement("nbAgentsEncadresCategorieC", checkString(dicoFiche.objectForKey("nbAgentsEncadresCategorieC").toString()));
				
				if ((fiche.toAffectationDetailActuelle() != null) 
						&& (fiche.toAffectationDetailActuelle().toAffectation() != null) 
						&& (fiche.toAffectationDetailActuelle().toAffectation().numQuotAffectation() != null)) {
					xmlWriter.writeElement("quotite", ""+fiche.toAffectationDetailActuelle().toAffectation().numQuotAffectation()+"%");
				} else {
					xmlWriter.writeElement("quotite", "");
				}

				xmlWriter.writeElement("intitulePoste",checkString(fiche.toPoste().posLibelle()));	
				xmlWriter.writeElement("missionPoste",checkString(fiche.fdpMissionPoste()));
				xmlWriter.writeElement("fdpddebut",checkString(DateCtrl.dateToString(fiche.fdpDDebut())));
			} else {
				xmlWriter.writeElement("fonctionConduiteProjet", "N");
				xmlWriter.writeElement("fonctionEncadrement", "N");
				xmlWriter.writeElement("nbAgentsEncadres", "0");
				xmlWriter.writeElement("nbAgentsEncadresCategorieA", "0");
				xmlWriter.writeElement("nbAgentsEncadresCategorieB", "0");
				xmlWriter.writeElement("nbAgentsEncadresCategorieC", "0");
				xmlWriter.writeElement("quotite", "");
				xmlWriter.writeElement("intitulePoste","");	
				xmlWriter.writeElement("missionPoste","");
				xmlWriter.writeElement("fdpddebut","");
				
			}
			
			// page 1 : objectifs precedents
			EOEvaluation lEvaluationEnCours = (EOEvaluation) dico.objectForKey("lEvaluationEnCours");
			EOEvaluation lEvaluationPrecedente = (EOEvaluation) dico.objectForKey("lEvaluationPrecedente");
			xmlWriter.startElement("objectifsPrecedents");
			if (lEvaluationPrecedente != null && lEvaluationPrecedente.tosObjectif() != null) {
				xmlWriter.writeElement("periodePrecedente", lEvaluationEnCours.toEvaluationPeriode().strAnneeDebutAnneeFin());
				for (int i = 0; i < lEvaluationPrecedente.tosObjectif().count(); i++) {
					EOObjectif objectif = (EOObjectif) lEvaluationPrecedente.tosObjectif().objectAtIndex(i);
					xmlWriter.startElement("objectifPrecedent");
					{
						xmlWriter.writeElement("objectif", false ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objObjectif()));
						xmlWriter.writeElement("moyen", false ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objMoyen()));
						xmlWriter.writeElement("mesure", false ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objMesure()));
						xmlWriter.writeElement("resultats", false ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objResultat()));
						xmlWriter.writeElement("observation", false ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objObservation()));
					}
					xmlWriter.endElement();
				}
			} else {
				// indiquer qu'il n'y a pas de periode precedent
				xmlWriter.writeElement("periodePrecedente", "<inconnue>");
			}
			xmlWriter.endElement();

			// page 2 : objectifs a venir
			xmlWriter.startElement("objectifsSuivants");
			if (lEvaluationEnCours != null && lEvaluationEnCours.tosObjectif() != null) {
				xmlWriter.writeElement("periodeSuivante", lEvaluationEnCours.toEvaluationPeriode().toNextPeriode() != null ?
						lEvaluationEnCours.toEvaluationPeriode().toNextPeriode().strAnneeDebutAnneeFin() : "<inconnue>");
				for (int i = 0; i < lEvaluationEnCours.tosObjectif().count(); i++) {
					EOObjectif objectif = (EOObjectif) lEvaluationEnCours.tosObjectif().objectAtIndex(i);
					xmlWriter.startElement("objectifSuivant");
					{
						xmlWriter.writeElement("objectif", isBrouillon ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objObjectif()));
						xmlWriter.writeElement("moyen", isBrouillon ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objMoyen()));
						xmlWriter.writeElement("mesure", isBrouillon ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objMesure()));
						xmlWriter.writeElement("observation", isBrouillon ? PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT : checkString(objectif.objObservation()));
					}
					xmlWriter.endElement();
				}
			}
			xmlWriter.endElement();

			// page 3 : situations d activites

			// est-ce un onglet à afficher ?
			if (feveSession().getEoTplOngletSituationActivite() != null
					&& eoTplOngletArray.containsObject(feveSession().getEoTplOngletSituationActivite())) {
				xmlWriter.startElement("situations");
				if (lEvaluationEnCours.tosSituActivite() != null) {
					for (int i = 0; i < lEvaluationEnCours.tosSituActivite().count(); i++) {
						if (lEvaluationEnCours.tosSituActivite().objectAtIndex(i).eva_type() != null) {
							String item = "";
							switch (lEvaluationEnCours.tosSituActivite().objectAtIndex(i).eva_type().intValue()) {
							case 1:
								item = "COMPETPROF";
								break;
							case 2:
								item = "CONTRIB";
								break;
							case 3:
								item = "CAPACIT";
								break;
							case 4:
								item = "ENCADCOND";
								break;
							case 5:
								item = "ACQUIS";
								break;
							}

							Hashtable<String, String> paramsItem = new Hashtable<String, String>();
							paramsItem.put("code", item);
							xmlWriter.startElement("situation", paramsItem);
							{
								EOSituActivite situation = (EOSituActivite) lEvaluationEnCours.tosSituActivite().objectAtIndex(i);
								if (isBrouillon || ("ENCADCOND".equals(item) && !hasManagement)) {
									xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT);
								} else {
									xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, checkString(situation.sacSituation()));
								}
							}
							xmlWriter.endElement();
						}
					}
				}
				xmlWriter.endElement();
			}

			// page 4 : competences + proposition
			NSArray fiches = lEvaluationEnCours.tosLastFicheDePoste();
			xmlWriter.startElement("competences");
			for (int i = 0; i < fiches.count(); i++) {
				xmlWriter.startElement("fdp");
				EOFicheDePoste fiche = (EOFicheDePoste) fiches.objectAtIndex(i);

				// la liste des competences
				NSArray repartList = fiche.tosRepartFdpComp();
				// si la configuration l'autorise, on ajoute les competences autres
				if (feveUserInfo().isFicheDePosteSaisieCompetenceAutre()) {
					repartList = repartList.arrayByAddingObjectsFromArray(fiche.tosRepartFdpCompetencesAutres());
				}

				for (int j = 0; j < repartList.count(); j++) {
					// xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, checkString((String)
					// fiche.display()));
					// xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, checkString((String)
					// fiche.toReferensEmplois().intitulemploi()));

					String intitulemploi = "";
					if (fiche.toReferensEmplois() != null) {
						intitulemploi = fiche.toReferensEmplois().intitulEmploi();
					}
					xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, checkString(intitulemploi));

					I_RepartCompetence uneRepart = (I_RepartCompetence) repartList.objectAtIndex(j);

					if (uneRepart instanceof EORepartFdpComp) {
						xmlWriter.startElement(PrintConsts.XML_KEY_COMPETENCE);
					} else {
						xmlWriter.startElement(PrintConsts.XML_KEY_COMPETENCE_AUTRE);
					}

					{
						xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, checkString((String) uneRepart.competenceDisplay()));
						EORepartNiveauComp repartNiveau = null;
						NSArray niveauList = uneRepart.tosRepartNiveauComp(
								CktlDataBus.newCondition(EORepartNiveauComp.TO_EVALUATION_KEY + "=%@", new NSArray(evaluation)));
						if (niveauList.count() > 0) {
							repartNiveau = (EORepartNiveauComp) niveauList.lastObject();
						}
						if (isBrouillon) {
							// brouillon : la liste de toutes les valeurs possible separees
							// par des "/"
							NSArray nivCompList = evaluation.toEvaluationPeriode().niveauCompetenceList();
							StringBuffer strNivCompList = new StringBuffer();
							for (int k = 0; k < nivCompList.count(); k++) {
								EONiveauCompetence nivComp = (EONiveauCompetence) nivCompList.objectAtIndex(k);
								strNivCompList.append(nivComp.ncpLibelle());
								// on separe avec des /
								if (k < nivCompList.count() - 1) {
									strNivCompList.append(" / ");
								}
							}
							xmlWriter.writeElement(PrintConsts.XML_KEY_EVALUATION_NIVEAU, strNivCompList.toString());

						} else {
							if (repartNiveau != null && repartNiveau.toNiveauCompetence() != null) {
								xmlWriter.writeElement(PrintConsts.XML_KEY_EVALUATION_NIVEAU, checkString((String) repartNiveau.toNiveauCompetence().ncpLibelle()));
							} else {
								xmlWriter.writeElement(PrintConsts.XML_KEY_EVALUATION_NIVEAU, PrintConsts.XML_VALUE_NON_RENSEIGNE);
							}
						}
					}
					xmlWriter.endElement(); // "competence" ou "competenceAutre"

				}
				xmlWriter.endElement(); // "fdp"
			}
			xmlWriter.endElement();

			// competencesProfessionnellesEtTechnicites
			xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_COMPETENCES_PROFESSIONNELLES_ET_TECHNICITE);
			feedXMLWriterForBloc(xmlWriter, evaluation, EOTplBloc.TPL_BLOC_COMPETENCES_PROFESSIONNELLES_ET_TECHNICITE_CODE, isBrouillon);
			xmlWriter.endElement();

			// "management"
			xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_MANAGEMENT);
			if (hasManagement) {
				feedXMLWriterForBloc(xmlWriter, evaluation, EOTplBloc.TPL_BLOC_MANAGEMENT_CODE, isBrouillon);
				xmlWriter.writeElement(PrintConsts.XML_KEY_EVALUATION_HAS_MANAGEMENT, PrintConsts.XML_VALUE_TRUE);
			} else {
				xmlWriter.writeElement(PrintConsts.XML_KEY_EVALUATION_HAS_MANAGEMENT, PrintConsts.XML_VALUE_FALSE);
			}
			xmlWriter.endElement(); // "management"

			// contributionALActiviteDuService
			//			xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_CONTRIBUTION_A_L_ACTIVITE_DU_SERVICE);
			//			feedXMLWriterForBloc(xmlWriter, evaluation, EOTplBloc.TPL_BLOC_CONTRIBUTION_A_L_ACTIVITE_DU_SERVICE_CODE, isBrouillon);
			//			xmlWriter.endElement(); // "contributionALActiviteDuService"

			// aualitesPersonnellesEtRelationnelles
			//			xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_QUALITES_PERSONNELLES_ET_RELATIONNELLES);
			//			feedXMLWriterForBloc(xmlWriter, evaluation, EOTplBloc.TPL_BLOC_QUALITES_PERSONNELLES_ET_RELATIONNELLES_CODE, isBrouillon);
			//			xmlWriter.endElement(); // "aualitesPersonnellesEtRelationnelles"

			// formationsSuivies
			xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_FORMATIONS_SUIVIES);

			// les formations suivies doivent apparaitre aussi dans le brouillon...

			/*
			 * if (isBrouillon) { for (int i = 0; i < 3; i++) {
			 * xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_FORMATION_SUIVIE); {
			 * xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE,
			 * PrintConsts.XML_VALUE_EVALUATION_EMPTY_FIELD);
			 * xmlWriter.writeElement(PrintConsts.XML_KEY_DEBUT,
			 * PrintConsts.XML_VALUE_EVALUATION_EMPTY_FIELD);
			 * xmlWriter.writeElement(PrintConsts.XML_KEY_FIN,
			 * PrintConsts.XML_VALUE_EVALUATION_EMPTY_FIELD);
			 * xmlWriter.writeElement(PrintConsts.XML_KEY_DUREE,
			 * PrintConsts.XML_VALUE_EVALUATION_EMPTY_FIELD);
			 * xmlWriter.writeElement(PrintConsts.XML_KEY_TYPE_UNITE_TEMPS,
			 * PrintConsts.XML_VALUE_EVALUATION_EMPTY_FIELD); } xmlWriter.endElement(); //
			 * "formationsSuivie" } } else {
			 */
			NSArray<EOIndividuFormations> formationList = EOIndividuFormations.findRecordsInContext(
					ec(), evaluation.toIndividu());
			// classement chronologique
			formationList = CktlSort.sortedArray(
					formationList, EOIndividuFormations.D_DEB_FORMATION_KEY);
			for (int i = 0; i < formationList.count(); i++) {
				EOIndividuFormations formationItem = formationList.objectAtIndex(i);
				 
				if (formationItem.dDebFormation()!=null)
				{
					// on ne prends que les formations sur les 2 dernières années dans les éditions
					NSTimestamp date2ans = DateCtrl.now().timestampByAddingGregorianUnits(-2, 0, 0, 0, 0, 0);
					if (formationItem.dDebFormation().after(date2ans))
					{
						xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_FORMATION_SUIVIE);
						{
							xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, formationItem.libelle() != null ? checkString(formationItem.libelle()) : "");
							xmlWriter.writeElement(PrintConsts.XML_KEY_DEBUT, DateCtrl.dateToString(formationItem.dDebFormation()));
							xmlWriter.writeElement(PrintConsts.XML_KEY_FIN, formationItem.dFinFormation() != null ? checkString(DateCtrl.dateToString(formationItem.dFinFormation())) : "");
							xmlWriter.writeElement(PrintConsts.XML_KEY_DUREE, !StringCtrl.isEmpty(formationItem.duree()) ? formationItem.duree() : "");
							xmlWriter.writeElement(PrintConsts.XML_KEY_TYPE_UNITE_TEMPS, formationItem.toTypeUniteTemps() != null ? formationItem.toTypeUniteTemps().libelle() : "");
						}
					xmlWriter.endElement(); // "formationsSuivie"
					}
				}
			}
			// }
			xmlWriter.endElement(); // "formationsSuivies"
			/*
			// formationsSouhaitees
			xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_FORMATIONS_SOUHAITEES);

			if (isBrouillon) {
				for (int i = 0; i < 3; i++) {
					xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_FORMATION_SOUHAITEE);
					{
						xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, PrintConsts.XML_VALUE_EVALUATION_EMPTY_FIELD);
					}
					xmlWriter.endElement(); // "formationSouhaitee"
				}
			} else {

				// gestion dynamique
				NSArray<EORepartFormationSouhaitee> array = evaluation.tosRepartFormationSouhaitee();

				for (EORepartFormationSouhaitee repart : array) {
					xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_FORMATION_SOUHAITEE);
					{
						String libelle = repart.libelle();
						xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, libelle != null ? checkString(libelle) : "");
					}
					xmlWriter.endElement(); // "formationSouhaitee"
				}

				// gestion ancienne en champ libre
				EORepartFicheItem repartFormationSouhaitee = EORepartFicheItem.findRecordForItemCodeInContext(
						ec(), EOTplItem.CODE_FORMATIONS_SOUHAITEES, evaluation);

				if (repartFormationSouhaitee != null && !StringCtrl.isEmpty(repartFormationSouhaitee.rfiValeurLibre())) {
					xmlWriter.startElement(PrintConsts.XML_ELEMENT_EVALUATION_FORMATION_SOUHAITEE);
					{
						String libelle = repartFormationSouhaitee.rfiValeurLibre();
						xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, libelle != null ? checkString(libelle) : "");
					}
					xmlWriter.endElement(); // "formationSouhaitee"
				}

			}

			xmlWriter.endElement(); // "formationsSouhaitees"

			// competences annexes
			xmlWriter.startElement("competencesAnnexes");
			if (isBrouillon) {
				xmlWriter.writeElement("hasCompetencesAnnexes", PrintConsts.XML_VALUE_TRUE);
				for (int i = 0; i < 3; i++) {
					xmlWriter.startElement("competenceAnnexe");
					{
						xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, PrintConsts.XML_VALUE_EVALUATION_EMPTY_FIELD);
					}
					xmlWriter.endElement();
				}
			} else {
				NSArray<EORepartEvaNouvelleComp> competencesAnnexes = (NSArray<EORepartEvaNouvelleComp>) evaluation.tosRepartEvaNouvelleComp();
				if (competencesAnnexes.count() > 0) {
					xmlWriter.writeElement("hasCompetencesAnnexes", PrintConsts.XML_VALUE_TRUE);
					for (int i = 0; i < competencesAnnexes.count(); i++) {
						EORepartEvaNouvelleComp uneRepart = competencesAnnexes.objectAtIndex(i);
						xmlWriter.startElement("competenceAnnexe");
						{
							xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, checkString((String) uneRepart.toReferensCompetences().displayLong()));
						}
						xmlWriter.endElement();
					}
				} else {
					xmlWriter.writeElement("hasCompetencesAnnexes", PrintConsts.XML_VALUE_FALSE);
				}
			}
			xmlWriter.endElement(); // "competencesAnnexes"
			 */
			if (isBrouillon) {
				xmlWriter.writeElement(PrintConsts.XML_KEY_SHOW_EVOLUTION_AGENT, PrintConsts.XML_VALUE_FALSE);
				xmlWriter.writeElement(PrintConsts.XML_KEY_SHOW_SIGNATURES, PrintConsts.XML_VALUE_FALSE);
				xmlWriter.writeElement("competencesChamplibre", PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT);
				xmlWriter.writeElement("evoluPropo", PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT);
				xmlWriter.writeElement("evoluEnvis", PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT);
			} else {

				if (evaluation.dTenueEntretien() != null) {
					String strDateTenueEntretien = DateCtrl.dateToString(evaluation.dTenueEntretien());
					xmlWriter.writeElement(PrintConsts.XML_KEY_DATE_TENUE_ENTRETIEN, strDateTenueEntretien);
				}

				xmlWriter.writeElement(PrintConsts.XML_KEY_SHOW_EVOLUTION_AGENT, PrintConsts.XML_VALUE_TRUE);
				xmlWriter.writeElement(PrintConsts.XML_KEY_SHOW_SIGNATURES, PrintConsts.XML_VALUE_TRUE);

				String champLibre = lEvaluationEnCours.evaChampLibre();
				if (!StringCtrl.isEmpty(champLibre)) {
					xmlWriter.writeElement("competencesChamplibre", checkString(lEvaluationEnCours.evaChampLibre()));
				}

				xmlWriter.writeElement("evoluPropo", checkString(lEvaluationEnCours.evaEvolutionPropo()));
				xmlWriter.writeElement("evoluEnvis", checkString(lEvaluationEnCours.evaEvolutionEnvis()));

			}

			if (!isBrouillon) {

				NSArray<EORepartAppreciationGeneral> eoRepartAppreciationGenerals = lEvaluationEnCours.tosRepartAppreciationGenerals();
				EORepartAppreciationGeneral eoRepartAppreciationGeneral;

				for (int i = 0; i < eoRepartAppreciationGenerals.count(); i++) {
					eoRepartAppreciationGeneral = eoRepartAppreciationGenerals.get(i);
					if (eoRepartAppreciationGeneral.toTplItem() != null && eoRepartAppreciationGeneral.toTplItemValeur() != null) {


						if (AppreciationGenerale.COMPETENCE_PRO.equals(eoRepartAppreciationGeneral.toTplItem().titCode())) {
							xmlWriter.writeElement("appreciationGeneraleCompetencesPro", checkString(eoRepartAppreciationGeneral.toTplItemValeur().tivLibelle()));	
						}
						if (AppreciationGenerale.CONTRIBUTION_ACTIVITE.equals(eoRepartAppreciationGeneral.toTplItem().titCode())) {
							xmlWriter.writeElement("appreciationGeneraleContribution", checkString(eoRepartAppreciationGeneral.toTplItemValeur().tivLibelle()));	
						}

						if (AppreciationGenerale.CAPACITE_PRO.equals(eoRepartAppreciationGeneral.toTplItem().titCode())) {
							xmlWriter.writeElement("appreciationGeneraleCapacitesPro", checkString(eoRepartAppreciationGeneral.toTplItemValeur().tivLibelle()));	
						}

						if (AppreciationGenerale.APTITUDE_ENCADREMENT.equals(eoRepartAppreciationGeneral.toTplItem().titCode())) {
							xmlWriter.writeElement("appreciationGeneraleAptitudeEncadrement", checkString(eoRepartAppreciationGeneral.toTplItemValeur().tivLibelle()));	
						}
					}
				}
			}

			if (!isBrouillon) {

				if (lEvaluationEnCours.formationPrevus() != null) {
					if ("O".equals(lEvaluationEnCours.formationPrevus())) {
						xmlWriter.writeElement("competencesAAcquerirActionRapide", "oui");
					} else {
						xmlWriter.writeElement("competencesAAcquerirActionRapide", "non");
					}
				} else {
					xmlWriter.writeElement("competencesAAcquerirActionRapide", "oui/non");
				}
				
				
				NSArray<EORepartCompetencePoste> competencePostes = lEvaluationEnCours.tosRepartCompetencePostes();
				if (competencePostes.count() > 0 || lEvaluationEnCours.competencePosteCommentaire() != null) {
					xmlWriter.startElement("competencesAAcquerirPoste");			
				}



				for (int i = 0; i < competencePostes.count(); i++) {
					xmlWriter.startElement("competenceAAcquerirPoste");	
					xmlWriter.writeElement("libelle", checkString(competencePostes.get(i).libelle()));   
					xmlWriter.writeElement("periode", checkString(competencePostes.get(i).periode()));
					xmlWriter.endElement(); // "competenceAAcquerirPoste"		
				}	

				xmlWriter.writeElement("competencesAAcquerirPosteChamplibre", checkString(lEvaluationEnCours.competencePosteCommentaire()));

				if (competencePostes.count() > 0 || lEvaluationEnCours.competencePosteCommentaire() != null) {
					xmlWriter.endElement(); // "competencesAAcquerirPoste"		
				}

				NSArray<EORepartCompetenceEvolution> competenceEvolutions = lEvaluationEnCours.tosRepartCompetenceEvolutions();

				if (competenceEvolutions.count() > 0 || lEvaluationEnCours.competenceEvolutionCommentaire() != null) {
					xmlWriter.startElement("competencesAAcquerirEvolution");		
				}

				for (int i = 0; i < competenceEvolutions.count(); i++) {
					xmlWriter.startElement("competenceAAcquerirEvolution");		
					xmlWriter.writeElement("libelle", checkString(competenceEvolutions.get(i).libelle()));   
					xmlWriter.writeElement("periode", checkString(competenceEvolutions.get(i).periode()));
					xmlWriter.endElement(); // "competenceAAcquerirEvolution"
				}	

				xmlWriter.writeElement("competencesAAcquerirEvolutionChamplibre", checkString(lEvaluationEnCours.competenceEvolutionCommentaire()));

				if (competenceEvolutions.count() > 0 || lEvaluationEnCours.competenceEvolutionCommentaire() != null) {
					xmlWriter.endElement(); // "competencesAAcquerirEvolution"
				}

				NSArray<EORepartPerspectiveFormation> perspectiveFormations = lEvaluationEnCours.tosRepartPerspectiveFormations();

				if (perspectiveFormations.count() > 0 || lEvaluationEnCours.competenceFormationCommentaire() != null) {
					xmlWriter.startElement("perspectivesFormation");
				}

				for (int i = 0; i < perspectiveFormations.count(); i++) {
					xmlWriter.startElement("perspectiveFormation");
					xmlWriter.writeElement("libelle", checkString(perspectiveFormations.get(i).libelle()));   
					xmlWriter.writeElement("periode", checkString(perspectiveFormations.get(i).echeance()));
					xmlWriter.endElement(); // "perspectiveFormation"
				}	

				xmlWriter.writeElement("perspectivesFormationChamplibre", checkString(lEvaluationEnCours.competenceFormationCommentaire()));

				if (perspectiveFormations.count() > 0 || lEvaluationEnCours.competenceFormationCommentaire() != null) {
					xmlWriter.endElement(); // "perspectivesFormation"
				}
			} else {
				xmlWriter.writeElement("competencesAAcquerirActionRapide", "oui/non");				
			}

			xmlWriter.startElement("utilisationDIF");

			xmlWriter.writeElement("soldeDIF", (String) dico.objectForKey("balanceDif"));

			if (!isBrouillon) {
				if (lEvaluationEnCours.modiliserDif() != null) {
					if ("O".equals(lEvaluationEnCours.modiliserDif())) {
						xmlWriter.writeElement("mobilisationDIFAgent", "oui");
					} else {
						xmlWriter.writeElement("mobilisationDIFAgent", "non");	
					}			
				} else { 
					xmlWriter.writeElement("mobilisationDIFAgent", "non");
				}
			} else {			
				xmlWriter.writeElement("mobilisationDIFAgent", "oui/non"); 
			}
			xmlWriter.endElement(); // "utilisationDIF"
			// notice de promotion

			if (lEvaluationEnCours.tosEvaluationNoticePromotion().count() > 0) {
				if (!isBrouillon) {

					EOEvaluationNoticePromotion eoPromo = (EOEvaluationNoticePromotion) lEvaluationEnCours.tosEvaluationNoticePromotion().objectAtIndex(0);

					String population = lEvaluationEnCours.getLibellePopulation();
					if (population.trim().length() > 0)	{
						xmlWriter.startElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS);
						{	
							xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_POPULATION, checkString(population));

							Integer reductionEchelon = eoPromo.enpReductionEchelon();
							if (reductionEchelon != null) {
								xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_REDUCTION_ECHELON, checkString(eoPromo.enpReductionEchelonLibelle()));
								if (EOEvaluationNoticePromotion.isAMotiver(reductionEchelon)) {
									xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_REDUCTION_ECHELON_A_MOTIVER, PrintConsts.XML_VALUE_TRUE);
									if (!StringCtrl.isEmpty(eoPromo.enpReductionEchelonRefusMotif())) {
										xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_REDUCTION_ECHELON_MOTIF, checkString(eoPromo.enpReductionEchelonRefusMotif()));
									}
								}
							}

							Integer promotionGrade = eoPromo.enpPromotionGrade();
							if (promotionGrade != null) {
								xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_PROMOTION_GRADE, checkString(eoPromo.enpPromotionGradeLibelle()));
								if (EOEvaluationNoticePromotion.isAMotiver(promotionGrade)) {
									xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_PROMOTION_GRADE_A_MOTIVER, PrintConsts.XML_VALUE_TRUE);
									if (!StringCtrl.isEmpty(eoPromo.enpPromotionGradeRefusMotif())) {
										xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_PROMOTION_GRADE_MOTIF, checkString(eoPromo.enpPromotionGradeRefusMotif()));
									}
								}
							}

							Integer promotionCorps = eoPromo.enpPromotionCorps();
							if (promotionCorps != null) {
								xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_PROMOTION_CORPS, checkString(eoPromo.enpPromotionCorpsLibelle()));
								if (EOEvaluationNoticePromotion.isAMotiver(promotionCorps)) {
									xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_PROMOTION_CORPS_A_MOTIVER, PrintConsts.XML_VALUE_TRUE);
									if (!StringCtrl.isEmpty(eoPromo.enpPromotionCorpsRefusMotif())) {
										xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_PROMOTION_CORPS_MOTIF, checkString(eoPromo.enpPromotionCorpsRefusMotif()));
									}
								}
							}

							String strEnpAppreciationGenerale = "";
							if (!StringCtrl.isEmpty(eoPromo.enpAppreciationGenerale())) {
								strEnpAppreciationGenerale = eoPromo.enpAppreciationGenerale();
							}
							xmlWriter.writeElement(PrintConsts.XML_ELEMENT_NOTICE_PROMOTIONS_AVIS_GENERAL, checkString(strEnpAppreciationGenerale));

						}
						xmlWriter.endElement(); // notice de promotions
					}
				}
			}




			// gestion dynamique des onglets

			for (EOTplOnglet onglet : eoTplOngletArray) {

				Hashtable<String, String> paramsOnglet = new Hashtable<String, String>();
				paramsOnglet.put("code", onglet.tonCode());

				xmlWriter.startElement("onglet", paramsOnglet);
				{

					xmlWriter.writeElement("libelle", onglet.tonLibelle());
					xmlWriter.writeElement("commentaire", checkString(onglet.tonCommentaire()));

					NSArray<EOTplBloc> eoTplBlocArray = onglet.tosTplBlocSortedByPosition(periode);

					for (EOTplBloc bloc : eoTplBlocArray) {

						Hashtable<String, String> paramsBloc = new Hashtable<String, String>();
						paramsBloc.put("code", bloc.tblCode());

						xmlWriter.startElement("bloc", paramsBloc); // bloc
						{
							xmlWriter.writeElement("libelle", bloc.tblLibelle());
							xmlWriter.writeElement("commentaire", checkString(bloc.tblCommentaire()));

							if (bloc.isBlocNatureDynamique()) {

								NSArray<EOTplItem> eoTplItemArray = bloc.tosTplItemSorted();

								for (EOTplItem item : eoTplItemArray) {

									// seule la gestion des champs libres et statiques est faite
									if (item.isChampLibre() || item.isTexteStatique()) {

										Hashtable<String, String> paramsItem = new Hashtable<String, String>();
										paramsItem.put("code", checkString(item.titCode()));

										xmlWriter.startElement("item", paramsItem);
										{

											xmlWriter.writeElement("libelle", "");

											if (item.isChampLibre()) {
												if (!isBrouillon) {
													xmlWriter.writeElement("commentaire", checkString(item.titCommentaire()));
													xmlWriter.writeElement("valeur", checkString(item.getStrChampLibre(evaluation)));
												} 
											} else {
												// cas particulier pour les textes statiques : le
												// libelle est
												// le commentaire et la valeur est le commentaire
												if (!isBrouillon) {													
													xmlWriter.writeElement("commentaire", checkString(item.titLibelle()));
													xmlWriter.writeElement("valeur", checkString(item.titCommentaire()));
												}
											}
											if (isBrouillon) {
												xmlWriter.writeElement("commentaire", PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT);
												xmlWriter.writeElement("valeur", PrintConsts.XML_VALUE_EVALUATION_EMPTY_TEXT);
											}
										}
										xmlWriter.endElement();

									}

								}

							} else {
								// blocs formation
							}

						}
						xmlWriter.endElement(); // / bloc

					}

				}
				xmlWriter.endElement(); // /onglet
			}

		}
		xmlWriter.endElement(); // "FevFicheEvaluation"
		endFicheDePoste();
		if (Application.affichageXMLdansLOG()) {
			CktlLog.log(sw.toString());
		}
		
		return sw.toString();

	}



	/**
	 * on remplace tous les "\n" par le code de saut de ligne
	 * @param unDico
	 * @return
	 */
	protected NSDictionary cleanDico(NSDictionary unDico) {
		// on remplace tous les "\n" par le code de saut de ligne
		NSMutableDictionary dico = new NSMutableDictionary();
		for (int i = 0; i < unDico.allKeys().count(); i++) {
			String aKey = (String) unDico.allKeys().objectAtIndex(i);
			// on a soit des string, soit des NSArray de String
			Object anObject = unDico.objectForKey(aKey);
			if (anObject instanceof String) {
				dico.setObjectForKey(StringCtrl.replace((String) anObject , "\n", SpecChars.br), aKey);
			} else if (anObject instanceof NSArray) {
				NSArray anArray = (NSArray) anObject;
				NSMutableArray newArray = new NSMutableArray();
				for (int j = 0; j < anArray.count(); j++) {
					if (anArray.objectAtIndex(j) instanceof String) {
						String aString = (String) anArray.objectAtIndex(j);
						newArray.addObject(StringCtrl.replace(aString , "\n", SpecChars.br));
					} else {
						newArray.addObject(anArray.objectAtIndex(j));                    
					}                  
				}
				dico.setObjectForKey(newArray.immutableClone(), aKey);
			} else {
				dico.setObjectForKey(anObject, aKey);
			}
		}

		return dico;
	}


	/**
	 * permet de remplacer les string null en string "" et les "&" par "&amp;"
	 * @param string
	 * @return
	 */
	protected String checkString(String string) {
		String str = "";
		if (string != null) {
			// sauts de ligne
			str = StringCtrl.replace(string, "<br>", "\n");
			str = StringCtrl.replace(str, "<br/>", "\n");
			str = StringCtrl.replace(str, "\n", SpecChars.br);
			// et commercial
			str = StringCtrl.replace(str, "&", SpecChars.amp);

			// worderies
			str = FeveStringCtrl.cleanWordSpecs(str);

			// pour remettre les sauts de lignes ...
			String saut = SpecChars.amp + SpecChars.br.substring(1, SpecChars.br.length());
			str = StringCtrl.replace(str, saut, SpecChars.br);
		}
		return str;
	}


	/**
	 * Completer un flux XML avec les donnees d'apres le code d'un bloc de
	 * données.
	 * 
	 * @param w
	 * @param recEvaluation
	 * @param strBlocCode
	 * @throws IOException
	 */
	private void feedXMLWriterForBloc(
			CktlXMLWriter w, EOEvaluation recEvaluation, String strBlocCode, boolean isEmptyEvaluation)
					throws IOException {
		EOTplBloc recTplBloc = EOTplBloc.fetch(
				ec, EOTplBloc.TBL_CODE_KEY, strBlocCode);
		// pour tous les items attendus sur ce bloc
		for (int i = 0; i < recTplBloc.tosTplItemSorted().count(); i++) {
			EOTplItem recTplItem = (EOTplItem) recTplBloc.tosTplItemSorted().objectAtIndex(i);

			if (!AppreciationGenerale.COMPETENCE_PRO.equals(recTplItem.titCode())
					&& !AppreciationGenerale.CONTRIBUTION_ACTIVITE.equals(recTplItem.titCode())
					&& !AppreciationGenerale.CAPACITE_PRO.equals(recTplItem.titCode())
					&& !AppreciationGenerale.APTITUDE_ENCADREMENT.equals(recTplItem.titCode())) {

				xmlWriter.startElement(PrintConsts.XML_KEY_COMPETENCE);


				xmlWriter.writeElement(PrintConsts.XML_KEY_LIBELLE, checkString(recTplItem.titLibelle()));
				if (isEmptyEvaluation) {
					// brouillon : la liste de toutes les valeurs possible separees par
					// des "/"
					EOEvaluationPeriode periode = recEvaluation.toEvaluationPeriode();
					NSArray<EOTplRepartItemItemValeur> repartList = recTplItem.tosTplRepartItemItemValeur(
							periode.epeDDebut(), periode.epeDFin());						 
					NSArray<EOTplItemValeur> tplItemValeurList;						 
					StringBuffer strItemValeurList = new StringBuffer();

					for (int j = 0; j < repartList.count(); j++) {
						tplItemValeurList=repartList.get(j).toTplItemValeur();		
						for (int k = 0; k < tplItemValeurList.count(); k++) {
							EOTplItemValeur tplItemValeur = (EOTplItemValeur) tplItemValeurList.objectAtIndex(k);
							strItemValeurList.append(tplItemValeur.tivLibelle());
							// on separe avec des /
							if (k < tplItemValeurList.count() - 1 || j < repartList.count() - 1) {
								strItemValeurList.append(" / ");
							}
						}
					}
					xmlWriter.writeElement(PrintConsts.XML_KEY_EVALUATION_NIVEAU, strItemValeurList.toString());

				} else {
					// trouver le <code>EORepartItem</code> associe a l'evaluation
					// courante
					// EORepartFicheItem recRepartFicheItem =
					// EORepartFicheItem.findRecordInContext(ec(), recTplItem,
					// recEvaluation);
					EORepartFicheItem recRepartFicheItem = recTplItem.getRepartItemForEvaluation(recEvaluation);
					// compte rendu
					if (recRepartFicheItem != null) {
						xmlWriter.writeElement(PrintConsts.XML_KEY_EVALUATION_NIVEAU, checkString(recRepartFicheItem.toTplItemValeur().tivLibelle()));
					} else {
						xmlWriter.writeElement(PrintConsts.XML_KEY_EVALUATION_NIVEAU, PrintConsts.XML_VALUE_NON_RENSEIGNE);
					}
				}

				xmlWriter.endElement(); // "competence"
			}
		}
	}



	public final FeveUserInfo feveUserInfo() {
		return session.feveUserInfo();
	}


	public NSMutableDictionary dico() {
		return dico;
	}

	public final Session feveSession() {
		return session;
	}

	public EOEditingContext ec() {
		return ec;
	}



}
