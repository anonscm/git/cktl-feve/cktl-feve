package org.cocktail.feve.app;

/*
 * Copyright Universit� de La Rochelle 2007
 *
 * ctarade@univ-lr.fr
 *
 * Ce logiciel est un programme informatique servant � g�rer les comptes
 * informatiques des utilisateurs. 
 * 
 * Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilit� au code source et des droits de copie,
 * de modification et de redistribution accord�s par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
 * seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les conc�dants successifs.

 * A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 * associ�s au chargement,  � l'utilisation,  � la modification et/ou au
 * d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 * manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 * avertis poss�dant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
 * logiciel � leurs besoins dans des conditions permettant d'assurer la
 * s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 * � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

 * Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accept� les
 * termes.
 */

import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlacces.server.handler.JarResourceRequestHandler;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxApplication;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveParametres;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFdpActi;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFdpAutre;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFdpComp;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOMessage;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOGeneralAdaptorException;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.woinject.WOInject;

import er.extensions.appserver.ERXMessageEncoding;
import er.extensions.eof.ERXDatabaseContext;
import er.extensions.eof.ERXDatabaseContextDelegate;
import er.extensions.eof.ERXDefaultEditingContextDelegate;
import er.extensions.foundation.ERXProperties;

public class Application extends CocktailAjaxApplication {

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
		WOInject.init("org.cocktail.feve.app.Application", argv);		
	}

	public Application() {
		super();
		setAllowsConcurrentRequestHandling(false);
		setPageRefreshOnBacktrackEnabled(true);

		ERXDatabaseContextDelegate.setDefaultDelegate(new FeveDatabaseContextDelegate());
		ERXDatabaseContext.setDefaultDelegate(new FeveDatabaseContextDelegate());

		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
		WOMessage.setDefaultEncoding("UTF-8");
		WOMessage.setDefaultURLEncoding("UTF-8");
		ERXMessageEncoding.setDefaultEncoding("UTF8");
		ERXMessageEncoding.setDefaultEncodingForAllLanguages("UTF8");

		/* Remplacement du requestHandler pour les ressources statiques */
		registerRequestHandler(new JarResourceRequestHandler(), "_wr_");

		int fetchTimestampLag = application().config().intForKey("org.cocktail.feve.fetch.timestamp.lag");

		if (fetchTimestampLag > 0) {
			CktlLog.log("org.cocktail.feve.fetch.timestamp.lag:" + fetchTimestampLag);
			EOEditingContext.setDefaultFetchTimestampLag(fetchTimestampLag);			
		}
	}

	/**
	 * Execute les operations au demarrage de l'application, juste apres
	 * l'initialisation standard de l'application.
	 */
	@Override
	public void startRunning() {

		java.util.TimeZone tz = java.util.TimeZone.getTimeZone(config().stringForKey("APP_TIME_ZONE"));
		java.util.TimeZone.setDefault(tz);
		NSTimeZone.setDefault(tz);

		rawLogAppInfos();
		rawLogVersionInfos();
		rawLogModelInfos();

		if (checkCustomParams() == false) {
			System.exit(-1);
		}

		if (checkModel() == false) {
			CktlLog.log("ATTENTION, tous les modèles ne pointent pas sur la même base de données !!");
		}

		//
		if (config().booleanForKey(KEY_APP_FIX_POSITION)) {
			fixFicheDePostePosition();
		}
	}

	@Override
	public String mainModelName() {
		return "Feve";
	}

	public String configTableName() {
		return "FwkCktlWebApp_GrhumParametres";
	}

	@Override
	public String configFileName() {
		return "Feve.config";
	}

	// controle de version

	private A_CktlVersion _appCktlVersion;

	@Override
	public A_CktlVersion appCktlVersion() {
		if (_appCktlVersion == null) {
			_appCktlVersion = new Version();
		}
		return _appCktlVersion;
	}

	private A_CktlVersion _appCktlVersionDb;

	@Override
	public A_CktlVersion appCktlVersionDb() {
		if (_appCktlVersionDb == null) {
			_appCktlVersionDb = new VersionMangueDbUser();
		}
		return _appCktlVersionDb;
	}


	public boolean appShouldSendCollecte() {
		return false;
	}

	@Override
	public String[] configMandatoryKeys() {
		return new String[] {
				"APP_USE_CAS",
				APP_ADMIN_MAIL };
	}

	@Override
	public String[] configOptionalKeys() {
		return new String[] {
				"GRHUM_PHOTO",
				"MAIN_LOGO_URL",
				"MAIN_WEB_SITE_URL",
				APP_ERROR_MAIL };
	}

	// getters des variables de l'application

	private String mainLogoUrl, mainWebSiteUrl, appAdminMail, appErrorMail;
	private Boolean grhumPhoto, appUseCas;
	private NSArray<String> additionnalUrlDocumentsEvaluation, additionnalUrlDocumentsEvaluationFonctionnaire;

	public boolean grhumPhoto() {
		if (grhumPhoto == null) {
			grhumPhoto = new Boolean(config().booleanForKey("GRHUM_PHOTO"));
		}
		return grhumPhoto.booleanValue();
	}

	public String mainLogoUrl() {
		if (mainLogoUrl == null) {
			mainLogoUrl = config().stringForKey("MAIN_LOGO_URL");
		}
		return mainLogoUrl;
	}

	public String mainWebSiteUrl() {
		if (mainWebSiteUrl == null) {
			mainWebSiteUrl = config().stringForKey("MAIN_WEB_SITE_URL");
		}
		return mainWebSiteUrl;
	}

	public Boolean appUseCas() {
		if (appUseCas == null) {
			appUseCas = new Boolean(config().booleanForKey("APP_USE_CAS"));
		}
		return appUseCas;
	}

	// l'adresse email pour l'envoi des erreurs java si different de
	// APP_ADMIN_MAIL
	private final static String APP_ADMIN_MAIL = "APP_ADMIN_MAIL";

	private String appAdminMail() {
		if (appAdminMail == null) {
			appAdminMail = config().stringForKey(APP_ADMIN_MAIL);
		}
		return appAdminMail;
	}

	// l'adresse email pour l'envoi des erreurs java si different de
	// APP_ADMIN_MAIL
	private final static String APP_ERROR_MAIL = "APP_ERROR_MAIL";

	private String appErrorMail() {
		if (appErrorMail == null) {
			appErrorMail = config().stringForKey(APP_ERROR_MAIL);
		}
		return appErrorMail;
	}



	@Override
	public WOResponse handleException(Exception anException, WOContext context) {
		CktlLog.log(anException.getMessage());
		WOResponse response = null;
		if (context != null && context.hasSession()) {
			Session session = (Session) context.session();
			sendMessageErreur(anException, context, session);
			response = createResponseInContext(context);
			NSMutableDictionary formValues = new NSMutableDictionary();
			formValues.setObjectForKey(session.sessionID(), "wosid");
			String applicationExceptionUrl = context.directActionURLForActionNamed("applicationException", formValues, context.request().isSecure(), true); 
			response.appendContentString("<script>document.location.href='" + applicationExceptionUrl + "';</script>");
			cleanInvalidEOFState(anException, context);
			return response;
		} else {
			return super.handleException(anException, context);
		}
	}

	private void cleanInvalidEOFState(Exception e, WOContext ctx) {
		if (e instanceof IllegalStateException || e instanceof EOGeneralAdaptorException) {
			ctx.session().defaultEditingContext().invalidateAllObjects();
		}
	}

	private void sendMessageErreur(Exception anException, WOContext context, Session session) {
		try {
			CktlMailBus cmb = session.mailBus();
			String smtpServeur = config().stringForKey("GRHUM_HOST_MAIL");
			String destinataires = config().stringForKey("APP_ADMIN_MAIL");

			String expediteur = "feve@asso-cocktail.fr";
			
			String email = session.feveUserInfo().email();
			if (StringUtils.isNotEmpty(email)) {
				expediteur = email;
			}

			String contenu = createMessageErreur(anException, context, session);
			session.setGeneralErrorMessage(contenu);

			if (cmb != null && smtpServeur != null && smtpServeur.equals("") == false && destinataires != null && destinataires.equals("") == false) {
				String objet = "[FEVE]:Exception:[";
				objet += appVersionDateInstanceInfo() + "]";
				boolean retour = false;
				retour = cmb.sendMail(expediteur, destinataires, null, objet, contenu);
				if (!retour) {
					CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! IMPOSSIBLE d'ENVOYER le mail d'exception !!!!!!!!!!!!!!!!");
					CktlLog.log("\nMail:\n\n" + contenu);

				}
			} else {
				CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! IMPOSSIBLE d'ENVOYER le mail d'exception !!!!!!!!!!!!!!!!");
				CktlLog.log("Veuillez verifier que les parametres HOST_MAIL et ADMIN_MAIL sont bien renseignes");
				CktlLog.log("HOST_MAIL = " + smtpServeur);
				CktlLog.log("ADMIN_MAIL = " + destinataires);
				CktlLog.log("cmb = " + cmb);
				CktlLog.log("\n\n\n");
			}
		} catch (Exception e) {
			CktlLog.log("\n\n\n");
			CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! Exception durant le traitement d'une autre exception !!!!!!!!!!!!!!!!");
			CktlLog.log("Message Exception dans exception: " + e.getMessage());
			CktlLog.log("Stack Exception dans exception: " + e.getStackTrace());
			super.handleException(e, context);
			CktlLog.log("\n");
			CktlLog.log("Message Exception originale: " + anException.getMessage());
			CktlLog.log("Stack Exception dans exception: " + anException.getStackTrace());
		}

	}


	private String createMessageErreur(Exception anException, WOContext context, Session session) {
		String contenu;
		// Si c'est une erreur de config, on affiche pas tout le tsoin tsoin,
		// juste une info claire
		if (anException instanceof NSForwardException) {
			Throwable cause = ((NSForwardException) anException).originalException();
			contenu = cause != null ? cause.getLocalizedMessage() : null;
		} else {
			NSDictionary extraInfo = extraInformationForExceptionInContext(anException, context);
			contenu = "Date : " + DateCtrl.dateToString(DateCtrl.now(), "%d/%m/%Y %H:%M") + "\n";
			contenu += "OS: " + System.getProperty("os.name") + "\n";
			contenu += "Java vm version: " + System.getProperty("java.vm.version") + "\n";
			contenu += "WO version: " + ERXProperties.webObjectsVersion() + "\n\n";
			contenu += "User agent: " + context.request().headerForKey("user-agent") + "\n\n";
			contenu += "Utilisateur(Numero individu): " + session.individuConnecte().getNomAndPrenom() + "("
					+ session.individuConnecte() + ")" + "\n";

			contenu += "\n\nException : " + "\n";
			if (anException instanceof InvocationTargetException) {
				contenu += getMessage(anException, extraInfo) + "\n";
				anException = (Exception) anException.getCause();
			}
			contenu += getMessage(anException, extraInfo) + "\n";
			contenu += "\n\n";
		}
		return contenu;
	}

	protected String getMessage(Throwable e, NSDictionary extraInfo) {
		String message = "";
		if (e != null) {
			message = stackTraceToString(e, false) + "\n\n";
			message += "Info extra :\n";
			if (extraInfo != null) {
				message += NSPropertyListSerialization.stringFromPropertyList(extraInfo) + "\n\n";
			}
		}
		return message;
	}

	/**
	 * permet de recuperer la trace d'une exception au format string message + trace
	 * @param e
	 * @return
	 */
	public static String stackTraceToString(Throwable e, boolean useHtml) {
		String tagCR = "\n";
		if (useHtml) {
			tagCR = "<br>";
		}
		String stackStr = e + tagCR + tagCR;
		StackTraceElement[] stack = e.getStackTrace();
		for (int i = 0; i < stack.length; i++) {
			stackStr += (stack[i]).toString() + tagCR;
		}
		return stackStr;
	}

	// numero de version

	private String _appVersionDateInstanceInfo;

	/**
	 * Les informations sur la base de donnees : - no version - date version -
	 * connection bdd
	 */
	public String appVersionDateInstanceInfo() {
		if (_appVersionDateInstanceInfo == null) {
			_appVersionDateInstanceInfo = appCktlVersion().txtVersion() + " - ";
			// information base de donnees
			EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
			EOModel vModel = vModelGroup.modelNamed(mainModelName());
			_appVersionDateInstanceInfo += org.cocktail.fwkcktlwebapp.server.util.EOModelCtrl.bdConnexionServerId(vModel);
		}
		return _appVersionDateInstanceInfo;
	}

	//

	public String[] listeVariablesCustomParametre() {
		return new String[] {
				EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_DEBUT,
				EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_FIN,
				EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_DEBUT,
				EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_FIN,
				EOFeveParametres.KEY_FEV_FICHE_DE_POSTE_SAISIE_ACTIVITES_AUTRES,
				EOFeveParametres.KEY_FEV_FICHE_DE_POSTE_SAISIE_COMPETENCES_AUTRES,
				EOFeveParametres.KEY_FEV_EDITION_FICHE_DE_POSTE_DISPOSITION_ACT_COMP_REFERENS_VERTICAL,
				EOFeveParametres.KEY_FEV_LIBELLE_CREATION_POSTE_VALEUR_PAR_DEFAUT,
				EOFeveParametres.KEY_FEV_DUREE_MINIMUM_AFFECTATION_POUR_EVALUATION,
				EOFeveParametres.KEY_FEV_MODIFICATION_FORMATIONS_SUIVIES_DANS_FEVE };
	}

	public String customConfigTableName() {
		return EOFeveParametres.ENTITY_NAME;
	}

	private EOEditingContext paramEditingContext;

	/**
	 * EditingContext particulier qui ne doit pas etre qu'en lecture seule
	 * (donnees modifiee dans l'interface d'administration)
	 * 
	 * @return
	 */
	public EOEditingContext paramEditingContext() {
		if (paramEditingContext == null) {
			paramEditingContext = new EOEditingContext();
			paramEditingContext.setDelegate(new ERXDefaultEditingContextDelegate());		
		}
		return paramEditingContext;
	}

	// gestion des variables globales a l'application et de leur cache

	public NSTimestamp getEvaluationSaisieDDebut() {
		return DateCtrl.stringToDate(getParamValueForKey(EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_DEBUT));
	}

	public NSTimestamp getEvaluationSaisieDFin() {
		return DateCtrl.stringToDate(getParamValueForKey(EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_FIN));
	}

	public NSTimestamp getFicheLolfSaisieDDebut() {
		return DateCtrl.stringToDate(getParamValueForKey(EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_DEBUT));
	}

	public NSTimestamp getFicheLolfSaisieDFin() {
		return DateCtrl.stringToDate(getParamValueForKey(EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_FIN));
	}

	/**
	 * Indique si l'application autorise la saisie des fiches LOLF
	 * 
	 * @return
	 */
	public boolean isPeriodeLolfOuverte() {
		return (DateCtrl.isAfterEq(DateCtrl.now(), getFicheLolfSaisieDDebut()) && DateCtrl.isBeforeEq(DateCtrl.now(), getFicheLolfSaisieDFin().timestampByAddingGregorianUnits(0, 0, 0, 23, 59, 59)));
	}

	/**
	 * Indique si l'application autorise la saisie des fiches LOLF
	 * 
	 * @return
	 */
	public boolean isPeriodeEvaluationOuverte() {
		return (DateCtrl.isAfterEq(DateCtrl.now(), getEvaluationSaisieDDebut()) && DateCtrl.isBeforeEq(DateCtrl.now(), getEvaluationSaisieDFin().timestampByAddingGregorianUnits(0, 0, 0, 23, 59, 59)));
	}

	private NSMutableDictionary dicoVariables = new NSMutableDictionary();

	/**
	 * Retrouver une variable depuis le dictionnaire des variables. Si pas
	 * disponible, alors on cherche dans la table de parametres et on stocke la
	 * valeur dans le dictionnaire.
	 * 
	 * @param paramKey
	 * @return
	 */
	public String getParamValueForKey(String paramKey) {
		String value = (String) dicoVariables.valueForKey(paramKey);
		if (value == null) {
			value = customConfigStringForKey(paramKey);
			// si null, on met la chaine vide
			if (value == null) {
				value = StringCtrl.emptyString();
			}
			dicoVariables.setObjectForKey(value, paramKey);
		}
		return value;
	}

	/**
	 * Obtenir la valeur booleene d'un parametre
	 * 
	 * @param paramKey
	 * @return
	 */
	public Boolean getBooleanParamValueForKey(String paramKey) {
		return new Boolean(getParamValueForKey(paramKey).equals(AfwkGRHRecord.OUI));
	}

	/**
	 * Obtenir la valeur date d'un parametre
	 * 
	 * @param paramKey
	 * @return
	 */
	public NSTimestamp getDateParamValueForKey(String paramKey) {
		return DateCtrl.stringToDate(getParamValueForKey(paramKey));
	}

	/**
	 * Obtenir la valeur Integer d'un parametre
	 * 
	 * @param paramKey
	 * @return
	 */
	public Integer getIntegerParamValueForKey(String paramKey) {
		return new Integer(Integer.parseInt(getParamValueForKey(paramKey)));
	}

	/**
	 * RAZ du cache d'une variable ciblee
	 */
	public void clearCache(String paramKey) {
		dicoVariables.removeObjectForKey(paramKey);
	}

	private final static String KEY_APP_FIX_POSITION = "APP_FIX_POSITION";

	/**
	 * Corrige un bug qui a mis toutes les positions des association d'activite et
	 * competences a 1.
	 */
	private void fixFicheDePostePosition() {
		EOEditingContext ec = new EOEditingContext();
		ec.setDelegate(new ERXDefaultEditingContextDelegate());

		NSArray ficheList = EOFicheDePoste.fetchAll(ec);
		for (int i = 0; i < ficheList.count(); i++) {
			EOFicheDePoste fiche = (EOFicheDePoste) ficheList.objectAtIndex(i);
			// les activites
			NSArray reparList = fiche.tosRepartFdpActi();
			for (int j = 0; j < reparList.count(); j++) {
				EORepartFdpActi repart = (EORepartFdpActi) reparList.objectAtIndex(j);
				if (repart.rfaPosition() == null || repart.rfaPosition().intValue() != (j + 1)) {
					repart.setRfaPosition(new Integer(j + 1));
				}
			}
			// les activites autres
			reparList = fiche.tosRepartFdpActivitesAutres();
			for (int j = 0; j < reparList.count(); j++) {
				EORepartFdpAutre repart = (EORepartFdpAutre) reparList.objectAtIndex(j);
				if (repart.fauPosition() == null || repart.fauPosition().intValue() != (j + 1)) {
					repart.setFauPosition(new Integer(j + 1));
				}
			}
			// les competences
			reparList = fiche.tosRepartFdpComp();
			for (int j = 0; j < reparList.count(); j++) {
				EORepartFdpComp repart = (EORepartFdpComp) reparList.objectAtIndex(j);
				if (repart.rfcPosition() == null || repart.rfcPosition().intValue() != (j + 1)) {
					repart.setRfcPosition(new Integer(j + 1));
				}
			}
			// les competences autres
			reparList = fiche.tosRepartFdpCompetencesAutres();
			for (int j = 0; j < reparList.count(); j++) {
				EORepartFdpAutre repart = (EORepartFdpAutre) reparList.objectAtIndex(j);
				if (repart.fauPosition() == null || repart.fauPosition().intValue() != (j + 1)) {
					repart.setFauPosition(new Integer(j + 1));
				}
			}
		}
		try {
			UtilDb.save(ec, "");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private final static String SB_SEPARATOR = ", ";

	// variables facultatives
	public boolean checkCustomParams() {
		boolean hasMandatoryMissing = false;
		StringBuffer sbMandatoryMissing = new StringBuffer();
		Hashtable<String, String> hMandatoryFound = new Hashtable<String, String>();

		StringBuffer sb = new StringBuffer("Controle de la presence des valeurs dans "+customConfigTableName()+"\n" +
				"-------------------------------------------------------\n\n");

		if (listeVariablesCustomParametre() != null && listeVariablesCustomParametre().length > 0) {
			for (String key : listeVariablesCustomParametre()) {
				String value = customConfigStringForKey(key);
				if (value == null) {
					sbMandatoryMissing.append(key).append(SB_SEPARATOR);
					hasMandatoryMissing = true;
				} else {
					hMandatoryFound.put(key, value);
				}
			}
			// enlever le dernier separateur et ajouter le message d'erreur
			if (sbMandatoryMissing.length() > 0) {
				int sbSize = sbMandatoryMissing.length();
				sbMandatoryMissing.replace(sbSize - SB_SEPARATOR.length(), sbSize, "");
				sbMandatoryMissing.insert(0, "  > Valeurs de "+customConfigTableName()+" absents : ");
				sbMandatoryMissing.append(" - ERREUR");
			} else {
				sbMandatoryMissing.insert(0, "  > Toutes les valeurs de "+customConfigTableName()+" sont presentes - OK");
			}		
			sb.append(sbMandatoryMissing).append("\n");
			sb.append(StringCtrl.getFormatted(hMandatoryFound));
		}
		CktlLog.rawLog(sb.toString());
		return !hasMandatoryMissing;
	}

	public String customConfigStringForKey(String key) {
		EOFeveParametres record = EOFeveParametres.fetch(dataBus().editingContext(), EOFeveParametres.PARAM_KEY_KEY, key);
		String value = null;

		if (record != null) {
			value = (String) record.paramValue();
			// si la valeur est vide, alors on indique explicitement la chaine vide
			// car le paramètre existe bel et bien
			if (value == null) {
				value = StringCtrl.emptyString();
			}
		}

		return value;
	}

	/**
	 * @return le chemin du dossier des jasper en local
	 */
	public static boolean affichageXMLdansLOG() {
		return application().config().booleanForKey("org.cocktail.feve.export.xml.dans.log");
	}


}