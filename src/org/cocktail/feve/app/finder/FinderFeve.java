/*
 * Copyright Universit� de La Rochelle 2005
 *
 * ctarade@univ-lr.fr
 *
 * Ce logiciel est un programme informatique servant � g�rer les comptes
 * informatiques des utilisateurs. 
 * 
 * Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilit� au code source et des droits de copie,
 * de modification et de redistribution accord�s par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
 * seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les conc�dants successifs.

 * A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 * associ�s au chargement,  � l'utilisation,  � la modification et/ou au
 * d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 * manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 * avertis poss�dant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
 * logiciel � leurs besoins dans des conditions permettant d'assurer la
 * s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 * � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

 * Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accept� les
 * termes.
 */
package org.cocktail.feve.app.finder;

import org.apache.commons.lang3.StringUtils;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOStructureInfo;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividuGrhService;
import org.cocktail.fwkcktlgrh.common.metier.services.StructureGrhService;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author ctarade 31 mars 2005
 * 
 */
public class FinderFeve extends Finder {

	/**
	 * liste tous les champ libres associes a la fiche de poste (emploi type,
	 * mission composante, service ...)
	 */
	public static NSDictionary findDicoFicheDePosteInContext(EOEditingContext ec, EOFicheDePoste fiche) {
		NSMutableDictionary dico = new NSMutableDictionary();

		if (fiche != null) {
			// la date pour recuperer les infos des structure
			// - si la date est la fiche en cours : today
			// - si date passe, on prend la fin de la fiche
			NSTimestamp dateRef = null;
			if (fiche.fdpDFin() != null && DateCtrl.isAfter(DateCtrl.now(), fiche.fdpDFin())) {
				dateRef = fiche.fdpDFin();
			} else {
				dateRef = DateCtrl.now();
			}
			
			String cLogo = LogoEdition.cheminLogoEtablissment(ec);
			
			if (cLogo != null) {
//				w.writeElement("logo_etablissement", StringCtrl.checkString(cLogo));
				dico.setObjectForKey(StringCtrl.checkString(cLogo), "logo_etablissement");
			}

			
			

			EOStructureInfo laMissionComposante = null;
			if ( fiche.toPoste().toStructure().toComposante() != null ) {
				laMissionComposante = EOStructureInfo.findStructureInfoForStructureAndDateAndTypeInContext(
						ec,
						fiche.toPoste().toStructure().toComposante(),
						dateRef,
						EOStructureInfo.getTYPE_MISSION_COMPOSANTE()
						);
			}

			if (laMissionComposante != null && !StringCtrl.isEmpty(laMissionComposante.sinLibelle())) {
				dico.setObjectForKey(laMissionComposante.sinLibelle(), "missionComposante");
			} else {
				dico.setObjectForKey("", "missionComposante");
			}

			EOStructureInfo laMissionService = null;

			if ( fiche.toPoste().toStructure() != null ) {
				laMissionService = EOStructureInfo.findStructureInfoForStructureAndDateAndTypeInContext(
						ec,
						fiche.toPoste().toStructure(),
						dateRef,
						EOStructureInfo.getTYPE_MISSION_SERVICE()
						);
			}
			if (laMissionService != null && !StringCtrl.isEmpty(laMissionService.sinLibelle())) {
				dico.setObjectForKey(laMissionService.sinLibelle(), "missionService");
			} else {
				dico.setObjectForKey("", "missionService");
			}

			EOStructureInfo leProjetService = null;

			if ( fiche.toPoste().toStructure() != null ) {
				leProjetService = EOStructureInfo.findStructureInfoForStructureAndDateAndTypeInContext(
						ec,
						fiche.toPoste().toStructure(),
						dateRef,
						EOStructureInfo.getTYPE_PROJET_SERVICE()
						);
			}
			if (leProjetService != null && !StringCtrl.isEmpty(leProjetService.sinLibelle())) {
				dico.setObjectForKey(leProjetService.sinLibelle(), "projetService");
			} else {
				dico.setObjectForKey("", "projetService");
			}

			if (!StringCtrl.isEmpty(fiche.fdpMissionPoste())) {
				dico.setObjectForKey(fiche.fdpMissionPoste(), "missionPoste");
			} else {
				dico.setObjectForKey("", "missionPoste");
			}

			if (!StringCtrl.isEmpty(fiche.fonctionConduiteProjet())) {
				dico.setObjectForKey(fiche.fonctionConduiteProjet(),"fonctionConduiteProjet");
			} else {
				dico.setObjectForKey("N","fonctionConduiteProjet");
			}
			if (!StringCtrl.isEmpty(fiche.fonctionEncadrement())) {
				dico.setObjectForKey(fiche.fonctionEncadrement(),"fonctionEncadrement");
			} else {
				dico.setObjectForKey("N","fonctionEncadrement");
			}
			dico.setObjectForKey("0","nbAgentsEncadres");
			dico.setObjectForKey("0","nbAgentsEncadresCategorieA");
			dico.setObjectForKey("0","nbAgentsEncadresCategorieB");
			dico.setObjectForKey("0","nbAgentsEncadresCategorieC");


			if (fiche.nbAgentsEncadres() != null) {
				dico.setObjectForKey(fiche.nbAgentsEncadres(),"nbAgentsEncadres");
			}
			if (fiche.nbAgentsEncadresCategorieA() != null) {
				dico.setObjectForKey(fiche.nbAgentsEncadresCategorieA(),"nbAgentsEncadresCategorieA");
			}
			if (fiche.nbAgentsEncadresCategorieB() != null) {			    
				dico.setObjectForKey(fiche.nbAgentsEncadresCategorieB(),"nbAgentsEncadresCategorieB");
			}
			if (fiche.nbAgentsEncadresCategorieC() != null) {			    
				dico.setObjectForKey(fiche.nbAgentsEncadresCategorieC(),"nbAgentsEncadresCategorieC");
			}

			if (!StringCtrl.isEmpty(fiche.fdpContexteTravail())) {
				dico.setObjectForKey(fiche.fdpContexteTravail(), "contexte");
			} else {
				dico.setObjectForKey("", "contexte");
			}

			// composante - service
			if ( fiche.toPoste().toStructure().toComposante() != null && 
					fiche.toPoste().toStructure().toComposante().llStructure() !=null )
			{
				dico.setObjectForKey(fiche.toPoste().toStructure().toComposante().llStructure(), "composante");
			}
			else {
				dico.setObjectForKey("", "composante");
			}

			if ( fiche.toPoste().toStructure() != null ) {
				dico.setObjectForKey(fiche.toPoste().toStructure().llStructure(), "structure");
			}
			else {
				dico.setObjectForKey("", "structure");
			}

			if (fiche.toReferensEmplois() != null) {
				if (fiche.toReferensEmplois().intitulEmploi()!=null) {
					dico.setObjectForKey(fiche.toReferensEmplois().intitulEmploi(), "emploiType");
				} else {
					dico.setObjectForKey("", "emploiType");
				}
				if (fiche.toReferensEmplois().toReferensFp().intitulFp()!=null) {
					dico.setObjectForKey(fiche.toReferensEmplois().toReferensFp().intitulFp(), "famillePro");
				} else {
					dico.setObjectForKey("", "famillePro");	
				}
				if (fiche.toReferensEmplois().toReferensFp().toReferensDcp().intitulDcp()!=null) {
					dico.setObjectForKey(fiche.toReferensEmplois().toReferensFp().toReferensDcp().intitulDcp(), "dcp");
				} else {
					dico.setObjectForKey("", "dcp");	
				}
			} else {
				dico.setObjectForKey("", "emploiType");
				dico.setObjectForKey("", "famillePro");
				dico.setObjectForKey("", "dcp");

			}
			// libelle
			dico.setObjectForKey(fiche.display(), "libelle");
		}

		return dico.immutableClone();
	}

	/**
	 * le dictionnaire contenant toutes les infos pour l'impression de
	 * l'evaluation (objectifs, niveau des competences ...)
	 */
	public static NSDictionary<String, Object> findDicoEvaluationInContext(EOEditingContext ec, EOEvaluation evaluation) {
		NSMutableDictionary<String, Object> dico = new NSMutableDictionary<String, Object>();

		if (evaluation != null) {
			StructureGrhService structureGrhService = new StructureGrhService();
			
			String cLogo = LogoEdition.cheminLogoEtablissment(ec);

			if (cLogo != null) {
				//				w.writeElement("logo_etablissement", StringCtrl.checkString(cLogo));
				dico.setObjectForKey(StringCtrl.checkString(cLogo), "logo_etablissement");
			}
			
			if (evaluation.toIndividuResp() != null) {
				dico.setObjectForKey(evaluation.toIndividuResp().nomUsuel(), "responsableNom");
				dico.setObjectForKey(evaluation.toIndividuResp().prenom(), "responsablePrenom");
			}

			EOFicheDePoste ficheDePoste = ((EOFicheDePoste)evaluation.tosLastFicheDePoste().lastObject());
			if (ficheDePoste != null && ficheDePoste.cotationPartF() != null) {
				dico.setObjectForKey(((EOFicheDePoste)evaluation.tosLastFicheDePoste().lastObject()).cotationPartF().toString(), "cotationPartF");
			}

			feedDicoWithCorpsGradeResponsable(ec, evaluation, dico); 
			
			dico.setObjectForKey(evaluation.getFonctionEvaluateur(), "responsableFonction");
			
			EOStructure structureResponsable = structureGrhService.getStructureAffectationCourante(ec, evaluation.toIndividuResp(), evaluation.toEvaluationPeriode().epeDDebut(), evaluation.toEvaluationPeriode().epeDDebut());
			
			if (structureResponsable != null) {
				dico.setObjectForKey(structureResponsable.lcStructure(), "responsableStructure");
			} else {
				dico.setObjectForKey(StringUtils.EMPTY, "responsableStructure");
			}
			
			

		} else {
			dico.setObjectForKey("aucun", "responsableNom");
			dico.setObjectForKey("", "responsablePrenom");
			dico.setObjectForKey("", "responsableCorps");	
			dico.setObjectForKey("", "responsableGrade");
			dico.setObjectForKey("", "responsableFonction");
		}

		// periode

		//			dico.setObjectForKey("du " +
		//			DateCtrl.dateToString(evaluation.toEvaluationPeriode().epeDDebut()) +
		//			" au " +
		//			DateCtrl.dateToString(evaluation.toEvaluationPeriode().epeDFin()),
		//			"periode");

		dico.setObjectForKey(evaluation.toEvaluationPeriode().strAnneeDebutAnneeFin(), "periode");

		NSArray fiches = evaluation.tosLastFicheDePoste();
		/*
		 * // composantes - services NSArray services = (NSArray)
		 * fiches.valueForKey("toPoste.toStructure.llStructure"); services =
		 * NSArrayCtrl.removeDuplicate(services); String strServices = ""; for
		 * (int i = 0; i < services.count(); i++) { String unService = (String)
		 * services.objectAtIndex(i); strServices += unService; if (i !=
		 * services.count() - 1) { strServices += " ; "; } }
		 * dico.setObjectForKey(strServices, "services");
		 */

		NSArray services = evaluation.tosStructure();
		String strServices = "";
		for (int i = 0; i < services.count(); i++) {
			EOStructure unService = (EOStructure) services.objectAtIndex(i);
			strServices += unService.llStructure();
			if (i != services.count() - 1) {
				strServices += " ; ";
			}
		}

		dico.setObjectForKey(strServices, "services");

		// emplois types
		NSArray emplois = new NSArray();
		String strEmplois = "";
		for (int i = 0; i < fiches.count(); i++) {
			EOFicheDePoste uneFicheDePoste = (EOFicheDePoste) fiches.objectAtIndex(i);
			if (uneFicheDePoste.toReferensEmplois() != null) {
				strEmplois += uneFicheDePoste.toReferensEmplois().intitulEmploi();
				if (i < emplois.count() - 1) {
					strEmplois += " ; ";
				}
			}
		}
		dico.setObjectForKey(strEmplois, "emplois");

		// evaluation precedente
		EOEvaluation evaluationPrec = evaluation.toEvaluationPrecedente();
		if (evaluationPrec != null) {
			dico.setObjectForKey(evaluationPrec, "lEvaluationPrecedente");
		}

		// evaluation actuelle
		dico.setObjectForKey(evaluation, "lEvaluationEnCours");

		// evaluation suivante
		EOEvaluation evaluationSuiv = evaluation.toEvaluationSuivante();
		if (evaluationSuiv != null) {
			dico.setObjectForKey(evaluationSuiv, "lEvaluationSuivante");
		}

		return dico.immutableClone();
	}

	private static void feedDicoWithCorpsGradeResponsable(EOEditingContext ec,
			EOEvaluation evaluation, NSMutableDictionary<String, Object> dico) {
		
		NSTimestamp debut = null;
		NSTimestamp fin = null;
		debut = evaluation.toEvaluationPeriode().epeDDebut();
		fin = evaluation.toEvaluationPeriode().epeDFin();
		IndividuGrhService individuService = new IndividuGrhService();	
		
		EOCorps eoCorps;
		EOGrade eoGrade;
		
		eoCorps = individuService.getCorpsForPeriode(evaluation.toIndividuResp(), ec, debut, fin);
		eoGrade = individuService.getGradeForPeriode(evaluation.toIndividuResp(), ec, debut, fin);

		dico.setObjectForKey("", "responsableCorps");	
		dico.setObjectForKey("", "responsableGrade");
		if (eoCorps != null) {
			dico.setObjectForKey(eoCorps.llCorps(), "responsableCorps");
		}				
		if (eoGrade != null) {
			dico.setObjectForKey(eoGrade.llGrade(), "responsableGrade");
		}

	}

	/**
	 * trouver un record pour un individu selon un nom et/ou un prenom
	 * 
	 * @param ec
	 * @param nomOuPrenom
	 * @param entity
	 * @param prefix
	 * @return
	 */
	public static NSArray filterIndividuForNomOrPrenomInArray(NSArray array, String nomOuPrenom, String prefix) {
		// mise en caps
		nomOuPrenom = nomOuPrenom.toUpperCase();
		NSArray records = new NSArray();
		NSArray mots = NSArray.componentsSeparatedByString(nomOuPrenom, " ");
		String premierMot, deuxiemeMot;
		premierMot = deuxiemeMot = "";
		String localPrefix = (prefix != null ? prefix : StringCtrl.emptyString());
		if (mots.count() > 0) {
			premierMot = (String) mots.objectAtIndex(0);
			if (mots.count() > 1) {
				deuxiemeMot = (String) mots.objectAtIndex(1);
			}
			NSArray args = new NSArray(new String[] { "*" + nomOuPrenom + "*", "*" + nomOuPrenom + "*", "*" + premierMot + "*", "*" + deuxiemeMot + "*", "*" + premierMot + "*", "*" + deuxiemeMot + "*" });
			String strQual =
					localPrefix + "nomUsuel like %@ OR " + localPrefix + "prenom like %@ OR (" +
							localPrefix + "nomUsuel like %@ AND " + localPrefix + "prenom like %@) OR (" +
							localPrefix + "prenom like %@ AND " + localPrefix + "nomUsuel like %@)";
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(strQual, args);
			NSArray arraySort = new NSArray(EOSortOrdering.sortOrderingWithKey(localPrefix + "nomUsuel", EOSortOrdering.CompareAscending));
			records = EOQualifier.filteredArrayWithQualifier(array, qual);
			records = EOSortOrdering.sortedArrayUsingKeyOrderArray(records, arraySort);
			records = NSArrayCtrl.removeDuplicate(records);
		}
		return records;
	}
}
