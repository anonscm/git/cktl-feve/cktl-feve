package org.cocktail.feve.app.finder;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.util.SystemCtrl;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe utilitaire pour gestion des logos d'etablisements dans les éditions
 * 
 * @author yannick
 * 
 */
public class LogoEdition {

	public static String cheminLogoEtablissment(EOEditingContext editingContext) {

		String cheminDir = SystemCtrl.tempDir();

		if (cheminDir != null)	{
			if (!(cheminDir.substring(cheminDir.length() - 1).equals(File.separator))) {
				cheminDir += File.separator;
			}

			String filePath = cheminDir + "Logo.jpg";
			if (!pathExiste(filePath)) {
				if (EOGrhumParametres.parametrePourCle(editingContext, "URL_LOGO") != null) {
					String urlLogo = EOGrhumParametres.parametrePourCle(editingContext, "URL_LOGO");
					try {
						saveImage(urlLogo, filePath);
					} catch (IOException e) {
						return null;
					}
				}
			}

			return filePath;
		}
		return null;
	}


	public static void saveImage(String imageUrl, String destinationFile) throws IOException {
		URL url = new URL(imageUrl);
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(destinationFile);

		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
	}

	private static boolean pathExiste(String unPath) {
		if (unPath == null || unPath.length() == 0) {
			return false;
		}
		File file = new File(unPath);
		return file.exists();
	}

}

