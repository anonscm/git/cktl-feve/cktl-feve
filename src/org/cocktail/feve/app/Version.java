package org.cocktail.feve.app;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleServer;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionWebObjects;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionWonder;

import er.extensions.appserver.ERXApplication;

/*
 * Copyright Universit� de La Rochelle 2008
 *
 * Ce logiciel est un programme informatique servant � g�rer les
 * fiche de poste
 * 
 * Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilit� au code source et des droits de copie,
 * de modification et de redistribution accord�s par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
 * seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les conc�dants successifs.

 * A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 * associ�s au chargement,  � l'utilisation,  � la modification et/ou au
 * d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 * manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 * avertis poss�dant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
 * logiciel � leurs besoins dans des conditions permettant d'assurer la
 * s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 * � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

 * Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accept� les
 * termes.
 */

/**
 * Classe de description de version de Feve
 * 
 * @author Cyril Tarade <cyril.tarade at univ-lr.fr>
 */
public class Version extends A_CktlVersion {

	/** Version de la base de donnees requise pour le User GRHUM. */
    private static final String BD_GRH_FEVE_VERSION_MIN = "2.0.0.0";
    private static final String BD_GRH_FEVE_VERSION_MAX = null;
    
    
	/** Version de la base de donnees requise pour le User GRHUM. */
    private static final String BD_GRHUM_VERSION_MIN = "1.9.2.0";
    private static final String BD_GRHUM_VERSION_MAX = null;
    
    
    /** Version de la base de donnees requise pour le User MANGUE. */
    private static final String BD_MANGUE_VERSION_MIN = "1.7.14.0";
    private static final String BD_MANGUE_VERSION_MAX = null;
    
	/* Version de WebObjects */
	private static final String WO_VERSION_MIN = "5.4.3.0";
	private static final String WO_VERSION_MAX = null;

	/* Version du JRE */
	private static final String JRE_VERSION_MIN = "1.6.0.0";
	private static final String JRE_VERSION_MAX = null;

	/* Version d'ORACLE */
	private static final String ORACLE_VERSION_MIN = "9.0";
	private static final String ORACLE_VERSION_MAX = null;

	/* Version du frmk FwkCktlWebApp */
	private static final String CKTLWEBAPP_VERSION_MIN = "4.0.1";
	private static final String CKTLWEBAPP_VERSION_MAX = null;

	/* Version de WONDER */
	private static final String WONDER_VERSION_MIN = "5.0.0.9000";
	private static final String WONDER_VERSION_MAX = null;
	
	
	@Override
	public String name() {
		return VersionMe.APPLICATIONFINALNAME;
	}

	@Override
	public int versionNumMaj() {
		return VersionMe.VERSIONNUMMAJ;
	}

	@Override
	public int versionNumMin() {
		return VersionMe.VERSIONNUMMIN;
	}

	@Override
	public int versionNumPatch() {
		return VersionMe.VERSIONNUMPATCH;
	}

	@Override
	public int versionNumBuild() {
		return VersionMe.VERSIONNUMBUILD;
	}

	@Override
	public String date() {
		return VersionMe.VERSIONDATE;
	}

	@Override
	public String comment() {
		return VersionMe.COMMENT;
	}
	
	@Override
	public String version() {
		StringBuffer sb = new StringBuffer();
		boolean shouldIgnorePrevNumber = true;
		if (versionNumBuild() > 0) {
			shouldIgnorePrevNumber = false;
		}
		if (!shouldIgnorePrevNumber) {
			sb.insert(0, "."+versionNumBuild());
		}
		if (shouldIgnorePrevNumber) {
			shouldIgnorePrevNumber = false;
		}
		if (!shouldIgnorePrevNumber) {
			sb.insert(0, "."+versionNumPatch());
		}
		if (shouldIgnorePrevNumber ) {
			shouldIgnorePrevNumber = false;
		}
		if (!shouldIgnorePrevNumber) {
			sb.insert(0, "."+versionNumMin());
		}
		// on affiche toujours le numero majeur
		sb.insert(0, versionNumMaj());
		return sb.toString();
	}
	

	
	@Override
	public CktlVersionRequirements[] dependencies() {
		boolean isDevMode = ERXApplication.isDevelopmentModeSafe();
		boolean isDbFlyway = ((Application) ERXApplication.application()).isDbFlyway();
		
		// Si on est en mode dev ou si la base est migrée avec flyway,
		// on n'empeche pas le démarrage de l'application si le contrôle des versions BDD échouent
		boolean controlePassant = isDevMode || isDbFlyway;
		
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new CktlVersionWebObjects(), WO_VERSION_MIN, WO_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionJava(), JRE_VERSION_MIN, JRE_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionOracleServer(), ORACLE_VERSION_MIN, ORACLE_VERSION_MAX, false),
				new CktlVersionRequirements(new CktlVersionWonder(), WONDER_VERSION_MIN, WONDER_VERSION_MAX, true),
	    		new CktlVersionRequirements(new org.cocktail.fwkcktlpersonne.server.VersionDatabase(), BD_GRHUM_VERSION_MIN, BD_GRHUM_VERSION_MAX, !controlePassant),
	    		new CktlVersionRequirements(new org.cocktail.fwkcktlgrh.server.VersionDatabase(), BD_MANGUE_VERSION_MIN, BD_MANGUE_VERSION_MAX, !controlePassant),
				new CktlVersionRequirements(new org.cocktail.feve.app.VersionDatabase(), BD_GRH_FEVE_VERSION_MIN, BD_GRH_FEVE_VERSION_MAX, 
	    				!controlePassant)
		};
	}


}
