package org.cocktail.feve.app;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.feve.app.droits.DroitCible;
import org.cocktail.feve.app.droits.DroitCibleIndividu;
import org.cocktail.feve.app.droits.DroitPersonneResponsableHolder;
import org.cocktail.feve.app.droits.Fonction;
import org.cocktail.feve.app.droits.GRHAutorisationsCache;
import org.cocktail.fwkcktlgrh.common.droits.DroitService;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveAnnulationDroit;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveDroit;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.droits.DroitsHelper;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.CktlLog;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Classe implémentant les méthodes de vérification des droits par profil pour l'application Feve "en dur".
 * @author juliencallewaert
 *
 */
public class FeveAutorisationsCache extends GRHAutorisationsCache {


	/** Nom de l'application (dans la table GD_APPLICATION). */
	public static final String APP_FEVE = "FEVE";

	private boolean isAdmin;
	private boolean isDrh;
	private boolean isAdminFoncRestreint;
	
	private EOGdProfil profilAdmin;
	private EOGdProfil profilAdminFoncRestreint;
	private EOGdProfil profilDrh;
	
	
	
	public FeveAutorisationsCache(EOIndividu individuConnecte, EOEditingContext ec) {
		super(APP_FEVE, individuConnecte, ec);
	}

	/**
	 * Est-ce que j'ai le droit de gérer les postes ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationGererPoste(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.GERER_POSTE, personne);
	}

	/**
	 * Est-ce que j'ai le droit d'éditer un profil de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationEditerPoste(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.EDITER_PROFIL_POSTE, personne);
	}

	/**
	 * Est-ce que j'ai le droit de gérer la fiche de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationGererFichePoste(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.GERER_FICHE_POSTE, personne);
	}
	
	/**
	 * Est-ce que j'ai le droit de gérer une fiche de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationGererFichePoste() {
		return hasDroitUtilisationOnFonction(Fonction.GERER_FICHE_POSTE);
	}
	
	
	/**
	 * Est-ce que j'ai le droit de valider une fiche de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValiderFichePoste(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.VALIDER_FICHE_POSTE, personne);
	}

	/**
	 * Est-ce que j'ai le droit d'éditer une fiche de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationEditerFichePoste(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.EDITER_FICHE_POSTE, personne);
	}

	/**
	 * Est-ce que j'ai le droit de gérer un entretien professionnel ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationGererEntretienProfessionnel(IPersonne personne, IStructure structure) {
		return hasDroitUtilisationOnFonction(Fonction.GERER_ENTRETIEN_PROFESSIONNEL, personne, structure);
	}
	
	/**
	 * Est-ce que j'ai le droit de gérer un entretien professionnel ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationGererEntretienProfessionnel(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.GERER_ENTRETIEN_PROFESSIONNEL, personne);
	}
	
	/**
	 * Est-ce que j'ai le droit d'éditer un compte-rendu d'entretien professionnel vierge ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationEditerCompteRenduEpVierge(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.EDITER_COMPTE_RENDU_EP_VIERGE, personne);
	}

	/**
	 * Est-ce que j'ai le droit d'éditer un compte-rendu d'entretien professionnel finalisé ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationEditerCompteRenduEpFinalise(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.EDITER_COMPTE_RENDU_EP_FINALISE, personne);
	}
	
	/**
	 * Est-ce que j'ai le droit de valider un compte-rendu d'entretien professionnel finalisé ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValiderCompteRenduEpFinalise(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.VALIDER_COMPTE_RENDU_EP_FINALISE, personne);
	}

	/**
	 * Est-ce que j'ai le droit de gérer les droits ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationGererDroits(IPersonne personne) {
		return hasDroitUtilisationOnFonction(Fonction.GERER_DROIT, personne);
	}

	/**
	 * Est-ce que j'ai le droit de gérer les droits ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationGererDroits() {
		return hasDroitUtilisationOnFonction(Fonction.GERER_DROIT);
	}
	
	
	/**
	 * Est-ce que j'ai le droit de gérer les périodes de campagnes d'entretiens ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationGererPeriodeCampagne() {
		return hasDroitUtilisationOnFonction(Fonction.GERER_PERIODE_CAMPAGNE);
	}
	
	public boolean hasDroitShowGererPeriodeCampagne() {
		return hasDroitUtilisationGererPeriodeCampagne() || hasDroitConnaissanceGererPeriodeCampagne();
	}
	
	/**
	 * Est-ce que j'ai le droit d'utiliser la bibliothèque ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationBibliotheque() {
		return hasDroitUtilisationOnFonction(Fonction.BIBLIO);
	}
	
	/**
	 * Est-ce que j'ai le droit de changer d'identité ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationChangerIdentite() {
		return hasDroitUtilisationOnFonction(Fonction.CHANGER_IDENTITE);
	}
	
	/**
	 * Est-ce que j'ai le droit d'administrer les outils de suivi ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationAdminOutilsSuivi() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_OUTILS_SUIVI);
	}
	
	/**
	 * Est-ce que j'ai le droit d'administrer la gestion des fonctions Silland aux destinations LOLF ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationAdminSillandLolf() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_SILLAND_LOLF);
	}
	
	/**
	 * Est-ce que j'ai le droit d'administrer la synchronisation des fiches LOLF ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationAdminSynchronisationLolf() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_SYNC_LOLF);
	}
	
	/**
	 * Est-ce que j'ai le droit d'administrer les périodes de saisies autorisées ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationAdminPeriodeSaisie() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_PERIODE_SAISIE);
	}

	/**
	 * Est-ce que j'ai le droit d'administrer les activités "Autres" et compétences "Autres" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationAdminAutres() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_AUTRES);
	}
	
	/**
	 * Est-ce que j'ai le droit d'administrer la création de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationAdminCreationPoste() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_CREER_POSTE);
	}
	
	/**
	 * Est-ce que j'ai le droit d'administrer les conditions à remplir pour être évalué ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationAdminConditionsEvaluation() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_CONDITIONS_EVALUATION);
	}
	
	/**
	 * Est-ce que j'ai le droit d'administrer les formations suivies ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationAdminFormations() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_FORMATIONS);
	}
	
	
	
	/**
	 * Est-ce que j'ai le droit de visualiserMissions de la composante: la gestion des postes ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceGererPoste(IPersonne personne) {
		return hasDroitConnaissanceOnFonction(Fonction.GERER_POSTE, personne);
	}

	/**
	 * Est-ce que j'ai le droit de visualiser l'édition d'un profil de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceEditerPoste(IPersonne personne) {
		return hasDroitConnaissanceOnFonction(Fonction.EDITER_PROFIL_POSTE, personne);
	}

	/**
	 * Est-ce que j'ai le droit de visualiser la fiche de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceGererFichePoste(IPersonne personne) {
		return hasDroitConnaissanceOnFonction(Fonction.GERER_FICHE_POSTE, personne);
	}
	
	/**
	 * Est-ce que j'ai le droit de visualiser les fiches de poste de nos services validées ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceValiderFichePoste(IPersonne personne) {
		return hasDroitConnaissanceOnFonction(Fonction.VALIDER_FICHE_POSTE, personne);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur l'édition d'une fiche de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceEditerFichePoste(IPersonne personne) {
		return hasDroitConnaissanceOnFonction(Fonction.EDITER_FICHE_POSTE, personne);
	}
	
	/**
	 * Est-ce que j'ai le droit de connaissance sur l'édition d'une fiche de poste ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceEditerCompteRenduEPFinalise(IPersonne personne) {
		return hasDroitConnaissanceOnFonction(Fonction.EDITER_COMPTE_RENDU_EP_FINALISE, personne);
	}
	
	/**
	 * Est-ce que j'ai le droit de connaissance sur la gestion d'un entretien professionnel ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceGererEntretienProfessionnel(IPersonne personne) {
		return hasDroitConnaissanceOnFonction(Fonction.GERER_ENTRETIEN_PROFESSIONNEL, personne);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la gestion d'un entretien professionnel ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceGererEntretienProfessionnel(IPersonne personne, IStructure structure) {
		return hasDroitConnaissanceOnFonction(Fonction.GERER_ENTRETIEN_PROFESSIONNEL, personne, structure);
	}
		
	/**
	 * Est-ce que j'ai le droit de connaissance sur la gestion des droits ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceGererDroits(IPersonne personne) {
		return hasDroitConnaissanceOnFonction(Fonction.GERER_DROIT, personne);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la gestion des périodes de campagnes d'entretiens ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceGererPeriodeCampagne() {
		return hasDroitConnaissanceOnFonction(Fonction.GERER_PERIODE_CAMPAGNE);
	}
	
	/**
	 * Est-ce que j'ai le droit de connaissance sur l'administration des périodes de saisies autorisées ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowAdminPeriodeSaisie() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_PERIODE_SAISIE);
	}

	public boolean hasDroitShowBibliotheque() {
		return hasDroitConnaissanceOnFonction(Fonction.BIBLIO);
	}
	
	public boolean hasDroitShowAdminAutres() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_AUTRES);
	}	
	
	public boolean hasDroitShowAdminConditionsEvaluation() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_CONDITIONS_EVALUATION);
	}
	
	public boolean hasDroitShowAdminOutilsSuivi() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_OUTILS_SUIVI);
	}
	
	public boolean hasDroitShowAdminSillandLolf() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_SILLAND_LOLF);
	}
	
	public boolean hasDroitShowAdminSynchronisationLolf() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_SYNC_LOLF);
	}
	
	public boolean hasDroitShowAdminCreationPoste() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_CREER_POSTE);
	}
	
	/**
	 * Est-ce que j'ai le droit de connaissance sur l'administration des formations suivies ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowAdminFormations() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_FORMATIONS);
	}
	
	
	
	public boolean hasDroitShowGererFichePoste(IPersonne personne) {
		return hasDroitConnaissanceGererFichePoste(personne) || hasDroitUtilisationGererFichePoste(personne);
	}
	
	public boolean hasDroitShowGererPoste(IPersonne personne) {
		return hasDroitConnaissanceGererFichePoste(personne) || hasDroitUtilisationGererFichePoste(personne);
	}
	
	public List<IStructure> getAllShowStructuresGererEntretienProfessionnel() {
		return getAllShowStructuresOnFonction(Fonction.GERER_ENTRETIEN_PROFESSIONNEL);
	}
	
	public List<EOIndividu> getIndividusGererEntretienProfessionnel(IStructure structure) {
		return alimenterCacheAffectation(structure);
	}
	
	public List<IStructure> getAllShowStructuresGererFichePoste() {
		return getAllShowStructuresOnFonction(Fonction.GERER_FICHE_POSTE);
	}
	
	public List<IStructure> getAllShowStructuresGererDroits() {
		return getAllShowStructuresOnFonction(Fonction.GERER_DROIT);
	}
	
	public List<IStructure> getAllUtilisationStructuresGererDroits() {
		return getAllUtilisationStructuresOnFonction(Fonction.GERER_DROIT);
	}
	
	public boolean hasDroitShowGererDroits() {
		return hasDroitConnaissanceOnFonction(Fonction.GERER_DROIT);
	}
	
	public boolean hasDroitShowAdminEditionFichePoste() {
		return hasDroitConnaissanceOnFonction(Fonction.ADMIN_EDITION_FICHE_POSTE);
	}

	public boolean hasDroitUtilisationAdminEditionFichePoste() {
		return hasDroitUtilisationOnFonction(Fonction.ADMIN_EDITION_FICHE_POSTE);
	}
	
	
	
	public boolean hasDroitShowMenuAdministration() {
		return hasDroitShowAdminAutres() || hasDroitShowAdminConditionsEvaluation() || hasDroitShowAdminFormations()
				|| hasDroitShowAdminPeriodeSaisie() || hasDroitShowGererPeriodeCampagne() || hasDroitShowAdminConditionsEvaluation()
				|| hasDroitShowAdminCreationPoste() || hasDroitShowAdminEditionFichePoste() || hasDroitShowAdminFormations() 
				|| hasDroitShowAdminOutilsSuivi()	|| hasDroitShowAdminSillandLolf() || hasDroitShowAdminSynchronisationLolf() 
				|| hasDroitShowGererDroits(); 
	}
	
	
	
	// Méthodes privée de transcodification enum Fonction => String
	// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	/**
	 * Méthode transcodant le paramètre de type {@link Fonction} en String.
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitUtilisationOnFonction(Fonction fonction, IPersonne personne) {
		if (personne == null) {
			return false;
		}
		return hasDroitUtilisationOnFonctionEtCible(fonction.getIdFonction(), personne);
	}

	/**
	 * Méthode gérant le cache (Individu - N Affectations).
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitUtilisationOnFonction(Fonction fonction, IPersonne personne, IStructure structure) {
		alimenterCacheAffectation(structure);
		return hasDroitUtilisationOnFonction(fonction, personne);
	}
	
	/**
	 * Méthode transcodant le paramètre de type {@link Fonction} en String.
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceOnFonction(Fonction fonction, IPersonne personne) {
		if (personne == null) {
			return false;
		}
		return hasDroitConnaissanceOnFonctionEtCible(fonction.getIdFonction(), personne);
	}

	
	/**
	 * Méthode gérant le cache (Individu - N Affectations).
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceOnFonction(Fonction fonction, IPersonne personne, IStructure structure) {
		alimenterCacheAffectation(structure);
		return hasDroitConnaissanceOnFonction(fonction, personne);
	}

	
	/**
	 * Méthode transcodant le paramètre de type {@link Fonction} en String.
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitUtilisationOnFonction(Fonction fonction) {
		return hasDroitUtilisationOnFonction(fonction.getIdFonction());
	}

	/**
	 * Méthode transcodant le paramètre de type {@link Fonction} en String.
	 *
	 * @param fonction Le droit fonction
	 * @return liste de structures
	 */
	private List<IStructure> getAllShowStructuresOnFonction(Fonction fonction) {
		return getAllShowStructuresOnFonction(fonction.getIdFonction());
	}
	
	/**
	 * Méthode transcodant le paramètre de type {@link Fonction} en String.
	 *
	 * @param fonction Le droit fonction
	 * @return liste de structures
	 */
	private List<IStructure> getAllUtilisationStructuresOnFonction(Fonction fonction) {
		return getAllUtilisationStructuresOnFonction(fonction.getIdFonction());
	}
	
	/**
	 * Méthode transcodant le paramètre de type {@link Fonction} en String.
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceOnFonction(Fonction fonction) {
		return hasDroitConnaissanceOnFonction(fonction.getIdFonction());
	}
	
	/**
	 * @return map profilID, List<IPersonne>
	 */
	@Override
	protected Map<Integer, List<DroitPersonneResponsableHolder>> getAccreditationsSpecificIndividu(EOEditingContext ec) {
		
		Map<Integer, List<DroitPersonneResponsableHolder>> dictionaryDroit = new HashMap<Integer, List<DroitPersonneResponsableHolder>>();
		
		/** Liste des structures sur lequel la personne connectée est responsable de service */
		List<EOStructure> structureRespService = DroitService.getEoStructureResponsableAnnuaireArray(ec, getIndividuConnecte().persId());
		if (!structureRespService.isEmpty()) {
			alimenterDictionaryAvecResponsables(ec, dictionaryDroit, EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitRespServ(ec), structureRespService);
		}
		
		/** Construire la liste des droits définis dans Feve pour un utilisateur **/
		dictionaryDroit = alimenterDictionaryAvecDroitsFeve(ec, dictionaryDroit);
		
		return dictionaryDroit;
	}

	
	private Map<Integer, List<DroitPersonneResponsableHolder>> alimenterDictionaryAvecDroitsFeve(EOEditingContext ec, Map<Integer, List<DroitPersonneResponsableHolder>> dictionaryDroit) {
		
		NSArray<Integer> listeIds = construireListeTitulaireIds(ec);
		
		List<EOFeveDroit> eoDroitArray = EOFeveDroit.rechercherAllDroitTitulaire(ec, listeIds);
		
		for (EOFeveDroit eoFeveDroit : eoDroitArray) {
			
			List<DroitPersonneResponsableHolder> listePers = new ArrayList<DroitPersonneResponsableHolder>();
			if (eoFeveDroit.isCibleChefDeService()) {
				
				EOStructure structure = (EOStructure) eoFeveDroit.toPersonneCible();
				listePers.add(new DroitPersonneResponsableHolder(structure.toResponsable(), true));
				if (eoFeveDroit.isCibleHeritage()) {
					List<EOStructure> listeSousService = structure.tosSousServiceDeep(false);
					for (EOStructure eoStructure : listeSousService) {
						if (!structure.equals(eoStructure)) {
							listePers.add(new DroitPersonneResponsableHolder(eoStructure.toResponsable(), true));
						}
					}
				}
					
			} else {
			
				if (eoFeveDroit.isPersonneCibleStructure()) {
					
					if (eoFeveDroit.isCibleHeritage()) {
						EOStructure structureCible = (EOStructure) eoFeveDroit.toPersonneCible();
						listePers.add(new DroitPersonneResponsableHolder(structureCible, true));
						
						List<EOStructure> sousServices = structureCible.tosSousServiceDeep(false);
						for (EOStructure eoStructure : sousServices) {
							if (!eoStructure.equals(structureCible)) {
								listePers.add(new DroitPersonneResponsableHolder(eoStructure, true));
							}
						}
						
						
					} else {
						listePers.add(new DroitPersonneResponsableHolder(eoFeveDroit.toPersonneCible(), false));
					}
					
				} else {
					
					listePers.add(new DroitPersonneResponsableHolder(eoFeveDroit.toPersonneCible(), false));
				}
			}
			alimenterDictionary(dictionaryDroit, eoFeveDroit.toTypeNiveauDroit(), listePers);
			
			determinerIsProfilSpecifiqueDepuisDroitsFeve(ec, eoFeveDroit);
			
		}
		
		return dictionaryDroit;
	}

	private NSArray<Integer> construireListeTitulaireIds(EOEditingContext ec) {
		
		List<EOAffectation> affectationsCourantes = EOAffectation.affectationCourantes(ec, getIndividuConnecte());
		
		NSArray<Integer> liste = new NSMutableArray<Integer>();
		for (EOAffectation eoAffectation : affectationsCourantes) {
			liste.add(Integer.valueOf(eoAffectation.toStructure().cStructure()));
		}
		liste.add(getIndividuConnecte().persId());
		
		return liste;
	}

	private EOGdProfil rechercherProfilDrh(EOEditingContext ec) {
		if (profilDrh == null) {
			profilDrh = EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitDrh(ec).toGdProfil();
		}
		return profilDrh;
	}

	private EOGdProfil rechercherProfilAdmin(EOEditingContext ec) {
		if (profilAdmin == null) {
			profilAdmin = EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitAdmin(ec).toGdProfil();
		}		
		return profilAdmin;
	}
	
	private EOGdProfil rechercherProfilAdminFoncRestreint(EOEditingContext ec) {
		if (profilAdminFoncRestreint == null) {
			profilAdminFoncRestreint = EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitAdminFonc(ec).toGdProfil();
		}
		return profilAdminFoncRestreint;
	}

	private void determinerIsProfilSpecifiqueDepuisDroitsFeve(EOEditingContext ec, EOFeveDroit eoFeveDroit) {
		if (!isAdmin() && eoFeveDroit.toTypeNiveauDroit().toGdProfil().equals(rechercherProfilAdmin(ec))) {
			setAdmin(true);
		}
		if (!isDrh() && eoFeveDroit.toTypeNiveauDroit().toGdProfil().equals(rechercherProfilDrh(ec))) {
			setDrh(true);
		}
		if (!isAdminFoncRestreint() && eoFeveDroit.toTypeNiveauDroit().toGdProfil().equals(rechercherProfilAdminFoncRestreint(ec))) {
			setDrh(true);
		}
	}
	
	
	private void alimenterDictionaryAvecResponsables(EOEditingContext ec, Map<Integer, List<DroitPersonneResponsableHolder>> dictionaryDroit, EOFeveTypeNiveauDroit typeNiveauDroit,
			List<EOStructure> structureRespService) {
		
		List<DroitPersonneResponsableHolder> listePers = new ArrayList<DroitPersonneResponsableHolder>();
		List<EOFeveAnnulationDroit> listeAnnulationDroit = EOFeveAnnulationDroit.fetchAll(ec);
		
		for (EOStructure eoStructure : structureRespService) {
			
			boolean droitHeriteAnnule = EOFeveAnnulationDroit.existsAnnulationDroitDansListe(getIndividuConnecte().persId(), eoStructure, typeNiveauDroit, listeAnnulationDroit);
			
			if (!droitHeriteAnnule) {
				listePers.add(new DroitPersonneResponsableHolder(eoStructure, true));
			}
		}
		if (!listePers.isEmpty()) {
			alimenterDictionary(dictionaryDroit, typeNiveauDroit, listePers);
		}
	}
	
	private void alimenterDictionary(Map<Integer, List<DroitPersonneResponsableHolder>> dictionaryDroit, EOFeveTypeNiveauDroit typeNiveauDroit, List<DroitPersonneResponsableHolder> listePers) {
		
		Integer key = Integer.valueOf(typeNiveauDroit.toGdProfil().primaryKey());
		
		if (dictionaryDroit.containsKey(key)) {
			List<DroitPersonneResponsableHolder> arrayPersonne = dictionaryDroit.get(key);
			listePers.addAll(arrayPersonne);
		}
		dictionaryDroit.put(key, listePers);
		
		
	}

	@Override
	protected Map<String, DroitCible> buildAutorizationParDefaut(EOEditingContext ec, String appStrId, Integer persId) {
		List<EOGdProfilDroitFonction> droitsFonction = DroitsHelper.droitsFonctionForProfilPasHerite(ec, appStrId, EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitAgent(ec).prId());
		
		Map<String, DroitCible> autorizationOnFonctionCache = new HashMap<String, DroitCible>(); 
		
		for (EOGdProfilDroitFonction droitFonction : droitsFonction) {
			autorizationOnFonctionCache.put(droitFonction.toGdFonction().fonIdInterne(), new DroitCibleIndividu(droitFonction.toGdTypeDroitFonction().tdfStrId(), getIndividuConnecte()));
		}
		
		return autorizationOnFonctionCache;
	}

	@Override
	protected boolean isFonctionAvecCibleParticuliere(String fonIdInterne) {
		return Fonction.getFonction(fonIdInterne).isPerimetreCible();
	}

	
	/**
	 * Liste toutes les structures utilisées sur Feve
	 */
	@Override
	protected List<EOStructure> getAllStructuresSpecific(EOEditingContext ec) {
		return EOStructure.rechercherServices(ec);
	}
	
	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public boolean isDrh() {
		return isDrh;
	}

	public void setDrh(boolean isDrh) {
		this.isDrh = isDrh;
	}

	@Override
	protected void determinerIsProfilSpecifiqueDepuisAnnuaire(EOEditingContext ec, 
			List<EOGdProfil> profils) {
		if (profils.size() > 0) {
			
			for (EOGdProfil profil : profils) {
			
				if (profil.equals(rechercherProfilAdmin(ec))) {
					setAdmin(true);
				}
				if (profil.equals(rechercherProfilDrh(ec))) {
					setDrh(true);
				}
				
				if (profil.equals(rechercherProfilAdminFoncRestreint(ec))) {
					setAdminFoncRestreint(true);
				}
				
			}
		}
		
	}

	public boolean isAdminFoncRestreint() {
		return isAdminFoncRestreint;
	}

	public void setAdminFoncRestreint(boolean isAdminFoncRestreint) {
		this.isAdminFoncRestreint = isAdminFoncRestreint;
	}
	
	public boolean isProfilAdmin() {
		return isAdmin() || isAdminFoncRestreint() || isDrh();
	}
	
		
}
