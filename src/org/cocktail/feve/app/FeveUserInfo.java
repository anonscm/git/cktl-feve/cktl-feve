package org.cocktail.feve.app;

import java.util.List;

import org.cocktail.fwkcktlgrh.common.metier.services.IndividuGrhService;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.filtre.StructureFiltre;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * La classe descriptive d'un utilisateur connecte a l'application
 * 
 * @author Cyril Tarade <cyril.tarade at univ-lr.fr>
 */
public class FeveUserInfo extends CktlUserInfoDB {

	/** CStructure de l'administration */
	private String cStructureAdmin;
	
	/** droits accordes */
	private Boolean isAdmin;

	/** le persId de la personne connectee */
	private Number persId;

	/** */
	private EOEditingContext editingContext;
	private EOIndividu recIndividu;
	
	/**
	 * Tous les services surlequel la personne connectee a un droit (directement,
	 * ou sur un poste ou une fiche)
	 */
	private List<IStructure> servicePosteList;

	/**
	 * Determiner si l'utilisateur n'a que des droits sur lui meme, a savoir il
	 * voit que ses propres trucs : evaluation et fiches
	 */
	private Boolean isBasicUser = null;

	private NSTimestamp evaluationSaisieDDebut;
	private NSTimestamp evaluationSaisieDFin;
	private NSTimestamp ficheLolfSaisieDDebut;
	private NSTimestamp ficheLolfSaisieDFin;
	private boolean fDPSaisieActivitesAutres;
	private boolean modificationFormationsSuiviesDansFeve;
	private boolean fDPSaisieCompetencesAutres;
	
	private FeveAutorisationsCache autorisation;
		
	/**
	 * 
	 * @param aCriApp
	 * @param bus
	 * @param anEc
	 * @param aPersId
	 */
	public FeveUserInfo(CktlDataBus bus, EOEditingContext anEc, Number aPersId) {
		super(bus);
		persId = aPersId;
		editingContext = anEc;
		
		initUserInfo();
		
	}

	private void initUserInfo() {
		
		IndividuGrhService individuGrhService = new IndividuGrhService();
		individuForPersId(persId, true);
		recIndividu = individuGrhService.findIndividuForPersIdInContext(editingContext, persId);
		
		setAutorisation(new FeveAutorisationsCache(recIndividu(), editingContext));
		
		isBasicUser = getAutorisation().isBasicUser();
		servicePosteList = getAutorisation().getAllShowStructuresGererFichePoste();
		isAdmin = getAutorisation().isAdmin();
		
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isAdmin() {
		return isAdmin.booleanValue();
	}

	/**
	 * 
	 * @return
	 */
	public EOIndividu recIndividu() {
		return recIndividu;
	}

	/**
	 * 
	 * @return
	 */
	public List<IStructure> getServicePosteList() {
		return servicePosteList;
	}

	/**
	 * 
	 * @param isAfficherArchive
	 * @return
	 */
	public List<IStructure> getServicePosteList(boolean isAfficherArchive) {
		List<IStructure> serviceList = getServicePosteList();
		StructureFiltre structureFiltre = new StructureFiltre();
		if (!isAfficherArchive) {
			serviceList = (List<IStructure>) structureFiltre.filtrerParIsArchivee(serviceList);
			
		}
		return serviceList;
	}

	// la liste des periodes d'evaluation
	private NSArray periodeList;

	/**
	 * la liste des periodes d'evaluation
	 */
	public NSArray getPeriodeList() {
		return periodeList;
	}

	// parametres d'acces aux zones activites / competences autres
	private Boolean _isFicheDePosteSaisieActiviteAutre;
	private Boolean _isFicheDePosteSaisieCompetenceAutre;

	// autoriser la modification des formations suivies
	private Boolean _isModificationFormationsSuivies;

	/**
	 * Autorise-t-on la saisie d'activités autres dans la fiche de poste
	 * 
	 * @return
	 */
	public boolean isFicheDePosteSaisieActiviteAutre() {
		if (_isFicheDePosteSaisieActiviteAutre == null) {
			_isFicheDePosteSaisieActiviteAutre = isfDPSaisieActivitesAutres();
		}
		return _isFicheDePosteSaisieActiviteAutre.booleanValue();
	}

	/**
	 * Autorise-t-on la saisie de compétences autres dans la fiche de poste ainsi
	 * que leur évaluation dans l'entretien professionnel
	 * 
	 * @return
	 */
	public boolean isFicheDePosteSaisieCompetenceAutre() {
		if (_isFicheDePosteSaisieCompetenceAutre == null) {
			_isFicheDePosteSaisieCompetenceAutre = isfDPSaisieCompetencesAutres();
		}
		return _isFicheDePosteSaisieCompetenceAutre.booleanValue();
	}

	/**
	 * autoriser la modification des formations suivies dans l'interface de feve
	 * ou uniquement dans Mangue
	 * 
	 * @return
	 */
	public boolean isModificationFormationsSuivies() {
		if (_isModificationFormationsSuivies == null) {
			_isModificationFormationsSuivies = isModificationFormationsSuiviesDansFeve();
		}
		return _isModificationFormationsSuivies.booleanValue();
	}

	/**
	 * Forcer la relecture des parametres dans la base de données
	 */
	public void clearParamCache() {
		_isFicheDePosteSaisieActiviteAutre = null;
		_isFicheDePosteSaisieCompetenceAutre = null;
		_isModificationFormationsSuivies = null;
	}

	@Override
	public Integer persId() {
		return new Integer(super.persId().intValue());
	}
	

	public String cStructureAdmin() {
    	
		if (cStructureAdmin == null) {
			cStructureAdmin = FwkCktlPersonne.paramManager.getParam("C_STRUCTURE_ADMIN");
		}
		return cStructureAdmin;
	}
	
	/**
	 * Indique si l'application autorise la saisie des fiches LOLF
	 * 
	 * @return
	 */
	public boolean isPeriodeLolfOuverte() {
		return (DateCtrl.isAfterEq(DateCtrl.now(), getFicheLolfSaisieDDebut()) && DateCtrl.isBeforeEq(DateCtrl.now(), getFicheLolfSaisieDFin().timestampByAddingGregorianUnits(0, 0, 0, 23, 59, 59)));
	}
	
	/**
	 * Indique si l'application autorise la saisie des fiches LOLF
	 * 
	 * @return
	 */
	public boolean isPeriodeEvaluationOuverte() {
		return (DateCtrl.isAfterEq(DateCtrl.now(), getEvaluationSaisieDDebut()) && DateCtrl.isBeforeEq(DateCtrl.now(), getEvaluationSaisieDFin().timestampByAddingGregorianUnits(0, 0, 0, 23, 59, 59)));
	}

	public NSTimestamp getEvaluationSaisieDDebut() {
		return evaluationSaisieDDebut;
	}

	public void setEvaluationSaisieDDebut(NSTimestamp evaluationSaisieDDebut) {
		this.evaluationSaisieDDebut = evaluationSaisieDDebut;
	}

	public NSTimestamp getEvaluationSaisieDFin() {
		return evaluationSaisieDFin;
	}

	public void setEvaluationSaisieDFin(NSTimestamp evaluationSaisieDFin) {
		this.evaluationSaisieDFin = evaluationSaisieDFin;
	}

	public NSTimestamp getFicheLolfSaisieDDebut() {
		return ficheLolfSaisieDDebut;
	}

	public void setFicheLolfSaisieDDebut(NSTimestamp ficheLolfSaisieDDebut) {
		this.ficheLolfSaisieDDebut = ficheLolfSaisieDDebut;
	}

	public NSTimestamp getFicheLolfSaisieDFin() {
		return ficheLolfSaisieDFin;
	}

	public void setFicheLolfSaisieDFin(NSTimestamp ficheLolfSaisieDFin) {
		this.ficheLolfSaisieDFin = ficheLolfSaisieDFin;
	}

	public boolean isfDPSaisieActivitesAutres() {
		return fDPSaisieActivitesAutres;
	}

	public void setfDPSaisieActivitesAutres(boolean fDPSaisieActivitesAutres) {
		this.fDPSaisieActivitesAutres = fDPSaisieActivitesAutres;
	}

	public boolean isModificationFormationsSuiviesDansFeve() {
		return modificationFormationsSuiviesDansFeve;
	}

	public void setModificationFormationsSuiviesDansFeve(boolean modificationFormationsSuiviesDansFeve) {
		this.modificationFormationsSuiviesDansFeve = modificationFormationsSuiviesDansFeve;
	}

	public boolean isfDPSaisieCompetencesAutres() {
		return fDPSaisieCompetencesAutres;
	}

	public void setfDPSaisieCompetencesAutres(boolean fDPSaisieCompetencesAutres) {
		this.fDPSaisieCompetencesAutres = fDPSaisieCompetencesAutres;
	}

	public FeveAutorisationsCache getAutorisation() {
		return autorisation;
	}

	public void setAutorisation(FeveAutorisationsCache autorisation) {
		this.autorisation = autorisation;
	}

	public Boolean getIsBasicUser() {
		return isBasicUser;
	}


}
