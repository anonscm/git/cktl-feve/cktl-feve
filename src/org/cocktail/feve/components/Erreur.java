package org.cocktail.feve.components;


import org.cocktail.feve.components.common.FeveWebComponent;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSDictionary;


public class Erreur extends FeveWebComponent{
	
	private NSDictionary exceptionInfos;
	
    public Erreur(WOContext context) {
        super(context);
    }

	public WOComponent retourAccueil() {
		PageLogin accueil = (PageLogin)session.getSavedPageWithName(PageLogin.class.getName());
//		session.resetDefaultEditingContext();
//		accueil.setOnloadJS(null);
		return accueil;
	}

	/**
	 * @return the exceptionInfos
	 */
	public NSDictionary exceptionInfos() {
//		exceptionInfos = (NSDictionary)session.()exceptionInfos();
		return exceptionInfos;
	}

	/**
	 * @param exceptionInfos the exceptionInfos to set
	 */
	public void setExceptionInfos(NSDictionary exceptionInfos) {
		this.exceptionInfos = exceptionInfos;
	}
}