package org.cocktail.feve.components.evaluation;

import java.io.IOException;

import org.cocktail.feve.app.Session;
import org.cocktail.feve.app.print.GenerateurPDFCtrl;
import org.cocktail.feve.app.print.XMLGenerateur;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Onglet relatif à l'agent dans l'entretien d'évaluation
 * 
 * @author ctarade
 */
public class OngletEvaluationAgent
		extends A_OngletEvaluationAvecImpression {

	
	private GenerateurPDFCtrl generateurPDF;
	
	
	public OngletEvaluationAgent(WOContext context) {
		super(context);
	}

	@Override
	public boolean isEmptyEvaluation() {
		return true;
	}
	
	
	
	public GenerateurPDFCtrl getGenerateurPDF() {
		return generateurPDF;
	}
	
	public WOActionResults genererCompteRenduVierge() throws IOException {
		XMLGenerateur xmlGenerateur = new XMLGenerateur(edc(), (Session) session());
		//String xml = xmlGenerateur.generateEvaluationXML(isEmptyEvaluation(),getInEvaluation());
		
		String xml = xmlGenerateur.generateEvaluationXML(true,getInEvaluation());
		
		generateurPDF = new GenerateurPDFCtrl(xml);
		generateurPDF.genererCompteRenduEvaluation();		
		
		//TO DO: gérer le message de fin
		//dico.setObjectForKey(PrintConsts.ENDING_MESSAGE_FICHE_EVALUATION_VIERGE, PrintConsts.DICO_KEY_ENDING_MESSAGE);
		
		return null;
	}

	/**
	 * @return the compteRenduViergeFile
	 */
	public String CompteRenduFilename() {
		return StringCtrl.toBasicString("CompteRendu"+
				(isEmptyEvaluation() ? "Brouillon" : "") +"_" +
				getInEvaluation().toIndividu().nomPrenom() + "_" +
				getInEvaluation().toEvaluationPeriode().strAnneeDebutAnneeFin()+ ".pdf");
	}

	
	public boolean hasDroitEditerCompteRenduVierge() {
		return session.getAutorisation().hasDroitUtilisationEditerCompteRenduEpVierge(getInEvaluation().toIndividu());
	}
	
	}