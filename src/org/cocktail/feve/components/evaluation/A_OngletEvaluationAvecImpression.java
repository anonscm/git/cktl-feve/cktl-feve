package org.cocktail.feve.components.evaluation;

import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;

import com.webobjects.appserver.WOContext;

/**
 * Onglet affichant un composant permettant d'imprimer
 * le CR d'entretien
 * 
 * @author ctarade
 */
public abstract class A_OngletEvaluationAvecImpression
	extends FeveWebComponent {
	
	// binding
	private EOEvaluation inEvaluation;

	public boolean canUpdateEvaluation;
	
	public A_OngletEvaluationAvecImpression(WOContext context) {
		super(context);
	}
	 
  // IMPRESSIONS

  /**
   *  indique s'il faut imprimer une edition vierge ou pas
   */
  public abstract boolean isEmptyEvaluation();
  

	public final EOEvaluation getInEvaluation() {
		return inEvaluation;
	}

  
  //FIXME copier collé de PageEvaluation => a fusionner
  

  public void setInEvaluation(EOEvaluation value) {
		inEvaluation = value;

  	//
    canUpdateEvaluation = false;
    if (inEvaluation != null) {
    	canUpdateEvaluation = session.getAutorisation().hasDroitUtilisationGererEntretienProfessionnel(inEvaluation.toIndividu());
    };
  }
  
  /**
   * une evaluation est modifiable si la personne connectee a les droits
   * - la personne a le droit
   * - le visa du RH n'est pas apposé
   * @return
   */
  public boolean isModifiable() {
  	boolean isModifiable = false;

  	if (inEvaluation != null &&
  			canUpdateEvaluation &&
  			inEvaluation.isViseParResponsableRh() == false) {
  		isModifiable = true;
  	}
  	
  	return isModifiable;
  }
    
  /**
   * les sous elements de l'évaluation sont verrouilles si :
   * - la personne n'a pas les droits
   * - la coche VR est presente
   * @return
   */
  public boolean isNotModifiable() {
  	return !isModifiable();
  }
  
  /**
   * DT8076 : Visa DRH de l'entretien
   * @param isNotModifiable
   */
  public void setIsNotModifiable(boolean isNotModifiable) {
		
	}
  
}
