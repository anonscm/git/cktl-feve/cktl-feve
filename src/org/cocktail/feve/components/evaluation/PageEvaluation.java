package org.cocktail.feve.components.evaluation;

import org.cocktail.feve.components.common.A_FeveSubMenuEOTplOngletPage;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveParametres;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFicheBlocActivation;
import org.cocktail.fwkcktlgrh.common.metier.EOTplBloc;
import org.cocktail.fwkcktlgrh.common.metier.EOTplOnglet;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * TODO migrer vers la gestion des controleurs
 * 
 * @author ctarade
 * 
 */
public class PageEvaluation
extends A_FeveSubMenuEOTplOngletPage {

	// variables entrantes
	public EOEvaluation inputLEvaluation;

	// cache du menu
	private NSArray<EOTplOnglet> eoTplOngletArray;

	/**
	 * indique si l'utilisateur connecte peut modifier l'evaluation
	 * <code>inputLEvaluation</code>
	 */
	private boolean canUpdateEvaluation;


	public PageEvaluation(WOContext context) {
		super(context);
		razCache();
	}

	@Override
	protected void razCache() {
		super.razCache();
		eoTplOngletArray = null;
	}

	// gestion du menu

	@Override
	public NSArray<EOTplOnglet> getEoTplOngletArray() {
		if (eoTplOngletArray == null) {
			
			// AJOUT CAS DU BLOC MANAGEMENT
			// CE BLOC EST DESORMAIS GERE VIA LA FICHE DE POSTE ASSOCIEE
			
			NSArray fiches = inputLEvaluation.tosLastFicheDePoste();
			
			if (fiches.count() > 0) {
				EOFicheDePoste fiche = (EOFicheDePoste) fiches.objectAtIndex(0);
				EOQualifier qualifier = ERXQ.equals(EOTplBloc.TBL_CODE_KEY,EOTplBloc.TPL_BLOC_MANAGEMENT_CODE);
				EOTplBloc blocManagement=EOTplBloc.fetch(ec, qualifier);
				qualifier = ERXQ.and(ERXQ.equals(EORepartFicheBlocActivation.TO_TPL_BLOC_KEY,blocManagement),
						ERXQ.equals(EORepartFicheBlocActivation.TO_EVALUATION_KEY,inputLEvaluation));
				EORepartFicheBlocActivation existingRecord = EORepartFicheBlocActivation.fetch(ec, qualifier);

				if ( (!"N".equals(fiche.fonctionConduiteProjet()) || (!"N".equals(fiche.fonctionEncadrement())))) {
					// ajout bloc management
					if (existingRecord == null) {
						EORepartFicheBlocActivation newRecord = EORepartFicheBlocActivation.create(ec, DateCtrl.now(), DateCtrl.now(), blocManagement);
						newRecord.setToEvaluationRelationship(inputLEvaluation);
						try {
							UtilDb.save(ec, "");
						} catch (Exception e) {						
							e.printStackTrace();
						}	
					}
				} else {

					if (existingRecord != null) {
						// suppression bloc management
						ec.deleteObject(existingRecord);
						try {
							UtilDb.save(ec, "");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			// FIN DU CAS BLOC MANAGEMENT
			
			eoTplOngletArray = session.getEoTplFicheEvaluation().tosOnglet(inputLEvaluation.toEvaluationPeriode(), inputLEvaluation);
		}
		return eoTplOngletArray;
	}

	

	
	
	
	public void setInputLEvaluation(EOEvaluation value) {
		if (value != inputLEvaluation) {
			razCache();
		}
		inputLEvaluation = value;
		
		initialiserDroits();
	}
	
	private void initialiserDroits() {
		if (session.getAutorisation().isAdmin()) {
			setCanUpdateEvaluation(true);
		} else {
			boolean isCanUpdateEvaluation = false;
			isCanUpdateEvaluation = session.getAutorisation().hasDroitUtilisationGererEntretienProfessionnel(inputLEvaluation.toIndividu()) && isPeriodeEvaluationOuverte();
			setCanUpdateEvaluation(isCanUpdateEvaluation);
		}
		
	}

	private boolean isPeriodeEvaluationOuverte() {
		return (DateCtrl.isAfterEq(DateCtrl.now(), getEvaluationSaisieDDebut()) && DateCtrl.isBeforeEq(DateCtrl.now(), getEvaluationSaisieDFin().timestampByAddingGregorianUnits(0, 0, 0, 23, 59, 59)));
	}
	
	private NSTimestamp getEvaluationSaisieDDebut() {
		return app.getDateParamValueForKey(EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_DEBUT);
	}

	private NSTimestamp getEvaluationSaisieDFin() {
		return app.getDateParamValueForKey(EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_FIN);
	}
	
	
	/**
	 * une evaluation est modifiable si la personne connectee a les droits - la
	 * personne a le droit - le visa du RH n'est pas apposé
	 * 
	 * @return
	 */
	public boolean isModifiable() {
		boolean isModifiable = false;

		if (inputLEvaluation != null &&
				isCanUpdateEvaluation() &&
				inputLEvaluation.isViseParResponsableRh() == false) {
			isModifiable = true;
		}

		return isModifiable;
	}

	/**
	 * les sous elements de l'évaluation sont verrouilles si : - la personne n'a
	 * pas les droits - la coche VR est presente
	 * 
	 * @return
	 */
	public boolean isNotModifiable() {
		return !isModifiable();
	}

	/**
	 * DT8076 : Visa DRH de l'entretien
	 * @param isNotModifiable
	 */
	public void setIsNotModifiable(boolean isNotModifiable) {
		
	}
	
	// bindings contenant la periode d'evaluation pour les sous composants
	// contenant les objectifs

	/**
	 * Période associée au binding <code>inputLEvaluation</code>
	 */
	public EOEvaluationPeriode recEvaluationPeriodePrec() {
		EOEvaluationPeriode record = null;
		if (inputLEvaluation != null) {
			record = inputLEvaluation.toEvaluationPeriode().toPrevPeriode();
		} 
		return record;
	}

	/**
	 * Période suivante à la periode associée au binding
	 * <code>inputLEvaluation</code>
	 * 
	 * @return
	 */
	public EOEvaluationPeriode recEvaluationPeriodeSuiv() {
		EOEvaluationPeriode record = null;
		if (inputLEvaluation != null) {
			record = inputLEvaluation.toEvaluationPeriode().toNextPeriode();
		}
		return record;
	}

	private final boolean isCanUpdateEvaluation() {
		return canUpdateEvaluation;
	}

	private final void setCanUpdateEvaluation(boolean canUpdateEvaluation) {
		this.canUpdateEvaluation = canUpdateEvaluation;
	}

	/**
	 * On peut modifier les objectifs précédents si le droit est explicitement
	 * donné (cas des nouveaux entrants)
	 * 
	 * @return
	 */
	public final boolean isModificationObjPrecAutorisee() {
		boolean result = false;

		result = inputLEvaluation.isModificationObjPrecAutorisee(session.individuConnecte());

		return result;
	}

	
	/**
	 * Methode permettant de gerer les évolutions de FEVE en 2014.
	 * L'onglet aptitude n'est affiché que pour les anciennes evaluations.
	 * @return
	 */
	public boolean isOngletAptitudeDisplayed() {
		NSArray<EOTplOnglet> listeOnglets = getEoTplOngletArray();
		for (EOTplOnglet onglet : listeOnglets) {
			if (EOTplOnglet.TPL_ONGLET_EVA_APTITUDE_CODE.equals(onglet.tonCode())) {
				return true;
			}
		}
		return false;
	}

}