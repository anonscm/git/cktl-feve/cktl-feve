package org.cocktail.feve.components.evaluation;
/*
 * Copyright Universit� de La Rochelle 2005
 *
 * ctarade@univ-lr.fr
 *
 * Ce logiciel est un programme informatique servant � g�rer les comptes
 * informatiques des utilisateurs. 
 * 
 * Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilit� au code source et des droits de copie,
 * de modification et de redistribution accord�s par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
 * seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les conc�dants successifs.

 * A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 * associ�s au chargement,  � l'utilisation,  � la modification et/ou au
 * d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 * manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 * avertis poss�dant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
 * logiciel � leurs besoins dans des conditions permettant d'assurer la
 * s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 * � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

 * Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accept� les
 * termes.
 */

import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOSituActivite;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public class CompPageEvalSituActi extends FeveWebComponent {

	public EOEvaluation inputLEvaluation; 	// l'evaluation en entree

	public boolean disabled;	// autorise-t-on la modification ?

	public boolean isModeModifierChampLibre1; // le mode modifier champ libre est-il on ?
	public boolean isModeModifierChampLibre2; 
	public boolean isModeModifierChampLibre3; 
	public boolean isModeModifierChampLibre4; 
	public boolean isModeModifierChampLibre5; 


	public CompPageEvalSituActi(WOContext context) {
		super(context);
		isModeModifierChampLibre1 = false;
		isModeModifierChampLibre2 = false;
		isModeModifierChampLibre3 = false;
		isModeModifierChampLibre4 = false;
		isModeModifierChampLibre5 = false;	
	}


	public boolean isFonctionsEncadrementOuConduiteProjet() {

		NSArray fdPs = inputLEvaluation.tosLastFicheDePoste();

		if (fdPs.count() > 0) {
			// on choisit la dernière fiche
			EOFicheDePoste fiche = (EOFicheDePoste) fdPs.objectAtIndex(0);
			return "O".equals(fiche.fonctionConduiteProjet())
					|| "O".equals(fiche.fonctionEncadrement());
		}

		return false;
	}


	public EOSituActivite getActiviteCompetencePro() {
		initEvaluations();
		EOQualifier qual = EOSituActivite.EVA_TYPE.eq(1);
		return inputLEvaluation.tosSituActivite(qual).get(0);
	}

	public EOSituActivite getActiviteService() {
		initEvaluations();
		EOQualifier qual = EOSituActivite.EVA_TYPE.eq(2);
		return inputLEvaluation.tosSituActivite(qual).get(0);
	}

	public EOSituActivite getCapacitePro() {
		initEvaluations();
		EOQualifier qual = EOSituActivite.EVA_TYPE.eq(3);
		return inputLEvaluation.tosSituActivite(qual).get(0);
	}

	public EOSituActivite getAptitudeEncadrement() {
		initEvaluations();
		EOQualifier qual = EOSituActivite.EVA_TYPE.eq(4);
		return inputLEvaluation.tosSituActivite(qual).get(0);
	}

	public EOSituActivite getAcquisExperiencePro() {
		initEvaluations();
		EOQualifier qual = EOSituActivite.EVA_TYPE.eq(5);
		return inputLEvaluation.tosSituActivite(qual).get(0);
	}

	private void initEvaluations() {
		NSTimestamp dCreation = new NSTimestamp();
		EOQualifier qual = EOSituActivite.EVA_TYPE.eq(1);
		NSArray<EOSituActivite> activites = inputLEvaluation.tosSituActivite(qual);
		if (activites.size() == 0) {
			EOSituActivite.create(ec, dCreation, null, 1, inputLEvaluation);
			ec.saveChanges();
		}
		qual = EOSituActivite.EVA_TYPE.eq(2);
		activites = inputLEvaluation.tosSituActivite(qual);
		if (activites.size() == 0) {
			EOSituActivite.create(ec, dCreation, null, 2, inputLEvaluation);
			ec.saveChanges();
		}
		qual = EOSituActivite.EVA_TYPE.eq(3);
		activites = inputLEvaluation.tosSituActivite(qual);		
		if (activites.size() == 0) {
			EOSituActivite.create(ec, dCreation, null, 3, inputLEvaluation);
			ec.saveChanges();
		}
		qual = EOSituActivite.EVA_TYPE.eq(4);
		activites = inputLEvaluation.tosSituActivite(qual);		
		if (activites.size() == 0) {
			EOSituActivite.create(ec, dCreation, null, 4, inputLEvaluation);
			ec.saveChanges();
		}
		qual = EOSituActivite.EVA_TYPE.eq(5);
		activites = inputLEvaluation.tosSituActivite(qual);		
		if (activites.size() == 0) {
			EOSituActivite.create(ec, dCreation, null, 5, inputLEvaluation);
			ec.saveChanges();
		}
	}

	public boolean isDisabledChampLibre1() {
		return disabled || !isModeModifierChampLibre1;
	}

	public boolean isDisabledChampLibre2() {
		return disabled || !isModeModifierChampLibre2;
	}

	public boolean isDisabledChampLibre3() {
		return disabled || !isModeModifierChampLibre3;
	}

	public boolean isDisabledChampLibre4() {
		return disabled || !isModeModifierChampLibre4;
	}

	public boolean isDisabledChampLibre5() {
		return disabled || !isModeModifierChampLibre5;
	}

	public WOComponent activerModeModifierChampLibre1() {
		isModeModifierChampLibre1 = true;
		return neFaitRien();
	}

	public WOComponent activerModeModifierChampLibre2() {
		isModeModifierChampLibre2 = true;
		return neFaitRien();
	}

	public WOComponent activerModeModifierChampLibre3() {
		isModeModifierChampLibre3 = true;
		return neFaitRien();
	}

	public WOComponent activerModeModifierChampLibre4() {
		isModeModifierChampLibre4 = true;
		return neFaitRien();
	}

	public WOComponent activerModeModifierChampLibre5() {
		isModeModifierChampLibre5 = true;
		return neFaitRien();
	}

	public WOComponent desactiverModeModifierChampLibre1() {
		isModeModifierChampLibre1 = false;
		return neFaitRien();
	}

	public WOComponent desactiverModeModifierChampLibre2() {
		isModeModifierChampLibre2 = false;
		return neFaitRien();
	}

	public WOComponent desactiverModeModifierChampLibre3() {
		isModeModifierChampLibre3 = false;
		return neFaitRien();
	}

	public WOComponent desactiverModeModifierChampLibre4() {
		isModeModifierChampLibre4 = false;
		return neFaitRien();
	}

	public WOComponent desactiverModeModifierChampLibre5() {
		isModeModifierChampLibre5 = false;
		return neFaitRien();
	}

	public WOComponent modifierChampLibre1() {
		session.setOnLoad("document.location='#TextCompetencesPro';");      
		activerModeModifierChampLibre1();
		return neFaitRien();
	}

	public WOComponent modifierChampLibre2() {
		session.setOnLoad("document.location='#TextContributionActiviteService';");      
		activerModeModifierChampLibre2();
		return neFaitRien();
	}

	public WOComponent modifierChampLibre3() {
		session.setOnLoad("document.location='#TextCapacitesPro';");      
		activerModeModifierChampLibre3();
		return neFaitRien();
	}

	public WOComponent modifierChampLibre4() {
		session.setOnLoad("document.location='#TextAptitudesEncadrement';");      
		activerModeModifierChampLibre4();
		return neFaitRien();
	}

	public WOComponent modifierChampLibre5() {
		session.setOnLoad("document.location='#TextAcquisExperience';");      
		activerModeModifierChampLibre5();
		return neFaitRien();
	}

	public WOComponent enregistrerChampLibre1() throws Throwable {
		desactiverModeModifierChampLibre1();
		UtilDb.save(ec, "");
		return neFaitRien();
	}

	public WOComponent enregistrerChampLibre2() throws Throwable {
		desactiverModeModifierChampLibre2();
		UtilDb.save(ec, "");
		return neFaitRien();
	}

	public WOComponent enregistrerChampLibre3() throws Throwable {
		desactiverModeModifierChampLibre3();
		UtilDb.save(ec, "");
		return neFaitRien();
	}

	public WOComponent enregistrerChampLibre4() throws Throwable {
		desactiverModeModifierChampLibre4();
		UtilDb.save(ec, "");
		return neFaitRien();
	}

	public WOComponent enregistrerChampLibre5() throws Throwable {
		desactiverModeModifierChampLibre5();
		UtilDb.save(ec, "");
		return neFaitRien();
	}

	public WOComponent annulerEnregisterChampLibre1() {
		session.setOnLoad("document.location='#TextCompetencesPro';");      
		desactiverModeModifierChampLibre1();
		ec.revert();
		return neFaitRien();
	}

	public WOComponent annulerEnregisterChampLibre2() {
		session.setOnLoad("document.location='#TextContributionActiviteService';");      
		desactiverModeModifierChampLibre2();
		ec.revert();
		return neFaitRien();
	}

	public WOComponent annulerEnregisterChampLibre3() {
		session.setOnLoad("document.location='#TextCapacitesPro';");      
		desactiverModeModifierChampLibre3();
		ec.revert();
		return neFaitRien();
	}

	public WOComponent annulerEnregisterChampLibre4() {
		session.setOnLoad("document.location='#TextAptitudesEncadrement';");      
		desactiverModeModifierChampLibre4();
		ec.revert();
		return neFaitRien();
	}

	public WOComponent annulerEnregisterChampLibre5() {
		session.setOnLoad("document.location='#TextAcquisExperience';");      
		desactiverModeModifierChampLibre5();
		ec.revert();
		return neFaitRien();
	}

}