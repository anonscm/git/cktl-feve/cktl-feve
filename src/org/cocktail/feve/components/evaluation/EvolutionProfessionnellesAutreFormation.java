package org.cocktail.feve.components.evaluation;


import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EODif;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp;
import org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;



public class EvolutionProfessionnellesAutreFormation extends FeveWebComponent {
	
	private static final long serialVersionUID = 3273953719824490071L;
	//variables
	private EOEvaluation inEvaluation;
	public boolean disable;
	public boolean isModeModifTableau;
	private boolean isAjouterCompetence;
//	private boolean isModificationEvolutionProCommentaire;
	private boolean isModificationAutreFormationCommentaire;
	
	
	private String evolutionActivitesStr;
	private String evolutioncarriereStr;
	
	private String newPeriode;
	private String newCompetence;
	
	private NSMutableArray<RepartCompetenceFormationBoolean> repartCompetenceBooleans;
	public RepartCompetenceFormationBoolean currentRepartCompetenceBoolean;
	
	public final String ANCRE_AUTRE_FORMATION = "aaf";
	public final String ANCRE_AUTRE_FORMATION_LIBRE = "aafl";
	
	private EORepartPerspectiveFormation currentRepartCompetences;
	
	private NSMutableArray<EORepartNiveauComp> listeRepartCompetences;
	private String mobilisationDif;
	
    public EvolutionProfessionnellesAutreFormation(WOContext context) {
        super(context);
        isModeModifTableau = true;
        
        listeRepartCompetences = new NSMutableArray<EORepartNiveauComp>();
        repartCompetenceBooleans = new NSMutableArray<RepartCompetenceFormationBoolean>();
        
    }

    
    private void ancreAutreFormation() {
		session.setOnLoad("document.location='#" + ANCRE_AUTRE_FORMATION + "';");
	}
    
    private void ancreAutreFormationLibre() {
		session.setOnLoad("document.location='#" + ANCRE_AUTRE_FORMATION_LIBRE + "';");
	}
    
    
    private void initMapRepartCompetencePoste() {
    	repartCompetenceBooleans.clear();
    	NSArray<EORepartPerspectiveFormation> rcps = inEvaluation.tosRepartPerspectiveFormations();
    	for (EORepartPerspectiveFormation rcp : rcps) {
    		repartCompetenceBooleans.add(new RepartCompetenceFormationBoolean(rcp, false));
    	}
    }
    
    
    public void addRepartCompetencePoste(String libelle, String periode) {
    	EORepartPerspectiveFormation cmpPoste = EORepartPerspectiveFormation.create(ec, periode,  inEvaluation.evaKeyVisible(), libelle, inEvaluation);
    	repartCompetenceBooleans.add(new RepartCompetenceFormationBoolean(cmpPoste, false));
    }
    
    
    public void alimenterCompetence() {
    	EOQualifier evaluation = EORepartNiveauComp.TO_EVALUATION.eq(inEvaluation);
    	NSArray<EORepartNiveauComp> competences = EORepartNiveauComp.fetchAll(ec, evaluation, null);
    	
    	for (EORepartNiveauComp repartNiveauComp : competences) {
    		if ((repartNiveauComp.toNiveauCompetence().ncpPosition() == 1) || (repartNiveauComp.toNiveauCompetence().ncpPosition() == 2)) {
    			listeRepartCompetences.add(repartNiveauComp);
    		}
    	}
    }
    
    
    public String getValeurDif() {
    	
    	EOQualifier qualifier = EODif.INDIVIDU.eq(inEvaluation.toIndividu());
		
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EODif.DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));
	
		int totalDif=0;
		
		EODif eodif =EODif.fetchFirstByQualifier(ec, qualifier,sortOrderings);
					
		if (eodif!=null)
		{
			totalDif=eodif.balance().intValue();
		}
    	
    	return "" + totalDif;
    }
    
    
    
    //action des bouttons -------------------------------------------
    
    
	public WOActionResults supprimerCompetence() {
		ancreAutreFormation();
		RepartCompetenceFormationBoolean tmpRepartCompetencePoste;
		for (RepartCompetenceFormationBoolean comp : repartCompetenceBooleans) {
			if (comp.getRepartCompetence().primaryKey().equals(currentRepartCompetenceBoolean.getRepartCompetence().primaryKey())) {
				tmpRepartCompetencePoste = currentRepartCompetenceBoolean;
			}
			currentRepartCompetenceBoolean.getRepartCompetence().delete();
		}
		
		
		ec.saveChanges();
		return null;
	}

    
    
	public WOActionResults addCompetence() {
		isModeModifTableau = true;
		ancreAutreFormation();
		if (newCompetence != null) {
			if (newPeriode == null) {
				addRepartCompetencePoste(newCompetence, "");
			} else {
				addRepartCompetencePoste(newCompetence, newPeriode);
			}
		}
		edc().saveChanges();
		newCompetence = "";
		newPeriode = "";
		setAjouterCompetence(false);
		return null;
	}
	
    
    
    public WOActionResults annulerAddCompetencePoste() {
    	isModeModifTableau = true;
    	ancreAutreFormation();
		setAjouterCompetence(false);
    	newCompetence = newPeriode = "";
    	return null;
    }
    
	
	public WOActionResults annulerModificationCompetencePoste() {
		isModeModifTableau = true;
		currentRepartCompetenceBoolean.setModifie(false);
		ec.revert();
		return null;
	}

	public WOActionResults validerModifierCompetence() throws Exception {
		isModeModifTableau = true;
		ancreAutreFormation();
		ec.saveChanges();
		currentRepartCompetenceBoolean.setModifie(false);
		return null;
	}
	
    
	public WOActionResults modifierCompetence() {
		isModeModifTableau = false;
		ancreAutreFormation();
		currentRepartCompetenceBoolean.setModifie(true);
		return null;
	}
	
	//contener id
	public String containerCompetencePosteId() {
        return getComponentId() + "_selection";
	}
	
	public WOActionResults enregistrerEvolutionProCommentaire() {
		ancreAutreFormationLibre();
		isModificationAutreFormationCommentaire = false;
		ec.saveChanges();
		return null;
	}
	
	public WOActionResults annulerEvolutionProCommentaire() {
		ancreAutreFormationLibre();
		isModificationAutreFormationCommentaire = false;
		ec.revert();
		return null;
	}
	
	public WOActionResults modifierEvolutionProCommentaire() {
		ancreAutreFormationLibre();
		isModificationAutreFormationCommentaire = true;
		return null;
	}
    //geter seteur -------------------------------------------------------------------------
    
	
    
	public EOEvaluation getInEvaluation() {
		return inEvaluation;
	}

	public void setInEvaluation(EOEvaluation inEvaluation) {
		this.inEvaluation = inEvaluation;
		repartCompetenceBooleans.clear();
		initMapRepartCompetencePoste();
	}


	public EORepartPerspectiveFormation getCurrentRepartCompetencesPoste() {
		return currentRepartCompetences;
	}



	public void setCurrentRepartCompetencesPoste(
			EORepartPerspectiveFormation currentRepartCompetences) {
		this.currentRepartCompetences = currentRepartCompetences;
	}


	public boolean isAjouterCompetence() {
		return isAjouterCompetence;
	}


	public void setAjouterCompetence(boolean isAjouterCompetence) {
		this.isAjouterCompetence = isAjouterCompetence;
	}
	
	public void setAjouterCompetenceTrue() {
		isModeModifTableau = false;
		this.isAjouterCompetence = true;
	}
	
	public void setAjouterCompetenceFalse() {
		this.isAjouterCompetence = false;
	}


	public String getNewPeriodePoste() {
		return newPeriode;
	}


	public void setNewPeriodePoste(String newPeriodePoste) {
		this.newPeriode = newPeriodePoste;
	}


	public String getNewCompetence() {
		return newCompetence;
	}


	public void setNewCompetence(String newCompetence) {
		this.newCompetence = newCompetence;
	}


	public String getEvolutionActivitesStr() {
		return evolutionActivitesStr;
	}


	public void setEvolutionActivitesStr(String evolutionActivitesStr) {
		this.evolutionActivitesStr = evolutionActivitesStr;
	}


	public String getEvolutioncarriereStr() {
		return evolutioncarriereStr;
	}


	public void setEvolutioncarriereStr(String evolutioncarriereStr) {
		this.evolutioncarriereStr = evolutioncarriereStr;
	}

	
	public boolean isModeModifierCompetence() {
		return currentRepartCompetenceBoolean.isModifie();
	}


	public NSArray<RepartCompetenceFormationBoolean> getRepartCompetenceBooleans() {
		return repartCompetenceBooleans;
	}


	public boolean isModificationEvolutionProCommentaire() {
		return !isModificationAutreFormationCommentaire;
	}


	public void setModificationEvolutionProCommentaire(
			boolean isModificationEvolutionProCommentaire) {
		this.isModificationAutreFormationCommentaire = isModificationEvolutionProCommentaire;
	}


	public boolean isModificationAutreFormationCommentaire() {
		return isModificationAutreFormationCommentaire;
	}


	public void setModificationAutreFormationCommentaire(
			boolean isModificationAutreFormationCommentaire) {
		this.isModificationAutreFormationCommentaire = isModificationAutreFormationCommentaire;
	}


	public class RepartCompetenceFormationBoolean {
		private EORepartPerspectiveFormation repartCompetence;
		private boolean isModifie;
		
		public RepartCompetenceFormationBoolean(EORepartPerspectiveFormation repart, boolean bool) {
			repartCompetence = repart;
			isModifie = bool;
		}

		public EORepartPerspectiveFormation getRepartCompetence() {
			return repartCompetence;
		}

		public void setRepartCompetence(EORepartPerspectiveFormation repartCompetence) {
			this.repartCompetence = repartCompetence;
		}

		public boolean isModifie() {
			return isModifie;
		}

		public void setModifie(boolean isModifie) {
			this.isModifie = isModifie;
		}
		
		
		public boolean mobilisationDif() {
			return ("O".equals(inEvaluation.modiliserDif()));
		}
		
		public void setMobilisationDif(boolean value) {
			inEvaluation.setModiliserDif((value)?"O":"N");
			ec.saveChanges();
		}
		
		public void savea() {
			ec.saveChanges();
		}
		
		public String toString() {
			String isModifieStr;
			if (isModifie) {
				isModifieStr = "O";
			} else {
				isModifieStr = "N";
			}
			return "repartCompetence: " + repartCompetence.toString() + "boolean: " + isModifieStr;
		}
		
	}


	public boolean formationPrevus() {
		return ("O".equals(inEvaluation.modiliserDif()));
	}
    
	public void setFormationPrevus(boolean value) {
		inEvaluation.setModiliserDif((value)?"O":"N");
		ec.saveChanges();
	}

	
}