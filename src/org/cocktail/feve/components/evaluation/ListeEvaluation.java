package org.cocktail.feve.components.evaluation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.feve.components.common.ListeRecordControled;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.util.UtilEOEvaluationKeyValueCoding;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;
import org.codehaus.plexus.util.StringUtils;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
//import com.lowagie.text.List;


/**
 * TODO urgent migrer vcomprendrecomprendreers la gestion des controleurs Composant permettant
 * l'affichage d'une liste d'evaluations.
 * 
 * @author Cyril Tarade <cyril.tarade at univ-lr.fr>
 */
public class ListeEvaluation
		extends ListeRecordControled {

	// liste des periodes
	public EOEvaluationPeriode periodeItem;
	private EOEvaluationPeriode periodeSelected;

	public boolean isPageAcceuil = false;
	
	public NSArray<EOEvaluationPeriode> periodes;
	public NSArray<EOVCandidatEvaluation> listeEvaluations;
	public NSArray<EOVCandidatEvaluation> listeEvaluationsNonFiltre;
	
	public NSArray<EOStructure> listeStructure;
	private EOStructure structureSelected;
	public EOStructure structureItem;
	
	private Map<IStructure, NSMutableArray<EOVCandidatEvaluation>> hashEvaluationParStructure = new HashMap<IStructure, NSMutableArray<EOVCandidatEvaluation>>();
	
	/**
	 * indique si l'utilisateur connecte peut creer l'evaluation
	 * <code>itemEvaluation()</code>
	 */
	private boolean canCreateEvaluation;
	/**
	 * indique si l'utilisateur connecte peut modifier l'evaluation
	 * <code>itemEvaluation()</code>
	 */
	public boolean canUpdateEvaluation;
	/**
	 * indique si l'utilisateur connecte peut visualiser l'evaluation
	 * <code>itemEvaluation()</code>
	 */
	public boolean canViewEvaluation;
	/**
	 * indique si l'utilisateur connecte ne peut acceder � l'evaluation
	 * <code>itemEvaluation()</code>
	 */
	public boolean isDisabledLnkSelectEvaluation;
	/**
	 * Indique si l'utilisateur connecté peut modifier les objectifs précédents
	 */
	public boolean canModifierObjectifsPrecedents;

	/** l'objet itemRecord pointant sur un enregistrement <code>EOEvaluation</code> */
	public EOVCandidatEvaluation itemEvaluation;

	public ListeEvaluation(WOContext context) {
		super(context);
		initComponent();
	}

	private void initComponent() {
		periodeList();
		periodeSelected = EOEvaluationPeriode.getCurrentPeriodeElseLast(edc());
		nomPrenom = STR_SEARCH_DEFAULT_VALUE;
	}
	
	
	
	
	public void initHashEvaluationParStructure() {
		
		if (!hashEvaluationParStructure.containsKey(getStructureSelected())) {
				
			NSArray<EOIndividu> listeIndividu = (NSArray<EOIndividu>) session.getAutorisation().getIndividusGererEntretienProfessionnel(getStructureSelected());
			
			if (CollectionUtils.isNotEmpty(listeIndividu)) {
				EOQualifier qual = ERXQ.in(EOVCandidatEvaluation.TO_INDIVIDU_KEY, listeIndividu);
				NSArray<EOVCandidatEvaluation> listeEvaluationsTmp = EOVCandidatEvaluation.fetchAll(edc(), qual, null);
				
				for (EOVCandidatEvaluation vCandidatEvaluation : listeEvaluationsTmp) {
					
					if (hashEvaluationParStructure.containsKey(getStructureSelected())) {
						if (!((NSMutableArray<EOVCandidatEvaluation>) hashEvaluationParStructure.get(getStructureSelected())).contains(vCandidatEvaluation)) {
							((NSMutableArray<EOVCandidatEvaluation>) hashEvaluationParStructure.get(getStructureSelected())).add(vCandidatEvaluation);
						}
					} else {
						hashEvaluationParStructure.put(getStructureSelected(), new NSMutableArray<EOVCandidatEvaluation>(vCandidatEvaluation));
					}
						
				}
				
			}
			
		}
	}
	
	public List<IStructure> getListeStructureAffichage() {
		return session.getAutorisation().getAllShowStructuresGererEntretienProfessionnel();
	}
	
	/**
	 * Toutes les periodes d'evaluation
	 */
	public NSArray<EOEvaluationPeriode> periodeList() {
		if (NSArrayCtrl.isEmpty(periodes)) {
			periodes = EOEvaluationPeriode.fetchAll(ec, CktlSort.newSort(EOEvaluationPeriode.EPE_D_DEBUT_KEY));
		}
		return periodes;
	}

	/**
	 * Retourne une liste d'enregistrements de l'entite
	 * <code>VCandidatEvaluation</code>
	 */
	public NSArray listRecords() {
		return getListeEvaluationForStructure();
	}
	
	
	private NSArray<EOVCandidatEvaluation> getListeEvaluationForStructure() {
		
		if (isPageAcceuil) {
			EOQualifier qualEvaluation = EOVCandidatEvaluation.TO_INDIVIDU.eq(session.individuConnecte());
			listeEvaluations = EOVCandidatEvaluation.fetchAll(ec, qualEvaluation, null);
			return listeEvaluations;
		}
		
		String filtreNom = nomPrenom;
		
		
		if (!isFiltreNomRenseigne(filtreNom) && getStructureSelected() == null) {
			return null;
		}
		
		NSArray<EOVCandidatEvaluation> result = new NSMutableArray<EOVCandidatEvaluation>();
		NSArray<EOVCandidatEvaluation> listesEvaluationTmp = new NSMutableArray<EOVCandidatEvaluation>();
		if (getStructureSelected() != null) {
			ajouterListeEvaluationTmpSelonStructureEtPeriode(listesEvaluationTmp, getStructureSelected());
		} else {
			
			if (isFiltreNomRenseigne(filtreNom)) {
				listesEvaluationTmp = EOVCandidatEvaluation.fetch(edc(), ERXQ.and(ERXQ.equals(EOVCandidatEvaluation.NOM_USUEL_KEY, StringUtils.upperCase(filtreNom)), 
																					ERXQ.equals(EOVCandidatEvaluation.TO_EVALUATION_PERIODE_KEY, getPeriodeSelected())), 
																	null);
			} else {
				Set<IStructure> it = hashEvaluationParStructure.keySet();
				for (IStructure eoStructure : it) {
					ajouterListeEvaluationTmpSelonStructureEtPeriode(listesEvaluationTmp, eoStructure);
				}
			}
		}
		
		listesEvaluationTmp = NSArrayCtrl.removeDuplicate(listesEvaluationTmp);
		
		for (EOVCandidatEvaluation vEvaluation : listesEvaluationTmp) {
			
			if (isFiltreNomRenseigne(filtreNom) && getStructureSelected() == null) {
				
				if (session.getAutorisation().hasDroitConnaissanceGererEntretienProfessionnel(vEvaluation.toIndividu())) {
					if (vEvaluation != null) {
						result.add(vEvaluation); 
					}
				}
				
			} else {

				if (session.getAutorisation().hasDroitConnaissanceGererEntretienProfessionnel(vEvaluation.toIndividu(), getStructureSelected())) {
					if (vEvaluation != null) {
						if (!isFiltreNomRenseigne(filtreNom)) {
							result.add(vEvaluation);
							
						} else {
							if (vEvaluation.nomUsuel().equals(filtreNom.toUpperCase())) {
								result.add(vEvaluation); 
							}
						}
					}
				}
			}
		}
		
		result = NSArrayCtrl.removeDuplicate(result);
		result = CktlSort.sortedArray(result, EOVCandidatEvaluation.NOM_USUEL_KEY);
		listeEvaluations = result;
		return result;
	}

	private void ajouterListeEvaluationTmpSelonStructureEtPeriode(
			NSArray<EOVCandidatEvaluation> listesEvaluationTmp,
			IStructure eoStructure) {
		NSMutableArray<EOVCandidatEvaluation> evaluationStructure = hashEvaluationParStructure.get(eoStructure);
		if (!NSArrayCtrl.isEmpty(evaluationStructure)) {
			for (EOVCandidatEvaluation eovCandidatEvaluation : evaluationStructure) {
				if (eovCandidatEvaluation.toEvaluationPeriode().equals(getPeriodeSelected())) {
					listesEvaluationTmp.add(eovCandidatEvaluation);
				}
			}
		}
	}
	
	
	private boolean isFiltreNomRenseigne(String filtreNom) {
		return StringUtils.isNotEmpty(filtreNom) && !filtreNom.equals(STR_SEARCH_DEFAULT_VALUE);
	}
	
	
	public int mode() {
		return MODE_EVALUATION;
	}


	/**
	 * coche responsable RH : seuls les administrateurs y ont accès, et si
	 * l'enregistrement evaluation existe
	 */
	public boolean disabledLaCocheResp() {
		boolean isDisabled = true;
		
		if (session.getAutorisation().isAdmin() || session.getAutorisation().isDrh()) {
			isDisabled = false;
		}

		if (isDisabled == false &&
				isEvaluationExisting() == false) {
			isDisabled = true;
		}

		return isDisabled;
	}

	/**
	 * Indique s'il faut afficher un avertissement sur la liste des evaluations
	 */
	public boolean hasWarning() {
		boolean result = false;
		if (listeEvaluations == null) {
			return result;
		}
		
		for (int i = 0; i < listeEvaluations.count(); i++) {
			EOVCandidatEvaluation eva = ((UtilEOEvaluationKeyValueCoding) listeEvaluations.objectAtIndex(i)).toVCandidatEvaluation();
			if (eva != null && eva.toEvaluation() != null) {
				boolean canModifEva = session.getAutorisation().hasDroitUtilisationGererEntretienProfessionnel(eva.toIndividu()) ||  session.getAutorisation().isAdmin();
				
				
				if (!eva.isViseParResponsableRh() && canModifEva) {
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public NSArray<EOVCandidatEvaluation> getListeEvaluations() {
		return listeEvaluations;
	}
	
	/**
	 * L'agent connecte peut il modifier l'evaluation en cours. Si elle existe
	 * alors on verifie si cette derniere appartient a la liste des evaluations
	 * modifiables.
	 */
	public boolean canModifItemRecord() {
		return canUpdateEvaluation;
	}

	/**
	 * L'evaluation est verrouillee si vis�e, ou bien si l'enregistrement n'existe
	 * pas encore.
	 */
	public boolean isLockedItemRecord() {
		return itemEvaluation == null || itemEvaluation.isViseParResponsableRh();
	}

	/**
	 * Indique si l'enregistrement <code>EOEvaluation</code> existe pour un
	 * <code>itemRecord</code> donn�.
	 * 
	 * @return
	 */
	public boolean isEvaluationExisting() {
		if (itemEvaluation == null) {
			return false;
		} 
		
		return itemEvaluation.toEvaluation() != null;	
	}

	// navigation

	/**
	 * Effacer la zone de recherche
	 */
	public WOComponent clearNomPrenom() {
		nomPrenom = STR_SEARCH_DEFAULT_VALUE;
		return null;
	}

	/**
	 * Retourne une page indique la liste des avertissement
	 */
	public WOComponent afficherWarning() {
		StringBuffer buff = new StringBuffer("<b>liste des entretiens professionnels &agrave; viser</b><br><br><ul>");
		for (int i = 0; i < listeEvaluations.count(); i++) {
			EOVCandidatEvaluation eva = ((UtilEOEvaluationKeyValueCoding) listeEvaluations.objectAtIndex(i)).toVCandidatEvaluation();
			
			boolean canModifEva = session.getAutorisation().isAdmin() || session.getAutorisation().hasDroitUtilisationGererEntretienProfessionnel(eva.toIndividu());
			
			// on ne comptabilise que les evaluation cr�es
			if (eva != null && eva.toEvaluation() != null && !eva.isViseParResponsableRh() && canModifEva) {
				buff.append("<li>");
				buff.append(eva.display());
				buff.append("</li>");
			}
		}
		buff.append("</ul>");
		return CktlAlertPage.newAlertPageWithCaller(this.parent(),
				"Avertissement sur les entretiens professionnels", buff.toString(), "Retourner", CktlAlertPage.ATTENTION);
	}

	/**
	 * La création doit être transparente pour l'utilisateur : si l'enregistrement
	 * n'existe pas, on la créé et on l'affiche, comme si elle existait déjà
	 * 
	 * @return
	 * @throws Throwable
	 */
	public WOComponent doCreateAndClicEvaluation() throws Throwable {
		session.isAfficherBouttonMiniCV = false;
		EOIndividu individuResponsable = getIndividuResponsable(itemEvaluation.toIndividu());
		
		EOEvaluation.doSelectEvaluation(
				ec, itemEvaluation, getPeriodeSelected(), session.individuConnecte(), individuResponsable);
		clicRecord();
		return null;
	}

	
	private EOIndividu getIndividuResponsable(EOIndividu individu) {
		NSArray<EORepartStructure> repartStructures;
		EOQualifier qualRepartStructures = ERXQ.and(EORepartStructure.PERS_ID.eq(individu.persId()), 
				new EOKeyValueQualifier(EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY, EOQualifier.QualifierOperatorContains, 
						EOTypeGroupe.TGRP_CODE_S));
		repartStructures = EORepartStructure.fetchAll(ec, qualRepartStructures);
		
		for (EORepartStructure repartStructure : repartStructures) {
			EOStructure structure = repartStructure.toStructureGroupe();
			if (structure.toResponsable() != null) {
				if (structure.toResponsable().equals(individu)) {
					// cas où l'individu est responsable de sa structure
					return structure.toStructurePere().toResponsable();
				} else {
					return structure.toResponsable();
				}
			}
		}
		return null;
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public ListeEvaluationCtrl ctrl() {
		return (ListeEvaluationCtrl) ctrl;
	}

	/**
	 * TODO ne plus utiliser directement (utiliser
	 * {@link #doCreateAndClicEvaluation()}
	 * 
	 * surcharge de la methode afin de passer dans <code>selectedRecord</code> 
	 * l'enregistrement <code>EOEvaluation</code> plutot que l'enregistrement
	 * <code>VCandidatEvaluation</code>
	 */
	@Override
	public WOComponent clicRecord() {
		selectedRecord = itemEvaluation;
		ctrl().doSelectEvaluation(itemEvaluation);
		return neFaitRien();
	}

	@Override
	public WOComponent hideRecord() {
		ctrl().doDeselectEvaluation();
		return super.hideRecord();
	}

	// display

	/**
	 * Message d'information sur la periode d'ouverture de saisie des evaluation
	 * 
	 * @return
	 */
	public String getInfoPeriodeEvaluation() {
		StringBuffer sb = new StringBuffer();
		sb.append("La saisie est autoris&eacute;e du ");
		sb.append(DateCtrl.dateToString(app.getEvaluationSaisieDDebut()));
		sb.append(" au ");
		sb.append(DateCtrl.dateToString(app.getEvaluationSaisieDFin()));
		sb.append("<br/>");

		if (app.isPeriodeEvaluationOuverte()) {
			sb.append("Vous pouvez saisir vos entretiens professionnels actuels.");
		} else {
			sb.append("<font class=textError>");
			sb.append("Vous ne pouvez pas saisir d'entretien professionnel.");
			if (session.getAutorisation().isAdmin()) {
				sb.append(" (<u>SAUF VOUS</u>, car vous &ecirc;tes administrateur!)");
			}
			sb.append("</font>");
		}
		return sb.toString();
	}

	// setters

	/**
	 * Interception de l'evaluation <code>itemRecord</code> pour determiner les
	 * droits qui s'y appliquent
	 */
	public void setItemRecord(AfwkGRHRecord value) {
		itemRecord = value;

		if (itemRecord != null) {
			itemEvaluation = ((UtilEOEvaluationKeyValueCoding) itemRecord).toVCandidatEvaluation();

			Object cible = itemEvaluation;
			if (cible == null) {
				cible = itemRecord;
			}

			isDisabledLnkSelectEvaluation = false;
			
			boolean isDrh = session.getAutorisation().isDrh();
			boolean isAdmin = session.getAutorisation().isAdmin();
			
			
			canCreateEvaluation = session.getAutorisation().hasDroitUtilisationGererEntretienProfessionnel(itemEvaluation.toIndividu()) || isAdmin;
			canUpdateEvaluation = canCreateEvaluation;
			canViewEvaluation = session.getAutorisation().hasDroitConnaissanceGererEntretienProfessionnel(itemEvaluation.toIndividu()) || isDrh || isAdmin;

			// controle supplémentaire : si l'évaluation n'est pas crée et que
			// la personne connectée n'a pas le droit de la créer, on bloque
			if (isDisabledLnkSelectEvaluation == false) {
				if (isEvaluationExisting() == false && canCreateEvaluation == false) {
					isDisabledLnkSelectEvaluation = true;
				}
			}
			
			canModifierObjectifsPrecedents = itemEvaluation.isModificationObjPrecAutorisee(session.individuConnecte());
		}

	}

	public boolean isPageAcceuil() {
		return isPageAcceuil;
	}

	public void setPageAcceuil(boolean isPageAcceuil) {
		this.isPageAcceuil = isPageAcceuil;
	}

	public EOEvaluationPeriode getPeriodeSelected() {
		return periodeSelected;
	}

	public void setPeriodeSelected(EOEvaluationPeriode periodeSelected) {
		this.periodeSelected = periodeSelected;
	}

	public EOStructure getStructureSelected() {
		return structureSelected;
	}

	public void setStructureSelected(EOStructure structureSelected) {
		this.structureSelected = structureSelected;
		initHashEvaluationParStructure();
	}
	

}