package org.cocktail.feve.components.evaluation;


import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EORepartCompetencePoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EvolutionProfessionnelles extends FeveWebComponent {
	
	
	//variables
	private EOEvaluation inEvaluation;
	public boolean disable;
	public boolean isModeModifTableau;
	
	private boolean isModificationEvolutionActivites;
	private boolean isModificationEvolutionCarriere;
	
	private boolean isAjouterCompetencePoste;
	
	private String evolutionActivitesStr;
	private String evolutioncarriereStr;
	
	private String newPeriodePoste;
	private String newCompetencePoste;
	
	private NSMutableArray<RepartCompetencePosteBoolean> repartCompetencePosteBooleans;
	public RepartCompetencePosteBoolean currentRepartCompetencePosteBoolean;
	
	
	private NSMutableArray<EORepartCompetencePoste> listesRepartCompetencesPostes;
	private EORepartCompetencePoste currentRepartCompetencesPoste;
	
	private NSMutableArray<EORepartNiveauComp> listeRepartCompetences;
	
    public EvolutionProfessionnelles(WOContext context) {
        super(context);
        isModeModifTableau = true;
        //initialisation des boolean de modification a false
        setModificationEvolutionActivites(false);
        setModificationEvolutionCarriere(false);
        setModeModifierCompetencePoste(false);
        //--------
        setAjouterCompetencePosteFalse();
        setListesRepartCompetencesPostes(new NSMutableArray<EORepartCompetencePoste>());
        
        listeRepartCompetences = new NSMutableArray<EORepartNiveauComp>();
        
        repartCompetencePosteBooleans = new NSMutableArray<EvolutionProfessionnelles.RepartCompetencePosteBoolean>();
        
    }

    
    private void initMapRepartCompetencePoste() {
    	repartCompetencePosteBooleans.clear();
    	NSArray<EORepartCompetencePoste> rcps = inEvaluation.tosRepartCompetencePostes();
    	for (EORepartCompetencePoste rcp : rcps) {    	
    		repartCompetencePosteBooleans.add(new RepartCompetencePosteBoolean(rcp, false));
    	}
    }
    
    public void initRepartCompetencesPostes() {
    	EORepartCompetencePoste.create(ec, inEvaluation.evaKeyVisible(), "testcompetence1", "testperiode1", inEvaluation);
    	ec.saveChanges();
    }
    
    
    
    public void addRepartCompetencePoste(String libelle, String periode) {
    	EORepartCompetencePoste cmpPoste = EORepartCompetencePoste.create(ec, inEvaluation.evaKeyVisible(), libelle, periode, inEvaluation);
    	repartCompetencePosteBooleans.add(new RepartCompetencePosteBoolean(cmpPoste, false));
    }
    
    
    public WOActionResults alimenterCompetence() {
    	EOQualifier evaluation = EORepartNiveauComp.TO_EVALUATION.eq(inEvaluation);
    	NSArray<EORepartNiveauComp> competences = EORepartNiveauComp.fetchAll(ec, evaluation, null);
    	for (EORepartNiveauComp repartNiveauComp : competences) {
    		
    		if ((repartNiveauComp.toNiveauCompetence() != null )) {
    			if ((repartNiveauComp.toNiveauCompetence().ncpLibelle().equals("à acquérir")) || (repartNiveauComp.toNiveauCompetence().ncpLibelle().equals("à développer"))) {
	    			EOQualifier qualComp = EORepartCompetencePoste.LIBELLE.eq(repartNiveauComp.toRepartFdpComp().toReferensCompetences().display());
	    			NSArray<EORepartCompetencePoste> nsarray = EORepartCompetencePoste.fetchAll(ec, qualComp, null);
	    			if (nsarray.size() == 0) {
	    				EORepartCompetencePoste repartCompPoste = EORepartCompetencePoste.create(ec, inEvaluation.epeKeyVisible(), repartNiveauComp.toRepartFdpComp().toReferensCompetences().display(), "", inEvaluation);
    					RepartCompetencePosteBoolean repart = new RepartCompetencePosteBoolean(repartCompPoste, false);
	    			}
	    		}
    		}
    	}
    	ec.saveChanges();
    	initMapRepartCompetencePoste();
    	return null;
    }
    
    
	public boolean formationPrevus() {
		return ("O".equals(inEvaluation.formationPrevus()));
	}
    
	public void setFormationPrevus(boolean value) {
		inEvaluation.setFormationPrevus((value)?"O":"N");
		ec.saveChanges();
	}
	
    //action des bouttons -------------------------------------------
    
    
	public WOActionResults supprimerCompetence() {
//		String tmp = "document.location='#" + containerCompetencePosteId() + "';";
//		session.setOnLoad(tmp);
		RepartCompetencePosteBoolean tmpRepartCompetencePoste;
		for (RepartCompetencePosteBoolean comp : repartCompetencePosteBooleans) {
			if (comp.getRepartCompetence().primaryKey().equals(currentRepartCompetencePosteBoolean.getRepartCompetence().primaryKey())) {
				tmpRepartCompetencePoste = currentRepartCompetencePosteBoolean;
			}
			currentRepartCompetencePosteBoolean.getRepartCompetence().delete();
		}
		
		
		ec.saveChanges();
		return null;
	}

    
    
	public WOActionResults addCompetence() {
		isModeModifTableau = true;
//		String tmp = "document.location='#" + containerCompetencePosteId() + "';";
//		session.setOnLoad(tmp);
		if (newCompetencePoste != null) {
			if (newPeriodePoste == null) {
				addRepartCompetencePoste(newCompetencePoste, "");
				System.err.println("add");
			} else {
				addRepartCompetencePoste(newCompetencePoste, newPeriodePoste);
			}
		}
		edc().saveChanges();
		newCompetencePoste = "";
		newPeriodePoste = "";
		setAjouterCompetencePosteFalse();
		return null;
	}
	
    
    
    public WOActionResults annulerAddCompetencePoste() {
    	isModeModifTableau = true;
//    	String tmp = "document.location='#" + containerCompetencePosteId() + "';";
//		session.setOnLoad(tmp);
    	setAjouterCompetencePosteFalse();
    	newCompetencePoste = newPeriodePoste = "";
    	return null;
    }
    
	public WOActionResults enregistrerEvolutionActivites() {
		//TODO
		setModificationEvolutionActivites(false);
		return null;
	}
    
    
	public WOActionResults annulerEvolutionActivites() {
		//TODO
		setModificationEvolutionActivites(false);
		return null;
	}
	
	public WOActionResults modifierEvolutionActivites() {
		//TODO
		setModificationEvolutionActivites(true);
		return null;
	}
	
	public WOActionResults enregistrerEvolutionCarriere() {
		//TODO
		setModificationEvolutionCarriere(false);
		return null;
	}
	
	public WOActionResults annulerEvolutionCarriere() {
		//TODO
		setModificationEvolutionCarriere(false);
		return null;
	}
	
	public WOActionResults modifierEvolutionCarriere() {
		setModificationEvolutionCarriere(true);
		return null;
	}

	
	public WOActionResults annulerModificationCompetencePoste() {
		isModeModifTableau = true;
		currentRepartCompetencePosteBoolean.setModifie(false);
		ec.revert();
		return null;
	}

	public WOActionResults validerModifierCompetence() throws Exception {
		isModeModifTableau = true;
//		String tmp = "document.location='#" + containerCompetencePosteId() + "';";
//		session.setOnLoad(tmp);
		ec.saveChanges();
		currentRepartCompetencePosteBoolean.setModifie(false);
		return null;
	}
	
    
	public WOActionResults modifierCompetence() {
		isModeModifTableau = false;
//		String tmp = "document.location='#" + containerCompetencePosteId() + "';";
//		session.setOnLoad(tmp);
		currentRepartCompetencePosteBoolean.setModifie(true);
		return null;
	}
	
	//contener id
	public String containerCompetencePosteId() {
        return getComponentId() + "_selection";
	}
	
	
    //geter seteur -------------------------------------------------------------------------
    
	
    
	public EOEvaluation getInEvaluation() {
		return inEvaluation;
	}

	public void setInEvaluation(EOEvaluation inEvaluation) {
		this.inEvaluation = inEvaluation;
		repartCompetencePosteBooleans.clear();
		initMapRepartCompetencePoste();
		
	}



	public NSMutableArray<EORepartCompetencePoste> getListesRepartCompetencesPostes() {
		return listesRepartCompetencesPostes;
	}



	public void setListesRepartCompetencesPostes(
			NSMutableArray<EORepartCompetencePoste> listesRepartCompetencesPostes) {
		this.listesRepartCompetencesPostes = listesRepartCompetencesPostes;
	}



	public EORepartCompetencePoste getCurrentRepartCompetencesPoste() {
		return currentRepartCompetencesPoste;
	}



	public void setCurrentRepartCompetencesPoste(
			EORepartCompetencePoste currentRepartCompetencesPostes) {
		this.currentRepartCompetencesPoste = currentRepartCompetencesPostes;
	}


	public boolean isAjouterCompetencePoste() {
		return isAjouterCompetencePoste;
	}


	public void setAjouterCompetencePoste(boolean isAjouterCompetencePoste) {
		this.isAjouterCompetencePoste = isAjouterCompetencePoste;
	}
	
	public void setAjouterCompetencePosteTrue() {
		isModeModifTableau = false;
//		String tmp = "document.location='#" + containerCompetencePosteId() + "';";
//		session.setOnLoad(tmp);
		this.isAjouterCompetencePoste = true;
	}
	
	public void setAjouterCompetencePosteFalse() {
		this.isAjouterCompetencePoste = false;
	}


	public String getNewPeriodePoste() {
		return newPeriodePoste;
	}


	public void setNewPeriodePoste(String newPeriodePoste) {
		this.newPeriodePoste = newPeriodePoste;
	}


	public String getNewCompetencePoste() {
		return newCompetencePoste;
	}


	public void setNewCompetencePoste(String newCompetencePoste) {
		this.newCompetencePoste = newCompetencePoste;
	}


	public String getEvolutionActivitesStr() {
		return evolutionActivitesStr;
	}


	public void setEvolutionActivitesStr(String evolutionActivitesStr) {
		this.evolutionActivitesStr = evolutionActivitesStr;
	}


	public boolean isModificationEvolutionActivites() {
		return isModificationEvolutionActivites;
	}
	
	public boolean isNotModificationEvolutionActivites() {
		return !isModificationEvolutionActivites;
	}


	public void setModificationEvolutionActivites(
			boolean isModificationEvolutionActivites) {
		this.isModificationEvolutionActivites = isModificationEvolutionActivites;
	}


	public String getEvolutioncarriereStr() {
		return evolutioncarriereStr;
	}


	public void setEvolutioncarriereStr(String evolutioncarriereStr) {
		this.evolutioncarriereStr = evolutioncarriereStr;
	}


	public boolean isModificationEvolutionCarriere() {
		return isModificationEvolutionCarriere;
	}
	
	public boolean isNotModificationEvolutionarriere() {
		return !isModificationEvolutionCarriere;
	}


	public void setModificationEvolutionCarriere(
			boolean isModificationEvolutionCarriere) {
		this.isModificationEvolutionCarriere = isModificationEvolutionCarriere;
	}


	
	
	public boolean isModeModifierCompetencePoste() {
		return currentRepartCompetencePosteBoolean.isModifie();
	}


	public void setModeModifierCompetencePoste(boolean isModeModifierCompetencePoste) {
//		this.isModeModifierCompetencePoste = isModeModifierCompetencePoste;
	}


	public NSArray<RepartCompetencePosteBoolean> getRepartCompetencePosteBooleans() {
		return repartCompetencePosteBooleans;
	}


	public class RepartCompetencePosteBoolean {
		private EORepartCompetencePoste repartCompetence;
		private boolean isModifie;
		
		public RepartCompetencePosteBoolean(EORepartCompetencePoste repart, boolean bool) {
			repartCompetence = repart;
			isModifie = bool;
		}

		public EORepartCompetencePoste getRepartCompetence() {
			return repartCompetence;
		}

		public void setRepartCompetence(EORepartCompetencePoste repartCompetence) {
			this.repartCompetence = repartCompetence;
		}

		public boolean isModifie() {
			return isModifie;
		}

		public void setModifie(boolean isModifie) {
			this.isModifie = isModifie;
		}
		
		
		public String toString() {
			String isModifieStr;
			if (isModifie) {
				isModifieStr = "O";
			} else {
				isModifieStr = "N";
			}
			return "repartCompetence: " + repartCompetence.toString() + "boolean: " + isModifieStr;
		}
		
	}


}