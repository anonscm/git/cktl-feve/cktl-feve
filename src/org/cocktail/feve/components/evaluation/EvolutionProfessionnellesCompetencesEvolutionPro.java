package org.cocktail.feve.components.evaluation;


import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceEvolution;
import org.cocktail.fwkcktlgrh.common.metier.EORepartNiveauComp;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;



public class EvolutionProfessionnellesCompetencesEvolutionPro extends FeveWebComponent {
	
	private static final long serialVersionUID = 3273953719824490071L;
	//variables
	private EOEvaluation inEvaluation;
	public boolean disable;
	public boolean isModeModifTableau;
	
	private boolean isAjouterCompetence;
	private boolean isModificationEvolutionProCommentaire;
	
	private String evolutionActivitesStr;
	private String evolutioncarriereStr;
	
	private String newPeriode;
	private String newCompetence;
	
	private NSMutableArray<RepartCompetenceBoolean> repartCompetenceBooleans;
	public RepartCompetenceBoolean currentRepartCompetenceBoolean;
	
	public final String ANCRE_COMPETENCE = "ac";
	public final String ANCRE_COMPETENCE_LIBRE = "acl";
	
	private EORepartCompetenceEvolution currentRepartCompetences;
	
	private NSMutableArray<EORepartNiveauComp> listeRepartCompetences;
	
    public EvolutionProfessionnellesCompetencesEvolutionPro(WOContext context) {
        super(context);
        isModeModifTableau = true;
        
        listeRepartCompetences = new NSMutableArray<EORepartNiveauComp>();
        repartCompetenceBooleans = new NSMutableArray<RepartCompetenceBoolean>();
        
    }

    private void ancreActivite() {
		session.setOnLoad("document.location='#" + ANCRE_COMPETENCE + "';");
	}
    
    private void ancreCarriere() {
		session.setOnLoad("document.location='#" + ANCRE_COMPETENCE_LIBRE + "';");
	}
    
    
    private void initMapRepartCompetencePoste() {
    	repartCompetenceBooleans.clear();
    	NSArray<EORepartCompetenceEvolution> rcps = inEvaluation.tosRepartCompetenceEvolutions();
    	for (EORepartCompetenceEvolution rcp : rcps) {
    		repartCompetenceBooleans.add(new RepartCompetenceBoolean(rcp, false));
    	}
    }
    
    
    public void addRepartCompetencePoste(String libelle, String periode) {
    	EORepartCompetenceEvolution cmpPoste = EORepartCompetenceEvolution.create(ec, inEvaluation.evaKeyVisible(), libelle, periode, inEvaluation);
    	repartCompetenceBooleans.add(new RepartCompetenceBoolean(cmpPoste, false));
    }
    
    
    public void alimenterCompetence() {
    	EOQualifier evaluation = EORepartNiveauComp.TO_EVALUATION.eq(inEvaluation);
    	NSArray<EORepartNiveauComp> competences = EORepartNiveauComp.fetchAll(ec, evaluation, null);
    	
    	for (EORepartNiveauComp repartNiveauComp : competences) {
    		if ((repartNiveauComp.toNiveauCompetence().ncpPosition() == 1) || (repartNiveauComp.toNiveauCompetence().ncpPosition() == 2)) {
    			listeRepartCompetences.add(repartNiveauComp);
    		}
    	}
//    	ec.saveChanges();
    }
    
    
    
    //action des bouttons -------------------------------------------
    
    
	public WOActionResults supprimerCompetence() {
		ancreActivite();
		RepartCompetenceBoolean tmpRepartCompetencePoste;
		for (RepartCompetenceBoolean comp : repartCompetenceBooleans) {
			if (comp.getRepartCompetence().primaryKey().equals(currentRepartCompetenceBoolean.getRepartCompetence().primaryKey())) {
				tmpRepartCompetencePoste = currentRepartCompetenceBoolean;
			}
			currentRepartCompetenceBoolean.getRepartCompetence().delete();
		}
		
		
		ec.saveChanges();
		return null;
	}

    
    
	public WOActionResults addCompetence() {
		isModeModifTableau = true;
		ancreActivite();
		if (newCompetence != null) {
			if (newPeriode == null) {
				addRepartCompetencePoste(newCompetence, "");
			} else {
				addRepartCompetencePoste(newCompetence, newPeriode);
			}
		}
		edc().saveChanges();
		newCompetence = "";
		newPeriode = "";
		setAjouterCompetence(false);
		return null;
	}
	
    
    
    public WOActionResults annulerAddCompetencePoste() {
    	isModeModifTableau = true;
    	ancreActivite();
		setAjouterCompetence(false);
    	newCompetence = newPeriode = "";
    	return null;
    }
    
	
	public WOActionResults annulerModificationCompetencePoste() {
		isModeModifTableau = true;
		currentRepartCompetenceBoolean.setModifie(false);
		ec.revert();
		return null;
	}

	public WOActionResults validerModifierCompetence() throws Exception {
		isModeModifTableau = true;
		ancreActivite();
		ec.saveChanges();
		currentRepartCompetenceBoolean.setModifie(false);
		return null;
	}
	
    
	public WOActionResults modifierCompetence() {
		isModeModifTableau = false;
		ancreActivite();
		currentRepartCompetenceBoolean.setModifie(true);
		return null;
	}
	
	//contener id
	public String containerCompetencePosteId() {
        return getComponentId() + "_selection";
	}
	
	public WOActionResults enregistrerEvolutionProCommentaire() {
		ancreCarriere();
		isModificationEvolutionProCommentaire = false;
		ec.saveChanges();
		return null;
	}
	
	public WOActionResults annulerEvolutionProCommentaire() {
		ancreCarriere();
		isModificationEvolutionProCommentaire = false;
		ec.revert();
		return null;
	}
	
	public WOActionResults modifierEvolutionProCommentaire() {
		ancreCarriere();
		isModificationEvolutionProCommentaire = true;
		return null;
	}
    //geter seteur -------------------------------------------------------------------------
    
	
    
	public EOEvaluation getInEvaluation() {
		return inEvaluation;
	}

	public void setInEvaluation(EOEvaluation inEvaluation) {
		this.inEvaluation = inEvaluation;
		repartCompetenceBooleans.clear();
		initMapRepartCompetencePoste();
	}


	public EORepartCompetenceEvolution getCurrentRepartCompetencesPoste() {
		return currentRepartCompetences;
	}



	public void setCurrentRepartCompetencesPoste(
			EORepartCompetenceEvolution currentRepartCompetences) {
		this.currentRepartCompetences = currentRepartCompetences;
	}


	public boolean isAjouterCompetence() {
		return isAjouterCompetence;
	}


	public void setAjouterCompetence(boolean isAjouterCompetence) {
		this.isAjouterCompetence = isAjouterCompetence;
	}
	
	public void setAjouterCompetenceTrue() {
		isModeModifTableau = false;
//		String tmp = "document.location='#" + containerCompetencePosteId() + "';";
//		session.setOnLoad(tmp);
		this.isAjouterCompetence = true;
	}
	
	public void setAjouterCompetenceFalse() {
		this.isAjouterCompetence = false;
	}


	public String getNewPeriodePoste() {
		return newPeriode;
	}


	public void setNewPeriodePoste(String newPeriodePoste) {
		this.newPeriode = newPeriodePoste;
	}


	public String getNewCompetence() {
		return newCompetence;
	}


	public void setNewCompetence(String newCompetence) {
		this.newCompetence = newCompetence;
	}


	public String getEvolutionActivitesStr() {
		return evolutionActivitesStr;
	}


	public void setEvolutionActivitesStr(String evolutionActivitesStr) {
		this.evolutionActivitesStr = evolutionActivitesStr;
	}


	public String getEvolutioncarriereStr() {
		return evolutioncarriereStr;
	}


	public void setEvolutioncarriereStr(String evolutioncarriereStr) {
		this.evolutioncarriereStr = evolutioncarriereStr;
	}

	
	public boolean isModeModifierCompetence() {
		return currentRepartCompetenceBoolean.isModifie();
	}


	public NSArray<RepartCompetenceBoolean> getRepartCompetenceBooleans() {
		return repartCompetenceBooleans;
	}


	public boolean isModificationEvolutionProCommentaire() {
		return !isModificationEvolutionProCommentaire;
	}


	public void setModificationEvolutionProCommentaire(
			boolean isModificationEvolutionProCommentaire) {
		this.isModificationEvolutionProCommentaire = isModificationEvolutionProCommentaire;
	}


	public class RepartCompetenceBoolean {
		private EORepartCompetenceEvolution repartCompetence;
		private boolean isModifie;
		
		public RepartCompetenceBoolean(EORepartCompetenceEvolution repart, boolean bool) {
			repartCompetence = repart;
			isModifie = bool;
		}

		public EORepartCompetenceEvolution getRepartCompetence() {
			return repartCompetence;
		}

		public void setRepartCompetence(EORepartCompetenceEvolution repartCompetence) {
			this.repartCompetence = repartCompetence;
		}

		public boolean isModifie() {
			return isModifie;
		}

		public void setModifie(boolean isModifie) {
			this.isModifie = isModifie;
		}
		
		
		public String toString() {
			String isModifieStr;
			if (isModifie) {
				isModifieStr = "O";
			} else {
				isModifieStr = "N";
			}
			return "repartCompetence: " + repartCompetence.toString() + "boolean: " + isModifieStr;
		}
		
	}

	
}