package org.cocktail.feve.components.evaluation;

import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartAppreciationGeneral;
import org.cocktail.fwkcktlgrh.common.metier.EOTplItem;
import org.cocktail.fwkcktlgrh.common.metier.EOTplItemValeur;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class AppreciationGenerale extends FeveWebComponent {

	private static final int _APTITUDE_ENCADREMENT_KEY = 40;
	private static final int _CAPACITE_PRO_KEY = 39;
	private static final int _CONTRIBUTION_ACTIVITE_KEY = 38;
	private static final int _COMPETENCE_PRO_KEY = 37;
	private static final int _EXPERT = 11;
	private static final int _MAITRISE = 10;
	private static final int _ADEVELOPPER = 9;
	private static final int _AACQUERIE = 8;
	public static String COMPETENCE_PRO = "REPCOMPPRO";
	public static String CONTRIBUTION_ACTIVITE = "CONTRIACTI";
	public static String CAPACITE_PRO = "CAPACITEPR";
	public static String APTITUDE_ENCADREMENT = "APTITUDEEN";
	
	private EOEvaluation inEvaluation;
	public boolean disabled;
	
	private EOTplItemValeur currentCompetencePro;
	private EOTplItemValeur currentContribution;
	private EOTplItemValeur currentCapacite;
	private EOTplItemValeur currentAptitudeEncadrement;
	
//	public EOTplItemValeur setCompetencePro, setContribution, setCapacitePro, setAptitude;
	
	
	
	//TODO supprimer et chercher dans la base
	public boolean competenceProAAcquerir = true;

	public AppreciationGenerale(WOContext context) {
		super(context);
	}
	
	
	
	public void setInEvaluation(EOEvaluation evaluation) {
		inEvaluation = evaluation;
		initRepartAppGeneral();
	}
	
	public EOEvaluation getInEvaluation() {
		return inEvaluation;
	}
	

	private void initRepartAppGeneral() {
		EOQualifier qual;
		//init competence pro
		qual = EORepartAppreciationGeneral.ITEM_TIT_KEY.eq(_COMPETENCE_PRO_KEY);
		NSArray<EORepartAppreciationGeneral> repartAppGeneral = inEvaluation.tosRepartAppreciationGenerals(qual);
		if (repartAppGeneral.size() == 0) {
			qual = EOTplItem.TIT_KEY.eq(37);
			EOTplItem item = EOTplItem.fetch(ec, qual);
			EORepartAppreciationGeneral.create(ec, inEvaluation.evaKeyVisible(), item.titKey(), inEvaluation, item);
		}
		
		//init contribution activite
		qual = EORepartAppreciationGeneral.ITEM_TIT_KEY.eq(_CONTRIBUTION_ACTIVITE_KEY);
		NSArray<EORepartAppreciationGeneral> repartContriActi = inEvaluation.tosRepartAppreciationGenerals(qual);
		if (repartContriActi.size() == 0) {
			qual = EOTplItem.TIT_KEY.eq(38);
			EOTplItem item = EOTplItem.fetch(ec, qual);
			EORepartAppreciationGeneral.create(ec, inEvaluation.evaKeyVisible(), item.titKey(), inEvaluation, item);
		}
		
		//init capacite pro
		qual = EORepartAppreciationGeneral.ITEM_TIT_KEY.eq(_CAPACITE_PRO_KEY);
		NSArray<EORepartAppreciationGeneral> repartCapPro = inEvaluation.tosRepartAppreciationGenerals(qual);
		if (repartCapPro.size() == 0) {
			qual = EOTplItem.TIT_KEY.eq(39);
			EOTplItem item = EOTplItem.fetch(ec, qual);
			EORepartAppreciationGeneral.create(ec, inEvaluation.evaKeyVisible(), item.titKey(), inEvaluation, item);
		}
		
		
		//init aptitude encadrement
		qual = EORepartAppreciationGeneral.ITEM_TIT_KEY.eq(_APTITUDE_ENCADREMENT_KEY);
		NSArray<EORepartAppreciationGeneral> repartAptiEncadrement = inEvaluation.tosRepartAppreciationGenerals(qual);
		if (repartAptiEncadrement.size() == 0) {
			qual = EOTplItem.TIT_KEY.eq(40);
			EOTplItem item = EOTplItem.fetch(ec, qual);
			EORepartAppreciationGeneral.create(ec, inEvaluation.evaKeyVisible(), item.titKey(), inEvaluation, item);
		}
		ec.saveChanges();
	}
	
	
	public EORepartAppreciationGeneral getRepartCompetencePro() {
		EOQualifier qual = EORepartAppreciationGeneral.ITEM_TIT_KEY.eq(37);
		NSArray<EORepartAppreciationGeneral> repartAppGeneral = inEvaluation.tosRepartAppreciationGenerals(qual);
		if(repartAppGeneral.size() != 0) {
			return repartAppGeneral.get(0);
		}
		return null;
	}
	
	
	public EORepartAppreciationGeneral getRepartContributionActivite() {
		EOQualifier qual = EORepartAppreciationGeneral.ITEM_TIT_KEY.eq(38);
		NSArray<EORepartAppreciationGeneral> repartAppGeneral = inEvaluation.tosRepartAppreciationGenerals(qual);
		if(repartAppGeneral.size() != 0) {
			return repartAppGeneral.get(0);
		}
		return null;
	}
	
	public EORepartAppreciationGeneral getRepartCapacitePro() {
		EOQualifier qual = EORepartAppreciationGeneral.ITEM_TIT_KEY.eq(39);
		NSArray<EORepartAppreciationGeneral> repartAppGeneral = inEvaluation.tosRepartAppreciationGenerals(qual);
		if(repartAppGeneral.size() != 0) {
			return repartAppGeneral.get(0);
		}
		return null;
	}
	
	public EORepartAppreciationGeneral getRepartAptitudeEncadrement() {
		EOQualifier qual = EORepartAppreciationGeneral.ITEM_TIT_KEY.eq(40);
		NSArray<EORepartAppreciationGeneral> repartAppGeneral = inEvaluation.tosRepartAppreciationGenerals(qual);
		if(repartAppGeneral.size() != 0) {
			return repartAppGeneral.get(0);
		}
		return null;
	}
	
	
	public NSArray<EOTplItemValeur> getNiveaux() {
		NSMutableArray<EOTplItemValeur> niveaux = new NSMutableArray<EOTplItemValeur>();
		EOQualifier qualAcqueri = EOTplItemValeur.TIV_KEY.eq(_AACQUERIE);
		EOQualifier qualDevelopper = EOTplItemValeur.TIV_KEY.eq(_ADEVELOPPER);
		EOQualifier qualMaitrise = EOTplItemValeur.TIV_KEY.eq(_MAITRISE);
		EOQualifier qualExpert = EOTplItemValeur.TIV_KEY.eq(_EXPERT);
		
		niveaux.add(EOTplItemValeur.fetch(ec, qualAcqueri));
		niveaux.add(EOTplItemValeur.fetch(ec, qualDevelopper));
		niveaux.add(EOTplItemValeur.fetch(ec, qualMaitrise));
		niveaux.add(EOTplItemValeur.fetch(ec, qualExpert));
		return niveaux;
	}
	
	
	public boolean afficherAptitudeEncadrement() {
		
		NSArray<EOFicheDePoste> fdPs = inEvaluation.tosLastFicheDePoste();
		boolean abtitudeEncadrement = false;
		
		for (EOFicheDePoste ficheDePoste : fdPs) {
			if ("O".equals(ficheDePoste.fonctionConduiteProjet()) || "O".equals(ficheDePoste.fonctionEncadrement())) {
				abtitudeEncadrement = true;
			}
		}
		

		return abtitudeEncadrement;
	}
	
	
	public WOActionResults activerModeModifCompetences() {
		return null;
	}
	
	public WOActionResults enregistrerCompetences() throws Exception {
		UtilDb.save(ec, "");
		return null;
	}
	
	public WOActionResults annulerCompetences() {
		ec.revert();
		return null;
	}
	
	public EOEvaluation getEvaluation() {
		return inEvaluation;
	}


	public WOActionResults sauvegarder() {
		return null;
	}

	public void sauvegarderNiveaux() {
		ec.saveChanges();
	}
	

	public EOTplItemValeur getCurrentCompetencePro() {
		return currentCompetencePro;
	}



	public void setCurrentCompetencePro(EOTplItemValeur currentCompetencePro) {
		this.currentCompetencePro = currentCompetencePro;
	}



	public EOTplItemValeur getCurrentCapacite() {
		return currentCapacite;
	}



	public void setCurrentCapacite(EOTplItemValeur currentCapacite) {
		this.currentCapacite = currentCapacite;
	}



	public EOTplItemValeur getCurrentContribution() {
		return currentContribution;
	}



	public void setCurrentContribution(EOTplItemValeur currentContribution) {
		this.currentContribution = currentContribution;
	}



	public EOTplItemValeur getCurrentAptitudeEncadrement() {
		return currentAptitudeEncadrement;
	}



	public void setCurrentAptitudeEncadrement(EOTplItemValeur currentAptitudeEncadrement) {
		this.currentAptitudeEncadrement = currentAptitudeEncadrement;
	}
	
	

}