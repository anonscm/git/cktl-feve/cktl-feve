package org.cocktail.feve.components.evaluation;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.CktlSort;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Affichage d'une liste de fiche de poste sous forme d'un tableau
 * 
 * @author ctarade
 */
public class CompEvaluationList
		extends A_CompEvaluationObjetList {

	//
	public boolean isAfficherDateEntretien;
	public boolean isAfficherDateVisaRh;
	public boolean isAfficherEvolutionCarriere;
	public boolean isAfficherEvolutionPoste;

	// evaluations affichées
	private NSArray<EOVCandidatEvaluation> evaluationArrayFiltree;
	public EOVCandidatEvaluation evaluationItem;
	public EOVCandidatEvaluation evaluationSelected;

	public CompEvaluationList(WOContext context) {
		super(context);
	}

	/**
	 * 
	 * @return
	 */
	public NSArray<EOVCandidatEvaluation> getEvaluationArrayFiltree() {
		if (evaluationArrayFiltree == null) {
			if (getEvaluateurSelected() != null) {
				evaluationArrayFiltree = EOQualifier.filteredArrayWithQualifier(
						getEvaluationList(),
						ERXQ.equals(EOVCandidatEvaluation.TO_EVALUATION.dot(EOEvaluation.TO_INDIVIDU_KEY).key(), getEvaluateurSelected()));
			} else {
				evaluationArrayFiltree = getEvaluationList();
			}
			// classement
			evaluationArrayFiltree = CktlSort.sortedArray(evaluationArrayFiltree, getSortStringSelected(), getSortOrder());
		}
		return evaluationArrayFiltree;
	}

	@Override
	public void resetListeAffichee() {
		evaluationArrayFiltree = null;
	}

	public void sortEvaluateur() {
		faireClassement(EOVCandidatEvaluation.TO_EVALUATION.dot(EOEvaluation.TO_INDIVIDU_RESP.dot(EOIndividu.NOM_PRENOM_KEY)).key());
	}

	public void sortAgent() {
		faireClassement(EOVCandidatEvaluation.SORT_AGENT);
	}

	public void sortDateEntretien() {
		faireClassement(EOVCandidatEvaluation.SORT_DATE_ENTRETIEN);
	}

	public void sortDateVisaRh() {
		faireClassement(EOVCandidatEvaluation.SORT_DATE_VISA_RH);
	}
}