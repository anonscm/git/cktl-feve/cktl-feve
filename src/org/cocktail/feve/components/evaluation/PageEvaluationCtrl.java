package org.cocktail.feve.components.evaluation;

import org.cocktail.feve.app.Session;
import org.cocktail.feve.components.common.A_ComponentControlerAndFilArianeNode;
import org.cocktail.fwkcktlgrh.common.metier.util.UtilEOEvaluationKeyValueCoding;

/**
 * Controleur du composant {@link PageEvaluation}
 *
 * @author ctarade
 *
 */
public class PageEvaluationCtrl 
	extends A_ComponentControlerAndFilArianeNode {

	private UtilEOEvaluationKeyValueCoding evaluation;
	
	/**
	 * @deprecated
	 * @see #PageEvaluationCtrl(Session, A_EOEvaluationKeyValueCoding)
	 * @param session
	 */
	public PageEvaluationCtrl(Session session) {
		super(session);
	}
	
	/**
	 * 
	 * @param session
	 * @param anEvaluation
	 */
	public PageEvaluationCtrl(Session session, UtilEOEvaluationKeyValueCoding anEvaluation) {
		super(session);
		evaluation = anEvaluation;
	}
	

	@Override
	public A_ComponentControlerAndFilArianeNode child() {
		return null;
	}

	@Override
	protected void toLocalFullComponent() {
		
	}

}
