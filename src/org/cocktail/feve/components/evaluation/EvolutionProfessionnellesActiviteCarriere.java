package org.cocktail.feve.components.evaluation;

import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class EvolutionProfessionnellesActiviteCarriere extends FeveWebComponent {
	
	public final String ANCRE_ACTIVITE = "aa";
	public final String ANCRE_CARRIERE = "ac";
	
	private EOEvaluation inEvaluation;
	
	public boolean disable;
	
	private boolean isModificationEvolutionActivites;
	private boolean isModificationEvolutionCarriere;
	
	
    public EvolutionProfessionnellesActiviteCarriere(WOContext context) {
        super(context);
        isModificationEvolutionActivites = isModificationEvolutionCarriere = false;
    }
    
    
    private void ancreActivite() {
		session.setOnLoad("document.location='#" + ANCRE_ACTIVITE + "';");
	}
    
    private void ancreCarriere() {
		session.setOnLoad("document.location='#" + ANCRE_CARRIERE + "';");
	}
    
   //bouttons ---------------------------------
    
	public WOActionResults enregistrerEvolutionActivites() {
		ancreActivite();
		setModificationEvolutionActivites(false);
		ec.saveChanges();
		return null;
	}
    
	public WOActionResults enregistrerEvolutionCarriere() {
		ancreCarriere();
		setModificationEvolutionCarriere(false);
		ec.saveChanges();
		return null;
	}
	
	public WOActionResults annulerEvolutionActivites() {
		ancreActivite();
		setModificationEvolutionActivites(false);
		ec.revert();
		return null;
	}
	
	public WOActionResults annulerEvolutionCarriere() {
		ancreCarriere();
		setModificationEvolutionCarriere(false);
		ec.revert();
		return null;
	}
	
	public WOActionResults modifierEvolutionActivites() {
		ancreActivite();
		setModificationEvolutionActivites(true);
		return null;
	}
	

	public WOActionResults modifierEvolutionCarriere() {
		ancreCarriere();
		setModificationEvolutionCarriere(true);
		return null;
	}

	
	
    
    //getteurs setteurs ---------------------------------------------


	public boolean isModificationEvolutionActivites() {
		return isModificationEvolutionActivites;
	}
	
	public boolean isNotModificationEvolutionActivites() {
		return !isModificationEvolutionActivites;
	}


	public void setModificationEvolutionActivites(
			boolean isModificationEvolutionActivites) {
		this.isModificationEvolutionActivites = isModificationEvolutionActivites;
	}


	public boolean isModificationEvolutionCarriere() {
		return isModificationEvolutionCarriere;
	}
	
	
	public boolean isNotModificationEvolutionarriere() {
		return !isModificationEvolutionCarriere;
	}


	public void setModificationEvolutionCarriere(
			boolean isModificationEvolutionCarriere) {
		this.isModificationEvolutionCarriere = isModificationEvolutionCarriere;
	}


	public EOEvaluation getInEvaluation() {
		return inEvaluation;
	}

	
	public void setInEvaluation(EOEvaluation inEvaluation) {
		this.inEvaluation = inEvaluation;
	}


}