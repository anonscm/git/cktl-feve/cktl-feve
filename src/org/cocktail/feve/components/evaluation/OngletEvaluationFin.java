package org.cocktail.feve.components.evaluation;

import java.io.IOException;

import org.cocktail.feve.app.Session;
import org.cocktail.feve.app.print.GenerateurPDFCtrl;
import org.cocktail.feve.app.print.PrintConsts;
import org.cocktail.feve.app.print.XMLGenerateur;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class OngletEvaluationFin
		extends A_OngletEvaluationAvecImpression {
	
	public boolean disabled;
	
	private GenerateurPDFCtrl generateurPDF;
	
	public boolean isAfficherAlerteTenueRealisee, isModeModifEvaluateur;
	
	public OngletEvaluationFin(WOContext context) {
		super(context);
		isAfficherAlerteTenueRealisee = isModeModifEvaluateur = false;
	}

	@Override
	public boolean isEmptyEvaluation() {
		return getInEvaluation().isEntretienTenu();
	}

	// gestion des dates

	/**
	 * Indique si on peut remettre à vide la date de tenue. Oui si droit de modif
	 * ou admin, et que cette derniere n'est pas déja vide
	 */
	public boolean isDTenueReinitialisable() {
		boolean isDTenueReinitialisable = false;

		if ((isModifiable() || feveUserInfo().isAdmin()) &&
				getInEvaluation().isEntretienTenu()) {
			isDTenueReinitialisable = true;
		}

		return isDTenueReinitialisable;
	}

	/**
	 * Effectuer la RAZ de la date de tenue
	 * 
	 * @return
	 * @throws Throwable
	 */
	public WOComponent doReinitialiserDTenue() throws Throwable {
		setDTenueEntretien(null);
		return null;
	}

	/**
	 * Indique si on peut remettre à vide la date de visa RH. Oui si droit rh
	 * (admin) et que cette derniere n'est pas déja vide
	 */
	public boolean isDVisaResponsableRhReinitialisable() {
		boolean isDVisaResponsableRhReinitialisable = false;

		if (feveUserInfo().isAdmin() &&
				getInEvaluation().isViseParResponsableRh()) {
			isDVisaResponsableRhReinitialisable = true;
		}

		return isDVisaResponsableRhReinitialisable;
	}

	/**
	 * Effectuer la RAZ de la date de visa RH
	 * 
	 * @return
	 * @throws Throwable
	 */
	public WOComponent doReinitialiserDVisaReponsableRh() throws Throwable {
		setDVisaResponsableRh(null);
		return null;
	}

	/**
	 * La date de tenue d'entretien n'est pas modifiable si l'entretien a déjà été
	 * tenu ou si le composant est en lecture seule
	 * 
	 * @return
	 */
	public boolean isDisableDdTenueEntretien() {
		boolean isDisableDdTenueEntretien = true;

		if (isModifiable() &&
				getInEvaluation().isEntretienTenu() == false) {
			isDisableDdTenueEntretien = false;
		}

		return isDisableDdTenueEntretien;
	}

	/**
	 * La date n'est modifiable que par les admins et que si celle ci est vide
	 * 
	 * @return
	 */
	public boolean isDisabledDVisaResponsableRh() {
		boolean isDisabledDVisaResponsableRh = true;

		if (feveUserInfo().isAdmin() &&
				!getInEvaluation().isViseParResponsableRh()) {
			isDisabledDVisaResponsableRh = false;
		}

		return isDisabledDVisaResponsableRh;
	}

	// getters setters date pour avoir la sauvegarde automatique avec les
	// CktlAjaxDatePicker

	public final NSTimestamp getDTenueEntretien() {
		return getInEvaluation().dTenueEntretien();
	}

	public final void setDTenueEntretien(NSTimestamp dTenueEntretien) throws Throwable {
		getInEvaluation().setDTenueEntretien(dTenueEntretien);
		// afficher le message d'avertissement si la date n'est pas vide
		if (dTenueEntretien != null) {
			isAfficherAlerteTenueRealisee = true;
		}
		sauvegarde();
	}

	public final NSTimestamp getDVisaResponsableRh() {
		return getInEvaluation().dVisaResponsableRh();
	}

	public final void setDVisaResponsableRh(NSTimestamp dVisaResponsableRh) throws Throwable {
		getInEvaluation().setDVisaResponsableRh(dVisaResponsableRh);
		sauvegarde();
	}

	//

	/**
	 * Le code JS permettant d'afficher le message invitant le responsable a
	 * valider
	 */
	public String getJsAlertTenueRealisee() {
		String str = "";

		str = "alert('" + PrintConsts.MESSAGE_FICHE_EVALUATION_APRES_SAISIE_DATE_ENTRETIEN + "');";

		// ne pas reafficher le message par la suite
		isAfficherAlerteTenueRealisee = false;

		return str;
	}

	public boolean isCRDefinitifDisabled() {
		return isDisabled() && !feveUserInfo().getAutorisation().hasDroitConnaissanceEditerCompteRenduEPFinalise(getInEvaluation().toIndividu());
	}
	
	public boolean hasDroitModifEvaluateur() {
		return feveUserInfo().getAutorisation().hasDroitUtilisationGererEntretienProfessionnel(getInEvaluation().toIndividu());
	}
	
	public WOActionResults genererCompteRenduNonVierge() throws IOException {
		XMLGenerateur xmlGenerateur = new XMLGenerateur(edc(), (Session) session());
		
		String xml = xmlGenerateur.generateEvaluationXML(false,getInEvaluation());
		
		generateurPDF = new GenerateurPDFCtrl(xml);
		generateurPDF.genererCompteRenduEvaluation();		
		
		//TO DO: gérer le message de fin
		//dico.setObjectForKey(PrintConsts.ENDING_MESSAGE_FICHE_EVALUATION_VIERGE, PrintConsts.DICO_KEY_ENDING_MESSAGE);
		
		return null;
	}
	
	public GenerateurPDFCtrl getGenerateurPDF() {
		return generateurPDF;
	}
	/**
	 * @return the CompteRenduFilename
	 */
	public String CompteRenduFilename() {
		return StringCtrl.toBasicString("CompteRendu"+"_" +
				getInEvaluation().toIndividu().nomPrenom() + "_" +
				getInEvaluation().toEvaluationPeriode().strAnneeDebutAnneeFin() + ".pdf");
	}
	
	public String getFonctionSuperieurHierarchique() {
		return getInEvaluation().getFonctionEvaluateur();
	}
	
	
	
	public void setFonctionSuperieurHierarchique(String valurFonction) {
		getInEvaluation().setFonctionEvaluateur(valurFonction);
	}
	
	public WOComponent activerModeModifEvaluateur() {
		isModeModifEvaluateur = true;
		return null;
	}
	
	public WOComponent desactiverModeModifEvaluateur() {
		isModeModifEvaluateur = false;
		return null;
	}
	
	public WOComponent enregistrerEvaluateur() {
		edc().saveChanges();
		desactiverModeModifEvaluateur();
		return null;
	}
	
	public WOComponent annulerEvaluateur() {
		edc().revert();
		desactiverModeModifEvaluateur();
		return null;
	}
	
	public boolean isNotModeModifEvaluateur() {
		return !isModeModifEvaluateur;
	}
	
	public boolean hasDroitEditerCompteRenduDefinitif() {
		return session.getAutorisation().hasDroitUtilisationEditerCompteRenduEpFinalise(getInEvaluation().toIndividu());
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public EOIndividu getIndividuResp() {
		return getInEvaluation().toIndividuResp();
		
	}

	public void setIndividuResp(EOIndividu individuResp) {
		getInEvaluation().setToIndividuRespRelationship(individuResp);
		getInEvaluation().setFonctionEvaluateur(individuResp.indQualite());
	}
	
}