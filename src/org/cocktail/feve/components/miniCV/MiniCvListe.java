package org.cocktail.feve.components.miniCV;


import java.util.List;

import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceMiniCV;
import org.cocktail.fwkcktlgrh.common.metier.EOVCandidatEvaluation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;

public class MiniCvListe extends FeveWebComponent {
	
	public final static String RADIO_TYPE_FILTRE_NOM = "filtreParNom";
	public final static String RADIO_TYPE_FILTRE_SERVICE = "filtreParService";
	
	private WODisplayGroup dgMiniCV;
	
	private EOIndividu individuConnecte;
	private NSArray<EOIndividu> listeIndividuMiniCV;
	private NSArray<EOIndividu> listeIndividuMiniCVAfficage;
	private EOIndividu currentIndividu;
	private EOIndividu individuSelectione;
	
	private boolean isAficherMiniCV;
	
	private boolean rechercheParService = false;
	private boolean rechercheParNom = true;
	
	private String rechercheType;
	
	//filtres
	private String filtreNom = "";
	private EOStructure filtreStructure;
	private EOStructure structureItem;
	
	public boolean filtreLeMiniCVExiste;
	
	MiniCvCTRL ctrl;
	
    public MiniCvListe(WOContext context) {
        super(context);
        initialiserFiltre();
        initialiserIndividuConecte();
        setAficherMiniCV(false);
        this.listeIndividuMiniCV = new NSMutableArray<EOIndividu>();
        this.listeIndividuMiniCVAfficage = new NSMutableArray<EOIndividu>();
        ctrl = new MiniCvCTRL();
        initListeIndividuMiniCV();
    }
    
    
    
    /**
     * Initialise la liste des individues a afficher
     */
    public void initListeIndividuMiniCV() {
    	listeIndividuMiniCV.clear();
    	
    	NSArray<EOVCandidatEvaluation> evaluations = EOVCandidatEvaluation.fetchAll(edc());
    	for (EOVCandidatEvaluation e : evaluations) {
    		if (!listeIndividuMiniCV.contains(e.toIndividu())) {
    			if (session.getAutorisation().hasDroitConnaissanceGererEntretienProfessionnel(e.toIndividu())) {
    				listeIndividuMiniCV.add(e.toIndividu());
    			}
    		}
    	}
    	
    	listeIndividuMiniCVAfficage.clear();
    	for (EOIndividu i : listeIndividuMiniCV) {
			listeIndividuMiniCVAfficage.add(i);
		}
    }
    
    
    /**
     * Initialise la liste des individues a afficher
     */
    public WODisplayGroup listeIndividu() {
		filtrer();
		refreshDg();
		return dgMiniCV;
    }
    
    
	private void refreshDg() {
		this.dgMiniCV = new ERXDisplayGroup<EOIndividu>();
		this.dgMiniCV.setDelegate(this);
		this.dgMiniCV.setSelectsFirstObjectAfterFetch(false);
		this.dgMiniCV.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECTS_PER_BATCH);
		this.dgMiniCV.setObjectArray(listeIndividuMiniCVAfficage);
		
	}
    
    
    
    public WOActionResults filtrer() {
    	filtrerNom();
    	filtrerParService();
    	filtrerMiniCV();
    	refreshDg();
    	return null;
    }
    
    public NSArray<EOIndividu>  filtrerNom() {
    	if (rechercheType.equals(RADIO_TYPE_FILTRE_NOM) && filtreNom != null && filtreNom != "") {
    		listeIndividuMiniCVAfficage.clear();
        	for (EOIndividu i : listeIndividuMiniCV) {
        		if (i.nomAffichage().equalsIgnoreCase(filtreNom)) {
        			listeIndividuMiniCVAfficage.add(i);
        		}
        		if (i.prenomAffichage().equalsIgnoreCase(filtreNom)) {
        			listeIndividuMiniCVAfficage.add(i);
        		}
        	}
    	} else {
    		listeIndividuMiniCVAfficage.clear();
    	 	for (EOIndividu i : listeIndividuMiniCV) {
        		listeIndividuMiniCVAfficage.add(i);
        	}
    	}
    	return listeIndividuMiniCVAfficage;
    }
    
    
    public NSArray<EOIndividu> filtrerParService() {
    	if (rechercheType.equals(RADIO_TYPE_FILTRE_SERVICE) && filtreStructure != null) {
    		listeIndividuMiniCVAfficage.clear();
    		for (EOIndividu i : listeIndividuMiniCV) {
    			NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure> repartStructures = i.toRepartStructures();
    			
    			for(org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure s : repartStructures) {
    				if (s.cStructure().equals(filtreStructure.cStructure())) {
    					listeIndividuMiniCVAfficage.add(i);
    				}
				}
    			
    		}
    	}
    	return listeIndividuMiniCVAfficage;
    }
    
    
    public NSArray<EOIndividu> filtrerMiniCV() {
    	if (filtreLeMiniCVExiste) {
        	NSArray<EOIndividu> individuTmp = new NSMutableArray<EOIndividu>();
        	EOQualifier qualRepart;
        	
        	for (EOIndividu i : listeIndividuMiniCVAfficage) {
        		qualRepart = EORepartCompetenceMiniCV.INDIVIDU.eq(i.persId());
        		if (EORepartCompetenceMiniCV.fetchAll(edc(), qualRepart, null).size() > 0) {
        			individuTmp.add(i);
        		}
        	}
        	
        	listeIndividuMiniCVAfficage.clear();
        	for (EOIndividu i : individuTmp) {
        		listeIndividuMiniCVAfficage.add(i);
        	}
    	}
    	
    	return listeIndividuMiniCVAfficage;
    }
    
    
    

    public List<IStructure> getStructureAuthorise() {
    	return session.feveUserInfo().getServicePosteList();
    }
    
    
    public EOFicheDePoste getFicheDePoste() {
    	EOQualifier qualAffectation = EOAffectation.TO_INDIVIDU.eq(currentIndividu);
    	NSArray<EOAffectation> affectations = EOAffectation.fetchAll(edc(), qualAffectation, null);
    	
    	if (affectations.size() <= 0) {
    		return null;
    	}
    	EOAffectation affectation = affectations.get(0);
    	
    	EOQualifier qualAffectationDetail = EOAffectationDetail.TO_AFFECTATION.eq(affectation);
    	EOAffectationDetail affDetail = EOAffectationDetail.fetch(edc(), qualAffectationDetail);
    	
    	
    	if (affDetail != null && affDetail.toPoste() != null && affDetail.toPoste().posLibelle() != null && affDetail.toPoste().tosFicheDePoste().size() >= 1) {
    		return affDetail.toPoste().tosFicheDePoste().get(0);
    	}
    	
    	return null;
    	
    }
    
    public boolean hasFicheDePoste() {
    	if (getFicheDePoste() != null) {
    		return true;
    	}
    	return false;
    }
    
    public MiniCvCTRL getCtrl() {
    	return ctrl;
    }
    
    public String getPoste() {
    	EOQualifier qualAffectation = EOAffectation.TO_INDIVIDU.eq(currentIndividu);
    	NSArray<EOAffectation> affectations = EOAffectation.fetchAll(edc(), qualAffectation, null);
    	
    	if (affectations.size() <= 0) {
    		return "null aff";
    	}
    	EOAffectation affectation = affectations.get(0);
    	
    	EOQualifier qualAffectationDetail = EOAffectationDetail.TO_AFFECTATION.eq(affectation);
    	EOAffectationDetail affDetail = EOAffectationDetail.fetch(edc(), qualAffectationDetail);
    	
    	if (affDetail != null && affDetail.toPoste() != null && affDetail.toPoste().posLibelle() != null) {
    		return affDetail.toPoste().posLibelle();
    	}
    	
    	return noString();
    	
    }
    
    
    public NSArray<EORepartCompetenceMiniCV> getMiniCV() {
    	EOQualifier qualMiniCV = EORepartCompetenceMiniCV.INDIVIDU.eq(currentIndividu.persId());
    	return EORepartCompetenceMiniCV.fetchAll(edc(), qualMiniCV, null);
    }
    
    public boolean hasMiniCV() {
    	if (getMiniCV().size() > 0) {
    		return true;
    	}
    	return false;
    }
    
    
    
    private NSArray<EOEvaluationPeriode> getListePeriode() {
    	return EOEvaluationPeriode.fetchAll(edc());
    }
    
    private void initialiserFiltre() {
    	filtreStructure = null;
    	filtreLeMiniCVExiste = false;
    	rechercheType = RADIO_TYPE_FILTRE_NOM;
    }

    private void initialiserIndividuConecte() {
    	this.individuConnecte = session.individuConnecte();
    }

    public NSArray<EOIndividu> getListeIndividuMiniCV() {
    	return listeIndividuMiniCV;
    }
    
    
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}


	public void setCurrentIndividu(EOIndividu individu) {
		this.currentIndividu = individu;
	}
	
	public WOActionResults openMiniCV() {
		individuSelectione = currentIndividu;
		ctrl.setIndividuCV(currentIndividu);
		afficherMiniCV();
		return null;
	}

	public void afficherMiniCV() {
		this.setAficherMiniCV(true);
	}
	
	public void cacherMiniCV() {
		this.setAficherMiniCV(false);
	}

	public EOIndividu getIndividuSelectione() {
		return individuSelectione;
	}



	public void setIndividuSelectione(EOIndividu individuSelectione) {
		this.individuSelectione = individuSelectione;
	}



	public boolean isAficherMiniCV() {
		return isAficherMiniCV;
	}



	public void setAficherMiniCV(boolean isAficherMiniCV) {
		this.isAficherMiniCV = isAficherMiniCV;
	}

	private String noString() {
		return "---";
	}



	public String getFiltreNom() {
		return filtreNom;
	}



	public void setFiltreNom(String filtreNom) {
		this.filtreNom = filtreNom;
	}



	public boolean isRechercheParService() {
		return rechercheParService;
	}



	public void setRechercheParService(boolean rechercheParService) {
		this.rechercheParService = rechercheParService;
		this.rechercheParNom = !rechercheParService;
	}



	public boolean isRechercheParNom() {
		return rechercheParNom;
	}



	public void setRechercheParNom(boolean rechercheParNom) {
		this.rechercheParNom = rechercheParNom;
		this.rechercheParService = !this.rechercheParService;
	}



	public String getRechercheType() {
		return rechercheType;
	}



	public void setRechercheType(String rechercheType) {
		this.rechercheType = rechercheType;
	}
	
	public void setFiltreStructure(EOStructure value) {
		rechercheType = "filtreParService";
		filtreStructure = value;
	}
	
	public EOStructure getFiltreStructure() {
		return filtreStructure;
	}



	public EOStructure getStructureItem() {
		return structureItem;
	}



	public void setStructureItem(EOStructure structureItem) {
		this.structureItem = structureItem;
	}
}