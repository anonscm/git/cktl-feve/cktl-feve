package org.cocktail.feve.components.miniCV;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

public class MiniCvCTRL {

	
	private IIndividu individuCV;

	public IIndividu getIndividuCV() {
		return individuCV;
	}

	public void setIndividuCV(IIndividu individuCV) {
		this.individuCV = individuCV;
	}
}
