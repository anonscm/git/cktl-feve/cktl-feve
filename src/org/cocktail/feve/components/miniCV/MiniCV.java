package org.cocktail.feve.components.miniCV;

import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceMiniCV;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences;
import org.cocktail.fwkcktlpersonne.common.metier.EOVReferens;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class MiniCV extends FeveWebComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8678943960225701928L;
	
	private MiniCvCTRL miniCvCTRL;
	private EOAffectationDetail eoAffectationDetailSelected;

	private boolean showLigneAjouter;
	private boolean disabled;
	
	public boolean showLigneAjouterLibre = false;
	
	public String champLibre = "";
	
	private EOFicheDePoste inputLaFicheDePoste;
	
	private EOReferensCompetences laCompetenceSelectionnee;
	
	private EORepartCompetenceMiniCV repartCompMiniCV;
	private NSArray<EORepartCompetenceMiniCV> listeRepartCompMiniCV;
	
	public IIndividu individuCV = null;
	
    public MiniCV(WOContext context) {
        super(context);
        setRepartCompMiniCV(null);
        champLibre = "";
    }

    
    public NSArray<EORepartCompetenceMiniCV> getListeReaprtCompMiniCV() {
    	if (individuCV != null) {
    		return getListeReaprtCompMiniCVPourIndividu(individuCV);
    	}
    	if (miniCvCTRL.getIndividuCV() != null) {
    		return getListeReaprtCompMiniCVPourIndividu(miniCvCTRL.getIndividuCV());
    	}
    	return null;
    	
    }
    
    
    private final static String COMPETENCES_ANNEXES_LABEL =	"CompetencesMiniCV";

    /**
     * code javascript de la fonction pour la selection / deselection de 
     * toutes les competences annexes
     */
    public String getJsFunctionSelectionCompetencesAnnexes() {
    	NSArray<String> a = new NSMutableArray<String>();
    	EOQualifier qualRepart;
    	if (individuCV != null) {
    		qualRepart = EORepartCompetenceMiniCV.INDIVIDU.eq(individuCV.persId());
    	} else {
    		qualRepart = EORepartCompetenceMiniCV.INDIVIDU.eq(miniCvCTRL.getIndividuCV().persId());
    	}
    	
    	NSArray<EORepartCompetenceMiniCV> array = EORepartCompetenceMiniCV.fetchAll(edc(), qualRepart, null);
    	
    	
    	for (EORepartCompetenceMiniCV r : array) {
    		a.add(r.primaryKey().toString()); //TODO
    	}
    	
    	return getJsFunctionSelection(COMPETENCES_ANNEXES_LABEL, 
    			(NSArray) a);
    }
    

    
    /**
     * Le code onclick pour la coche de toutes les activites
     * @return
     */
    public String getOnClickSelectionCompetencesAnnexes() {
    	return getOnClickSelection(COMPETENCES_ANNEXES_LABEL);
    }
    
    
    private  NSArray<EORepartCompetenceMiniCV> getListeReaprtCompMiniCVPourIndividu(IIndividu individu) {
    	
    	EOQualifier qualRepartCompMiniCV = EORepartCompetenceMiniCV.INDIVIDU.eq(individu.persId());
    	return EORepartCompetenceMiniCV.fetchAll(edc(), qualRepartCompMiniCV, CktlSort.newSort(EORepartCompetenceMiniCV.POSITION_KEY));
    }
    
    
    /**
     * associer une nouvelle competence
     */
    public WOComponent associerNouvelleCompetence() throws Throwable {
	EOQualifier qualCompetence;
    	
      if (laCompetenceSelectionnee != null && !showLigneAjouterLibre) {
    	  
    	  if (individuCV != null) {
    		  qualCompetence = EORepartCompetenceMiniCV.INDIVIDU.eq(individuCV.persId());
    		  int nb = EORepartCompetenceMiniCV.fetchAll(edc(), qualCompetence, null).size();
    		  EORepartCompetenceMiniCV repart = EORepartCompetenceMiniCV.create(ec, new NSTimestamp(), new NSTimestamp(), individuCV.persId(), nb+1);
    		  repart.setCodeEmploi(laCompetenceSelectionnee.CODE_EMPLOI_KEY);
    		  repart.setToReferensCompetencesRelationship(laCompetenceSelectionnee);
    	    	 
    	  } else {
    		  qualCompetence = EORepartCompetenceMiniCV.INDIVIDU.eq(miniCvCTRL.getIndividuCV().persId());
    		  int nb = EORepartCompetenceMiniCV.fetchAll(edc(), qualCompetence, null).size();
    		  EORepartCompetenceMiniCV repart = EORepartCompetenceMiniCV.create(ec,  new NSTimestamp(), new NSTimestamp(), miniCvCTRL.getIndividuCV().persId(), nb+1);
        	  repart.setCodeEmploi(laCompetenceSelectionnee.CODE_EMPLOI_KEY);
        	  repart.setToReferensCompetencesRelationship(laCompetenceSelectionnee);
    	  }
    	  
    	  showLigneAjouter = false;
    	  UtilDb.save(ec, "associerNouvelleCompetence");
      }
      
      //ajout champ libre
      if (showLigneAjouterLibre) {
    	  if (champLibre.length() < 200) {
	    	  qualCompetence = EORepartCompetenceMiniCV.INDIVIDU.eq(miniCvCTRL.getIndividuCV().persId());
	    	  int nb = EORepartCompetenceMiniCV.fetchAll(edc(), qualCompetence, null).size();
			  EORepartCompetenceMiniCV repart = EORepartCompetenceMiniCV.create(ec, new NSTimestamp(), new NSTimestamp(),  miniCvCTRL.getIndividuCV().persId(), nb+1);
			  repart.setChampLibre(champLibre); 
			  UtilDb.save(ec, "associerNouvelleCompetence");
			  showLigneAjouterLibre = false;
    	  } else {
    		  showLigneAjouterLibre = false;
    		  //afficher message erreur
    	  }
      }
      
      return neFaitRien();
    }

    
    /**
     * Supprimer les competences annexes selectionn�es
     * @return
     * @throws Throwable
     */
    public WOComponent supprimerCompetencesAnnexesSelectionnees() throws Throwable {
    	// compte le nombre de repart a supprimer
    	NSArray repartList = EORepartCompetenceMiniCV.fetchAll(edc());
    	// recuperer la liste des enregistrements a supprimer
    	repartList = EOQualifier.filteredArrayWithQualifier(
    			repartList,
    			CktlDataBus.newCondition(AfwkGRHRecord.IS_MARKED_TO_DELETE_KEY + "=%@", new NSArray(Boolean.TRUE)));
    	// 
    	if (repartList.count() > 0) {
    		for (int i=0; i<repartList.count(); i++) {
    			///
    			AfwkGRHRecord recordToDelete = (AfwkGRHRecord) repartList.objectAtIndex(i);
    			reorganiserCompetences((EORepartCompetenceMiniCV)recordToDelete);
    			ec.deleteObject(recordToDelete);
    		}
    		edc().saveChanges();
    	}
    	return neFaitRien();
    }
    
    private void reorganiserCompetences(EORepartCompetenceMiniCV compMiniCV) {
    	EOQualifier qualComps = EORepartCompetenceMiniCV.INDIVIDU.eq(compMiniCV.individu());
    	int position = compMiniCV.position();
    	NSArray<EORepartCompetenceMiniCV> rComps = EORepartCompetenceMiniCV.fetchAll(ec, qualComps, null);
    	for (EORepartCompetenceMiniCV rc : rComps) {
    		if (rc.position() > position) {
    			rc.setPosition(rc.position() - 1);
    		}
    	}
    }
    
    
    public WOComponent annulerModif() {
      ec.revert();
      desactiverModeAjouter();
      return neFaitRien();
    }

	public EORepartCompetenceMiniCV getRepartCompMiniCV() {
		return repartCompMiniCV;
	}


	public void setRepartCompMiniCV(EORepartCompetenceMiniCV repartCompMiniCV) {
		this.repartCompMiniCV = repartCompMiniCV;
	}


	private void desactiverModeAjouter(){
		showLigneAjouterLibre = false;
		showLigneAjouter = false;
	}
	
	public void activerModeAjouter(){
		showLigneAjouter = true;
	}
	
	public void activerModeAjouterLibre(){
		showLigneAjouterLibre = true;
	}


	public EOReferensCompetences getLaCompetenceSelectionnee() {
		return laCompetenceSelectionnee;
	}


	public void setLaCompetenceSelectionnee(EOReferensCompetences laCompetenceSelectionnee) {
		this.laCompetenceSelectionnee = laCompetenceSelectionnee;
	}
	
	public IIndividu getIndividuCV() {
		if (individuCV == null) {
			return miniCvCTRL.getIndividuCV();
		}
		return individuCV;
	}
	
	public void setIndividuCV(IIndividu individu) {
		
		this.miniCvCTRL.setIndividuCV(individu);
	}


	public EOFicheDePoste getInputLaFicheDePoste() {
		return inputLaFicheDePoste;
	}


	public void setInputLaFicheDePoste(EOFicheDePoste inputLaFicheDePoste) {
		this.inputLaFicheDePoste = inputLaFicheDePoste;
	}


	public boolean isShowLigneAjouter() {
		return showLigneAjouter;
	}


	public void setShowLigneAjouter(boolean showLigneAjouter) {
		this.showLigneAjouter = showLigneAjouter;
	}


	public MiniCvCTRL getMiniCvCTRL() {
		return miniCvCTRL;
	}


	public void setMiniCvCTRL(MiniCvCTRL miniCvCTRL) {
		this.miniCvCTRL = miniCvCTRL;
	}
	
	
	public WOComponent descendreCompetence() throws Throwable {
		repartCompMiniCV.down();
		edc().saveChanges();
		return neFaitRien();
	}

	public WOComponent remonterCompetence() throws Throwable {
		repartCompMiniCV.up();
		edc().saveChanges();
		return neFaitRien();
	}
	
	
	public boolean isBooleanChampLibre() {
		if (repartCompMiniCV != null && repartCompMiniCV.champLibre() != null) {
			return true;
		}
		return false;
	}
	
	public String getCompetenceAffichage() {
		if (repartCompMiniCV.champLibre() != null) {
			return repartCompMiniCV.champLibre();
		}
		return "-";
	}


	public boolean isDisabled() {
		if (session.getAutorisation().isAdmin()) {
			return false || disabled;
		}
		if (session.individuConnecte() != miniCvCTRL.getIndividuCV()) {
			return true;
		}
		return disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}


	public EOAffectationDetail getEoAffectationDetailSelected() {
		return eoAffectationDetailSelected;
	}


	public void setEoAffectationDetailSelected(
			EOAffectationDetail eoAffectationDetailSelected) {
		this.eoAffectationDetailSelected = eoAffectationDetailSelected;
	}
	
	private NSArray<EOVReferens> referensCompetencesToHideList;
	
	public NSArray<EOVReferens> referensCompetencesToHideList() {
		referensCompetencesToHideList = EOVReferens.findListVReferensForReferensCompetences(edc(), inputLaFicheDePoste.tosReferensCompetences());
		return referensCompetencesToHideList;
	}


	public void setReferensCompetencesToHideList(NSArray<EOVReferens> referensCompetencesToHideList) {
		this.referensCompetencesToHideList = referensCompetencesToHideList;
	}
	
	
	
	
}