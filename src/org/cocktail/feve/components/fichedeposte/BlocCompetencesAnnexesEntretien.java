package org.cocktail.feve.components.fichedeposte;

import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartEvaNouvelleComp;
import org.cocktail.fwkcktlpersonne.common.metier.EOReferensCompetences;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * La liste des compétences annexes detectées lors des entretiens ayant eu lieu
 * sur la période de la fiche de poste pour son occupant
 * 
 * @author ctarade
 */
public class BlocCompetencesAnnexesEntretien
		extends FeveWebComponent {

	// bindings
	public EOFicheDePoste eoFicheDePoste;
	public EOAffectationDetail eoAffectationDetail;
	public boolean disabled;

	private NSMutableArray<EOEvaluation> _eoEvaluationArray;
	public EOEvaluation eoEvaluationItem;

	private NSArray<EORepartEvaNouvelleComp> _eoRepartNouvelleCompAbsentesArray;
	public EORepartEvaNouvelleComp eoRepartNouvelleCompAbsencesItem;

	private NSArray<EOReferensCompetences> _eoReferensCompetencesArray;
	public EOReferensCompetences eoReferensCompetencesItem;

	public BlocCompetencesAnnexesEntretien(WOContext context) {
		super(context);
	}

	/**
	 * La liste des évaluations coincident avec la fiche de poste et son occupant
	 * 
	 * @return
	 */
	private NSMutableArray<EOEvaluation> getEoEvaluationArray() {
		if (_eoEvaluationArray == null) {

			_eoEvaluationArray = new NSMutableArray<EOEvaluation>();

			if (eoAffectationDetail != null) {

				NSArray<EOEvaluation> array = EOEvaluation.getEvaluationsForIndividu(this.edc(), eoAffectationDetail.toAffectation().toIndividu());

				// filtrer par rapport aux dates l'occupation et de la fiche de poste

				// prendre la fenetre la plus restreinte
				NSTimestamp dDebut = null;
				if (DateCtrl.isBefore(eoAffectationDetail.dDebut(), eoFicheDePoste.dDebut())) {
					dDebut = eoFicheDePoste.dDebut();
				} else {
					dDebut = eoAffectationDetail.dDebut();
				}

				NSTimestamp dFin = null;
				if (!(eoAffectationDetail.dFin() != null && eoFicheDePoste.dFin() != null)) {
					if (eoAffectationDetail.dFin() == null) {
						dFin = eoFicheDePoste.dFin();
					} else if (eoFicheDePoste.dFin() == null) {
						dFin = eoFicheDePoste.dFin();
					} else {
						if (DateCtrl.isAfter(eoAffectationDetail.dFin(), eoFicheDePoste.dFin())) {
							dFin = eoFicheDePoste.dFin();
						} else {
							dFin = eoAffectationDetail.dFin();
						}
					}
				}

				CktlLog.log("dDebut=" + dDebut + " dFin=" + dFin);

				for (EOEvaluation eoEvaluation : array) {
					CktlLog.log("eoEvaluation.toEvaluationPeriode().epeDDebut()=" + eoEvaluation.toEvaluationPeriode().epeDDebut() + " eoEvaluation.toEvaluationPeriode().epeDFin()=" + eoEvaluation.toEvaluationPeriode().epeDFin());
					if (DateCtrl.isAfterEq(dDebut, eoEvaluation.toEvaluationPeriode().epeDDebut()) &&
							DateCtrl.isBeforeEq(dDebut.timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0), eoEvaluation.toEvaluationPeriode().epeDFin()) && (
									dFin == null || DateCtrl.isAfter(dFin, eoEvaluation.toEvaluationPeriode().epeDFin()))) {
						_eoEvaluationArray.addObject(eoEvaluation);
						CktlLog.log("OK");
					} else {
						CktlLog.log("NOK");
					}

				}

			}

		}
		return _eoEvaluationArray;
	}

	/**
	 * Les compétences annexes réperées mais non contenues dans l'évaluation
	 * 
	 * @return
	 */
	public NSArray<EORepartEvaNouvelleComp> getEoRepartNouvelleCompAbsentesArray() {
		if (_eoRepartNouvelleCompAbsentesArray == null) {

			_eoRepartNouvelleCompAbsentesArray = new NSArray<EORepartEvaNouvelleComp>();

			NSArray<EORepartEvaNouvelleComp> repartArray = NSArrayCtrl.flattenArray(
					(NSArray) getEoEvaluationArray().valueForKeyPath(EOEvaluation.TOS_REPART_EVA_NOUVELLE_COMP_KEY));

			for (EORepartEvaNouvelleComp eoRepart : repartArray) {
				if (!eoFicheDePoste.tosReferensCompetences().containsObject(eoRepart.toReferensCompetences())) {
					_eoRepartNouvelleCompAbsentesArray = _eoRepartNouvelleCompAbsentesArray.arrayByAddingObject(eoRepart);
				}
			}
		}
		return _eoRepartNouvelleCompAbsentesArray;
	}

	/**
	 * @return
	 */
	public NSArray<EOReferensCompetences> getEoReferencesCompetencesArray() {
		if (_eoReferensCompetencesArray == null) {
			_eoReferensCompetencesArray = NSArrayCtrl.removeDuplicate(
					NSArrayCtrl.flattenArray(
							(NSArray<EOReferensCompetences>) getEoRepartNouvelleCompAbsentesArray().valueForKey(EORepartEvaNouvelleComp.TO_REFERENS_COMPETENCES_KEY)));
		}
		return _eoReferensCompetencesArray;
	}

	/**
	 * @return
	 */
	public NSArray<EORepartEvaNouvelleComp> getEoRepartNouvelleCompAbsenceForCompetenceItemArray() {
		NSArray<EORepartEvaNouvelleComp> array = new NSArray<EORepartEvaNouvelleComp>();

		array = EOQualifier.filteredArrayWithQualifier(
				getEoRepartNouvelleCompAbsentesArray(),
				ERXQ.equals(EORepartEvaNouvelleComp.TO_REFERENS_COMPETENCES_KEY, eoReferensCompetencesItem));

		return array;
	}

	/**
	 * Recopier l'ensemble des compétences repérées vers la fiche de poste
	 * 
	 * @return
	 * @throws Exception
	 */
	public WOComponent doRecopierVersFiche() throws Exception {
		eoFicheDePoste.addReferensCompetencesArray(getEoReferencesCompetencesArray());
		UtilDb.save(ec, "");
		return null;
	}

	public final EOFicheDePoste getEoFicheDePoste() {
		return eoFicheDePoste;
	}

	public final void setEoFicheDePoste(EOFicheDePoste eoFicheDePoste) {
		this.eoFicheDePoste = eoFicheDePoste;
		razCache();
	}

	public final EOAffectationDetail getEoAffectationDetail() {
		return eoAffectationDetail;
	}

	public final void setEoAffectationDetail(EOAffectationDetail eoAffectationDetail) {
		this.eoAffectationDetail = eoAffectationDetail;
		razCache();
	}

	private void razCache() {
		_eoEvaluationArray = null;
		_eoRepartNouvelleCompAbsentesArray = null;
		_eoReferensCompetencesArray = null;
	}
}