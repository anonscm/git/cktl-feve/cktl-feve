package org.cocktail.feve.components.fichedeposte;

import java.io.IOException;

import org.cocktail.feve.app.Session;
import org.cocktail.feve.app.print.GenerateurPDFCtrl;
import org.cocktail.feve.app.print.XMLGenerateur;
import org.cocktail.feve.components.common.A_FeveSubMenuPageControled;
import org.cocktail.feve.components.miniCV.MiniCvCTRL;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividuGrhService;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 * 
 * @author ctarade
 * 
 */
public class CompFicheDePoste
		extends A_FeveSubMenuPageControled {

	// variables entrantes
	public MiniCvCTRL miniCVCTRL;

	private EOFicheDePoste inputLaFicheDePoste;
	private EOFicheDePoste inputLaFicheDePostePrecedente;
	
	//
	private boolean hasDroitVisualiserOccupants = false;
	public boolean showEditionFicheDePoste = false;
	public boolean showEditionProfilDePoste = false;
	
	private GenerateurPDFCtrl generateurPDF;

	// l'affectation detail selectionnée dans le sous composnte
	// CompFicheDePosteAgent
	private EOAffectationDetail eoAffectationDetailSelected;

	public CompFicheDePoste(WOContext context) {
		super(context);
		miniCVCTRL = new MiniCvCTRL();
		miniCVCTRL.setIndividuCV(session.individuConnecte());
		setSelectedItemMenu(MENU_ITEM_IDENTITE);
	}

	// gestion du menu

	private final static String MENU_ITEM_IDENTITE = "Identit&eacute; de l'agent";
	private final static String MENU_ITEM_SERVICE = "Service - Poste";
	private final static String MENU_ITEM_DESCRIPTION = "Description du poste";
	private final static String MENU_ITEM_MINI_CV = "Mini CV";

	public NSArray getMenuItems() {
		NSMutableArray menuItems = new NSMutableArray();
		// par defaut, on voit les infos d'environement et la description
		if (inputLaFicheDePoste() != null) {
			menuItems = new NSMutableArray(new String[] {
					MENU_ITEM_SERVICE, MENU_ITEM_DESCRIPTION });
			if (session.niveauConnexion() != Session.NIVEAU_DA_FICHE_DE_POSTE) {
				//
				if (hasDroitVisualiserOccupants) {
					menuItems.insertObjectAtIndex(MENU_ITEM_IDENTITE, 0);
					menuItems.insertObjectAtIndex(MENU_ITEM_MINI_CV, 3);
				} 
				
				
				if (getEoAffectationDetailSelected() != null) {
					
					EOIndividu individu = getEoAffectationDetailSelected().toAffectation().toIndividu();
					
					showEditionFicheDePoste = feveUserInfo().getAutorisation().hasDroitUtilisationEditerFichePoste(individu);
					showEditionProfilDePoste = feveUserInfo().getAutorisation().hasDroitUtilisationEditerPoste(individu);
					
				} else {
					
					showEditionFicheDePoste = feveUserInfo().getAutorisation().hasDroitUtilisationEditerFichePoste(inputLaFicheDePoste().toPoste().toStructure());
					showEditionProfilDePoste = feveUserInfo().getAutorisation().hasDroitUtilisationEditerPoste(inputLaFicheDePoste().toPoste().toStructure());
					
				}
				
				
			}
		}
		return menuItems.immutableClone();
	}

	// setter

	/**
	 * detection du changement du point d'entree -> RAZ
	 */
	public void setInputLaFicheDePoste(EOFicheDePoste value) {
		inputLaFicheDePostePrecedente = inputLaFicheDePoste;
		inputLaFicheDePoste = value;
		this.eoAffectationDetailSelected = inputLaFicheDePoste.toAffectationDetailActuelleUniquement();
		if (inputLaFicheDePoste != inputLaFicheDePostePrecedente) {
			setSelectedItemMenu(null);
			// determiner s'il peut voir les occupants de la fiche -->
			// la partie identit� n'est visible que si la personnel connectee
			// le droit de consuler ou de modifier le poste
			if (getEoAffectationDetailSelected() != null) {
				hasDroitVisualiserOccupants = feveUserInfo().getAutorisation().hasDroitShowGererFichePoste(getEoAffectationDetailSelected().toAffectation().toIndividu());
			} else {
				hasDroitVisualiserOccupants = feveUserInfo().getAutorisation().hasDroitShowGererFichePoste(inputLaFicheDePoste().toPoste().toStructure());
			}
			
		}
		selectionerMiniCV();
		if (getEoAffectationDetailSelected() != null) {
			miniCVCTRL.setIndividuCV(getEoAffectationDetailSelected().toAffectation().toIndividu());
		}
		
	}

	// getters

	public EOFicheDePoste inputLaFicheDePoste() {
		return inputLaFicheDePoste;
	}

	// boolean interface
	public boolean isPageFdp1() {
		return getSelectedItemMenu().equals(MENU_ITEM_IDENTITE);
	}

	public boolean isPageFdp2() {
		return getSelectedItemMenu().equals(MENU_ITEM_SERVICE);
	}

	public boolean isPageFdp3() {
		return getSelectedItemMenu().equals(MENU_ITEM_DESCRIPTION);
	}

	public boolean isPageFdp4() {
		return getSelectedItemMenu().equals(MENU_ITEM_MINI_CV);
	}

	
	
	public boolean isInputFicheDePosteExiste() {
		return inputLaFicheDePoste() != null;
	}
	
	public void selectionerMiniCV() {
		if (session.isChargerMiniCV) {
			setSelectedItemMenu(MENU_ITEM_MINI_CV);
			session.isChargerMiniCV = false;
		}
	}
	

	public WOActionResults genererFicheDePoste() throws IOException {
		XMLGenerateur xmlGenerateur = new XMLGenerateur(edc(), (Session) session());
		String xml = xmlGenerateur.generateFicheDePosteXML(inputLaFicheDePoste,true);
		generateurPDF = new GenerateurPDFCtrl(xml);
		generateurPDF.genererFicheDePoste();		
		return null;
	}
	
	public String fichedePosteFilename()
	{
		IndividuGrhService individuService = new IndividuGrhService();
		NSDictionary dicoAgent = individuService.findDicoAgentGepetoInContext(edc(), inputLaFicheDePoste.toAffectationDetailActuelle());
		
		String identifiant = StringCtrl.checkString((String)dicoAgent.objectForKey("identifiant"));
		String nomUsuel = StringCtrl.checkString((String)dicoAgent.objectForKey("nomUsuel"));
		String prenom = StringCtrl.checkString((String)dicoAgent.objectForKey("prenom"));
		String libelle= StringCtrl.checkString(inputLaFicheDePoste.display());
		
		return "FicheDePoste_"+prenom+"_"+nomUsuel+"_"+identifiant+"_"+libelle+".pdf";
	}
	
	public String profildePosteFilename()
	{		
		return "ProfilDePoste_"+inputLaFicheDePoste.toPoste().posLibelleBasic()+".pdf";
	}
	

	public WOActionResults genererProfilDePoste() throws IOException {
		XMLGenerateur xmlGenerateur = new XMLGenerateur(edc(), (Session) session());
		String xml = xmlGenerateur.generateFicheDePosteXML(inputLaFicheDePoste,false);
		generateurPDF = new GenerateurPDFCtrl(xml);
		generateurPDF.genererFicheDePoste();		
		return null;
	}
	
	
	public GenerateurPDFCtrl getGenerateurPDF() {
		return generateurPDF;
	}

	public void setGenerateurPDF(GenerateurPDFCtrl generateurPDF) {
		this.generateurPDF = generateurPDF;
	}

	public EOAffectationDetail getEoAffectationDetailSelected() {
		return eoAffectationDetailSelected;
	}

	public void setEoAffectationDetailSelected(
			EOAffectationDetail eoAffectationDetailSelected) {
		this.eoAffectationDetailSelected = eoAffectationDetailSelected;
	}
	
}