package org.cocktail.feve.components.administration.periode;
import org.cocktail.feve.components.common.A_FeveSubMenuPage;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;


public class PageAdministrationPeriode extends A_FeveSubMenuPage {

  public final static String ITEM_MENU_PERIODE			   	= "P&eacute;riodes d'entretien";
  
  public PageAdministrationPeriode(WOContext context) {
    super(context);
  }
  
  public boolean isPeriode()	 				              {   return getSelectedItemMenu().equals(ITEM_MENU_PERIODE); }
  
  public NSArray getMenuItems() {
		return new NSArray(new String[]{ITEM_MENU_PERIODE});
	}

}