package org.cocktail.feve.components.administration;

/*
 * Copyright Universit� de La Rochelle 2004
 *
 * ctarade@univ-lr.fr
 *
 * Ce logiciel est un programme informatique servant � g�rer les comptes
 * informatiques des utilisateurs. 
 * 
 * Ce logiciel est r�gi par la licence CeCILL soumise au droit fran�ais et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffus�e par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilit� au code source et des droits de copie,
 * de modification et de redistribution accord�s par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limit�e.  Pour les m�mes raisons,
 * seule une responsabilit� restreinte p�se sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les conc�dants successifs.

 * A cet �gard  l'attention de l'utilisateur est attir�e sur les risques
 * associ�s au chargement,  � l'utilisation,  � la modification et/ou au
 * d�veloppement et � la reproduction du logiciel par l'utilisateur �tant 
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � 
 * manipuler et qui le r�serve donc � des d�veloppeurs et des professionnels
 * avertis poss�dant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invit�s � charger  et  tester  l'ad�quation  du
 * logiciel � leurs besoins dans des conditions permettant d'assurer la
 * s�curit� de leurs syst�mes et ou de leurs donn�es et, plus g�n�ralement, 
 * � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�. 

 * Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accept� les
 * termes.
 */

import org.cocktail.feve.app.Application;
import org.cocktail.feve.components.common.A_FeveSubMenuPage;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class PageAdministration
		extends A_FeveSubMenuPage {

	public PageAdministration(WOContext context) {
		super(context);
	}

	public final static String MENU_DROITS_EVALUATION = "P&eacute;riodes";
	public final static String MENU_SUIVI = "Outils de suivi";
	public final static String MENU_LOLF = "LOLF";
	public final static String MENU_OUVERTURE = "Param&egrave;tres g&eacute;n&eacute;raux";
	public final static String MENU_GESTION_DROITS = "Gestion des droits";

	// gestion du menu
	public NSArray<String> getMenuItems() {
		
		NSArray<String> menuItems = new NSMutableArray<String>();

		if (feveUserInfo().getAutorisation().hasDroitShowGererPeriodeCampagne()) {
			menuItems.add(MENU_DROITS_EVALUATION);
		}
		
		if (feveUserInfo().getAutorisation().hasDroitShowAdminOutilsSuivi()) {
			menuItems.add(MENU_SUIVI);
		}
		
		if (feveUserInfo().getAutorisation().hasDroitShowAdminAutres() || feveUserInfo().getAutorisation().hasDroitShowAdminPeriodeSaisie() ||
				feveUserInfo().getAutorisation().hasDroitShowAdminConditionsEvaluation() || feveUserInfo().getAutorisation().hasDroitShowAdminFormations()
				|| feveUserInfo().getAutorisation().hasDroitShowAdminCreationPoste() || feveUserInfo().getAutorisation().hasDroitShowAdminEditionFichePoste()) {
			menuItems.add(MENU_OUVERTURE);
		}
		
		if (feveUserInfo().getAutorisation().hasDroitShowAdminSillandLolf() || feveUserInfo().getAutorisation().hasDroitShowAdminSynchronisationLolf()) {
			if (Application.application().config().booleanForKey("APP_AFFICHER_LOLF")) {
				menuItems.add(MENU_LOLF);
			}
		}
		
		if (feveUserInfo().getAutorisation().hasDroitShowGererDroits()) {
			menuItems.add(MENU_GESTION_DROITS);
		}
		
		return menuItems;
		
	}

	public boolean isPageAdministrationPeriode() {
		return getSelectedItemMenu().equals(MENU_DROITS_EVALUATION);
	}

	public boolean isPageAdministrationSuivi() {
		return getSelectedItemMenu().equals(MENU_SUIVI);
	}

	public boolean isPageAdministrationLofl() {
		return getSelectedItemMenu().equals(MENU_LOLF);
	}

	public boolean isPageAdministrationOuverture() {
		return getSelectedItemMenu().equals(MENU_OUVERTURE);
	}
	
	public boolean isPageAdminGestionDroits() {
		return getSelectedItemMenu().equals(MENU_GESTION_DROITS);
	}

	/**
	 * Pouvoir faire la selection de la page de droits sur evaluation depuis une
	 * autre page
	 */
	public void selectMenuDroitEvaluation(EOIndividu value) {
		setSelectedItemMenu(MENU_DROITS_EVALUATION);
	}

}