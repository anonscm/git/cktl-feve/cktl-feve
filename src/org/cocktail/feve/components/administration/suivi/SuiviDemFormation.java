package org.cocktail.feve.components.administration.suivi;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationPeriode;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFicheItem;
import org.cocktail.fwkcktlgrh.common.metier.EORepartFormationSouhaitee;
import org.cocktail.fwkcktlgrh.common.metier.EORepartPerspectiveFormation;
import org.cocktail.fwkcktlgrh.common.metier.EOTplItem;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_RepartFormation;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_ToEvaluation;
import org.cocktail.fwkcktlpersonne.common.metier.EOFormationPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOVService;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

/**
 * Page de suivi des demandes de formation
 * 
 * @author ctarade
 */
public class SuiviDemFormation
		extends A_SuiviGeneric {

	// liste des formations bloc champ libre
	public NSMutableArray<EORepartPerspectiveFormation> repartFormationList;
	public EORepartPerspectiveFormation repartFormationItem;

	//
	public WODisplayGroup periodeDg;
	public WODisplayGroup serviceDg;

	public SuiviDemFormation(WOContext context) {
		super(context);
		initComponent();
	}

	/**
	 * 
	 */
	private void initComponent() {
		// on preselectionne la periode actuelle
		periodeSelected = EOEvaluationPeriode.getCurrentPeriode(ec);
		// ne pas rafraichir le DG principal tout se suite
		shouldRefreshMainDg = false;
		
		repartFormationList = new NSMutableArray<EORepartPerspectiveFormation>();
	}

	/**
	 * XXX plus utilisé en ajax ... On n'affiche que les evaluation avec une
	 * demande de formation
	 */
	protected void doRefreshMainDg() {

	}

	/**
	 *
	 */
	public void setServiceSelected(EOVService value) {
		serviceSelected = value;
		repartFormationList = new NSMutableArray<EORepartPerspectiveFormation>();
	}

	/**
	 * 
	 */
	public void setPeriodeSelected(EOEvaluationPeriode value) {
		periodeSelected = value;
		repartFormationList = new NSMutableArray<EORepartPerspectiveFormation>();
	}

	
	public NSArray<EORepartPerspectiveFormation> getRepartFormationList() {
		if (NSArrayCtrl.isEmpty(repartFormationList)) {
			NSArray<EORepartPerspectiveFormation> result = new NSMutableArray<EORepartPerspectiveFormation>();
			NSArray<EORepartPerspectiveFormation>  listeFormations = EORepartPerspectiveFormation.fetchAll(edc());
			
			for (EORepartPerspectiveFormation formation : listeFormations) {
				if (formation.toEvaluation().toEvaluationPeriode().equals(periodeSelected)) {
					if (serviceSelected != null && formation.toEvaluation().tosStructure().contains(serviceSelected.toStructure())) {
						result.add(formation);
					} else if (serviceSelected == null){
						result.add(formation);
					}
				}
			}
			repartFormationList.addAll(result);
		}
		
		
		return repartFormationList;
	}

	/**
	 * Pas besoin ici
	 */
	protected WODisplayGroup mainDg() {
		return null;
	}

	/**
	 * Pas besoin ici
	 */
	protected String prefixEntityDgToPoste() {
		return null;
	}

	// edition

	public WOResponse printCsv() {
		CktlDataResponse resp = new CktlDataResponse();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < getRepartFormationList().count(); i++) {
			EORepartPerspectiveFormation repart = getRepartFormationList().objectAtIndex(i);
			String nomPrenomEvaluateur = "inconnu";
			if (repart.toEvaluation() != null) {
				nomPrenomEvaluateur = repart.toEvaluation().getNomPrenomEvaluateur();
			}
			sb.append(nomPrenomEvaluateur).append(CSV_COLUMN_SEPARATOR);
			sb.append(repart.toEvaluation().toIndividu().nomPrenom()).append(CSV_COLUMN_SEPARATOR);
			String libelleFormation = repart.libelle().trim();
			libelleFormation = StringCtrl.replace(libelleFormation, "\n", " / ");
			libelleFormation = StringCtrl.replace(libelleFormation, ";", " / ");
			libelleFormation = StringCtrl.replace(libelleFormation, "\r", "");
			sb.append(libelleFormation).append(CSV_COLUMN_SEPARATOR);
			String strDTenueEntretien = "non fait";
			if (repart.toEvaluation().isEntretienTenu()) {
				strDTenueEntretien = DateCtrl.dateToString(repart.toEvaluation().dTenueEntretien());
			}
			sb.append(strDTenueEntretien).append(CSV_COLUMN_SEPARATOR);
			String strDVisaResponsable = "non visé";
			if (repart.toEvaluation().isViseParResponsableRh()) {
				strDVisaResponsable = DateCtrl.dateToString(repart.toEvaluation().dVisaResponsableRh());
			}
			sb.append(strDVisaResponsable).append(CSV_COLUMN_SEPARATOR);
			sb.append(CSV_NEW_LINE);
		}
		NSData stream = new NSData(sb.toString(), CSV_ENCODING);
		resp.setContent(stream);
		resp.setContentEncoding(CSV_ENCODING);
		resp.setHeader(String.valueOf(stream.length()), "Content-Length");
		resp.setFileName("suivi_demandes_formation.csv");
		return resp;
	}

	// classement

	public void sortEvaluateur() {
		faireClassement(I_ToEvaluation.SORT_EVALUATEUR);
	}

	public void sortAgent() {
		faireClassement(I_ToEvaluation.SORT_AGENT);
	}

	public void sortFormationSouhaitee() {
		faireClassement(I_RepartFormation.SORT_FORMATION_SOUHAITEE);
	}

	public void sortIsNomenclature() {
		faireClassement(I_RepartFormation.SORT_IS_NOMENCLATURE);
	}

	public void sortDateEntretien() {
		faireClassement(I_ToEvaluation.SORT_DATE_ENTRETIEN);
	}

	public void sortDateVisaRh() {
		faireClassement(I_ToEvaluation.SORT_DATE_VISA_RH);
	}

	// modification des données

	public EORepartPerspectiveFormation repartFormationSelected;
	public EOFormationPersonnel eoFormationPersonnelSelected;
	public String champLibre;
	public boolean isNomenclature;

	/**
	 * Modification de la formation en cours
	 */
	public WOComponent editFormation() {
//		repartFormationSelected = (EORepartPerspectiveFormation) repartFormationItem;
//		eoFormationPersonnelSelected = repartFormationSelected.toFormationPersonnel();
//		champLibre = repartFormationSelected.libelleFormation();
//		mode = MODE_EDIT;
		
		//TODO
		return null;
	}

	// mode d'utilisation du composant
	public int mode;
	private final static int MODE_READ = 0;
	private final static int MODE_EDIT = 1;

	/**
	 * On autorise la modification de la formation {@link #repartFormationItem}
	 * uniquement en lecture seule, et pour les données provenant de l'entité
	 * {@link EORepartFormationSouhaitee}
	 */
	public boolean isShowLnkEditFormation() {
		boolean isShow = true;

		if (mode != MODE_READ) {
			isShow = false;
		}

		if (isShow &&
				!(repartFormationItem instanceof EORepartPerspectiveFormation)) {
			isShow = false;
		}

		return isShow;
	}

	/**
	 * Determine quelle est la ligne en cours de modification. On ne modifie que
	 * si le mode le permet et que si la formation en cours
	 * <code>repartFormationItem</code> est la meme que la selection
	 * <code>repartFormationSelected</code>
	 */
	public boolean isEditingCurrentFormation() {
		boolean isEditing = false;

		if (mode == MODE_EDIT) {
			isEditing = (repartFormationItem == repartFormationSelected);
		}

		return isEditing;
	}

	/**
	 * Enregistrement des modifications d'une formation existante
	 * 
	 * @return
	 * @throws Throwable
	 */
	public WOComponent doEditFormation() throws Throwable {
		clearError();

		return null;
	}

	/**
	 * Sauvegarder l'operation en cours et met a jour le message d'erreur s'il y
	 * en a une
	 * 
	 * @return <code>true</code> si aucun pb
	 */
	private boolean sauverEtGestionMessageErreur() {
		boolean noError = true;
		ec.lock();
		try {
			ec.saveChanges();
		} catch (Throwable e) {
			errorMessage = e.getMessage();
			ec.revert();
			noError = false;
		} finally {
			ec.unlock();
		}
		return noError;
	}

	// message d'erreur
	public String errorMessage;

	private void clearError() {
		errorMessage = StringCtrl.emptyString();
	}

	/**
	 * On ne peut annuler une modification de la formation en cours
	 * <code>repartFormationSelected</code> (formulaire active)
	 */
	public boolean isShowLnkCancel() {
		boolean isShow = false;

		if (mode == MODE_EDIT &&
				repartFormationItem == repartFormationSelected) {
			isShow = true;
		}

		return isShow;
	}

	/**
	 * Annuler l'operation en cours
	 * 
	 * @return
	 */
	public WOComponent doCancel() {
		// annuler les modifications / insertions
		ec.revert();
		// nettoyage des eventuelles erreurs
		clearError();
		// repasser en mode consultation
		mode = MODE_READ;
		return null;
	}

	@Override
	public void doApresClassement() {
		repartFormationList = null;
	}

}