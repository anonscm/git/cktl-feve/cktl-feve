package org.cocktail.feve.components.administration;

import org.cocktail.feve.components.common.A_FeveSubMenuPage;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * Page rassemblant l'ensemble des outils de gestion des droits
 * 
 * @author ctarade
 */
public class PageAdminLolf extends A_FeveSubMenuPage {

	public final static String ITEM_MENU_LOLF_GESTION   							= "Gestion";
	public final static String ITEM_MENU_LOLF_SYNC					      		= "Synchroniser les fiches";


	public PageAdminLolf(WOContext context) {
		super(context);
	}

	public NSArray<String> getMenuItems() {
		NSArray<String> menuItems = new NSMutableArray<String>();
		if (feveUserInfo().getAutorisation().hasDroitUtilisationAdminSillandLolf()) {
			menuItems.add(ITEM_MENU_LOLF_GESTION);
		}
		if (feveUserInfo().getAutorisation().hasDroitUtilisationAdminSynchronisationLolf()) {
			menuItems.add(ITEM_MENU_LOLF_SYNC);
		}
		return menuItems;
	}

	public boolean isLolfGestion()   	{   return getSelectedItemMenu().equals(ITEM_MENU_LOLF_GESTION); }
	public boolean isLolfSync()      	{   return getSelectedItemMenu().equals(ITEM_MENU_LOLF_SYNC); }



}