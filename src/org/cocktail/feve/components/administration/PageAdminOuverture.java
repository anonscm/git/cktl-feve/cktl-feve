package org.cocktail.feve.components.administration;

import org.cocktail.feve.app.Application;
import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.AfwkGRHRecord;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveParametres;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Ecran de saisie des dates d'ouvertures et de fermeture des données
 * 
 * @author ctarade
 */
public class PageAdminOuverture extends FeveWebComponent {

	/** dates d'ouvertures */
	public NSTimestamp evaluationSaisieDDebut, evaluationSaisieDFin, ficheLolfSaisieDDebut, ficheLolfSaisieDFin;
	/** */
	public String libelleCreationPosteValeurParDefaut;
	/** */
	private boolean isVisaDirecteur;
	/** */
	public Integer dureeMinimumAffectationPourEvaluation;
	
	private String libelle1;
	public String libelle2;

	/** le dictionnaire contenant toutes les variable de type boolean */
	public NSMutableDictionary<String, Boolean> dicoBoolean;
	/** la liste de toutes les cles du dictionnaire de boolean */
	public NSArray<String> keyList = EOFeveParametres.ARRAY_KEY_BOOLEAN;
	public String keyItem;
	
	public PageAdminOuverture(WOContext context) {
		super(context);
		initComponent();
	}

	private void initComponent() {
		restore();
	}

	// manipulation des données

	/**
	 * Resynchroniser les données du formulaire avec celle des parametres
	 */
	private void restore() {
		evaluationSaisieDDebut = app.getDateParamValueForKey(EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_DEBUT);
		evaluationSaisieDFin = app.getDateParamValueForKey(EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_FIN);
		ficheLolfSaisieDDebut = app.getDateParamValueForKey(EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_DEBUT);
		ficheLolfSaisieDFin = app.getDateParamValueForKey(EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_FIN);
		libelleCreationPosteValeurParDefaut = app.getParamValueForKey(EOFeveParametres.KEY_FEV_LIBELLE_CREATION_POSTE_VALEUR_PAR_DEFAUT);
		dureeMinimumAffectationPourEvaluation = app.getIntegerParamValueForKey(EOFeveParametres.KEY_FEV_DUREE_MINIMUM_AFFECTATION_POUR_EVALUATION);

		app.getParamValueForKey(EOFeveParametres.KEY_FEV_IS_VISA_DIRECTEUR_FICHE_DE_POSTE);
		
		if (app.getParamValueForKey(EOFeveParametres.KEY_FEV_IS_VISA_DIRECTEUR_FICHE_DE_POSTE) != null && app.getParamValueForKey(EOFeveParametres.KEY_FEV_IS_VISA_DIRECTEUR_FICHE_DE_POSTE).equals("O")) {
			isVisaDirecteur = true;
		} else {
			isVisaDirecteur = false;
		}
		
		setLibelle1(app.getParamValueForKey(EOFeveParametres.KEY_FEV_IS_LIBELLE_SIGNATURE_AGENT_FICHE_DE_POSTE));
		libelle2 = app.getParamValueForKey(EOFeveParametres.KEY_FEV_IS_LIBELLE_SIGNATURE_SUPP_AGENT_FICHE_DE_POSTE);
		
		dicoBoolean = new NSMutableDictionary<String, Boolean>();
		for (int i = 0; i < keyList.count(); i++) {
			String key = (String) keyList.objectAtIndex(i);
			dicoBoolean.setObjectForKey(app.getBooleanParamValueForKey(key), key);
		}
	}

	/**
	 * Sauvegarder les modifications
	 * 
	 * @throws Throwable
	 */
	public WOComponent doSave() throws Throwable {
		if (libelle1 != null) {
			update(EOFeveParametres.KEY_FEV_IS_LIBELLE_SIGNATURE_AGENT_FICHE_DE_POSTE, libelle1);
		}
		
		if (libelle2 != null) {
			update(EOFeveParametres.KEY_FEV_IS_LIBELLE_SIGNATURE_SUPP_AGENT_FICHE_DE_POSTE, libelle2);
		}
		
		if (evaluationSaisieDDebut != null) {
			update(EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_DEBUT, DateCtrl.dateToString(evaluationSaisieDDebut));
		}
		if (evaluationSaisieDFin != null) {
			update(EOFeveParametres.KEY_FEV_EVALUATION_SAISIE_D_FIN, DateCtrl.dateToString(evaluationSaisieDFin));
		}
		if (ficheLolfSaisieDDebut != null) {
			update(EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_DEBUT, DateCtrl.dateToString(ficheLolfSaisieDDebut));
		}
		if (ficheLolfSaisieDFin != null) {
			update(EOFeveParametres.KEY_FEV_FICHE_LOLF_SAISIE_D_FIN, DateCtrl.dateToString(ficheLolfSaisieDFin));
		}
		
		if (isVisaDirecteur) {
			update(EOFeveParametres.KEY_FEV_IS_VISA_DIRECTEUR_FICHE_DE_POSTE, "O");
		} else {
			update(EOFeveParametres.KEY_FEV_IS_VISA_DIRECTEUR_FICHE_DE_POSTE, "N");
		}
		
		update(EOFeveParametres.KEY_FEV_LIBELLE_CREATION_POSTE_VALEUR_PAR_DEFAUT, libelleCreationPosteValeurParDefaut);
		if (dureeMinimumAffectationPourEvaluation != null) {
			update(EOFeveParametres.KEY_FEV_DUREE_MINIMUM_AFFECTATION_POUR_EVALUATION, Integer.toString(dureeMinimumAffectationPourEvaluation.intValue()));
		}

		for (int i = 0; i < keyList.count(); i++) {
			String key = (String) keyList.objectAtIndex(i);
			Boolean value = (Boolean) dicoBoolean.objectForKey(key);
			update(key, booleanToString(value));
		}

		try {
			UtilDb.save(ec(), "");
			restore();
			feveUserInfo().clearParamCache();
			session.addSimpleInfoMessage("Sauvegarde OK", "Vos modifications ont bien été enregistrées");
		} catch (Exception e) {
			session.addSimpleErrorMessage("Erreur de sauvegarde", e);
		}

		return null;
	}

	/**
	 * Annuler les modifications
	 * 
	 * @return
	 */
	public WOComponent doCancel() {
		restore();
		session.addSimpleInfoMessage("RAZ OK", "Votre demande d'annulation a bien été prise en compte");
		return null;
	}

	/**
	 * Effectuer la mise a jour d'un enregistrement de la table
	 * Feve.FEVE_PARAMETRES
	 * 
	 * @param paramKey
	 */
	private void update(String paramKey, String paramValue) {
		EOFeveParametres record = EOFeveParametres.fetchFirstByQualifier(
				ec(), CktlDataBus.newCondition(EOFeveParametres.PARAM_KEY_KEY + "='" + paramKey + "'"));
		if (record != null) {
			record.setParamValue(paramValue);
			app.clearCache(paramKey);
		}
	}

	/**
	 * L'editingcontext utilisé pour ces données est particulier puisque c'est
	 * celui de Application
	 * 
	 * @return
	 */
	private EOEditingContext ec() {
		return app.paramEditingContext();
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private static String booleanToString(Boolean value) {
		return value.booleanValue() ? AfwkGRHRecord.OUI : AfwkGRHRecord.NON;
	}

	public boolean isVisaDirecteur() {
		return this.isVisaDirecteur;
	}

	public void setVisaDirecteur(boolean isVisaDirecteur) {
		this.isVisaDirecteur = isVisaDirecteur;
	}

	public String getLibelle1() {
		setLibelle1(app.getParamValueForKey(EOFeveParametres.KEY_FEV_IS_LIBELLE_SIGNATURE_AGENT_FICHE_DE_POSTE));
		return libelle1;
	}

	public void setLibelle1(String libelle1) {
		this.libelle1 = libelle1;
	}

	public boolean periodeSaisieDisabled() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationAdminPeriodeSaisie();
	}

	public boolean activitesAutresDisabled() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationAdminAutres();
	}

	public boolean showCreationPoste() {
		return feveUserInfo().getAutorisation().hasDroitShowAdminCreationPoste();
	}
	
	public boolean creationPosteDisabled() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationAdminCreationPoste();
	}
	

	public boolean conditionEvaluationDisabled() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationAdminConditionsEvaluation();
	}
	
	public boolean formationSuiviDisabled() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationAdminFormations();
	}

	public boolean editionFichePosteDisabled() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationAdminEditionFichePoste();
	}
	
	public boolean showBtnSave() {
		
		return feveUserInfo().getAutorisation().hasDroitUtilisationAdminPeriodeSaisie() 
				|| feveUserInfo().getAutorisation().hasDroitUtilisationAdminAutres()
				|| feveUserInfo().getAutorisation().hasDroitUtilisationAdminConditionsEvaluation()
				|| feveUserInfo().getAutorisation().hasDroitUtilisationAdminConditionsEvaluation()
				|| feveUserInfo().getAutorisation().hasDroitUtilisationAdminCreationPoste()
				|| feveUserInfo().getAutorisation().hasDroitUtilisationAdminEditionFichePoste();
		
	}

	public boolean showAutres() {
		return feveUserInfo().getAutorisation().hasDroitShowAdminAutres();
	}
	public boolean showPeriodeSaisie() {
		return feveUserInfo().getAutorisation().hasDroitShowAdminPeriodeSaisie();
	}
	public boolean showFormations() {
		return feveUserInfo().getAutorisation().hasDroitShowAdminFormations();
	}
	public boolean showConditionsEvaluation() {
		return feveUserInfo().getAutorisation().hasDroitShowAdminConditionsEvaluation();
	}
	
	public boolean showEditionFichePoste() {
		return feveUserInfo().getAutorisation().hasDroitShowAdminEditionFichePoste();
	}
	
	public boolean isAfficherParramLolf() {
		return Application.application().config().booleanForKey("APP_AFFICHER_LOLF");
	}

}