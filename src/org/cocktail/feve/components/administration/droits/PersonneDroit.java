package org.cocktail.feve.components.administration.droits;

import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_Accreditation;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

public class PersonneDroit extends WOComponent {
    
	public I_Accreditation accreditation;
	public Boolean isCible;

	public PersonneDroit(WOContext context) {
		super(context);
	}

	/**
	 * @return
	 */
	public IPersonne personne() {
		IPersonne personne = null;
		if (isCible) {
			personne = accreditation.toPersonneCible();
		} else {
			personne = accreditation.toPersonneTitulaire();
		}
		return personne;
	}

	/**
	 * @return
	 */
	public boolean isIndividu() {
		boolean isIndividu = false;

		if (isCible) {
			isIndividu = accreditation.toPersonneCible().isIndividu();
		} else {
			isIndividu = accreditation.toPersonneTitulaire().isIndividu();
		}

		return isIndividu;
	}

	/**
	 * @return
	 */
	public boolean isServiceSimple() {
		boolean isServiceSimple = false;

		if (isCible) {
			isServiceSimple = !accreditation.isCibleChefDeService() && accreditation.toPersonneCible().isStructure();
		} 

		return isServiceSimple;
	}

	/**
	 * @return
	 */
	public boolean isChefDeService() {
		boolean isChefDeService = false;

		if (isCible) {
			isChefDeService = accreditation.isCibleChefDeService();
		}

		return isChefDeService;
	}

	/**
	 * @return
	 */
	public boolean isHeritage() {
		boolean isHeritage = false;

		if (isCible) {
			isHeritage = accreditation.isCibleHeritage();
		}

		return isHeritage;
	}

	/**
	 * @return
	 */
	public String getTitleGroupe() {
		String str = "";

		if (isCible) {
			str = "Tous les agents ayant une affectation RH au groupe";
		} else {
			str = "Tous les agents membres du groupe (via annuaire)";
		}

		return str;
	}
	
}