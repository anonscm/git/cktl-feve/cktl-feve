package org.cocktail.feve.components.administration.droits;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktlgrh.common.metier.EOFeveAnnulationDroit;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveDroit;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_Accreditation;
import org.cocktail.fwkcktlgrh.common.metier.services.StructureGrhService;
import org.cocktail.fwkcktlgrh.common.metier.util.IAccreditationCompare;
import org.cocktail.fwkcktlgrh.common.utilities.StringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.droits.DroitsHelper;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

public class PageAdminGestionDroits extends A_Droit {
	
	
	private WODisplayGroup dgDroit;
	private I_Accreditation accreditationItem;
	
	private CriteresRechercheAccreditation criteresRechercheAccreditation;
	
	public String globalContainerId = "ContainerGlobal";
	public String containerFiltreTitulaireId = "ContainerFiltreTitulaire";
	public String containerFiltreCibleId = "ContainerFiltreCible";
	public String filtresContainerId = "ContainerFiltre";
	
	private List<EOFeveAnnulationDroit> listeAnnulationDroit;
	
	public String getGlobalContainerId() {
		return globalContainerId;
	}


	public void setGlobalContainerId(String globalContainerId) {
		this.globalContainerId = globalContainerId;
	}
	
	
    public PageAdminGestionDroits(WOContext context) {
        super(context);
        initCriteresRecherche();
        initListeAnnulationDroit();
    }


	private void initCriteresRecherche() {
		criteresRechercheAccreditation = new CriteresRechercheAccreditation();
	}


	private void initListeAnnulationDroit() {
		listeAnnulationDroit = new ArrayList<EOFeveAnnulationDroit>();
        listeAnnulationDroit.addAll(EOFeveAnnulationDroit.fetchAll(edc()));
	}


	public WODisplayGroup getDgDroit() {
		if (dgDroit == null) {
			NSArray<I_Accreditation> listeAccreditation = new NSMutableArray<I_Accreditation>();
			listeAccreditation.addAll(EOFeveDroit.fetchAll(edc()));
			
			refreshDg(listeAccreditation);
		}
		
		
		return dgDroit;
	}

	
	private void refreshDg(NSArray<I_Accreditation> listeAccreditation) {
		
		
		listeAccreditation = restreindreListeAccreditation(listeAccreditation);
		Collections.sort(listeAccreditation, new IAccreditationCompare());
		this.dgDroit = new ERXDisplayGroup<I_Accreditation>();
		this.dgDroit.setDelegate(this);
		this.dgDroit.setSelectsFirstObjectAfterFetch(false);
		this.dgDroit.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECTS_PER_BATCH);
		this.dgDroit.setObjectArray(listeAccreditation);
		
	}


	private NSArray<I_Accreditation> restreindreListeAccreditation(NSArray<I_Accreditation> listeAccreditation) {
		
		NSArray<I_Accreditation> arrayAcc = new NSArray<I_Accreditation>(listeAccreditation);
		NSArray<I_Accreditation> arrayAccToKeep = new NSMutableArray<I_Accreditation>();
		
		for (I_Accreditation accreditation : arrayAcc) {
			if (feveUserInfo().getAutorisation().hasDroitUtilisationGererDroits(accreditation.toPersonneTitulaire())
				|| feveUserInfo().getAutorisation().hasDroitUtilisationGererDroits(accreditation.toPersonneCible())
				|| feveUserInfo().getAutorisation().hasDroitConnaissanceGererDroits(accreditation.toPersonneTitulaire())
				|| feveUserInfo().getAutorisation().hasDroitConnaissanceGererDroits(accreditation.toPersonneCible())) {
				arrayAccToKeep.add(accreditation);
			} 
		}
		return arrayAccToKeep;
		
	}

	
	public final WOActionResults doAnnulerDroit() {
		
		I_Accreditation accreditation = (I_Accreditation) (getDgDroit().selectedObject());
		
		if (accreditation != null) {
			EOFeveAnnulationDroit annulationDroit = EOFeveAnnulationDroit.getAnnulationDroitDansListe(accreditation, listeAnnulationDroit);
			boolean isAjout = false;
			if (annulationDroit == null) {
				annulationDroit = EOFeveAnnulationDroit.create(edc(), accreditation.toPersonneCible(), accreditation.toPersonneTitulaire(), accreditation.toTypeNiveauDroit());
				isAjout = true;
			} else {
				annulationDroit.delete();
			}
			
			try {
				edc().lock();
				edc().saveChanges();
				
				// forcer le rechargement de l'objet créé pour que les tomany se refasse
				// correctement
				if (isAjout) {
					edc().invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(annulationDroit.globalID()));
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				edc().revert();
			} finally {
				edc().unlock();
			}
			
			initListeAnnulationDroit();
		}
		
		return null;
	}

	/**
	 * Effacer le droit
	 * @throws Throwable 
	 */
	public final WOComponent doDelete() throws Throwable {

		if (getDgDroit().selectObject(accreditationItem)) {
			((EOFeveDroit) (getDgDroit().selectedObject())).delete();
			getDgDroit().deleteSelection();
			UtilDb.save(edc(), "");
		}

		return null;
	}


	public I_Accreditation getAccreditationItem() {
		return accreditationItem;
	}


	public void setAccreditationItem(I_Accreditation accreditationItem) {
		this.accreditationItem = accreditationItem;
	}

	/**
	 * 
	 * @return {@link NSArray<I_Accreditation>}
	 */
	private NSArray<I_Accreditation> getDeductedDroitsAdmins() {
		
		NSArray<I_Accreditation> arrayAccreditation = new NSMutableArray<I_Accreditation>();
		
		EOFeveTypeNiveauDroit tndAdmin = EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitAdmin(edc());
		EOFeveTypeNiveauDroit tndAdminFonc = EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitAdminFonc(edc());
		EOFeveTypeNiveauDroit tndDrh = EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitDrh(edc());
		
		
		NSArray<Integer> persIdsAdmin = DroitsHelper.personnesWithProfil(edc(), tndAdmin.toGdProfil());
		NSArray<Integer> persIdsAdminFonc = DroitsHelper.personnesWithProfil(edc(), tndAdminFonc.toGdProfil());
		NSArray<Integer> persIdsDrh = DroitsHelper.personnesWithProfil(edc(), tndDrh.toGdProfil());
		
		EOStructure etablissement = EOStructure.rechercherEtablissement(edc());
		
		for (Integer persId : persIdsDrh) {
			arrayAccreditation.add(new Accreditation(EOIndividu.individuWithPersId(edc(), persId), tndDrh, etablissement, true, false, true));
		}
		for (Integer persId : persIdsAdmin) {
			arrayAccreditation.add(new Accreditation(EOIndividu.individuWithPersId(edc(), persId), tndAdmin, etablissement, true, false, true));
		}
		for (Integer persId : persIdsAdminFonc) {
			arrayAccreditation.add(new Accreditation(EOIndividu.individuWithPersId(edc(), persId), tndAdminFonc, etablissement, true, false, true));
		}
		
		return arrayAccreditation;
	}
	

	/**
	 * 
	 * @return {@link NSArray<I_Accreditation>}
	 */
	private NSArray<I_Accreditation> getDeductedDroitsResponsable(EOQualifier qual) {
		
		NSArray<I_Accreditation> arrayAccreditation = new NSMutableArray<I_Accreditation>();
		
		List<EOStructure> listeStructureTmp = EOStructure.rechercherServices(edc(), qual);
		
		List<EOStructure> listeStructure = filtrerListeServiceParResponsable(listeStructureTmp);
		
		EOFeveTypeNiveauDroit tndRespServ = EOFeveTypeNiveauDroit.getEOFevTypeNiveauDroitRespServ(edc());
		
		for (int i = 0; i < listeStructure.size(); i++) {
			EOStructure serviceAnnuaire = listeStructure.get(i);
			
			if (serviceAnnuaire.toResponsable() != null) {
				
				NSArray<EOStructure> sousServices = serviceAnnuaire.tosSousServiceDeep(false);
				if (!NSArrayCtrl.isEmpty(sousServices)) {
						
					for (EOStructure eoStructure : sousServices) {
							
						EOStructure structureEnCours = eoStructure;
							
						int nbNiveau = 1;
						while (!eoStructure.equals(serviceAnnuaire)) {
							eoStructure = eoStructure.toStructurePere();
							nbNiveau++;
						}
							
						I_Accreditation accreditationAnnuaire = new Accreditation(serviceAnnuaire.toResponsable(), tndRespServ, tndRespServ.tndLibelle() + " N + " + nbNiveau,
								structureEnCours, false, false, true);
						arrayAccreditation.add(accreditationAnnuaire);
							
					}
					
				}
				
			}
			
		}
			
		if (!StringCtrl.isEmpty(criteresRechercheAccreditation.getStrPersonneCible())) {
			arrayAccreditation = EOQualifier.filteredArrayWithQualifier(arrayAccreditation, 
					ERXQ.likeInsensitive(Accreditation.PERSONNE_CIBLE_KEY + "." + EOStructure.LL_STRUCTURE_KEY, "*" + criteresRechercheAccreditation.getStrPersonneCible() + "*"));
		}
		
		return arrayAccreditation;
	}
	
	private List<EOStructure> filtrerListeServiceParResponsable(List<EOStructure> serviceRespList) {
		
		Map<EOIndividu, List<EOStructure>> mapRespStructures = new HashMap<EOIndividu, List<EOStructure>>();
		StructureGrhService structureGrhService = new StructureGrhService();
		
		buildMapRespStructures(serviceRespList, mapRespStructures);
		conserverStructurePerePourResponsable(mapRespStructures, structureGrhService);
		
		return construireListeStructureDepuisStructurePereResp(mapRespStructures);
	}


	private List<EOStructure> construireListeStructureDepuisStructurePereResp(Map<EOIndividu, List<EOStructure>> mapRespStructures) {
		List<EOStructure> listeStructure = new ArrayList<EOStructure>();
		Set<EOIndividu> listeResponsable = mapRespStructures.keySet();
		for (EOIndividu eoIndividu : listeResponsable) {
			listeStructure.addAll(mapRespStructures.get(eoIndividu));
		}
		return listeStructure;
	}

	private void conserverStructurePerePourResponsable(Map<EOIndividu, List<EOStructure>> mapRespStructures, StructureGrhService structureGrhService) {
		Set<EOIndividu> listeResponsable = mapRespStructures.keySet();
		for (EOIndividu individu : listeResponsable) {
			List<EOStructure> listeStructures = mapRespStructures.get(individu);
			if (listeStructures.size() > 1) {
				List<EOStructure> listeStructuresAConserver = new ArrayList<EOStructure>();
				listeStructuresAConserver.addAll(structureGrhService.recupererStructuresPere(listeStructures));
				mapRespStructures.put(individu, listeStructuresAConserver);
			}
		}
	}

	private void buildMapRespStructures(List<EOStructure> serviceRespList, Map<EOIndividu, List<EOStructure>> mapRespStructures) {
		for (EOStructure structure : serviceRespList) {
			if (mapRespStructures.containsKey(structure.toResponsable())) {
				List<EOStructure> listeStructures = mapRespStructures.get(structure.toResponsable());
				listeStructures.add(structure);
				mapRespStructures.put(structure.toResponsable(), listeStructures);
			} else {
				List<EOStructure> listeStructures = new ArrayList<EOStructure>();
				listeStructures.add(structure);
				if (structure.toResponsable() != null) {
					mapRespStructures.put(structure.toResponsable(), listeStructures);
				}
			}			
		}
	}

	public boolean isAccreditationModifiable() {
		return !accreditationItem.isAnnuaire();
	}

	/**
	 * @return
	 */
	public WOActionResults doRechercher() {
		
		criteresRechercheAccreditation.initRechercheAEffectuer();
		
		NSArray<I_Accreditation> listeAccreditation = new NSMutableArray<I_Accreditation>();
		
		if (criteresRechercheAccreditation.isEffectuerRechercheAdmin()) {
			EOQualifier qual = buildQualifierPostRechercheAdmin();
			listeAccreditation.addAll(EOQualifier.filteredArrayWithQualifier(getDeductedDroitsAdmins(), qual));
		}
		
		if (criteresRechercheAccreditation.isEffectuerRechercheRespServ()) {
			EOQualifier qual = buildQualifierRechercheRespServ();
			listeAccreditation.addAll(getDeductedDroitsResponsable(qual));
		}
		
		if (criteresRechercheAccreditation.isEffectuerRechercheFeve()) {
			EOQualifier qual = buildQualifierRechercheFeve();
			listeAccreditation.addAll(EOQualifier.filteredArrayWithQualifier(EOFeveDroit.fetchAll(edc()), qual));
		}
		
		refreshDg(listeAccreditation);
		
		return null;
	}


	


	private EOQualifier buildQualifierRechercheRespServ() {
		
		NSArray<EOQualifier> arrayQual = new NSMutableArray<EOQualifier>();
		
		if (!StringCtrl.isEmpty(criteresRechercheAccreditation.getStrPersonneTitulaire())) {
			arrayQual.add(ERXQ.or(
							ERXQ.likeInsensitive(EOStructure.TO_RESPONSABLE_KEY + "." + EOIndividu.NOM_AFFICHAGE_KEY, "*" + criteresRechercheAccreditation.getStrPersonneTitulaire() + "*"),
							ERXQ.likeInsensitive(EOStructure.TO_RESPONSABLE_KEY + "." + EOIndividu.PRENOM_AFFICHAGE_KEY, "*" + criteresRechercheAccreditation.getStrPersonneTitulaire() + "*"))
						);
		}
		
		return ERXQ.and(arrayQual);

	}

	private EOQualifier buildQualifierPostRechercheAdmin() {
		
		NSArray<EOQualifier> arrayQual = new NSMutableArray<EOQualifier>();
		
		if (!StringCtrl.isEmpty(criteresRechercheAccreditation.getStrPersonneCible())) {
			arrayQual.add(ERXQ.likeInsensitive(Accreditation.PERSONNE_CIBLE_KEY + "." + EOStructure.LL_STRUCTURE_KEY, "*" + getCriteresRechercheAccreditation().getStrPersonneCible() + "*"));
		}
		if (!StringCtrl.isEmpty(criteresRechercheAccreditation.getStrPersonneTitulaire())) {
			arrayQual.add(ERXQ.or(
							ERXQ.likeInsensitive(Accreditation.PERSONNE_TITULAIRE_KEY + "." + EOIndividu.NOM_AFFICHAGE_KEY, "*" + getCriteresRechercheAccreditation().getStrPersonneTitulaire() + "*"),
							ERXQ.likeInsensitive(Accreditation.PERSONNE_TITULAIRE_KEY + "." + EOIndividu.PRENOM_AFFICHAGE_KEY, "*" + getCriteresRechercheAccreditation().getStrPersonneTitulaire() + "*"))
						);
		}		
		if (criteresRechercheAccreditation.getEoTypeNiveauDroitSelected() != null) {
			arrayQual.add(ERXQ.equals(Accreditation.EO_TYPE_NIVEAU_DROIT_KEY + "." + EOFeveTypeNiveauDroit.TND_CODE_KEY, criteresRechercheAccreditation.getEoTypeNiveauDroitSelected().tndCode()));
		}
		
		
		
		
		return ERXQ.and(arrayQual);
	}


	private EOQualifier buildQualifierRechercheFeve() {
		
		NSArray<EOQualifier> arrayQual = new NSMutableArray<EOQualifier>();
		
		if (!StringCtrl.isEmpty(criteresRechercheAccreditation.getStrPersonneCible())) {
			arrayQual.add(ERXQ.likeInsensitive(EOFeveDroit.TO_PERSONNE_CIBLE_KEY + "." + IPersonne.PERS_LIBELLE_KEY, "*" + criteresRechercheAccreditation.getStrPersonneCible() + "*"));
		}
		if (!StringCtrl.isEmpty(criteresRechercheAccreditation.getStrPersonneTitulaire())) {
			arrayQual.add(ERXQ.likeInsensitive(EOFeveDroit.TO_PERSONNE_TITULAIRE_KEY + "." + IPersonne.PERS_LIBELLE_KEY, "*" + criteresRechercheAccreditation.getStrPersonneTitulaire() + "*"));
		}
		if (criteresRechercheAccreditation.isFiltrerCible()) {
			if (criteresRechercheAccreditation.isCibleCodeIndividuSelected()) {
				arrayQual.add(ERXQ.isTrue(EOFeveDroit.TO_PERSONNE_CIBLE_KEY + "." + IPersonne.IS_INDIVIDU_KEY));
			} else if (criteresRechercheAccreditation.isCibleCodeGroupeSelected()) {
				arrayQual.add(ERXQ.isTrue(EOFeveDroit.TO_PERSONNE_CIBLE_KEY + "." + IPersonne.IS_STRUCTURE_KEY));
			} else {
				arrayQual.add(ERXQ.equals(EOFeveDroit.TEM_CIBLE_CDC_KEY, EOFeveDroit.OUI));
			}
		}		
		if (criteresRechercheAccreditation.isFiltrerTitulaire()) {
			if (criteresRechercheAccreditation.isTitulaireCodeIndividuSelected()) {
				arrayQual.add(ERXQ.isTrue(EOFeveDroit.TO_PERSONNE_TITULAIRE_KEY + "." + IPersonne.IS_INDIVIDU_KEY));
			} else if (criteresRechercheAccreditation.isTitulaireCodeGroupeSelected()) {
				arrayQual.add(ERXQ.isTrue(EOFeveDroit.TO_PERSONNE_TITULAIRE_KEY + "." + IPersonne.IS_STRUCTURE_KEY));
			} 
		}
		
		if (criteresRechercheAccreditation.getEoTypeNiveauDroitSelected() != null) {
			arrayQual.add(ERXQ.equals(EOFeveDroit.TO_TYPE_NIVEAU_DROIT.dot(EOFeveTypeNiveauDroit.TND_CODE_KEY).key(), criteresRechercheAccreditation.getEoTypeNiveauDroitSelected().tndCode()));
		}
		
		
		return ERXQ.and(arrayQual);
	}


	public CriteresRechercheAccreditation getCriteresRechercheAccreditation() {
		return criteresRechercheAccreditation;
	}


	public WOActionResults doReinitialiserCriteresRecherche() {
		initCriteresRecherche();
		return null;
	}

	public boolean annulerDroitAnnuaire() {
		return isAnnulerAccreditation(accreditationItem);
	}


	private boolean isAnnulerAccreditation(final I_Accreditation accreditation) {
		return EOFeveAnnulationDroit.existsAnnulationDroitDansListe(accreditation, listeAnnulationDroit);
	}
	
	public boolean isAnnulable() {
		return accreditationItem.isAnnuaire() && accreditationItem.toTypeNiveauDroit().isTypeNiveauDroitResponsable();
	}

	public boolean isDroitUtilisationPage() {
		return feveUserInfo().getAutorisation().hasDroitUtilisationGererDroits();
	}
	
	public boolean isNonDroitUtilisationPage() {
		return !isDroitUtilisationPage() || !accreditationItem.equals(getDgDroit().selectedObject());
	}
	
    
}