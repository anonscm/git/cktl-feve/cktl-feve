package org.cocktail.feve.components.administration.droits;

import java.util.List;

import org.cocktail.fwkcktlgrh.common.metier.EOFeveDroit;
import org.cocktail.fwkcktlgrh.common.metier.services.StructureGrhService;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Page de saisie des droits
 * @author juliencallewaert
 *
 */
public class SaisieDroit extends A_Droit {
    
	
	private String updateContainerID;
	
	public WODisplayGroup dgDroit;
	
	public SaisieDroit(WOContext context) {
        super(context);
    }
	
	public String getUpdateContainerID() {
		return updateContainerID;
	}
	
	
	private IPersonne personneTitulaireSelected;
	private IPersonne personneCibleSelected;
	
	private boolean isAjout;
	
	private boolean isCibleCdc;
	private boolean isCibleHeritage;

	private EOQualifier qualifierForIndividu;

	private EOQualifier qualifierForStructures;
	
	private final static String CONTAINER_ID = "SaisieDroitContainer";
	
	public final String getContainerID() {
		return CONTAINER_ID;
	}

		
	public boolean isTitulaireSelected() {
		return personneTitulaireSelected != null;
	}

	public boolean isCibleSelected() {
		return personneCibleSelected != null;
	}	
	
	/**
	 * Effectuer l'enregistrement du droit dans la base de données
	 * @return {@link WOActionResults}
	 */
	public WOActionResults doCreateDroit() {
		
		
		if (getEoTypeNiveauDroitSelected().isTypeNiveauDroitAvecCibleEtablissement()) {
			EOStructure structureEtab = EOStructure.findRacineInContext(edc());
			
			setPersonneCibleSelected(structureEtab);
			setIsCibleHeritage(true);
		}
		
		EOFeveDroit eoDroit = EOFeveDroit.create(
				edc(), getPersonneTitulaireSelected(), getPersonneCibleSelected(),
				isCibleCdc(), isCibleHeritage(), getEoTypeNiveauDroitSelected());
		
		getDgDroit().insertObjectAtIndex(eoDroit, 0);

		try {
			edc().lock();
			edc().saveChanges();

			// forcer le rechargement de l'objet créé pour que les tomany se refasse
			// correctement
			edc().invalidateObjectsWithGlobalIDs(new NSArray<EOGlobalID>(eoDroit.globalID()));
			
		} catch (Exception e) {
			e.printStackTrace();
			edc().revert();
		} finally {
			edc().unlock();
			// repasser en mode lecture
			isAjout = false;
		}
		return null;
	}
	
	
	/**
	 * Effectuer l'enregistrement du droit dans la base de données
	 * @return {@link WOActionResults}
	 */
	public WOActionResults toAnnuler() {
		isAjout = false;
		return null;
	}
	
	
	/**
	 * Passer le composant en mode ajout
	 * 
	 * @return {@link WOActionResults}
	 */
	public WOActionResults toAjout() {
		isAjout = true;
		return null;
	}
	
	public boolean isCibleSelectionnable() {
		return !getEoTypeNiveauDroitSelected().isTypeNiveauDroitAvecCibleEtablissement();
	}
	
	public boolean isCreateDroitVisible() {
		return isCibleSelected() || (!isCibleSelected() && !isCibleSelectionnable());
	}

	public IPersonne getPersonneTitulaireSelected() {
		return personneTitulaireSelected;
	}

	public void setPersonneTitulaireSelected(IPersonne personneTitulaireSelected) {
		this.personneTitulaireSelected = personneTitulaireSelected;
	}

	public IPersonne getPersonneCibleSelected() {
		return personneCibleSelected;
	}

	public void setPersonneCibleSelected(IPersonne personneCibleSelected) {
		this.personneCibleSelected = personneCibleSelected;
	}

	public boolean isAjout() {
		return isAjout;
	}

	public boolean isCibleCdc() {
		return isCibleCdc;
	}

	public void setIsCibleCdc(boolean isCibleCdc) {
		this.isCibleCdc = isCibleCdc;
	}

	public boolean isCibleHeritage() {
		return isCibleHeritage;
	}

	public void setIsCibleHeritage(boolean isCibleHeritage) {
		this.isCibleHeritage = isCibleHeritage;
	}
	
	public final WODisplayGroup getDgDroit() {
		return dgDroit;
	}

	public void setUpdateContainerID(String updateContainerID) {
		this.updateContainerID = updateContainerID;
	}
	
	
	public EOQualifier qualifierForStructures() {
		
		if (qualifierForStructures == null) {
		
			EOQualifier qualifier = null;
			
			if (!feveUserInfo().getAutorisation().isProfilAdmin()) {
				List<IStructure> listeStructure = feveUserInfo().getAutorisation().getAllUtilisationStructuresGererDroits();
				
				NSArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				
				for (IStructure structure : listeStructure) {
					qualifiers.add(ERXQ.equals(EOStructure.C_STRUCTURE_KEY, structure.cStructure()));
				}
				qualifier = ERXQ.or(qualifiers);
			}
		
			qualifierForStructures = qualifier;
			
		}
		
		return qualifierForStructures;
	}

	
	public EOQualifier qualifierForIndividus() {
		
		if (qualifierForIndividu == null) {
		
			EOQualifier qualifier = null;
			
			if (!feveUserInfo().getAutorisation().isProfilAdmin()) {
				List<IStructure> listeStructure = feveUserInfo().getAutorisation().getAllUtilisationStructuresGererDroits();
				NSArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
				StructureGrhService structureGrhService = new StructureGrhService();
				for (IStructure eoStructure : listeStructure) {
					NSArray<EOIndividu> listeIndividu = structureGrhService.getIndividuAffecteVPersonnelNonEns(edc(), (EOStructure) eoStructure);
					for (EOIndividu individu : listeIndividu) {
						qualifiers.add(ERXQ.equals(EOIndividu.PERS_ID_KEY, individu.persId()));
					}
				}
				qualifier = ERXQ.or(qualifiers);			
			}
			qualifierForIndividu = qualifier;
		}
		
		return qualifierForIndividu;
	}
	
}