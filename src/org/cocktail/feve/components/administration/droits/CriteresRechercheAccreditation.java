package org.cocktail.feve.components.administration.droits;

import org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit;

public class CriteresRechercheAccreditation {

	private String strPersonneTitulaire;
	private String strPersonneCible;
	
	private boolean isFiltrerTitulaire = false;
	private boolean isFiltrerCible = false;
	private boolean isInclureDroitsHerites = false;
	
	private EOFeveTypeNiveauDroit eoTypeNiveauDroitSelected;
	
	private boolean effectuerRechercheAdmin;
	private boolean effectuerRechercheRespServ;
	private boolean effectuerRechercheFeve;
	
	
	protected final static String CODE_INDIVIDU = "I";
	protected final static String CODE_GROUPE = "G";
	protected final static String CODE_CHEF_DE_SERVICE = "C";

	public final String codeIndividu = CODE_INDIVIDU;
	public final String codeGroupe = CODE_GROUPE;
	public final String codeChefDeService = CODE_CHEF_DE_SERVICE;

	private String titulaireSelected, cibleSelected;
	

	public String getStrPersonneTitulaire() {
		return strPersonneTitulaire;
	}


	public void setStrPersonneTitulaire(String strPersonneTitulaire) {
		this.strPersonneTitulaire = strPersonneTitulaire;
	}


	public String getStrPersonneCible() {
		return strPersonneCible;
	}


	public void setStrPersonneCible(String strPersonneCible) {
		this.strPersonneCible = strPersonneCible;
	}


	public boolean isFiltrerTitulaire() {
		return isFiltrerTitulaire;
	}


	public void setIsFiltrerTitulaire(boolean isFiltrerTitulaire) {
		this.isFiltrerTitulaire = isFiltrerTitulaire;
	}


	public boolean isFiltrerCible() {
		return isFiltrerCible;
	}


	public void setIsFiltrerCible(boolean isFiltrerCible) {
		this.isFiltrerCible = isFiltrerCible;
	}
	
	
	/**
	 * @return true/false
	 */
	public boolean isNotFiltrerCible() {
		return !isFiltrerCible;
	}

	/**
	 * @return true/false
	 */
	public boolean isNotFiltrerTitulaire() {
		return !isFiltrerTitulaire;
	}
	
	public boolean isInclureDroitsHerites() {
		return isInclureDroitsHerites;
	}


	public void setIsInclureDroitsHerites(boolean isInclureDroitsHerites) {
		this.isInclureDroitsHerites = isInclureDroitsHerites;
	}


	public EOFeveTypeNiveauDroit getEoTypeNiveauDroitSelected() {
		return eoTypeNiveauDroitSelected;
	}


	public void setEoTypeNiveauDroitSelected(
			EOFeveTypeNiveauDroit eoTypeNiveauDroitSelected) {
		this.eoTypeNiveauDroitSelected = eoTypeNiveauDroitSelected;
	}
	
	public final String getTitulaireSelected() {
		if (titulaireSelected == null) {
			titulaireSelected = CODE_INDIVIDU;
		}
		return titulaireSelected;
	}

	public final void setTitulaireSelected(String titulaireSelected) {
		this.titulaireSelected = titulaireSelected;
	}

	public final String getCibleSelected() {
		if (cibleSelected == null) {
			cibleSelected = CODE_INDIVIDU;
		}
		return cibleSelected;
	}

	public final void setCibleSelected(String cibleSelected) {
		this.cibleSelected = cibleSelected;
	}

	
	public void initRechercheAEffectuer() {
		effectuerRechercheRespServ = true;
		effectuerRechercheAdmin = true;
		effectuerRechercheFeve = true;
		determinerSiRechercheAdmin();
		determinerSiRechercheRespServ();
		determinerSiRechercheFeve();
	}
	
	private void determinerSiRechercheAdmin() {
		if (!isInclureDroitsHerites()) {
			effectuerRechercheAdmin = false;
		}
		if (isFiltrerCible() && (getCibleSelected().equals(CODE_INDIVIDU) || getCibleSelected().equals(CODE_CHEF_DE_SERVICE))) {
			effectuerRechercheAdmin = false;
		}
		if (isFiltrerTitulaire() && getTitulaireSelected().equals(CODE_GROUPE)) {
			effectuerRechercheAdmin = false;
		}
		if (getEoTypeNiveauDroitSelected() != null && !getEoTypeNiveauDroitSelected().isTypeNiveauDroitAvecCibleEtablissement()) {
			effectuerRechercheAdmin = false;
		}
		
	}
	
	private void determinerSiRechercheRespServ() {
		if (!isInclureDroitsHerites()) {
			effectuerRechercheRespServ = false;
		}
		if (isFiltrerCible() && (getCibleSelected().equals(CODE_INDIVIDU) || getCibleSelected().equals(CODE_CHEF_DE_SERVICE))) {
			effectuerRechercheRespServ = false;
		}
		if (isFiltrerTitulaire() && getTitulaireSelected().equals(CODE_GROUPE)) {
			effectuerRechercheRespServ = false;
		}
		if (getEoTypeNiveauDroitSelected() != null && !getEoTypeNiveauDroitSelected().isTypeNiveauDroitResponsable()) {
			effectuerRechercheRespServ = false;
		}
	}
	
	private void determinerSiRechercheFeve() {
		effectuerRechercheFeve = true;
	}


	public boolean isEffectuerRechercheAdmin() {
		return effectuerRechercheAdmin;
	}


	public boolean isEffectuerRechercheRespServ() {
		return effectuerRechercheRespServ;
	}


	public boolean isEffectuerRechercheFeve() {
		return effectuerRechercheFeve;
	}
	
	
	public boolean isTitulaireCodeIndividuSelected() {
		return getTitulaireSelected().equals(CODE_INDIVIDU);
	}
	
	public boolean isCibleCodeIndividuSelected() {
		return getCibleSelected().equals(CODE_INDIVIDU);
	}
	
	public boolean isTitulaireCodeGroupeSelected() {
		return getTitulaireSelected().equals(CODE_GROUPE);
	}
	
	public boolean isCibleCodeGroupeSelected() {
		return getCibleSelected().equals(CODE_GROUPE);
	}
	
	public boolean isCibleCodeChefDeServiceSelected() {
		return getCibleSelected().equals(CODE_CHEF_DE_SERVICE);
	}
	
	
}
