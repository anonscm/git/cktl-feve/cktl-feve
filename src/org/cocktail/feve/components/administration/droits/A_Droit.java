package org.cocktail.feve.components.administration.droits;

import org.cocktail.feve.components.common.FeveWebComponent;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public abstract class A_Droit extends FeveWebComponent {

	
	
	
	public A_Droit(WOContext context) {
		super(context);
	}

	private NSArray<EOFeveTypeNiveauDroit> eoTypeNiveauDroitArray;
	public EOFeveTypeNiveauDroit eoTypeNiveauDroitItem;
	private EOFeveTypeNiveauDroit eoTypeNiveauDroitSelected;	
	
	/**
	 * @return
	 */
	public NSArray<EOFeveTypeNiveauDroit> getEoTypeNiveauDroitArray() {
		if (eoTypeNiveauDroitArray == null) {
			if (feveUserInfo().getAutorisation().isProfilAdmin()) {
				eoTypeNiveauDroitArray = EOFeveTypeNiveauDroit.fetchAll(edc(), EOFeveTypeNiveauDroit.getQualifierPourTypeNiveauDroitVisible(), null);
			} else {
				eoTypeNiveauDroitArray = EOFeveTypeNiveauDroit.fetchAll(edc(), EOFeveTypeNiveauDroit.getQualifierPourTypeNiveauDroitVisibleNonAdmin(), null);
			}
		}
		return eoTypeNiveauDroitArray;
	}
	
	public final EOFeveTypeNiveauDroit getEoTypeNiveauDroitSelected() {
		return eoTypeNiveauDroitSelected;
	}

	public final void setEoTypeNiveauDroitSelected(EOFeveTypeNiveauDroit eoTypeNiveauDroitSelected) {
		this.eoTypeNiveauDroitSelected = eoTypeNiveauDroitSelected;
	}

	public boolean isTypeNiveauDroitSelected() {
		return getEoTypeNiveauDroitSelected() != null;
	}

	

}
