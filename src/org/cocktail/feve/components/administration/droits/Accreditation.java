package org.cocktail.feve.components.administration.droits;

import org.cocktail.fwkcktlgrh.common.metier.EOFeveDroit;
import org.cocktail.fwkcktlgrh.common.metier.EOFeveTypeNiveauDroit;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.I_Accreditation;
import org.cocktail.fwkcktlgrh.common.utilities.StringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

public class Accreditation implements I_Accreditation {

	private IPersonne personneTitulaire;
	private EOFeveTypeNiveauDroit eoTypeNiveauDroit;
	private IPersonne personneCible;
	
	private boolean isCibleHeritage;
	private boolean isCibleChefDeService;
	
	private boolean isAnnuaire;
	
	private String libelleDroitSurcharge;
	
	public final static String EO_TYPE_NIVEAU_DROIT_KEY = "eoTypeNiveauDroit";
	public final static String PERSONNE_CIBLE_KEY = "toPersonneCible";
	public final static String PERSONNE_TITULAIRE_KEY = "toPersonneTitulaire";
	
	
	public Accreditation(IPersonne personneTitulaire,
			EOFeveTypeNiveauDroit eoTypeNiveauDroit,  IPersonne personneCible,
			boolean isHeritage, boolean isCibleChefDeService, boolean isAnnuaire) {
		super();
		this.personneTitulaire = personneTitulaire;
		this.eoTypeNiveauDroit = eoTypeNiveauDroit;
		this.personneCible = personneCible;
		this.isCibleHeritage = isHeritage;
		this.isCibleChefDeService = isCibleChefDeService;
		this.isAnnuaire = isAnnuaire;
	}
	
	public Accreditation(IPersonne personneTitulaire,
			EOFeveTypeNiveauDroit eoTypeNiveauDroit, String libelleDroitSurcharge, IPersonne personneCible,
			boolean isHeritage, boolean isCibleChefDeService, boolean isAnnuaire) {
		super();
		this.personneTitulaire = personneTitulaire;
		this.eoTypeNiveauDroit = eoTypeNiveauDroit;
		this.libelleDroitSurcharge = libelleDroitSurcharge;
		this.personneCible = personneCible;
		this.isCibleHeritage = isHeritage;
		this.isCibleChefDeService = isCibleChefDeService;
		this.isAnnuaire = isAnnuaire;
	}
	
	
	
	public Accreditation(EOFeveDroit eoFeveDroit) {
		super();
		this.personneTitulaire = eoFeveDroit.toPersonneTitulaire();
		this.eoTypeNiveauDroit = eoFeveDroit.toTypeNiveauDroit();
		this.personneCible = eoFeveDroit.toPersonneCible();
		this.isCibleHeritage = eoFeveDroit.isCibleHeritage();
		this.isCibleChefDeService = eoFeveDroit.isCibleChefDeService();
		this.isAnnuaire = false;
	}
	
	
	public IPersonne toPersonneTitulaire() {
		return personneTitulaire;
	}
	public void setPersonneTitulaire(IPersonne personneTitulaire) {
		this.personneTitulaire = personneTitulaire;
	}
	public EOFeveTypeNiveauDroit toTypeNiveauDroit() {
		return eoTypeNiveauDroit;
	}
	public EOFeveTypeNiveauDroit getEoTypeNiveauDroit() {
		return eoTypeNiveauDroit;
	}
	public void setEoTypeNiveauDroit(EOFeveTypeNiveauDroit eoTypeNiveauDroit) {
		this.eoTypeNiveauDroit = eoTypeNiveauDroit;
	}
	public IPersonne toPersonneCible() {
		return personneCible;
	}
	public void setPersonneCible(IPersonne personneCible) {
		this.personneCible = personneCible;
	}
	public boolean isCibleHeritage() {
		return isCibleHeritage;
	}
	
	public void setCibleHeritage(boolean isCibleHeritage) {
		this.isCibleHeritage = isCibleHeritage;
	}
	
	public boolean isCibleChefDeService() {
		return isCibleChefDeService;
	}
	
	public void setCibleChefDeService(boolean isCibleChefDeService) {
		this.isCibleChefDeService = isCibleChefDeService;
	}

	public boolean isAnnuaire() {
		return isAnnuaire;
	}

	public boolean isCibleServiceSimple() {
		boolean isCibleServiceSimple = false;

		if (!isCibleChefDeService() && toPersonneCible().isStructure()) {
			isCibleServiceSimple = true;
		}

		return isCibleServiceSimple;
	}

	public String getLibelleDroitSurcharge() {
		return libelleDroitSurcharge;
	}

	public void setLibelleDroitSurcharge(String libelleDroitSurcharge) {
		this.libelleDroitSurcharge = libelleDroitSurcharge;
	}

	public String libelleTypeNiveauDroit() {
		if (StringCtrl.isEmpty(this.libelleDroitSurcharge)) {
			return this.eoTypeNiveauDroit.tndLibelle();
		} else {
			return this.libelleDroitSurcharge;
		}
	}

	@Override
	public String toString() {
		String toString = "";

		toString += toPersonneTitulaire().getNomPrenomAffichage();
		toString += " ";
		toString += libelleTypeNiveauDroit();
		

		if (toPersonneCible() != null) {
			toString += " sur ";
			toString += toPersonneCible().getNomPrenomAffichage();
		}

		return toString;
	}
	
	
	
	
}
