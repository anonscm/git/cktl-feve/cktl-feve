package org.cocktail.feve.components.common;

import org.cocktail.fwkcktlgrh.common.metier.EOFeveParametres;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Ecran de gestion de la documentation locale : guides d'entretien et d'utilisation
 * 
 * @author ctarade
 */
public class DocumentationLocale 
	extends FeveWebComponent {

	public boolean isDisabled = true;
	
	public NSMutableArray<EOFeveParametres> guideEntretienArray;
	public NSMutableArray<EOFeveParametres> guideUtilisationArray;
	
	public EOFeveParametres guideItem;
	
	public DocumentationLocale(WOContext context) {
		super(context);
		relireBase();
	}
	
	/**
	 * Relire les informations de la table des paramètres
	 */
	private void relireBase() {
		
		guideEntretienArray = EOFeveParametres.getParametres(
				ec, 
				EOFeveParametres.PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_ENTRETIEN,
				EOFeveParametres.PARAM_KEY_KEY);
	
		guideUtilisationArray = EOFeveParametres.getParametres(
				ec, 
				EOFeveParametres.PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_UTILISATION,
				EOFeveParametres.PARAM_KEY_KEY);
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isAfficherAjouterGuideEntretien() {
		boolean isAfficherAjouterGuideEntretien = true;

		isAfficherAjouterGuideEntretien = isAfficherAjouterGuide(guideEntretienArray);
		
		return isAfficherAjouterGuideEntretien;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isAfficherAjouterGuideUtilisation() {
		boolean isAfficherAjouterGuideUtilisation = true;

		isAfficherAjouterGuideUtilisation = isAfficherAjouterGuide(guideUtilisationArray);
		
		return isAfficherAjouterGuideUtilisation;
	}
	
	/**
	 * On autorise l'ajout d'un guide uniquement s'il n'y a aucun
	 * d'entre eux en cours de modification
	 * @return
	 */
	private boolean isAfficherAjouterGuide(NSArray<EOFeveParametres> array) {
		
		boolean isAfficherAjouterGuide = true;

		EOFeveParametres item = null;
		int i = 0;

		while (isAfficherAjouterGuide && i < array.count()) {
			item = array.objectAtIndex(i);
			if (item.getObjetInterfaceDocumentation().isEnModification()) {
				isAfficherAjouterGuide = false;
			}
			i++;
		}
		
		return isAfficherAjouterGuide;
	}
		
	/**
	 * 
	 * @return
	 * @throws Throwable 
	 */
	public WOComponent ajouterGuideEntretien() throws Throwable {
		ajouterGuide(EOFeveParametres.PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_ENTRETIEN);
		return null;
	}
	
	/**
	 * 
	 * @return
	 * @throws Throwable 
	 */
	public WOComponent ajouterGuideUtilisation() throws Throwable {
		ajouterGuide(EOFeveParametres.PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_UTILISATION);
		return null;
	}
	
	/**
	 * 
	 * @param paramKeyPrefix
	 * @throws Throwable 
	 */
	private void ajouterGuide(String paramKeyPrefix) throws Throwable {
		EOFeveParametres nouveauParametre = EOFeveParametres.nouveauParametre(ec, paramKeyPrefix);
		if (paramKeyPrefix.equals(EOFeveParametres.PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_ENTRETIEN)) {
			guideEntretienArray.addObject(nouveauParametre);
		} else if (paramKeyPrefix.equals(EOFeveParametres.PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_UTILISATION)) {
			guideUtilisationArray.addObject(nouveauParametre);
		}
	}

	/**
	 * 
	 * @return
	 * @throws Throwable 
	 */	
	public WOComponent supprimerGuide() throws Throwable {
		EOFeveParametres.supprimerParametre(guideItem);
		sauvegarde();
		relireBase();
		return null;
	}
	
	/**
	 * 
	 * @return
	 * @throws Throwable
	 */
  public WOActionResults changementOrdreListeGuideEntretien() throws Throwable {
  	EOFeveParametres.majPosition(ec, guideEntretienArray);
  	return null;
  } 
	
	/**
	 * 
	 * @return
	 * @throws Throwable
	 */
  public WOActionResults changementOrdreListeGuideUtilisation() throws Throwable {
  	EOFeveParametres.majPosition(ec, guideUtilisationArray);
  	return null;
  } 

  /**
   * Annuler l'opération en cours ou l'ajout d'une nouvelle doc
   * @return
   */
  public WOActionResults annuler() {
  	// objet déjà en base ? si non, suppression du tableau associé
  	if (ec.insertedObjects().containsObject(guideItem)) {
  		if (guideItem.getObjetInterfaceDocumentation().getBaseParamKey().equals(
  				EOFeveParametres.PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_ENTRETIEN)) {
  			guideEntretienArray.removeIdenticalObject(guideItem);
  		} else if (guideItem.getObjetInterfaceDocumentation().getBaseParamKey().equals(
  				EOFeveParametres.PREFIX_KEY_FEV_DOCUMENTATION_GUIDE_UTILISATION)) {
  			guideUtilisationArray.removeIdenticalObject(guideItem);
  		}
  	}
  	//
  	guideItem.getObjetInterfaceDocumentation().annuler();
  	return null;
  }
  
}