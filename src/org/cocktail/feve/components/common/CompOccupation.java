package org.cocktail.feve.components.common;

import org.cocktail.fwkcktlgrh.common.metier.EOChangementPosition;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOModalitesService;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Composant de gestion des occupations d'un poste
 * @author ctarade
 *
 */
public class CompOccupation
	extends A_ComponentControled {
	
	public CompOccupation(WOContext context) {
		super(context);
	}
	
	/**
	 * cast du {@link A_ComponentControler} en {@link CompOccupationCtrl}
	 * @return
	 */
	public CompOccupationCtrl ctrl() {
		return (CompOccupationCtrl) ctrl;
	}

  /**
   * On combine les classes CSS pour connaitre l'etat de l'occupation
   * @return
   */
  public String classTrOccupation() {
  	StringBuffer classList = new StringBuffer();
  	if (ctrl().affectationDetailItem.isActuelle()) {
  		classList.append(FeveWebComponent.CLASS_TR_OCCUPATION_ACTUELLE);
  	} else {
  		classList.append(FeveWebComponent.CLASS_TR_OCCUPATION_NON_ACTUELLE);
  	}
  	return classList.toString();
  }
  
  /**
   * Action de supprimer l'occupation
   * @return
   */
  public WOComponent supprimerOccupation() {
    // page de confirmation
  	CompOccupationCtrl.SupprimerOccupationResponder responder = ctrl().new SupprimerOccupationResponder(getTopParent());
    return CktlAlertPage.newAlertPageWithResponder(this, "Suppression de l'occupation<br>",
        "<center>Confirmation de l'op&eacute;ration:<br><br>"+
        "Etes vous sur de vouloir supprimer l'occupation du poste " + 
        ctrl().affectationDetailItem.fullDisplay(true) + " ?",
        "Confirmer", "Annuler", null, CktlAlertPage.QUESTION, responder);
  

  }
  

	
	public int getQuotiteTravail() {
		int result = 100;
		int resultModalite = 100;
		int resultDetachement = 100;
		
		boolean modaliteTrouve = false;
		boolean detachementTrouve = false;
		
		NSTimestamp dateNow = DateCtrl.now();
		
		int affectatonContrat = 100;
		
		NSArray<EOContrat> contrats = EOContrat.contratsActuelsForIndividu(ec, ctrl().affectationDetailItem.toAffectation().toIndividu());
		if (contrats.size() > 0 ) {
			if (contrats.get(0).toContratAvenants().size() > 0) {
				affectatonContrat = (contrats.get(0).toContratAvenants().get(0)).numQuotRecrutement();
			}
		}
		
		
		//recherche des modalitées  (temps partielle, congées sans traitement, mi-temps therapetique)
		EOQualifier qualModalite = EOModalitesService.INDIVIDU.eq(ctrl().affectationDetailItem.toAffectation().toIndividu());
		NSArray<EOModalitesService> modalites = EOModalitesService.modalitesServiceValidesForIndividu(ec, ctrl().affectationDetailItem.toAffectation().toIndividu());
		for (EOModalitesService modalite : modalites) {
			if ((modalite.dateDebut().before(dateNow)) && (modalite.dateFin().after(dateNow))) {
				modaliteTrouve = true;
				resultModalite = modalite.quotite();
			}
		}
		
		//recherche détachement
		EOQualifier qualDetachement = EOChangementPosition.NO_DOSSIER_PERS.eq(ctrl().affectationDetailItem.toAffectation().toIndividu().noIndividu());
		NSArray<EOChangementPosition> detachements = EOChangementPosition.fetchAll(ec, qualDetachement, null);
		
		EORne  rneEtab = ctrl().affectationDetailItem.toAffectation().toStructure().etablissement().toRne();
		if (rneEtab != null) {
			for (EOChangementPosition detachement : detachements) {
				if (detachement.dDebPosition().before(dateNow) &&  (detachement.dDebPosition().before(dateNow) ||  detachement.dFinPosition().after(dateNow)) ) {
					if (detachement.cPosition().equals("ACTI")) {
					} else {
						if (detachement.toRne() != null) {
							if (detachement.toRne().equals(rneEtab)) {
								//rentrant
								resultDetachement =  detachement.quotitePosition();
								detachementTrouve = true;
							}
						}
						if (detachement.toRneOrigine() != null) {
							if (detachement.toRneOrigine().equals(rneEtab)) {
								//sortant
								resultDetachement = 100 - detachement.quotitePosition();
								detachementTrouve = true;
							}
						}
						if (detachement.lieuPosition() != null) {
							//rentrant
							resultDetachement =  detachement.quotitePosition();
							detachementTrouve = true;
						}
					}
				}
			}
		}
		
		
		//calcul final
		if (!modaliteTrouve && !detachementTrouve) {
			result = affectatonContrat;
		}
		
		if (!modaliteTrouve && detachementTrouve) {
			result = affectatonContrat/100*resultDetachement;
		}
		
		if (modaliteTrouve && !detachementTrouve) {
			result = affectatonContrat/100*resultModalite;
		}
		
		if (modaliteTrouve && detachementTrouve) {
			result = affectatonContrat/100*(resultModalite * resultDetachement)/100;
		}
	
		return result;
	}
	

}