package org.cocktail.feve.components.common;

import org.cocktail.fwkcktlgrh.common.metier.EOEvaluation;
import org.cocktail.fwkcktlgrh.common.metier.EOEvaluationNoticePromotion;
import org.cocktail.fwkcktlgrh.common.metier.EOReliquatsAnciennete;
import org.cocktail.fwkcktlgrh.common.metier.EOTplBloc;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Gestion de la notice de promotion selon un template ( <code>EOTplBloc</code>
 * ).
 * 
 * Le points d'entree est <code>EOEvaluation</code>
 * 
 * @author ctarade
 */
public class CompNoticePromotion
		extends FeveWebComponent {

	// binding de template d'onglet
	public EOTplBloc inTplBloc;
	// binding de l'evaluation ou la fiche
	public EOEvaluation inFiche;
	// binding disabled
	public boolean inDisabled;

	private NSArray<Integer> reductionEchelonArray;

	public NSArray<Integer> promotionGradeArray = EOEvaluationNoticePromotion.getPromotionGradeArray();
	public NSArray<Integer> promotionCorpsArray = EOEvaluationNoticePromotion.getPromotionCorpsArray();

	public Integer item;

	//
	private EOEvaluationNoticePromotion eoEvaluationNoticePromotion;

	//
	public boolean isTextAppreciationGeneraleEnCoursDeModif;
	private String reducAnneeEnCours;
	private String reducAnciennetePrecedente;

	public CompNoticePromotion(WOContext context) {
		super(context);
	}

	/**
	 * La liste des items liés au corps de l'agent
	 * 
	 * @return
	 */
	public NSArray<Integer> getReductionEchelonArray() {
		if (reductionEchelonArray == null) {
			if (inFiche.isAenes()) {
				reductionEchelonArray = EOEvaluationNoticePromotion.getAenesReductionEchelonArray();
			} else if (inFiche.isItrf()) {
				reductionEchelonArray = EOEvaluationNoticePromotion.getItrfReductionEchelonArray();
			} else if (inFiche.isBu()) {
				reductionEchelonArray = EOEvaluationNoticePromotion.getBuReductionEchelonArray();
			}
		}
		return reductionEchelonArray;
	}

	/**
	 * 
	 * @return
	 */
	public final String getLibelleReductionEchelonItem() {
		String libelle = null;

		if (inFiche.isAenes()) {
			libelle = EOEvaluationNoticePromotion.libelleAenesReductionEchelon(item);
		} else if (inFiche.isItrf()) {
			libelle = EOEvaluationNoticePromotion.libelleItrfReductionEchelon(item);
		} else if (inFiche.isBu()) {
			libelle = EOEvaluationNoticePromotion.libelleBuReductionEchelon(item);
		}

		return libelle;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isReductionEchelonAMotiver() {
		boolean isAMotiver = false;

		isAMotiver = EOEvaluationNoticePromotion.isAMotiver(getEoEvaluationNoticePromotion().enpReductionEchelon());

		return isAMotiver;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isPromotionGradeAMotiver() {
		boolean isAMotiver = false;

		isAMotiver = EOEvaluationNoticePromotion.isAMotiver(getEoEvaluationNoticePromotion().enpPromotionGrade());

		return isAMotiver;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isPromotionCorpsAMotiver() {
		boolean isAMotiver = false;

		isAMotiver = EOEvaluationNoticePromotion.isAMotiver(getEoEvaluationNoticePromotion().enpPromotionCorps());

		return isAMotiver;
	}

	/**
	 * 
	 * @return
	 */
	public String libellePromotionGradeItem() {
		String libelle = null;

		libelle = EOEvaluationNoticePromotion.libellePromotionGrade(item);

		return libelle;
	}

	/**
	 * 
	 * @return
	 */
	public String libellePromotionCorpsItem() {
		String libelle = null;

		libelle = EOEvaluationNoticePromotion.libellePromotionCorps(item);

		return libelle;
	}

	/**
	 * Création de l'objet en base de données si nécéssaire
	 * 
	 * @return
	 * @throws Throwable
	 */
	public final EOEvaluationNoticePromotion getEoEvaluationNoticePromotion() {
		if (eoEvaluationNoticePromotion == null) {
			if (inFiche.tosEvaluationNoticePromotion().count() > 0) {
				eoEvaluationNoticePromotion = (EOEvaluationNoticePromotion) inFiche.tosEvaluationNoticePromotion().objectAtIndex(0);
			} else {
				eoEvaluationNoticePromotion = EOEvaluationNoticePromotion.create(edc(), inFiche);
				try {
					sauvegarde();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return eoEvaluationNoticePromotion;
	}

	public final void setEoEvaluationNoticePromotion(EOEvaluationNoticePromotion eoEvaluationNoticePromotion) {
		this.eoEvaluationNoticePromotion = eoEvaluationNoticePromotion;
	}

	/**
	 * Raz des variables locales lorsqu'on change de fiche
	 * 
	 * @param inFiche
	 */
	public final void setInFiche(EOEvaluation inFiche) {
		EOEvaluation prevFiche = this.inFiche;
		this.inFiche = inFiche;
		if (prevFiche != inFiche) {
			setEoEvaluationNoticePromotion(null);
			reductionEchelonArray = null;
		}
	}

	/**
	 * Action de récopier l'appréciation générale de l'entretien dans celle de la
	 * notice
	 * 
	 * @return
	 */
	public WOComponent doRecupererAppreciationGeneraleEntretien() {
		getEoEvaluationNoticePromotion().recupererAppreciationGeneraleEntretien();
		return null;
		// sauvegarde();
	}
	


	public NSArray<EOReliquatsAnciennete> getReliquatsAncienneteEnCours() {
		NSArray<EOReliquatsAnciennete> result = new NSMutableArray<EOReliquatsAnciennete>();
		
		EOQualifier qualAnciennete = EOReliquatsAnciennete.NO_DOSSIER_PERS.eq(inFiche.noIndividuVisible());
		NSArray<EOReliquatsAnciennete> reliquats = EOReliquatsAnciennete.fetchAll(edc(), qualAnciennete, null);
		
		for (EOReliquatsAnciennete r : reliquats) {
			if (r.ancAnnee() >= inFiche.toEvaluationPeriode().epeDDebut().getYear() + 1900) {
				if (r.ancAnnee() <= inFiche.toEvaluationPeriode().epeDFin().getYear() + 1900) {
					result.add(r);
				}
			}
		}
		
		return result;
	}
	
	public NSArray<EOReliquatsAnciennete> getReliquatsAncienneteEvalPrecedente() {
		NSArray<EOReliquatsAnciennete> result = new NSMutableArray<EOReliquatsAnciennete>();
		
		EOEvaluation evaluation = inFiche.toEvaluationPrecedente();
		if (evaluation == null) {
			return new NSMutableArray<EOReliquatsAnciennete>();
		}
		
		EOQualifier qualAnciennete = EOReliquatsAnciennete.NO_DOSSIER_PERS.eq(inFiche.noIndividuVisible());
		NSArray<EOReliquatsAnciennete> reliquats = EOReliquatsAnciennete.fetchAll(edc(), qualAnciennete, null);
		
		
		for (EOReliquatsAnciennete r : reliquats) {
			if (r.ancAnnee() >= evaluation.toEvaluationPeriode().epeDDebut().getYear() + 1900) {
				if (r.ancAnnee() < evaluation.toEvaluationPeriode().epeDFin().getYear() + 1900) {
					result.add(r);
				}
			}
		}
		
		return result;
	}
	

	public boolean isReducAnneeEnCours() {
		if (getReliquatsAncienneteEnCours().size() > 0) {
			return true;
		}
		return false;
	}
	
	public String reducAnneeEnCours() {
		EOReliquatsAnciennete anciennete = getReliquatsAncienneteEnCours().get(0);

		if (anciennete == null) {
			return "";
		}
		
		String r = "";
		
		if (anciennete.ancNbAnnees() != null  && anciennete.ancNbAnnees() != 0) {
			r += anciennete.ancNbAnnees() + " années ";
		}
		if (anciennete.ancNbMois() != null  && anciennete.ancNbMois() != 0) {
			r += anciennete.ancNbMois() + " mois ";
		}
		if (anciennete.ancNbJours() != null  && anciennete.ancNbJours() != 0) {
			r += anciennete.ancNbJours() + " jours";
		}
		
		r += ".";
		
		return r;
	}
	



	public String reducAnciennetePrecedente() {
		EOReliquatsAnciennete anciennete = getReliquatsAncienneteEvalPrecedente().get(0);
		
		if (anciennete == null || getReliquatsAncienneteEvalPrecedente() == null) {
			return "";
		}
		
		String r = "";
		
		if (anciennete.ancNbAnnees() != null  && anciennete.ancNbAnnees() != 0) {
			r += anciennete.ancNbAnnees() + " années ";
		}
		if (anciennete.ancNbMois() != null  && anciennete.ancNbMois() != 0) {
			r += anciennete.ancNbMois() + " mois ";
		}
		if (anciennete.ancNbJours() != null  && anciennete.ancNbJours() != 0) {
			r += anciennete.ancNbJours() + " jours";
		}
		
		r += ".";
		
		return r;
	}

	public boolean isReducAnneePrecedente() {
		if (getReliquatsAncienneteEvalPrecedente() != null && getReliquatsAncienneteEvalPrecedente().size() > 0) {
			return true;
		}
		return false;
	}


}