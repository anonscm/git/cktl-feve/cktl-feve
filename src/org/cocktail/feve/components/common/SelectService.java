package org.cocktail.feve.components.common;

import java.util.Collection;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.appserver.WOContext;

public class SelectService extends FeveWebComponent {

	// liste des services
	public Collection<IStructure> eoStructureArray;
	public IStructure eoStructureSelected;
	public IStructure eoStructureItem;
	public boolean isAfficherArchive;
	
	// le container a mettre a jour si besoin
	public String updateContainerID;
	
	public SelectService(WOContext context) {
		super(context);
		initComponent();
	}
	

	private void initComponent() {
		// par defaut, on masque les groupes archives
		isAfficherArchive = false;
		// par defaut, si 1 seul service, on le selectionne
		if (getEoStructureArray().size() == 1) {
			eoStructureSelected = ((List<IStructure>) getEoStructureArray()).get(0);
		}
	}
	
	
	/**
	 * La liste des services visibles. 
	 * Elle est conditionnée par la valeur du boolean
	 * <code>isShowArchive</code> qui permet de masquer 
	 * ou non les services archivés
	 * @return
	 */
	public Collection<IStructure> getEoStructureArray() {
		return feveUserInfo().getServicePosteList(isAfficherArchive);
	}
	
}