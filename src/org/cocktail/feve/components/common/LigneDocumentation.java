package org.cocktail.feve.components.common;

import org.cocktail.fwkcktlgrh.common.metier.EOFeveParametres;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

/**
 * 
 * @author ctarade
 */
public class LigneDocumentation 
	extends WOComponent {
  
	public EOFeveParametres eoMangueParametre;
	public String updateContainerID;
	public boolean isDisabled = true;
	
	public LigneDocumentation(WOContext context) {
		super(context);
	}
}