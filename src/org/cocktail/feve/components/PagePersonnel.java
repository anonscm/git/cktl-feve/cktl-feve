package org.cocktail.feve.components;

import org.cocktail.feve.app.Application;
import org.cocktail.feve.components.common.A_ComponentControled;
import org.cocktail.feve.components.evaluation.ListeEvaluationCtrl;
import org.cocktail.feve.components.fichedeposte.CompFicheDePosteListCtrl;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf;
import org.cocktail.fwkcktlgrh.common.metier.EOPoste;
import org.cocktail.fwkcktlgrh.common.metier.EORepartCompetenceMiniCV;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividuGrhService;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * Rassemble l'ensemble des donnees liees a la personne
 * connectee : ses fiches de poste, ses fiche LOLF et
 * ses evaluations.
 */

public class PagePersonnel extends A_ComponentControled {
	
	/**
	 * @deprecated
	 * la fiche LOLF en cours d'examination
	 */
	public EOGenericRecord selectedFlo;

	// gestion des postes
	public EOPoste posteItem;
	public EOFicheLolf posteItemLolf;
	public EOFicheLolf posteSelectedLolf;
	public EOPoste posteSelected;
	
	public boolean isAfficherBouttonMiniCV;
	
	private NSArray<EOFicheDePoste> fichesPersonneConnecte;
	private NSArray<EORepartCompetenceMiniCV> mesMiniCV;
	
	
	/**
	 * l'evaluation en cours d'examination
	 */
	public EOGenericRecord selectedEva;

  /**
   * Le controleur utilisé pour afficher la liste des fiches de poste
   */
  public CompFicheDePosteListCtrl compFicheDePosteListCtrl;
  
  
  
  /**
   * Le controleur utilisé pour afficher la liste des evaluations
   */
  public ListeEvaluationCtrl listeEvaluationCtrl;

private String name;

	
	public PagePersonnel(WOContext context) {
		super(context);
		initComponent();
	}
	
	private void initComponent() {
		isAfficherBouttonMiniCV = true;
		IndividuGrhService i = new IndividuGrhService();
		// on selectionne le poste actuel par defaut
		NSArray postesActuels = i.tosPosteActuel(session.individuConnecte(), edc());
		if (session.individuConnecte() != null && postesActuels.count() > 0) {
			posteSelected = (EOPoste) postesActuels.lastObject();
		}


		NSArray ficheDePoste = getFichesDePostePersonneConnectee();
		compFicheDePosteListCtrl = new CompFicheDePosteListCtrl(session,ficheDePoste, 
				"Vous n'avez aucune fiche de poste personnelle enregistr&eacute;e", 
				false, false, false, false, true, null, false, false);
		// 
		listeEvaluationCtrl = new ListeEvaluationCtrl(session);
		listeEvaluationCtrl.setShowNode(false);
	}
	
	/**
	 * Un message d'avertissement : indique si l'utilisateur
	 * a des choses a faire
	 */
	public String warnMessage() {
		StringBuffer message = new StringBuffer();
		
		// il doit viser sa fiche de poste actuelle
		for (EOFicheDePoste ficheDePoste : getFichesDePostePersonneConnectee()) {
			if (!ficheDePoste.fdpVisaAgentBool()) {
				message.append("<li>fiche de poste ").append(ficheDePoste.identifiant()).append("</li>");
			}
		}
		if (message.length() > 0)  {
			message.insert(0, "Attention, vous n'avez pas vis&eacute; vos fiches <b>actuelles</b> suivantes :<ul>");
			message.append("</ul>");
		}
		return message.toString();
	}
	
	/**
	 * Indique s'il faut afficher le message d'avertissement.
	 * S'il est vide, alors non. On ne l'affiche que sur la 
	 * page d'ensemble
	 */
	public boolean showWarnMessage() {
		return (compFicheDePosteListCtrl == null || compFicheDePosteListCtrl.ficheDePosteSelected == null) && /*selectedFlo == null &&*/ selectedEva == null && warnMessage().length() > 0;
	}

	/**
	 */
	public boolean showFdp() {
		return /*selectedFlo == null &&*/ selectedEva == null;
	}

	/**
	 */
	public boolean showFlo() {
		return (compFicheDePosteListCtrl == null || compFicheDePosteListCtrl.ficheDePosteSelected == null) && selectedEva == null;
	}
	
	/**
	 */
	public boolean showEva() {
		return /*selectedFlo == null &&*/ (compFicheDePosteListCtrl == null || compFicheDePosteListCtrl.ficheDePosteSelected == null);
	}
	
	/**
	 * Masquer ce qui affiche en detail. Retour a la liste.
	 */
	public void redisplayGlobalComponent() {
		selectedEva = null;
		compFicheDePosteListCtrl.ficheDePosteSelected = null;
		//selectedFlo = null;
		listeEvaluationCtrl.redisplayGlobalComponent();
	}
	
	/**
	 * On affiche la legende que sur la page generale
	 */
	public boolean showLegende() {
		return (compFicheDePosteListCtrl == null || compFicheDePosteListCtrl.ficheDePosteSelected == null) && /*selectedFlo == null && */selectedEva == null;
	}

	// gestion du poste et de la fiche LOLF
	
	/**
	 * @return the listeFichesLolf
	 */
	public NSArray<EOPoste> listeFichesLolf() {		
		isAfficherBouttonMiniCV = true;
		return new IndividuGrhService().tosFicheLolf(session.individuConnecte(), edc());		
	}
	
	public WOActionResults selectionnerMiniCV() {
		session.isChargerMiniCV = true;
		session.isAfficherBouttonMiniCV = false;
		if (getFichesDePostePersonneConnectee().size() > 0) {
			compFicheDePosteListCtrl.setFicheDePosteItem(getFichesDePostePersonneConnectee().get(0));
			compFicheDePosteListCtrl.doSelectFicheDePoste();
		}
		return this.parent();
	}
	
	public boolean hasMiniCV() {
		
		boolean hasMiniCV = false;
		
		if (mesMiniCV == null) {
			
			EOQualifier qualMiniCV = EORepartCompetenceMiniCV.INDIVIDU.eq(session.individuConnecte().persId());
			mesMiniCV = EORepartCompetenceMiniCV.fetchAll(edc(), qualMiniCV, null);
			
		}
		
		if (mesMiniCV.size() > 0) {
			hasMiniCV = true;
		}
		
		return hasMiniCV;
	}
	
	public boolean hasFicheDePoste() {
		NSArray<EOFicheDePoste> fiches = getFichesDePostePersonneConnectee();
		if (fiches.size() > 0) {
			return true;
		}
		return false;
	}

	
	private NSArray<EOFicheDePoste> getFichesDePostePersonneConnectee() {
		if (fichesPersonneConnecte == null) {
			EOQualifier qualFicheDeposte = (EOFicheDePoste.TO_POSTE.dot(EOPoste.TOS_AFFECTATION_DETAIL.dot(EOAffectationDetail.TO_AFFECTATION.dot(EOAffectation.TO_INDIVIDU.dot(EOIndividu.PERS_ID))))).eq(session.individuConnecte().persId());
			fichesPersonneConnecte = EOFicheDePoste.fetchAll(edc(), qualFicheDeposte, null);
		}
		return fichesPersonneConnecte;
	}
	
	public boolean isAfficherFicheLOLF() {
		return Application.application().config().booleanForKey("APP_AFFICHER_LOLF");
	}

}