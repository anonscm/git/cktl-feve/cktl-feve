package org.cocktail.feve.components.poste;

import org.cocktail.feve.app.Session;
import org.cocktail.feve.components.common.A_ComponentControler;
import org.cocktail.feve.components.common.CompSelectAffectationCtrl;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheDePoste;
import org.cocktail.fwkcktlgrh.common.metier.EOFicheLolf;
import org.cocktail.fwkcktlgrh.common.metier.EOPoste;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderExercice;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.ycrifwk.utils.UtilDb;

import com.webobjects.appserver.WOComponent;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Controleur du composant {@link CompPosteMetaData}
 * 
 * @author ctarade
 */
public class CompPosteMetaDataCtrl 
extends A_ComponentControler {

	/**
	 * La page appelante pour le retour
	 */
	public I_CallingCompPosteMetaData caller;


	// -- consultation et modification --

	/**
	 * BINDING
	 * Le poste en cours de consultation ou en cours de modification
	 */
	public EOPoste inPoste;

	/** le message d'information sur le droit de modification des meta donnees du poste */
	public boolean showMsgInfoModifPoste;
	public String strMsgInfoModifPoste;



	// -- creation --

	/**
	 * BINDING
	 * Indique sur quelle service le nouveau poste doit être affecté
	 */
	public EOStructure inNewPosteService;

	/**
	 * Creer le poste a partir d'une affectation existante ?
	 */
	public boolean isCreatingFromExistingAffectation = true;

	/**
	 * Le code du nouveau poste
	 */
	public String newPosCode;

	/**
	 * Le libellé du nouveau poste
	 */
	public String newPosLibelle;

	/**
	 * La date d'ouverture du nouveau poste
	 */
	public NSTimestamp newPosDDebut; 

	/**
	 * La date de fermeture du nouveau poste
	 */
	public NSTimestamp newPosDFin; 

	/**
	 * L'affectation selectionnée dans le cas d'une création a partir d'une
	 * affectation déjà existante
	 */
	public org.cocktail.fwkcktlgrh.common.metier.EOAffectation newPosAffectation;

	/** 
	 * le controleur du composant d'affectation pour la creation d'un 
	 * nouveau poste avec une occupation 
	 */
	public CompSelectAffectationCtrl compSelectAffectationCtrlNouveauPoste;

	/**
	 * Creer la fiche de poste en meme temps que le nouveau poste
	 */
	private boolean isCreatingFicheDePoste;

	/**
	 * Le constructeur par defaut. 
	 * Il ne faut pas l'utiliser directement
	 * - {@link #CompPosteCtrl(Session, EOPoste, I_CallingCompPosteMetaData)} pour la modification
	 * - {@link #CompPosteCtrl(Session, EOStructure, I_CallingCompPosteMetaData)} pour la creation
	 * @param session
	 */
	public CompPosteMetaDataCtrl(Session session) {
		super(session);
		// le contenu du message d'information
		strMsgInfoModifPoste = feveApp().config().stringForKey("MSG_INFO_MODIF_POSTE");
		showMsgInfoModifPoste = !StringCtrl.isEmpty(strMsgInfoModifPoste);
	}

	/**
	 * Constructeur a appeler en mode creation
	 * @param session
	 * @param anInNewPosteService
	 * @param aCaller
	 */
	public CompPosteMetaDataCtrl(Session session, EOStructure anInNewPosteService, I_CallingCompPosteMetaData aCaller) {
		super(session);
		inPoste = null;
		inNewPosteService = anInNewPosteService;
		caller = aCaller;
		// instancier le gestionnaire de selection d'affectation
		compSelectAffectationCtrlNouveauPoste = new CompSelectAffectationCtrl(session, inNewPosteService);
		
	}

	/**
	 * Constructeur a appeler en mode modification
	 * @param session
	 * @param anInPoste
	 * @param aCaller
	 */
	public CompPosteMetaDataCtrl(Session session, EOPoste anInPoste, I_CallingCompPosteMetaData aCaller) {
		super(session);
		inPoste = anInPoste;
		caller = aCaller;
	}

	// boolean interface

	public boolean isDisabledCodePoste() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationGererPoste(determinerStructureDuPoste());
	}

	public boolean isDisabledDDebutPoste() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationGererPoste(determinerStructureDuPoste());
	}

	public boolean isDisabledDFinPoste() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationGererPoste(determinerStructureDuPoste());
	}

	private EOStructure determinerStructureDuPoste() {
		EOStructure structure = null;
		if (inNewPosteService == null) {
			structure = inPoste.toStructure();
		} else {
			structure = inNewPosteService;
		}
		return structure;
	}
	
	/**
	 * On affiche le formulaire de creation de poste si l'affectation 
	 * est selectionnee pour la creation depuis une affectation
	 */
	public boolean showInfosPoste() {
		return !isCreatingFromExistingAffectation ||
				(isCreatingFromExistingAffectation && newPosAffectation != null);
	}

	// setters

	/**
	 * Indique que l'affectation vient d'etre selectionnee. On remplit le
	 * formulaire avec les donnees par default en mode creation
	 */
	public void setNewPosAffectation(org.cocktail.fwkcktlgrh.common.metier.EOAffectation value) {
		newPosAffectation = value;

		// on pre-remplit le formulaire en mode creation de poste
		if (isModeCreatePoste()) {
			fillDefaultValuesNewPoste();
		}
		
	}


	// actions


	/**
	 * L'action de creer le poste
	 */
	public WOComponent doCreatePoste() {
		if (isFormValid()) {

			EOQualifier qualPosCode = ERXQ.equals(EOPoste.POS_CODE_KEY, newPosCode);
			NSArray<EOPoste> eoPosteArray = EOPoste.fetchAll(ec(), qualPosCode, null);
			if (eoPosteArray.size() > 0) {
				setErrMsg("Le code de ce poste est dèjà utilisé");
				return null;
			}
			
			// creer le poste
			EOPoste newPoste = EOPoste.newRecordInContext(ec(), inNewPosteService, newPosCode, newPosLibelle, newPosDDebut, newPosDFin);        
			// creer l'occupation
			if (isCreatingFromExistingAffectation) {
				EOAffectationDetail.newRecordInContext(ec(), newPosAffectation, newPoste, newPosDDebut, newPosDFin, false);
			}
			// creer la fiche de poste (uniquement pour les postes non enseignants)
			if (isCreatingFicheDePoste) {
				EOFicheDePoste.newRecordInContext(ec(), newPoste, newPosDDebut, newPosDFin);
			}
			// creer la fiche lolf
			EOFicheLolf.newRecordInContext(ec(), newPoste, FinderExercice.getExerciceDepensePourDate(ec(), DateCtrl.now()));
			// enregistrement
			try {
				UtilDb.save(ec(),"");
			} catch (Throwable e) {
				e.printStackTrace();
			}
			// on retourne sur la liste des postes en rafraichissant son contenu
			((I_CallingCompPosteMetaData) caller).doAfterCompPosteMetaDataSuccess();
			return null;
		} else {
			return null;
		}
	}


	/**
	 * L'action d'enregistrer les modifications
	 */
	public WOComponent doUpdatePoste() {
		if (isFormValid()) {
			// enregistrement
			try {
				UtilDb.save(ec(),"");
			} catch (Throwable e) {
				e.printStackTrace();
			}
			// on retourne sur la liste des postes en rafraichissant son contenu
			((I_CallingCompPosteMetaData) caller).doAfterCompPosteMetaDataSuccess();
			return null;
		} else {
			return null;
		}

	}

	/**
	 * Remplir les donnes par default lors de la creation d'un poste.
	 * Les valeurs dependent si creation a partir d'une affectation ou pas.
	 */
	private void fillDefaultValuesNewPoste() {
		// on ecrase pas les donnes du poste si un code a ete saisi
		if (StringCtrl.isEmpty(newPosCode)) {
			newPosCode = EOPoste.getDefaultPosCodeForStructure(inNewPosteService);
		}
		newPosLibelle = EOPoste.getLibelleDefautPourAffectation(newPosAffectation);
		if (isCreatingFromExistingAffectation) {
			newPosDDebut = newPosAffectation.dDebAffectation();
			newPosDFin = newPosAffectation.dFinAffectation();
		} else {
			newPosDDebut = new NSTimestamp();
			newPosDFin = null;
		}
	}


	// temoins internes	

	/**
	 * Effectue la verification des donnees saisies. Si tout est
	 * OK, alors <code>true</code> est retourne. 
	 * 
	 * Sinon <code>false</code> et <code>errMsg</code> est mis a jour
	 */
	private boolean isFormValid() {
		// raz du msg
		clearError();
		
		if (isModeCreatePoste()) {
			
			// le code, le libelle et la date de debut sont obligatoires
			if (StringCtrl.isEmpty(newPosCode)) {
				setErrMsg("Le code du poste est obligatoire");
			} else if (StringCtrl.isEmpty(newPosLibelle)) {
				setErrMsg("Le libellé du poste est obligatoire");
			} else if (newPosCode.length()>30) {
				setErrMsg("Le code du poste est limité à 30 caractères");
			} else if (newPosLibelle.length()>128) {
				setErrMsg("Le libellé du poste est limité à 128 caractères");
			} else	if (newPosDDebut == null) {
				setErrMsg("La date d'ouverture du poste est obligatoire");
			}
		} else {
			// le code, le libelle et la date de debut sont obligatoires
			if (StringCtrl.isEmpty(inPoste.posCode())) {
				setErrMsg("Le code du poste est obligatoire");
			} else if (StringCtrl.isEmpty(inPoste.posLibelle())) {
				setErrMsg("Le libellé du poste est obligatoire");
			} else if (inPoste.posCode().length()>30) {
				setErrMsg("Le code du poste est limité à 30 caractères");
			} else if (inPoste.posLibelle().length()>128) {
				setErrMsg("Le libellé du poste est limité à 128 caractères");
			} else if (inPoste.posDDebut() == null) {
				setErrMsg("La date d'ouverture du poste est obligatoire");
			}
		} 
		return !hasError();
	}

	/**
	 * 
	 */
	public boolean isModeCreatePoste() {
		return inPoste == null;
	}

	public boolean isCreatingFicheDePoste() {
		return isCreatingFicheDePoste;
	}

	public boolean isDisabledChkCreateFicheDePoste() {
		return !feveUserInfo().getAutorisation().hasDroitUtilisationGererFichePoste(determinerStructureDuPoste()) || (newPosAffectation != null && newPosAffectation.toIndividu().isEnseignant());
	}

	public void setIsCreatingFicheDePoste(boolean isCreatingFicheDePoste) {
		this.isCreatingFicheDePoste = isCreatingFicheDePoste;
	}

	
}
