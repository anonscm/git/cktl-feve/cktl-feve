package org.cocktail.feve.components.poste;

import org.cocktail.feve.app.Session;
import org.cocktail.feve.components.common.A_ComponentControlerAndFilArianeNode;
import org.cocktail.feve.components.common.CompOccupationCtrl;
import org.cocktail.feve.components.fichedeposte.CompFicheDePosteListCtrl;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectationDetail;
import org.cocktail.fwkcktlgrh.common.metier.EOPoste;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOComponent;

/**
 * Controleur du composant {@link CompPosteDetail}
 * @author ctarade
 *
 */
public class CompPosteDetailCtrl 
	extends A_ComponentControlerAndFilArianeNode 
		implements I_CallingCompPosteMetaData {
 
	/**
	 * Le poste en cours de detail
	 */
	public EOPoste poste;
	
	/**
   * L'individu connecte a-t-il les droits de modifier le {@link #poste()}
   */
  private boolean canUpdatePoste;
  
  /**
   * L'individu connecte a-t-il les droits de creer des fiches
   * de postes sur le  {@link #poste()}
   */
  private boolean canCreateFicheDePosteOnPoste;
    
  /**
   * Le controleur utilisé pour la changement des meta données
   * du poste (code, libelle, dates)
   */
  public CompPosteMetaDataCtrl compPosteMetaDataCtrlUpdate;
    
  /**
   * Le controleur utilisé pour la gestion des occupations 
   */
  public CompOccupationCtrl compOccupationCtrl;
  
  /**
   * Le controleur utilisé pour afficher la liste des fiches de poste
   */
  public CompFicheDePosteListCtrl compFicheDePosteListCtrl;
  
  /**
   * Indique si on est en train de mettre a jour les meta données
   * du poste (on affiche alors {@link CompPosteMetaData} en pleine page)
   */
  public boolean showCompPosteMetaDataCtrl;
  
  
  /**
   * @deprecated
   * Constructeur par defaut, utiliser {@link #CompPosteDetailCtrl(Session, EOPoste)}
   * @param session
   */
	public CompPosteDetailCtrl(Session session) {
		super(session);
	}
	
	/**
	 * 
	 * @param session
	 * @param aPoste
	 */
	public CompPosteDetailCtrl(Session session, EOPoste aPoste) {
		super(session);
		poste = aPoste;
		initCtrl();
	}
	
	/**
	 * Initialisation des variables
	 */
	private void initCtrl() {
		
		IPersonne personne = null;		
		EOAffectationDetail aff = poste.toAffectationDetailActuelleUniquement();
		if (aff != null) {
			personne = aff.toAffectation().toIndividu();
		}
		if (personne == null) {
			personne = poste.toStructure();
		}
		
		
		canUpdatePoste = feveUserInfo().getAutorisation().hasDroitUtilisationGererPoste(poste.toStructure());
		
		canCreateFicheDePosteOnPoste = feveSession().getAutorisation().hasDroitUtilisationGererFichePoste(poste.toStructure()) || 
				feveSession().getAutorisation().hasDroitUtilisationGererFichePoste(poste.toAffectationDetailActuelle().toAffectation().toIndividu());
		
		// le composant de gestion des occupations
		compOccupationCtrl = new CompOccupationCtrl(feveSession(), poste);
		// le composant de gestion des fiches de poste
		compFicheDePosteListCtrl = new CompFicheDePosteListCtrl(feveSession(),
				poste.tosFicheDePoste(), "Pas de fiche de poste",
				true, feveSession().getAutorisation().hasDroitUtilisationGererFichePoste(personne), false, canCreateFicheDePosteOnPoste, true, poste, feveSession().getAutorisation().hasDroitUtilisationGererFichePoste(personne), false);
		// on affiche pas le menu associé car la liste est au sein meme de ce composant
		compFicheDePosteListCtrl.setShowNode(false);
	}
	
	
	// navigation
	
	/**
	 * Aller a la page de modification des meta données du poste
	 */
	public WOComponent toCompPosteMetaDataUpdate() {
		compPosteMetaDataCtrlUpdate = new CompPosteMetaDataCtrl(feveSession(), poste, this);
		showCompPosteMetaDataCtrl = true;
		return null;
	}

	public void doAfterCompPosteMetaDataCancel() {
		showCompPosteMetaDataCtrl = false;
	}

	public void doAfterCompPosteMetaDataSuccess() {
		showCompPosteMetaDataCtrl = false;
	}
	
	
	/**
	 * Revenir a {@link CompPosteDetail} en page pleine
	 * @return
	 */
	public void toLocalFullComponent() {
		// modifications de meta données
		if (compPosteMetaDataCtrlUpdate != null) {
			doAfterCompPosteMetaDataCancel();
		}
		// la liste des occupations
		compOccupationCtrl.doCancelOccupationAdd();
		compOccupationCtrl.doCancelOccupationUpdate();
		// la liste des fiches de poste (pas forcément remplit pour les postes enseignant par exemple)
		if (compFicheDePosteListCtrl != null) {
			// ajout ou modification de meta donnée
			compFicheDePosteListCtrl.doCancelCompFicheDePosteAdd();
			compFicheDePosteListCtrl.doCancelCompFicheDePosteUpdate();
			// contenu de la fiche
			compFicheDePosteListCtrl.ficheDePosteSelected = null;
		}
	}
	
	@Override
	public A_ComponentControlerAndFilArianeNode child() {
		return compFicheDePosteListCtrl;
	}
	
	
	// getters manuel
	
	/**
	 * On affiche le composant entier des lors qu'on ne change pas
	 * les meta données du poste ou qu'on inspecte pas une fiche de poste
	 */
	public boolean showFullComponent() {
		return !compFicheDePosteListCtrl.isAFicheDePosteSelected();
	}
	
	
	
	// getters générés automatiquement
	
	public final boolean isCanUpdatePoste() {
		return canUpdatePoste;
	}
}
